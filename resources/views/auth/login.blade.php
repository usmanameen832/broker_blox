
<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc." />

    <!-- Favicon -->
    <link rel="shortcut icon" href="{{asset('user/assets/images/favicon.png')}}" type="image/x-icon"/>

    <!-- Libs CSS -->
    <link rel="stylesheet" href="{{asset('user/assets/css/libs.bundle.css')}}" />

    <!-- Theme CSS -->
    <link rel="stylesheet" href="{{asset('user/assets/css/theme.bundle.css')}}" id="stylesheetLight" />
    <link rel="stylesheet" href="{{asset('user/assets/css/theme-dark.bundle.css')}}" id="stylesheetDark" />

    <style>body { display: none; }</style>

    <!-- Title -->
    <title>BrokerBlox</title>

</head>
<body class="d-flex align-items-center bg-auth border-top border-top-2 border-primary">

<!-- CONTENT
================================================== -->
<div class="container">
    <div class="row align-items-center">
        <div class="col-12 col-md-6 offset-xl-2 offset-md-1 order-md-2 mb-5 mb-md-0">

            <!-- Image -->
            <div class="text-center">
                <img src="{{asset('user/assets/images/loginPage.png')}}" alt="..." class="img-fluid">
            </div>

        </div>
        <div class="col-12 col-md-5 col-xl-4 order-md-1 my-5">

            <!-- Logo -->
            <div class="col-lg-12 col-md-12 text-center">
            <img src="{{ asset('user/assets/images/brokerblox.png') }}" style="height: 100px; width: 150px" class="">
            </div>

            <!-- Heading -->
            <h1 class="display-4 text-center mb-3">
                Sign in
            </h1>

            <!-- Subheading -->
            <p class="text-muted text-center mb-5">
               access to brokerblox dashboard.
            </p>

            @if(session()->has('error'))
                <div class="alert alert-danger">
                    {{session('error')}}
                </div>
            @endif

            <!-- Form -->
            <form method="post" action="{{ route('postLogin') }}">
                @csrf

                <!-- Email address -->
                <div class="form-group">
                    <label class="form-label">
                        Email Address
                    </label>
                    <input type="email" name="email" class="form-control" placeholder="example@gmail.com">
                </div>

                <!-- Password -->
                <div class="form-group">
                    <div class="row">
                        <div class="col">
                            <label class="form-label">
                                Password
                            </label>
                        </div>
                        <div class="col-auto">
                            <a href="#" class="form-text small text-muted">
                                Forgot password?
                            </a>
                        </div>
                    </div> <!-- / .row -->

                    <!-- Input group -->
                    <div class="input-group input-group-merge">

                        <!-- Input -->
                        <input class="form-control" name="password" type="password" placeholder="Enter your password">

                    </div>
                </div>

                <!-- Submit -->
                <button class="btn btn-lg w-100 btn-primary mb-3">
                    Sign in
                </button>

                <!-- Link -->
                <div class="text-center">
                    <small class="text-muted text-center">
                        Don't have an account yet? <a href="{{ route('signUp') }}"> <b>Sign up</b> </a>.
                    </small>
                </div>

            </form>

        </div>
    </div> <!-- / .row -->
</div> <!-- / .container -->

<!-- JAVASCRIPT -->
<!-- Map JS -->
{{--<script src='https://api.mapbox.com/mapbox-gl-js/v0.53.0/mapbox-gl.js'></script>--}}

<!-- Vendor JS -->
<script src="{{asset('user/js/vendor.bundle.js')}}"></script>

<!-- Theme JS -->
<script src="{{asset('user/js/theme.bundle.js')}}"></script>

</body>
</html>
