
<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc." />

    <!-- Favicon -->
    <link rel="shortcut icon" href="{{asset('user/assets/images/favicon.png')}}" type="image/x-icon"/>

{{--    <!-- Map CSS -->--}}
{{--    <link rel="stylesheet" href="https://api.mapbox.com/mapbox-gl-js/v0.53.0/mapbox-gl.css" />--}}

<!-- Libs CSS -->
    <link rel="stylesheet" href="{{asset('user/assets/css/libs.bundle.css')}}" />

    <!-- Theme CSS -->
    <link rel="stylesheet" href="{{asset('user/assets/css/theme.bundle.css')}}" id="stylesheetLight" />
    <link rel="stylesheet" href="{{asset('user/assets/css/theme-dark.bundle.css')}}" id="stylesheetDark" />

    <style>body { display: none; }</style>

    <!-- Title -->
    <title>Dashkit</title>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-156446909-1"></script><script>window.dataLayer = window.dataLayer || [];function gtag(){dataLayer.push(arguments);}gtag("js", new Date());gtag("config", "UA-156446909-1");</script>
</head>
<body class="d-flex align-items-center bg-auth border-top border-top-2 border-primary">

<!-- CONTENT
================================================== -->
<div class="container">
    <div class="row align-items-center">
        <div class="col-12 col-md-6 offset-xl-2 offset-md-1 order-md-2 mb-5 mb-md-0">

            <!-- Image -->
            <div class="text-center">
                <img src="{{asset('user/assets/images/loginPage.png')}}" alt="..." class="img-fluid">
            </div>

        </div>
        <div class="col-12 col-md-5 col-xl-4 order-md-1 my-5">

            <div class="col-lg-12 col-md-12 text-center">
                <img src="{{ asset('user/assets/images/brokerblox.png') }}" style="height: 100px; width: 150px" class="">
            </div>

            <!-- Heading -->
            <h1 class="display-4 text-center mb-3">
                Sign up
            </h1>

            <!-- Subheading -->
            <p class="text-muted text-center mb-5">
                Free access to brokerblox dashboard.
            </p>
            @if(session()->has('error'))
                <div class="alert alert-danger">
                    <p>{{ session('error') }}</p>
                </div>
            @endif

            @if(session()->has('status'))
                <div class="alert alert-success">
                    <p>{{ session('status') }}</p>
                </div>
        @endif
        <!-- Form -->
            <form action="{{ route('saveUser') }}" method="post">
                @csrf
                <div class="form-group form-group-row">
                    <input type="email" class="form-control" required name="email" placeholder="Enter Email">
                </div>

                <div class="form-group">
                    <input type="text" class="form-control" required name="company_name" placeholder="Enter Company Name">
                </div>

                <div class="form-group">
                    <input type="number" class="form-control" required name="number_of_agents" placeholder="Enter Number Of Agents">
                </div>

                <div class="form-group">
                    <input type="text" class="form-control" required name="first_name" placeholder="Enter First Name">
                </div>

                <div class="form-group">
                    <input type="text" class="form-control" required name="last_name" placeholder="Enter Last Name">
                </div>

                <div class="form-group">
                    <input type="number" class="form-control" required name="phone_number" placeholder="Enter Phone Number">
                </div>

                <div class="form-group">
                    <div class="input-group input-group-merge">
                        <input class="form-control" type="password" required name="password" placeholder="Enter Password">
                    </div>
                </div>

                <!-- Submit -->
                <button class="btn btn-lg w-100 btn-primary mb-3">
                    Sign up
                </button>

                <!-- Link -->
                <div class="text-center">
                    <small class="text-muted text-center">
                        Already have an account? <a href="{{ route('login') }}"> <b>Log in</b> </a>.
                    </small>
                </div>

            </form>

        </div>
    </div> <!-- / .row -->
</div> <!-- / .container -->

<!-- JAVASCRIPT -->
<!-- Map JS -->
{{--<script src='https://api.mapbox.com/mapbox-gl-js/v0.53.0/mapbox-gl.js'></script>--}}

<!-- Vendor JS -->
<script src="{{asset('user/js/vendor.bundle.js')}}"></script>

<!-- Theme JS -->
<script src="{{asset('user/js/theme.bundle.js')}}"></script>

</body>
</html>
