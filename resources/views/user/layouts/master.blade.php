
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <title>BrokerBlox</title>
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <style>body { display: none; }</style>
    <!-- Title -->

    @include('user.partials.header_scripts')
    @stack('css')

</head>
<body>

<!-- MODALS -->
{{--@include('user.partials.modals')--}}

<!-- OFFCANVAS -->
{{--@include('user.partials.offcanvas')--}}

<!-- NAVIGATION -->
@include('user.partials.navbar_vertical')

<!-- MAIN CONTENT -->
<div class="main-content">

@include('user.partials.topbar')
    <!-- CARDS -->
    @yield('content')
</div>
<!-- / .main-content -->

@include('user.partials.footer_scripts')
@stack('js')
</body>
</html>
