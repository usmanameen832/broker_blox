@extends('user.layouts.master')

@push('css')
@endpush

@section('content')
    <div id="taskChecklist">
        <div class="header">
            <div class="container-fluid">
                <div class="header-body">
                    <div class="row align-items-end">
                        <div class="col">
                            <h1 class="header-title">
                                Settings / Task Checklist
                            </h1>
                        </div>
                        <div class="col-auto">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12 col-lg-12 col-xl">
                    <div class="card">
                        <div class="card-body">
                            <h2>Task Checklist</h2>
                            <div class="col-md-12">
                                <div class="form-group pull-right mt-5">
                                    <button type="button" onclick="hideTaskChecklist()" class="btn btn-secondary">+ Add Checklist</button>
                                    <button type="submit" data-bs-toggle="modal" data-bs-target="#autoAddToTransaction" class="btn btn-primary">+ Auto Add to Transaction</button>
                                </div>
                            </div>

                            <!-- Modal -->
                            <div class="modal fade" id="autoAddToTransaction" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                                <div class="modal-dialog modal-1-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h3 class="modal-title" id="staticBackdropLabel">Add Tag</h3>
                                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                        </div>
                                        <div class="modal-body">
                                            <div class="form-group">
                                                <label> <b>Transaction Type:</b> </label>
                                                <div class="input_container select_container">
                                                    <select class="form-select mb-3" data-choices>
                                                        <option>Listing</option>
                                                        <option>Selling</option>
                                                        <option>Dual (both)</option>
                                                        <option>Rental</option>
                                                        <option>Referral</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label> <b>Task Checklist:</b> </label>
                                                <div class="input_container select_container">
                                                    <select class="form-select mb-3" disabled data-choices>
                                                        <option>No result match</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer form-group-row">
                                            <button type="button" class="btn btn-light">Cancel</button>
                                            <button type="button" class="btn btn-primary">Update</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>

                            <div class="col-md-12">
                                <div class="table-responsive" data-list='{"valueNames": ["tables-row", "tables-first", "tables-last", "tables-handle"]}'>
                                    <table class="table table-sm">
                                        <thead>
                                        <tr>
                                            <th scope="col">
                                                <a href="#" class="text-muted list-sort" data-sort="tables-first">Checklist Name</a>
                                            </th>
                                            <th scope="col">
                                                <a href="#" class="text-muted list-sort">Office Name</a>
                                            </th>
                                            <th scope="col">
                                                <a href="#" class="text-muted list-sort">Transaction Type</a>
                                            </th>
                                            <th scope="col">
                                                <a href="#" class="text-muted list-sort">Auto Selected</a>
                                            </th>
                                            <th scope="col">
                                                <a href="#" class="text-muted list-sort">Action</a>
                                            </th>
                                        </tr>
                                        </thead>
                                        <tbody class="list">
                                        <tr>
                                            <td class="tables-first">Test</td>
                                            <td>N/A</td>
                                            <td>N/A</td>
                                            <td>N/A</td>
                                            <td>
                                                <a><i class="far fa-edit"></i></a>
                                                <a><i class="fas fa-trash-alt"></i></a>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- / .row -->
        </div>
    </div>

    <div id="addTask" style="display: none">
        <div class="header">
            <div class="container-fluid">
                <div class="header-body">
                    <div class="row align-items-end">
                        <div class="col">
                            <h1 class="header-title">
                                Settings / Task Checklist / Add Task
                            </h1>
                        </div>
                        <div class="col-auto">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12 col-lg-12 col-xl">
                    <div class="card">
                        <div class="card-body">
                            <h2>Add Task</h2>
                            <div class="col-md-12 row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="mb-2">Checklist Name:</label>
                                        <input type="text" placeholder="Enter Checklist Name" class="form-control input-group">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="mb-2">Transaction Type:</label>
                                        <div class="input_container select_container">
                                            <select class="form-select mb-3" data-choices>
                                                <option>Select an Option</option>
                                                <option>Listing</option>
                                                <option>Selling</option>
                                                <option>Dual (both)</option>
                                                <option>Rental</option>
                                                <option>Referral</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="mb-2">Office Location(s):</label>
                                        <div class="input_container select_container">
                                            <select class="form-select mb-3" data-choices>
                                                <option>All Offices</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group pull-right mt-5">
                                        <button type="button" class="btn btn-secondary">
                                            + Add Item
                                        </button>
                                    </div>
                                </div>
                            </div>

                            <div class="clearfix"></div>

                            <div class="col-md-12">
                                <div class="table-responsive">
                                    <table class="table table-sm">
                                        <thead>
                                        <tr>
                                            <th scope="col">
                                                <a href="#" class="text-muted list-sort">#</a>
                                            </th>
                                            <th scope="col">
                                                <a href="#" class="text-muted list-sort">Item Name</a>
                                            </th>
                                            <th scope="col">
                                                <a href="#" class="text-muted list-sort">Document</a>
                                            </th>
                                            <th scope="col">
                                                <a href="#" class="text-muted list-sort">Action</a>
                                            </th>
                                        </tr>
                                        </thead>
                                        <tbody class="list">
                                        <tr>
                                            <td>1</td>
                                            <td>N/A</td>
                                            <td>N/A</td>
                                            <td>
                                                <a><i class="far fa-edit"></i></a>
                                                <a><i class="fas fa-trash-alt"></i></a>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <div class="pull-right">
                                <button class="btn btn-light" onclick="showTaskChecklist()">Cancel</button>
                                <button class="btn btn-primary">Save</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('js')
    <script>
        function hideTaskChecklist(){
            $('#taskChecklist').hide();
            $('#addTask').show();
        }
        function showTaskChecklist(){
            $('#taskChecklist').show();
            $('#addTask').hide();
        }
    </script>
@endpush
