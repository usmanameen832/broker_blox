@extends('user.layouts.master')

@push('css')
    <style>
        label{
            font-weight: bold;
        }
        .switch {
            position: relative;
            display: inline-block;
            width: 60px;
            height: 34px;
        }
        .switch input {
            opacity: 0;
            width: 0;
            height: 0;
        }
        .slider {
            position: absolute;
            cursor: pointer;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            background-color: #ccc;
            -webkit-transition: .4s;
            transition: .4s;
        }
        .slider:before {
            position: absolute;
            content: "";
            height: 26px;
            width: 26px;
            left: 4px;
            bottom: 4px;
            background-color: white;
            -webkit-transition: .4s;
            transition: .4s;
        }
        .switch input:checked + .slider {
            background-color: #2196F3;
        }

        .switch input:focus + .slider {
            box-shadow: 0 0 1px #2196F3;
        }

        .switch input:checked + .slider:before {
            -webkit-transform: translateX(26px);
            -ms-transform: translateX(26px);
            transform: translateX(26px);
        }
        /* Rounded sliders */
        .slider.round {
            border-radius: 34px;
        }

        .slider.round:before {
            border-radius: 50%;
        }
    </style>
@endpush

@section('content')
    <div class="header">
        <div class="container-fluid">
            <div class="header-body">
                <div class="row align-items-end">
                    <div class="col">
                        <h1 class="header-title">
                            Settings / Field Validation
                        </h1>
                    </div>
                    <div class="col-auto">
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="field-validation">
        <div class="container-fluid">
            @if(session()->has('status'))
                <div class="alert alert-success">
                    <h3>{{ session('status') }}</h3>
                </div>
            @endif
            <div class="row">
                <div class="col-md-12 col-lg-12 col-xl">
                    <!-- Value  -->
                    <div class="card">
                        <div class="card-body">
                            <div class="col-md-12">
                                <div class="col-md-6">
                                    <h2>Field Validation</h2>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <button class="btn btn-primary pull-right" onclick="hideFieldValidation()">Add Validation</button>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="table-responsive" data-list='{"valueNames": ["tables-row", "tables-first", "tables-last", "tables-handle"]}'>
                                <table class="table table-sm">
                                    <thead>
                                    <tr>
                                        <th scope="col">
                                            <a href="#" class="text-muted list-sort">#</a>
                                        </th>
                                        <th scope="col">
                                            <a href="#" class="text-muted list-sort">Field Name</a>
                                        </th>
                                        <th scope="col">
                                            <a href="#" class="text-muted list-sort">Page Name</a>
                                        </th>
                                        <th scope="col">
                                            <a href="#" class="text-muted list-sort">Error Message</a>
                                        </th>
                                        <th scope="col">
                                            <a href="#" class="text-muted list-sort">Regular Expression</a>
                                        </th>
                                        <th scope="col">
                                            <a href="#" class="text-muted list-sort">Required?</a>
                                        </th>
                                        <th scope="col">
                                            <a href="#" class="text-muted list-sort">Action</a>
                                        </th>
                                    </tr>
                                    </thead>
                                    <tbody class="list">
                                    @forelse($fieldValidationData as $fieldData)
                                        <tr>
                                            <td>{{ $fieldData->id }}</td>
                                            <td>{{ $fieldData->view_agent_field }}{{ $fieldData->add_agent_field }}{{ $fieldData->office_licenses_field }}</td>
                                            <td>{{ $fieldData->page_name }}</td>
                                            <td>{{ $fieldData->error_message }}</td>
                                            <td>{{ $fieldData->regular_expression_name }}</td>
                                            <td>{{ $fieldData->required }}</td>
                                            <td>
                                                <a><i class="far fa-edit"></i></a>
                                                <a><i class="fas fa-trash-alt"></i></a>
                                            </td>
                                        </tr>
                                    @empty
                                        <tr>
                                            <td colspan="7">No Record is available</td>
                                        </tr>
                                    @endforelse
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="add-field-validation" style="display: none">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12 col-lg-12 col-xl">
                    <!-- Value  -->
                    <div class="card">
                        <form action="{{ route('saveFieldValidation') }}" method="post">
                            @csrf
                            <div class="card-body">
                                <h2>Add Field Validation</h2>
                                <div class="col-lg-12 col-md-12 row">
                                    <div class="col-lg-2 col-md-1"></div>
                                    <div class="col-lg-8 col-md-10">
                                        <div class="form-group mt-3 form-group-row">
                                            <label class="w-25 mt-2">Select Page Name:</label>
                                            <div class="input_container select_container w-75">
                                                <select class="form-select selectPageName" name="page_name" id="" data-choices>
                                                    <option selected disabled>Select Page</option>
                                                    <option>View Agent</option>
                                                    <option>Add Agent</option>
                                                    <option>Offices & Licenses</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group mt-3 form-group-row agentFieldName" style="display: none">
                                            <label class="w-25 mt-2">Select Field Name:</label>
                                            <div class="input_container select_container w-75">
                                                <select class="form-select selectFieldName" name="view_agent_field" id="" data-choices>
                                                    <option selected disabled>Select Field</option>
                                                    <option>First Name</option>
                                                    <option>Middle Name</option>
                                                    <option>Last Name</option>
                                                    <option>Nick Name</option>
                                                    <option>Name 1099</option>
                                                    <option>Start Date</option>
                                                    <option>Commissions Email</option>
                                                    <option>Spouse Name</option>
                                                    <option>Emergency Contact Name</option>
                                                    <option>DBA/Entity Name</option>
                                                    <option>DOB</option>
                                                    <option>Termination End Date</option>
                                                    <option>Stop Communications on Date</option>
                                                    <option>Deceased End Date</option>
                                                    <option>Gender</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group mt-3 form-group-row fieldName" style="display: none">
                                            <label class="w-25 mt-2">Select Field Name:</label>
                                            <div class="input_container select_container w-75">
                                                <select class="form-select selectFieldName" name="add_agent_field" id="" data-choices>
                                                    <option selected disabled>Select Field</option>
                                                    <option>First Name</option>
                                                    <option>Last Name</option>
                                                    <option>Start Date</option>
                                                    <option>Office Location</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group mt-3 form-group-row OfficeLicenseName" style="display: none">
                                            <label class="w-25 mt-2">Select Field Name:</label>
                                            <div class="input_container select_container w-75">
                                                <select class="form-select selectFieldName" name="office_licenses_field" id="" data-choices>
                                                    <option selected disabled>Select Field</option>
                                                    <option>License Number</option>
                                                    <option>License Expiration</option>
                                                    <option>License Type</option>
                                                    <option>License Issues</option>
                                                    <option>Agent Type</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group form-group-row RegularExpressions">
                                            <label class="w-25 mt-6">Regular Expression:</label>
                                            <div class="input_container select_container w-75">
                                                <div class="form-group form-group-row mb-1">
                                                    <input type="text" placeholder="Regular Expression" name="regular_expression_name" class="form-control input-group">
                                                </div>
                                                <div class="form-group-row">
                                                    <input type="checkbox" style="margin-right: 3px;" class="input-group form-check-input">
                                                    <label>All</label>
                                                </div>
                                                <div class="form-group-row">
                                                    <input type="checkbox" style="margin-right: 3px;" class="input-group form-check-input">
                                                    <label>Only Alphabet</label>
                                                </div>
                                                <div class="form-group-row">
                                                    <input type="checkbox" style="margin-right: 3px;" class="input-group form-check-input">
                                                    <label>Only Numbers</label>
                                                </div>
                                                <div class="form-group-row">
                                                    <input type="checkbox" style="margin-right: 3px;" class="input-group form-check-input">
                                                    <label>Only Special Characters</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group form-group-row DatesRegularExpressions" style="display: none">
                                            <label class="w-25 mt-5">Regular Expression:</label>
                                            <div class="input_container select_container w-75">
                                                <div class="form-group-row">
                                                    <input type="checkbox" style="margin-right: 3px;" class="input-group form-check-input">
                                                    <label>Only Future Date</label>
                                                </div>
                                                <div class="form-group-row">
                                                    <input type="checkbox" style="margin-right: 3px;" class="input-group form-check-input">
                                                    <label>Only Past Date</label>
                                                </div>
                                                <div class="form-group-row">
                                                    <input type="checkbox" style="margin-right: 3px;" class="input-group form-check-input">
                                                    <label>Minimum Past Year to be set: Today - # of years</label>
                                                </div>
                                                <div class="form-group-row">
                                                    <input type="checkbox" style="margin-right: 3px;" class="input-group form-check-input">
                                                    <label>Maximum Past Year to be set: Today - # of years</label>
                                                </div>
                                                <div class="form-group-row">
                                                    <input type="checkbox" style="margin-right: 3px;" class="input-group form-check-input">
                                                    <label>Minimum Future Year to be set: Today + # of years</label>
                                                </div>
                                                <div class="form-group-row">
                                                    <input type="checkbox" style="margin-right: 3px;" class="input-group form-check-input">
                                                    <label>Maximum Future Year to be set: Today + # of years</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group form-group-row">
                                            <label class="w-25 mt-2">Error Message:</label>
                                            <div class="input_container select_container w-75">
                                                <textarea class="form-control" rows="4" name="error_message" cols="50" maxlength="200" placeholder="Error Message"></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group form-group-row">
                                            <label class="w-25">Required?</label>
                                            <div class="input_container select_container w-75">
                                                <label class="switch">
                                                    <input name="required" type="checkbox">
                                                    <span class="slider round"></span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-2 col-md-1"></div>
                                </div>
                            </div>
                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary pull-right">Save</button>
                                <button class="btn btn-light pull-right" style="margin-right: 5px" onclick="showFieldValidation()">Cancel</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('js')
    <script>
        $('body').on('change', '.selectPageName', function (){
            let a = $(this).val();
            if (a == 'Add Agent'){
                $('.agentFieldName').hide();
                $('.fieldName').show();
                $('.OfficeLicenseName').hide();
            } else if (a == 'View Agent'){
                $('.agentFieldName').show();
                $('.fieldName').hide();
                $('.OfficeLicenseName').hide();
            } else if (a == 'Offices & Licenses'){
                $('.fieldName').hide();
                $('.OfficeLicenseName').show();
                $('.agentFieldName').hide();
            }
        });

        $('body').on('change', '.selectFieldName', function (){
            let b = $(this).val();
            if (b == 'Start Date'){
                $('.DatesRegularExpressions').show();
                $('.RegularExpressions').hide();
            } else if (b == 'First Name'){
                $('.DatesRegularExpressions').hide();
                $('.RegularExpressions').show();
            } else if (b == 'Last Name'){
                $('.DatesRegularExpressions').hide();
                $('.RegularExpressions').show();
            } else if (b == 'Office Location'){
                $('.DatesRegularExpressions').hide();
                $('.RegularExpressions').hide();
            }
        })

        function hideFieldValidation(){
            $('#field-validation').hide();
            $('#add-field-validation').show();
        }

        function showFieldValidation(){
            $('#field-validation').show();
            $('#add-field-validation').hide();
        }
    </script>
@endpush
