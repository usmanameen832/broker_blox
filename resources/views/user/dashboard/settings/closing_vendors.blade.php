@extends('user.layouts.master')

@push('css')
    <style>
        .add-contact {
            color: #4481cc;
            font-size: 39px;
            /*position: absolute;*/
            /*top: -2px;*/
            /*right: -2px;*/
            cursor: pointer;
            background-color: white;
        }
    </style>
@endpush

@section('content')
    <div class="header">
        <div class="container-fluid">
            <div class="header-body">
                <div class="row align-items-end">
                    <div class="col">
                        <h1 class="header-title">
                            Settings / Title Company
                        </h1>
                    </div>
                    <div class="col-auto">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 col-lg-12 col-xl">
                <div class="card">
                    <div class="card-body">
                        <ul class="nav nav-tabs">
                            <li class="nav-item">
                                <a class="nav-link active" aria-current="page" href="#">Title Company</a>
                            </li>
                        </ul>

                        <div class="tab-content" id="title-attorneys">
                            <div class="active" id="#">
                                @if(session()->has('delete'))
                                    <div class="alert alert-success">
                                        <h3>{{ session('delete') }}</h3>
                                    </div>
                                @endif
                                @if(session()->has('status'))
                                    <div class="alert alert-success">
                                        <h3>{{ session('status') }}</h3>
                                    </div>
                                @endif
                                <div class="col-lg-12 col-md-12 col-sm-12">
                                    <p class="font-24 margin-bottom-10">Title/Attorneys</p>
                                    <div class="table-header">
                                        <div class="col-md-6">
                                            <div class="input-group">
                                                <div class="form-outline">
                                                    <input type="search" id="form1" placeholder="search.." class="form-control" />
                                                </div>
                                                <button type="button" class="btn btn-primary">
                                                    <i class="fas fa-search"></i>
                                                </button>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="filter-btn pull-right">
                                                <button onclick="hideTitleAttorneys()" class="btn btn-primary">+ Add Title/Attorneys</button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="table-responsive" data-list='{"valueNames": ["tables-row", "tables-first", "tables-last", "tables-handle"]}'>
                                            <table class="table table-sm">
                                                <thead>
                                                <tr>
                                                    <th scope="col">
                                                        <a href="#" class="text-muted list-sort" data-sort="tables-first">#</a>
                                                    </th>
                                                    <th scope="col">
                                                        <a href="#" class="text-muted list-sort" data-sort="tables-last">Company</a>
                                                    </th>
                                                    <th scope="col">
                                                        <a href="#" class="text-muted list-sort" data-sort="tables-handle">Agent/Attorney</a>
                                                    </th>
                                                    <th scope="col">
                                                        <a href="#" class="text-muted list-sort" data-sort="tables-handle">Email</a>
                                                    </th>
                                                    <th scope="col">
                                                        <a href="#" class="text-muted list-sort" data-sort="tables-handle">Phone</a>
                                                    </th>
                                                    <th scope="col">
                                                        <a href="#" class="text-muted list-sort" data-sort="tables-handle">Fax</a>
                                                    </th>
                                                    <th scope="col">
                                                        <a href="#" class="text-muted list-sort" data-sort="tables-handle">Address</a>
                                                    </th>
                                                    <th scope="col">
                                                        <a href="#" class="text-muted list-sort" data-sort="tables-handle">Internal Company</a>
                                                    </th>
                                                    <th scope="col">
                                                        <a href="#" class="text-muted list-sort" data-sort="tables-handle">Action</a>
                                                    </th>
                                                </tr>
                                                </thead>
                                                <tbody class="list">
                                                @foreach($company as $companies)
                                                    <tr>
                                                        <td class="tables-first">{{ $companies->id }}</td>
                                                        <td class="tables-last">{{ $companies->company }}</td>
                                                        <td class="tables-handle">{{ $companies->agent_attorney }}</td>
                                                        <td class="tables-handle">{{ $companies->email }}</td>
                                                        <td class="tables-handle">{{ $companies->phone }}</td>
                                                        <td class="tables-handle">{{ $companies->fax }}</td>
                                                        <td class="tables-handle">{{ $companies->address_1 }},{{ $companies->city }},{{ $companies->country }},{{ $companies->zip_code }}</td>
                                                        <td class="tables-handle">{{ $companies->internal_company }}</td>
                                                        <td class="tables-handle">
                                                            <a href="javascript:void(0)" onclick="editTitleCompany('{{$companies->id}}')"><i class="far fa-edit"></i></a>
                                                            <a href="{{ route('deleteTitleCompany', $companies->id) }}"><i class="fas fa-trash-alt"></i></a>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-content" id="add-title-attorneys" style="display: none">
                            <div class="active" id="#">
                                <form action="{{ route('saveTitleCompany') }}" method="post">
                                    @csrf
                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                        <p class="font-24 margin-bottom-10">Add Title/Attorneys</p>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="mb-2">Company:</label>
                                                    <div class="input_container">
                                                        <input type="text" placeholder="Enter Company" name="company" required class="form-control form-group CompanyName">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="mb-2">Agent/Attorney:</label>
                                                    <div class="input_container form-group-row">
                                                        <select class="form-control" name="agent_attorney" id="agentAttorneyName">
                                                        </select>
                                                        <i data-bs-toggle="modal" data-bs-target="#createAgentAttorney" class="fas fa-plus-square add-contact"></i>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="mb-2">Email:</label>
                                                    <div class="input_container select_container">
                                                        <input type="text" placeholder="Enter Email Address" name="email" class="form-control input-group companyEmail">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="mb-2">Phone #:</label>
                                                    <div class="input_container select_container">
                                                        <input type="text" placeholder="(_ _ _)-_ _ _-_ _ _" name="phone" class="form-control input-group companyPhone">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="mb-2">Fax #:</label>
                                                    <div class="input_container select_container">
                                                        <input type="text" placeholder="(_ _ _)-_ _ _-_ _ _" name="fax" class="form-control input-group companyFax">
                                                    </div>
                                                </div>
                                                <div class="form-group form-group-row mt-5">
                                                    <label class="mb-2">Internal Company:</label>
                                                    <input style="margin-left: 10px" type="checkbox" name="internal_company" class="form-check-input input-group internalCompanyCheckbox">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="mb-2">Address Line 1:</label>
                                                    <div class="input_container select_container">
                                                        <input type="text" name="address_1" placeholder="Address (Optional)" class="form-control input-group addressOne">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="mb-2">Address Line 2:</label>
                                                    <div class="input_container select_container">
                                                        <input type="text" name="address_2" placeholder="Address (Optional)" class="form-control input-group addressTwo">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="mb-2">Zip/Postal Code:</label>
                                                    <div class="input_container select_container">
                                                        <input type="text" name="zip_code" placeholder="Zip/Postal Code (Optional)" class="form-control input-group companyZipCode">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="mb-2">City:</label>
                                                    <div class="input_container select_container">
                                                        <input type="text" name="city" placeholder="City (Optional)" class="form-control input-group companyCity">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="mb-2">State/Province:</label>
                                                    <div class="input_container select_container">
                                                        <input type="text" name="state" placeholder="Enter State/Province" class="form-control input-group companyState">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="mb-2">Country:</label>
                                                    <div class="input_container select_container">
                                                        <input type="text" name="country" placeholder="Enter Country" class="form-control input-group companyCountry">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card-footer">
                                        <div class="form-group pull-right mt-5">
                                            <div>
                                                <button type="button" class="btn btn-light" onclick="showTitleAttorneys()">Cancel</button>
                                                <button type="submit" onclick="editTitleCompany()" class="btn btn-primary">Save</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>

                        <!-- Create New Agent/Attorney Modal -->
                        <div class="modal fade" id="createAgentAttorney" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h3 class="modal-title" id="staticBackdropLabel">Create New Agent/Attorney</h3>
                                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                    </div>
                                    <div class="modal-body">
                                        <div class="form-group form-group-row mb-0">
                                            <label class="w-50 fw-bold mt-2">Agent/Attorney Name:</label>
                                            <input type="text" placeholder="Office Name" class="form-control OfficeNameInput">
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                                        <button type="button" class="btn btn-primary saveOfficeName">Save</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('js')
    <script>
        $('body').on('click', '.saveOfficeName', function (){
            var  office_name = $('body').find('.OfficeNameInput').val();

            var html = `<option class="officeName">`+ office_name +`</option>`

            $('#agentAttorneyName').append(html);
            $('#createAgentAttorney').modal('hide');
        });
        $('body').on('click', '.internalCompanyCheckbox', function (){
            if ($(this).is(':checked')){
                $(this).val('Yes');
            } else if (!$(this).is(':checked')) {
                $(this).val('No');
            }
            console.log(abc.val());
            console.log(abc2.val());
        });
        function hideTitleAttorneys() {
            $('#title-attorneys').hide();
            $('#add-title-attorneys').show();
        }
        function showTitleAttorneys() {
            $('#title-attorneys').show();
            $('#add-title-attorneys').hide();
        }

        function editTitleCompany(id) {
            console.log(id);
            var titleCompanyId = id;
            console.log(titleCompanyId);
            $.ajax({
                url: "/user/edit-title-company/"+id,
                method: "GET",
                data: {
                    titleCompanyId: titleCompanyId,
                },
                dataType: "html",
                success: function(res){
                    var result = JSON.parse(res);
                    var status = result.status;
                    var companyData = result.companyData;
                    console.log(companyData);
                    console.log(companyData.company);
                    if (status == true){
                        var company_name = $('.CompanyName').val(companyData.company);
                        var office_name = $('.officeName').append(companyData.agent_attorney);
                        var company_email = $('.companyEmail').val(companyData.email);
                        var company_phone = $('.companyPhone').val(companyData.phone);
                        var company_fax = $('.companyFax').val(companyData.fax);
                        var address_one = $('.addressOne').val(companyData.address_1);
                        var address_two = $('.addressTwo').val(companyData.address_2);
                        var company_zip_code = $('.companyZipCode').val(companyData.zip_code);
                        var company_city = $('.companyCity').val(companyData.city);
                        var company_state = $('.companyState').val(companyData.state);
                        var company_country = $('.companyCountry').val(companyData.country);

                        $('#title-attorneys').hide();
                        $('#add-title-attorneys').show();
                    }
                }
            });
        }

        function  updateTitleCompany(id)
        {
            console.log(id);
            let update_company_id = id;
            let update_company_name = $('.CompanyName').val();
            let update_office_name = $('.officeName').val();
            let update_company_email = $('.companyEmail').val();
            let update_company_phone = $('.companyPhone').val();
            let update_company_fax = $('.companyFax').val();
            let update_address_one = $('.addressOne').val();
            let update_address_two = $('.addressTwo').val();
            let update_zip_code = $('.companyZipCode').val();
            let update_company_city = $('.companyCity').val();
            let update_company_state = $('.companyState').val();
            let update_company_country = $('.companyCountry').val();

            console.log(update_company_name);
            console.log(update_office_name);
            console.log(update_company_email);
            console.log(update_company_phone);
            console.log(update_company_fax);
            console.log(update_address_one);
            console.log(update_address_two);
            console.log(update_zip_code);
            console.log(update_company_city);
            console.log(update_company_state);
            console.log(update_company_country);

            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            var request = $.ajax({
                url: "/user/update-title-company/"+id,
                method: "post",
                data: {
                    _token: CSRF_TOKEN, brand_name:brandName,
                    update_company_id:update_company_id,
                    update_company_name:update_company_name,
                    update_office_name:update_office_name,
                    update_company_email:update_company_email,
                    update_company_phone:update_company_phone,
                    update_company_fax:update_company_fax,
                    update_address_one:update_address_one,
                    update_address_two:update_address_two,
                    update_zip_code:update_zip_code,
                    update_company_city:update_company_city,
                    update_company_state:update_company_state,
                    update_company_country:update_company_country,
                },
                dataType: "html",
                success: function(res) {
                    console.log(res);
                }
            });

        }
    </script>
@endpush
