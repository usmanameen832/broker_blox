@extends('user.layouts.master')

@push('css')
@endpush

@section('content')
    <div class="header">
        <div class="container-fluid">
            <div class="header-body">
                <div class="row align-items-end">
                    <div class="col">
                        <h1 class="header-title">
                            Settings / Default State
                        </h1>
                    </div>
                    <div class="col-auto">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 col-lg-12 col-xl">
                <div class="card">
                    <div class="card-body">
                        <div class="table-header">
                            <h2>Default State</h2>
                        </div>

                        <div class="col-md-8">
                            <div class="col-md-8">
                                <div class="form-group form-group-row">
                                    <label class="w-25">Country:</label>
                                    <div class="input_container w-75">
                                        <input type="text" placeholder="Enter Country" class="form-control input-group">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-8">
                                <div class="form-group form-group-row">
                                    <label class="w-25">State/Province:</label>
                                    <div class="input_container select_container w-75">
                                        <input type="text" placeholder="Enter State" class="form-control input-group">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-8">
                                <div class="form-group pull-right mt-5">
                                    <div>
                                        <button type="button" class="btn btn-secondary">Reset
                                            Filter</button>
                                        <button type="submit" class="btn btn-primary">Apply
                                            Filter(s)</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- / .row -->
    </div>
@endsection

@push('js')
@endpush
