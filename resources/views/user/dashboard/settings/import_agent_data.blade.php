@extends('user.layouts.master')

@push('css')
@endpush

@section('content')
    <div class="header">
        <div class="container-fluid">
            <div class="header-body">
                <div class="row align-items-end">
                    <div class="col">
                        <h1 class="header-title">
                            Settings / Import Agent Billing Data
                        </h1>
                    </div>
                    <div class="col-auto">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 col-lg-12 col-xl">
                <div class="card">
                    <div class="card-header">
                        <h4>
                            Import Agent Data
                        </h4>
                    </div>
                    <div class="card-body">
                        <div class="form-group form-group-row" style="margin-bottom: 0">
                            <label class="mt-2" style="margin-right: 40px"> <b>Upload:</b> </label>
                            <button class="btn btn-secondary">Import Billing</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 col-lg-12 col-xl">
                <div class="card">
                    <div class="card-header">
                        <h4>Import Billing Logs</h4>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-sm">
                                <thead>
                                <tr>
                                    <th scope="col">
                                        <a href="#" class="text-muted list-sort">Date</a>
                                    </th>
                                    <th scope="col">
                                        <a href="#" class="text-muted list-sort">Agent Detail</a>
                                    </th>
                                    <th scope="col">
                                        <a href="#" class="text-muted list-sort">Status</a>
                                    </th>
                                </tr>
                                </thead>
                                <tbody class="list">
                                <tr>
                                    <td class="tables-handle">01-01-2021</td>
                                    <td class="tables-handle">N/A</td>
                                    <td class="tables-handle">Pending</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('js')
@endpush
