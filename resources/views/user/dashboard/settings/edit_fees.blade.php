@extends('user.layouts.master')

@push('css')
@endpush

@section('content')
    <div class="header">
        <div class="container-fluid">
            <div class="header-body">
                <div class="row align-items-end">
                    <div class="col">
                        <h1 class="header-title">
                            Settings / Edit Fees Items
                        </h1>
                    </div>
                    <div class="col-auto">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="col-md-12 col-lg-12 col-xl">
            <div class="card">
                <div class="card-body">
                    <div class="col-md-6">
                        <div class="form-group form-group-row">
                            <label class="mb-2 mt-2">Fee Type:</label>
                            <div class="input_container select_container w-50" style="margin-left: 6px;">
                                <select class="form-select mb-3 feeTypeSelect" data-choices>
                                    <option>Additional Closing Fees</option>
                                    <option>Pre Credit/Debit</option>
                                    <option>Post Credit/Debit</option>
                                    <option>Agent Billing</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    @if(session()->has('closingFeeStatus'))
                        <div class="alert alert-success">
                            <h3>{{ session('closingFeeStatus') }}</h3>
                        </div>
                    @endif
                    @if(session()->has('preCreditDebit'))
                        <div class="alert alert-success">
                            <h3>{{ session('preCreditDebit') }}</h3>
                        </div>
                    @endif
                    @if(session()->has('postCreditDebit'))
                        <div class="alert alert-success">
                            <h3>{{ session('postCreditDebit') }}</h3>
                        </div>
                    @endif
                    @if(session()->has('agentBillingStatus'))
                        <div class="alert alert-success">
                            <h3>{{ session('agentBillingStatus') }}</h3>
                        </div>
                    @endif
                    @if(session()->has('feeDeleteStatus'))
                        <div class="alert alert-success">
                            <h3>{{ session('feeDeleteStatus') }}</h3>
                        </div>
                    @endif
                    @if(session()->has('preCreditStatus'))
                        <div class="alert alert-success">
                            <h3>{{ session('preCreditStatus') }}</h3>
                        </div>
                    @endif
                    @if(session()->has('postCreditStatus'))
                        <div class="alert alert-success">
                            <h3>{{ session('postCreditStatus') }}</h3>
                        </div>
                    @endif
                    @if(session()->has('updateStatus'))
                        <div class="alert alert-success">
                            <h3>{{ session('updateStatus') }}</h3>
                        </div>
                    @endif
                    <section class="closingFeeSection">
                        <div class="col-md-12">
                            <h2>Closing Fee</h2>
                            <div class="clearfix">
                                <button data-bs-toggle="modal" data-bs-target="#AddClosingFee" class="btn btn-primary pull-right">+ Add Closing Fee</button>
                            </div>
                            <!-- Add Closing Fees -->
                            <div class="table-responsive mt-3" data-list='{"valueNames": ["tables-row", "tables-first", "tables-last", "tables-handle"]}'>
                                <table class="table table-sm">
                                    <thead>
                                    <tr>
                                        <th scope="col">
                                            <a href="#" class="text-muted list-sort" data-sort="tables-first">Fee Item</a>
                                        </th>
                                        <th scope="col">
                                            <a href="#" class="text-muted list-sort">Item Type</a>
                                        </th>
                                        <th scope="col">
                                            <a href="#" class="text-muted list-sort">Fee</a>
                                        </th>
                                        <th scope="col">
                                            <a href="#" class="text-muted list-sort" data-sort="tables-handle">Based On</a>
                                        </th>
                                        <th scope="col">
                                            <a href="#" class="text-muted list-sort">Action</a>
                                        </th>
                                    </tr>
                                    </thead>
                                    <tbody class="list">
                                    @forelse($closingFeeData as $feeData)
                                        <tr>
                                            <td class="tables-first">{{ $feeData->item_name }}</td>
                                            <td>{{ $feeData->item_type }}</td>
                                            <td>{{ $feeData->closing_fee_percentage }}{{ $feeData->closing_flat_fee }}</td>
                                            <td class="tables-handle">{{ $feeData->based_on }}</td>
                                            <td>
                                                <a href="{{ route('editClosingFee', $feeData->id) }}" data-bs-toggle="modal" data-bs-target="#edit{{$feeData->id}}"><i class="far fa-edit"></i></a>
                                                <a href="{{ route('deleteClosingFee', $feeData->id)  }}"><i class="fas fa-trash-alt"></i></a>
                                            </td>
                                        </tr>
                                        <!-- Edit Closing Fees Modal -->
                                        <div class="modal fade" id="edit{{$feeData->id}}" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                                            <form action="{{ route('updateClosingFee',$feeData->id) }}" method="post">
                                                @csrf
                                                <div class="modal-dialog modal-1-dialog">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h3 class="modal-title" id="staticBackdropLabel">Edit Closing Fees</h3>
                                                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                        </div>
                                                        <div class="modal-body">
                                                            <div class="col-md-12 row">
                                                                <div class="col-md-4">
                                                                    <div class="form-group">
                                                                        <label>Item Name</label>
                                                                        <input type="text" name="item_name" value="{{ $feeData->item_name }}" required placeholder="Enter Item Name" class="form-control input-group mt-2">
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-4">
                                                                    <div class="form-group">
                                                                        <label>Item Type</label>
                                                                        <div class="input_container select_container mt-2">
                                                                            <select class="form-select" name="item_type">
                                                                                <option disabled selected value="{{ $feeData->item_type }}">{{ $feeData->item_type }}</option>
                                                                                <option>Debit</option>
                                                                                <option>Credit</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-4">
                                                                    <div class="form-group">
                                                                        <label>Based On</label>
                                                                        <div class="input_container select_container mt-2">
                                                                            <select class="form-select text-truncate" name="based_on">
                                                                                <option value="{{ $feeData->based_on }}" selected>{{ $feeData->based_on }}</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-12 row">
                                                                <div class="col-md-4">
                                                                    <div class="form-group">
                                                                        <label>Percentage %</label>
                                                                        <input type="number" placeholder="0" value="{{ $feeData->closing_fee_percentage }}" name="closing_fee_percentage" class="form-control input-group mt-2">
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-4">
                                                                    <div class="form-group">
                                                                        <label>Or Flat Fee $</label>
                                                                        <input type="number" placeholder="0" value="{{ $feeData->closing_flat_fee }}" name="closing_flat_fee" class="form-control input-group mt-2">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button class="btn btn-light" data-bs-dismiss="modal"> Cancel </button>
                                                            <button type="submit" class="btn btn-primary">Update</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    @empty
                                        <tr style="text-align: center">
                                            <td colspan="5">No Record is available</td>
                                        </tr>
                                    @endforelse
                                    </tbody>
                                </table>
                            </div>
                            <!-- Add Closing Fees Modal -->
                            <div class="modal fade" id="AddClosingFee" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                                <form action="{{ route('saveClosingFee') }}" method="post">
                                    @csrf
                                    <div class="modal-dialog modal-1-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h3 class="modal-title" id="staticBackdropLabel">Add Closing Fees</h3>
                                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                            </div>
                                            <div class="modal-body">
                                                <div class="col-md-12 row">
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label>Item Name</label>
                                                            <input type="text" name="item_name" required placeholder="Enter Item Name" class="form-control input-group mt-2">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label>Item Type</label>
                                                            <div class="input_container select_container mt-2">
                                                                <select class="form-select" name="item_type" data-choices>
                                                                    <option>Debit</option>
                                                                    <option>Credit</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label>Based On</label>
                                                            <div class="input_container select_container mt-2">
                                                                <select class="form-select text-truncate" name="based_on">
                                                                    <option>Agent Commission</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12 row">
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label>Percentage %</label>
                                                            <input type="number" placeholder="0" name="closing_fee_percentage" class="form-control input-group mt-2">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label>Or Flat Fee $</label>
                                                            <input type="number" placeholder="0" name="closing_flat_fee" class="form-control input-group mt-2">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button class="btn btn-light" data-bs-dismiss="modal"> Cancel </button>
                                                <button type="submit" class="btn btn-primary">Save</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </section>
                    <section class="preCreditDebitSection" style="display: none">
                        <div class="col-md-12">
                            <h2>Pre Credit/Debit</h2>
                            <div class="clearfix">
                                <button data-bs-toggle="modal" data-bs-target="#addPreCreditDebit" class="btn btn-primary pull-right">+ Add Credit/Debit</button>
                            </div>
                            <!-- Pre Credit/Debit Table -->
                            <div class="table-responsive mt-3" data-list='{"valueNames": ["tables-row", "tables-first", "tables-last", "tables-handle"]}'>
                                <table class="table table-sm">
                                    <thead>
                                    <tr>
                                        <th scope="col">
                                            <a href="#" class="text-muted list-sort" data-sort="tables-first">Items</a>
                                        </th>
                                        <th scope="col">
                                            <a href="#" class="text-muted list-sort">Item Type</a>
                                        </th>
                                        <th scope="col">
                                            <a href="#" class="text-muted list-sort">Fee</a>
                                        </th>
                                        <th scope="col">
                                            <a href="#" class="text-muted list-sort" data-sort="tables-handle">Based On</a>
                                        </th>
                                        <th scope="col">
                                            <a href="#" class="text-muted list-sort">Action</a>
                                        </th>
                                    </tr>
                                    </thead>
                                    <tbody class="list">
                                    @forelse($preCreditDebitData as $preData)
                                        <tr>
                                            <td class="tables-first">{{ $preData->pre_credit_item }}</td>
                                            <td>{{ $preData->pre_credit_item_type }}</td>
                                            <td>{{ $preData->pre_credit_percentage }}{{ $preData->pre_credit_fee }}</td>
                                            <td class="tables-handle"> {{ $preData->pre_credit_based_on }} </td>
                                            <td>
                                                <a href="{{ route('editPreCredit', $preData->id) }}" data-bs-toggle="modal" data-bs-target="#editPreCreditDebit{{$preData->id}}" ><i class="far fa-edit"></i></a>
                                                <a href="{{ route('deletePreCredit', $preData->id) }}"><i class="fas fa-trash-alt"></i></a>
                                            </td>
                                        </tr>
                                        <!-- Edit Pre Credit/Debit Modal -->
                                        <div class="modal fade" id="editPreCreditDebit{{$preData->id}}" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                                            <form action="{{ route('updatePreCredit', $preData->id) }}" method="post">
                                                @csrf
                                                <div class="modal-dialog modal-1-dialog">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h3 class="modal-title" id="staticBackdropLabel">Add Pre Credit/Debit</h3>
                                                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                        </div>
                                                        <div class="modal-body">
                                                            <div class="col-md-12 row">
                                                                <div class="col-md-4">
                                                                    <div class="form-group">
                                                                        <label>Items</label>
                                                                        <input type="text" placeholder="Enter Item Name" value="{{ $preData->pre_credit_item }}" name="pre_credit_item" class="form-control input-group mt-2">
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-4">
                                                                    <div class="form-group">
                                                                        <label>Item Type</label>
                                                                        <div class="input_container select_container mt-2">
                                                                            <select class="form-select" name="pre_credit_item_type">
                                                                                <option disabled selected value="{{$preData->pre_credit_item_type}}">{{$preData->pre_credit_item_type}}</option>
                                                                                <option>Debit</option>
                                                                                <option>Credit</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-4">
                                                                    <div class="form-group">
                                                                        <label>Based On</label>
                                                                        <div class="input_container select_container mt-2">
                                                                            <select class="form-select text-truncate" name="pre_credit_based_on">
                                                                                <option selected value="{{ $preData->pre_credit_based_on }}">{{ $preData->pre_credit_based_on }}</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-12 row">
                                                                <div class="col-md-4">
                                                                    <div class="form-group">
                                                                        <label>Percentage %</label>
                                                                        <input type="number" placeholder="0" value="{{ $preData->pre_credit_percentage }}" name="pre_credit_percentage" class="form-control input-group mt-2">
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-4">
                                                                    <div class="form-group">
                                                                        <label>Or Flat Fee $</label>
                                                                        <input type="number" placeholder="0" value="{{ $preData->pre_credit_fee }}" name="pre_credit_fee" class="form-control input-group mt-2">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-12">
                                                                <div class="form-group form-group-row">
                                                                    <label class="w-75 mt-2">Calculate agent's commission before on after this item?</label>
                                                                    <select class="form-select w-25" name="calculate_agent_commission">
                                                                        <option selected disabled value="{{ $preData->calculate_agent_commission }}">{{ $preData->calculate_agent_commission }}</option>
                                                                        <option>Before</option>
                                                                        <option>After</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-12">
                                                                <div class="form-group form-group-row">
                                                                    <label class="w-75 mt-2">Include this credit/debit in the Total Due to Brokerage?</label>
                                                                    <select class="form-select w-25" name="total_due_brokerage">
                                                                        <option selected disabled value="{{ $preData->total_due_brokerage }}">{{ $preData->total_due_brokerage }}</option>
                                                                        <option>No</option>
                                                                        <option>Yes</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button class="btn btn-light" data-bs-dismiss="modal"> Cancel </button>
                                                            <button type="submit" class="btn btn-primary">Update</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    @empty
                                        <tr style="text-align: center">
                                            <td colspan="5">No Record is Available</td>
                                        </tr>
                                    @endforelse
                                    </tbody>
                                </table>
                            </div>
                            <!-- Pre Credit/Debit Modal -->
                            <div class="modal fade" id="addPreCreditDebit" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                                <form action="{{ route('savePreCreditDebit') }}" method="post">
                                    @csrf
                                    <div class="modal-dialog modal-1-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h3 class="modal-title" id="staticBackdropLabel">Add Pre Credit/Debit</h3>
                                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                            </div>
                                            <div class="modal-body">
                                                <div class="col-md-12 row">
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label>Items</label>
                                                            <input type="text" placeholder="Enter Item Name" name="pre_credit_item" class="form-control input-group mt-2">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label>Item Type</label>
                                                            <div class="input_container select_container mt-2">
                                                                <select class="form-select" name="pre_credit_item_type" data-choices>
                                                                    <option>Debit</option>
                                                                    <option>Credit</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label>Based On</label>
                                                            <div class="input_container select_container mt-2">
                                                                <select class="form-select text-truncate" name="pre_credit_based_on" data-choices>
                                                                    <option>Agent Commission</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12 row">
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label>Percentage %</label>
                                                            <input type="number" placeholder="0" name="pre_credit_percentage" class="form-control input-group mt-2">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label>Or Flat Fee $</label>
                                                            <input type="number" placeholder="0" name="pre_credit_fee" class="form-control input-group mt-2">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group form-group-row">
                                                        <label class="w-75 mt-2">Calculate agent's commission before on after this item?</label>
                                                        <select class="form-select w-25" name="calculate_agent_commission">
                                                            <option>Before</option>
                                                            <option>After</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group form-group-row">
                                                        <label class="w-75 mt-2">Include this credit/debit in the Total Due to Brokerage?</label>
                                                        <select class="form-select w-25" name="total_due_brokerage">
                                                            <option>No</option>
                                                            <option>Yes</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button class="btn btn-light" data-bs-dismiss="modal"> Cancel </button>
                                                <button type="submit" class="btn btn-primary">Save</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </section>
                    <section class="postCreditDebitSection" style="display: none">
                        <div class="col-md-12">
                            <h2>Post Credit/Debit</h2>
                            <div class="clearfix">
                                <button data-bs-toggle="modal" data-bs-target="#addPostCreditDebit" class="btn btn-primary pull-right">+ Add Credit/Debit</button>
                            </div>
                            <!-- Post Credit/Debit  -->
                            <div class="table-responsive mt-3" data-list='{"valueNames": ["tables-row", "tables-first", "tables-last", "tables-handle"]}'>
                                <table class="table table-sm">
                                    <thead>
                                    <tr>
                                        <th scope="col">
                                            <a href="#" class="text-muted list-sort" data-sort="tables-first">Items</a>
                                        </th>
                                        <th scope="col">
                                            <a href="#" class="text-muted list-sort">Item Type</a>
                                        </th>
                                        <th scope="col">
                                            <a href="#" class="text-muted list-sort" data-sort="tables-last">Fee</a>
                                        </th>
                                        <th scope="col">
                                            <a href="#" class="text-muted list-sort" data-sort="tables-handle">Based On</a>
                                        </th>
                                        <th scope="col">
                                            <a href="#" class="text-muted list-sort">Action</a>
                                        </th>
                                    </tr>
                                    </thead>
                                    <tbody class="list">
                                    @forelse($postCreditDebitData as $postData)
                                        <tr>
                                            <td class="tables-first">{{ $postData->post_credit_item }}</td>
                                            <td>{{ $postData->post_item_type }}</td>
                                            <td class="tables-last">{{ $postData->post_credit_percentage }} {{ $postData->post_credit_fee }}</td>
                                            <td class="tables-handle">{{ $postData->post_credit_based_on }}</td>
                                            <td>
                                                <a href="{{ route('editPostCredit',$postData->id) }}" data-bs-toggle="modal" data-bs-target="#editPostCreditDebit{{$postData->id}}"><i class="far fa-edit"></i></a>
                                                <a href="{{ route('deletePostCredit', $postData->id) }}"><i class="fas fa-trash-alt"></i></a>
                                            </td>
                                        </tr>
                                        <!-- Edit Post Credit Debit Modal -->
                                        <div class="modal fade" id="editPostCreditDebit{{$postData->id}}" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                                            <div class="modal-dialog modal-1-dialog">
                                                <form action="{{ route('updatePostCredit', $postData->id) }}" method="post">
                                                    @csrf
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h3 class="modal-title" id="staticBackdropLabel">Add Pre Credit/Debit</h3>
                                                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                        </div>
                                                        <div class="modal-body">
                                                            <div class="col-md-12 row">
                                                                <div class="col-md-4">
                                                                    <div class="form-group">
                                                                        <label>Item Name</label>
                                                                        <input type="text" placeholder="Enter Item Name" value="{{ $postData->post_credit_item}}" name="post_credit_item" class="form-control input-group mt-2">
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-4">
                                                                    <div class="form-group">
                                                                        <label>Item Type</label>
                                                                        <div class="input_container select_container mt-2">
                                                                            <select class="form-select" name="post_item_type">
                                                                                <option selected disabled value="{{ $postData->post_item_type }}">{{ $postData->post_item_type }}</option>
                                                                                <option>Debit</option>
                                                                                <option>Credit</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-4">
                                                                    <div class="form-group">
                                                                        <label>Based On</label>
                                                                        <div class="input_container select_container mt-2">
                                                                            <select class="form-select text-truncate" name="post_credit_based_on">
                                                                                <option selected value="{{ $postData->post_credit_based_on }}">{{ $postData->post_credit_based_on }}</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-12 row">
                                                                <div class="col-md-4">
                                                                    <div class="form-group">
                                                                        <label>Percentage %</label>
                                                                        <input type="number" placeholder="0" value="{{ $postData->post_credit_percentage }}" name="post_credit_percentage" class="form-control input-group mt-2">
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-4">
                                                                    <div class="form-group">
                                                                        <label>Or Flat Fee $</label>
                                                                        <input type="number" placeholder="0" value="{{ $postData->post_credit_fee }}" name="post_credit_fee" class="form-control input-group mt-2">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button class="btn btn-light" data-bs-dismiss="modal"> Cancel </button>
                                                            <button type="submit" class="btn btn-primary">Update</button>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    @empty
                                        <tr style="text-align: center">
                                            <td colspan="5">No Record is available</td>
                                        </tr>
                                    @endforelse
                                    </tbody>
                                </table>
                            </div>
                            <!-- Post Credit Debit Modal -->
                            <div class="modal fade" id="addPostCreditDebit" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                                <div class="modal-dialog modal-1-dialog">
                                    <form action="{{ route('savePostCreditDebit') }}" method="post">
                                        @csrf
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h3 class="modal-title" id="staticBackdropLabel">Add Pre Credit/Debit</h3>
                                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                            </div>
                                            <div class="modal-body">
                                                <div class="col-md-12 row">
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label>Item Name</label>
                                                            <input type="text" placeholder="Enter Item Name" name="post_credit_item" class="form-control input-group mt-2">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label>Item Type</label>
                                                            <div class="input_container select_container mt-2">
                                                                <select class="form-select" name="post_item_type" data-choices>
                                                                    <option>Debit</option>
                                                                    <option>Credit</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label>Based On</label>
                                                            <div class="input_container select_container mt-2">
                                                                <select class="form-select text-truncate" name="post_credit_based_on" data-choices>
                                                                    <option>Agent Commission</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12 row">
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label>Percentage %</label>
                                                            <input type="number" placeholder="0" name="post_credit_percentage" class="form-control input-group mt-2">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label>Or Flat Fee $</label>
                                                            <input type="number" placeholder="0" name="post_credit_fee" class="form-control input-group mt-2">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button class="btn btn-light" data-bs-dismiss="modal"> Cancel </button>
                                                <button type="submit" class="btn btn-primary">Save</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </section>
                    <section class="billingItem" style="display: none">
                        <div class="col-md-12">
                            <h2>Billing Item</h2>
                            <div class="clearfix">
                                <button data-bs-toggle="modal" data-bs-target="#addBillingItem" class="btn btn-primary pull-right">+ Add Billing Item</button>
                            </div>
                            <!-- Billing Item Table  -->
                            <div class="table-responsive mt-3">
                                <table class="table table-sm">
                                    <thead>
                                    <tr>
                                        <th scope="col">
                                            <a href="#" class="text-muted list-sort">Description</a>
                                        </th>
                                        <th scope="col">
                                            <a href="#" class="text-muted list-sort">Quantity</a>
                                        </th>
                                        <th scope="col">
                                            <a href="#" class="text-muted list-sort">Price</a>
                                        </th>
                                        <th scope="col">
                                            <a href="#" class="text-muted list-sort">Total Amount</a>
                                        </th>
                                        <th scope="col">
                                            <a href="#" class="text-muted list-sort">Action</a>
                                        </th>
                                    </tr>
                                    </thead>
                                    <tbody class="list">
                                    @forelse($agentBillingData as $billingData)
                                        <tr>
                                            <td>{{ $billingData->billing_description }}</td>
                                            <td>{{ $billingData->billing_quantity }}</td>
                                            <td>{{ $billingData->billing_price }}</td>
                                            <td>{{ $billingData->total_amount }}</td>
                                            <td>
                                                <a href="{{ route('editAgentBilling', $billingData->id ) }}" data-bs-toggle="modal" data-bs-target="#editBillingItem{{$billingData->id}}"><i class="far fa-edit"></i></a>
                                                <a href="{{ route('deleteAgentBilling', $billingData->id) }}"><i class="fas fa-trash-alt"></i></a>
                                            </td>
                                        </tr>
                                        <!-- Edit Billing Item Modal -->
                                        <div class="modal fade" id="editBillingItem{{$billingData->id}}" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                                            <div class="modal-dialog modal-1-dialog">
                                                <form action="{{ route('updateAgentBilling', $billingData->id) }}" method="post">
                                                    @csrf
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h3 class="modal-title" id="staticBackdropLabel">Add Billing Item</h3>
                                                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                        </div>
                                                        <div class="modal-body">
                                                            <div class="col-md-12 row">
                                                                <div class="col-md-4">
                                                                    <div class="form-group">
                                                                        <label>Description</label>
                                                                        <input type="text" placeholder="Enter Description" value="{{$billingData->billing_description}}" name="billing_description" class="form-control input-group mt-2 BillingDescription">
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-4">
                                                                    <div class="form-group">
                                                                        <label>Quantity</label>
                                                                        <input type="number" placeholder="0" value="{{$billingData->billing_quantity}}" name="billing_quantity" class="form-control input-group mt-2 BillingQuantity">
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-4">
                                                                    <div class="form-group">
                                                                        <label>Price</label>
                                                                        <input type="number" placeholder="0" value="{{$billingData->billing_price}}" name="billing_price" class="form-control input-group mt-2 BillingPrice">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-12 row">
                                                                <div class="col-md-4">
                                                                    <div class="form-group">
                                                                        <label>Total Amount</label>
                                                                        <input type="number" placeholder="$0.00" name="total_amount" readonly class="form-control input-group mt-2 TotalAmount">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button class="btn btn-light" data-bs-dismiss="modal"> Cancel </button>
                                                            <button type="submit" class="btn btn-primary">Update</button>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    @empty
                                        <tr style="text-align: center">
                                            <td colspan="5">No Record is available</td>
                                        </tr>
                                    @endforelse
                                    </tbody>
                                </table>
                            </div>
                            <!-- Add Billing Item Modal -->
                            <div class="modal fade" id="addBillingItem" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                                <div class="modal-dialog modal-1-dialog">
                                    <form action="{{ route('saveAgentBilling') }}" method="post">
                                        @csrf
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h3 class="modal-title" id="staticBackdropLabel">Add Billing Item</h3>
                                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                            </div>
                                            <div class="modal-body">
                                                <div class="col-md-12 row">
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label>Description</label>
                                                            <input type="text" placeholder="Enter Description" name="billing_description" class="form-control input-group mt-2 BillingDescription">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label>Quantity</label>
                                                            <input type="number" placeholder="0" name="billing_quantity" class="form-control input-group mt-2 BillingQuantity">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label>Price</label>
                                                            <input type="number" placeholder="0" name="billing_price" class="form-control input-group mt-2 BillingPrice">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12 row">
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label>Total Amount</label>
                                                            <input type="number" placeholder="$0.00" name="total_amount" readonly class="form-control input-group mt-2 TotalAmount">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button class="btn btn-light" data-bs-dismiss="modal"> Cancel </button>
                                                <button type="submit" class="btn btn-primary">Save</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('js')
    <script>
        $('body').on('change', '.BillingQuantity, .BillingPrice', function (){
            var quantity_value = $('.BillingQuantity').val();
            var price_value = $('.BillingPrice').val();
            var total_amount = $('.TotalAmount').val(quantity_value * price_value);
        });

        $('body').on('change', '.feeTypeSelect', function (){
            var type = $(this).val();
            if (type == "Pre Credit/Debit"){
                $('.closingFeeSection').hide();
                $('.postCreditDebitSection').hide();
                $('.billingItem').hide();
                $('.preCreditDebitSection').show();
            } else if(type == "Post Credit/Debit"){
                $('.closingFeeSection').hide();
                $('.preCreditDebitSection').hide();
                $('.billingItem').hide();
                $('.postCreditDebitSection').show();
            } else if (type == "Agent Billing"){
                $('.closingFeeSection').hide();
                $('.preCreditDebitSection').hide();
                $('.postCreditDebitSection').hide();
                $('.billingItem').show();
            } else if (type == "Additional Closing Fees"){
                $('.preCreditDebitSection').hide();
                $('.postCreditDebitSection').hide();
                $('.billingItem').hide();
                $('.closingFeeSection').show();
            }
        });
    </script>
@endpush
