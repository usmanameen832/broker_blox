@extends('user.layouts.master')

@push('css')
    <style>
        .inside-box h4{
            border-bottom: 1px solid #d2d6de;
            padding: 15px 10px;
            margin-top: 0;
            font-size: 22px;
            font-weight: 500;
        }

        .switch {
            position: relative;
            display: inline-block;
            width: 54px;
            height: 30px;
        }
        .switch input {
            opacity: 0;
            width: 0;
            height: 0;
        }
        .slider {
            position: absolute;
            cursor: pointer;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            background-color: #ccc;
            -webkit-transition: .4s;
            transition: .4s;
        }
        .slider:before {
            position: absolute;
            content: "";
            height: 22px;
            width: 22px;
            left: 4px;
            bottom: 4px;
            background-color: white;
            -webkit-transition: .4s;
            transition: .4s;
        }
        .switch input:checked + .slider {
            background-color: #2196F3;
        }

        .switch input:focus + .slider {
            box-shadow: 0 0 1px #2196F3;
        }

        .switch input:checked + .slider:before {
            -webkit-transform: translateX(26px);
            -ms-transform: translateX(26px);
            transform: translateX(26px);
        }
        /* Rounded sliders */
        .slider.round {
            border-radius: 34px;
        }

        .slider.round:before {
            border-radius: 50%;
        }

        .hide{
            display: none;
        }
        .first-row input{
            width: 140px;
            height: 40px;
            margin-right: 3px;
        }
        .first-row select{
            height: 40px;
        }
        .first-row select option{
            font-size: 15px !important;
        }
        .first-row label{
            height: 40px;
            margin: 12px 5px -5px 5px;
        }
        .third-row label{
            margin-right: 5px;
            font-size: 14px;
            font-weight: bold;
            margin-top: 10px;
        }

        .third-row select{
            width: 100px;
            height: 40px;
        }

        .modal-1-dialog{
            max-width: 80% !important;
        }
        .tdInput{
            border: none;
            width: 70%;
            outline: none;
        }
    </style>
@endpush

@section('content')
    <div id="commission-plan">
        <div class="header">
            <div class="container-fluid">
                <div class="header-body">
                    <div class="row align-items-end">
                        <div class="col">
                            <h1 class="header-title">
                                Settings / Commission Plans
                            </h1>
                        </div>
                        <div class="col-auto">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12 col-lg-12 col-xl">
                    @if(session()->has('status'))
                        <div class="alert alert-success">
                            <p>{{ session('status') }}</p>
                        </div>
                    @endif
                    @if(session()->has('error'))
                        <div class="alert alert-danger">
                            <p>{{ session('error') }}</p>
                        </div>
                    @endif
                    <div class="card">
                        <div class="card-header">
                            <h4>Commission Plan</h4>
                        </div>
                        <div class="card-body">
                            <div class="table-header">
                                <div class="col-md-6">
                                    <div class="input-group">
                                        <div class="form-outline">
                                            <input type="search" placeholder="search.." class="form-control" />
                                        </div>
                                        <button type="button" class="btn btn-primary">
                                            <i class="fas fa-search"></i>
                                        </button>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div style="float: right">
                                        <button class="btn btn-primary" onclick="hideCommissionPlan()">+ Add Plan</button>
                                    </div>
                                </div>
                            </div>

                            <form>
                                <div class="col-md-12 row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="mb-2">Date Created:</label>
                                            <div class="input_container select_container">
                                                <input type="date" class="form-control input-group">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="mb-2">Plan Status:</label>
                                            <div class="input_container select_container">
                                                <select class="form-select mb-3" data-choices>
                                                    <option>All</option>
                                                    <option>Active</option>
                                                    <option>Inactive</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group filter_action pull-right">
                                            <div>
                                                <button type="button" class="btn btn-secondary">Reset
                                                    Filter</button>
                                                <button type="submit" class="btn btn-primary">Apply
                                                    Filter(s)</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <div class="table-responsive">
                                <table class="table table-sm" id="dtBasicExample">
                                    <thead>
                                    <tr>
                                        <th scope="col">
                                            <a href="#" class="text-muted list-sort">#</a>
                                        </th>
                                        <th scope="col">
                                            <a href="#" class="text-muted list-sort">Plan Name</a>
                                        </th>
                                        <th scope="col">
                                            <a href="#" class="text-muted list-sort">Type of Plan</a>
                                        </th>
                                        <th scope="col">
                                            <a href="#" class="text-muted list-sort">Agent(s) Assigned</a>
                                        </th>
                                        <th scope="col">
                                            <a href="#" class="text-muted list-sort">Date Created</a>
                                        </th>
                                        <th scope="col">
                                            <a href="#" class="text-muted list-sort">Status</a>
                                        </th>
                                        <th scope="col">
                                            <a href="#" class="text-muted list-sort">Action</a>
                                        </th>
                                    </tr>
                                    </thead>
                                    <tbody class="list">
                                    @if(session()->has('delete_Message'))
                                        <div class="alert alert-success">
                                            {{ session('delete_Message') }}
                                        </div>
                                    @endif
                                    @foreach($data as $plans)
                                        <tr>
                                            <td>{{ $plans->id }}</td>
                                            <td><a href="javascript:void(0)" onclick="editCommissionPlan('{{$plans->id}}')">{{ $plans->plan_name }} ({{$plans->agent_percentage}}%)</a> </td>
                                            <td>{{ $plans->type_of_plan }}</td>
                                            <td></td>
                                            <td>{{ $plans->day }}</td>
                                            <td>{{ $plans->plan_status }}</td>
                                            <td>
                                                <a href="javascript:void(0)" onclick="editCommissionPlan('{{$plans->id}}')"><i class="far fa-edit"></i></a>
                                                <a href="#" style="margin-left: 5px;"><i class="far fa-copy"></i></a>
                                                <a href="{{ route('deleteCommissionPlan', $plans->id) }}" style="margin-left: 5px"> <i class="far fa-trash-alt"></i> </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                                <ul class="pagination justify-content-end">
                                    <li class="page-item disabled"><a class="page-link" href="#">Previous</a></li>
                                    <li class="page-item"><a class="page-link" href="#">1</a></li>
                                    <li class="page-item"><a class="page-link" href="#">2</a></li>
                                    <li class="page-item"><a class="page-link" href="#">3</a></li>
                                    <li class="page-item"><a class="page-link" href="#">Next</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="add-plan" class="hide">
        <div class="header">
            <div class="container-fluid">
                <div class="header-body">
                    <div class="row align-items-end">
                        <div class="col">
                            <h1 class="header-title">
                                Commission Plans / Add Plan / <span class="spanPlanName"></span>
                            </h1>
                        </div>
                        <div class="col-auto">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12 col-lg-12 col-xl">
                    <div class="card">
                        <div class="card-body">
                            <ul class="nav nav-tabs" id="nav-tabs" role="tablist">
                                <li class="nav-item" role="presentation">
                                    <a class="nav-link active" data-bs-toggle="tab" data-bs-target="#tab1" id="tab-a" type="button" role="tab">Plan Info</a>
                                </li>
                                <li class="nav-item" role="presentation">
                                    <a class="nav-link" data-bs-toggle="tab" data-bs-target="#tab2" id="tab-b" type="button" role="tab">Agent(s) Assigned</a>
                                </li>
                            </ul>
                            <div class="tab-content mt-3">
                                <div class="tab-pane show active" id="tab1">
                                    <form action="{{ route('postCommissionPlan') }}" method="post">
                                        @csrf
                                        <div class="col-lg-12 col-md-12 col-sm-12 row">
                                            <div class="col-md-6">
                                                <div class="card">
                                                    <div class="card-header">
                                                        <h4>General Info</h4>
                                                    </div>
                                                    <div class="card-body">
                                                        <div class="form-group form-group-row">
                                                            <label class="w-25 mt-2">Plan Name:</label>
                                                            <div class="input_container select_container w-75">
                                                                <input type="text" name="plan_name" required class="input-group form-control planName">
                                                            </div>
                                                        </div>
                                                        <div class="form-group form-group-row">
                                                            <label class="w-25 mt-2">Plan Status:</label>
                                                            <div class="input_container select_container w-75">
                                                                <select class="form-select" name="plan_status" data-choices>
                                                                    <option>Active</option>
                                                                    <option>Inactive</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="form-group form-group-row">
                                                            <label class="w-25 mt-2">Type of Plan:</label>
                                                            <div class="input_container select_container w-75">
                                                                <select class="form-select type-of-plan" name="type_of_plan" data-choices>
                                                                    <option>Select an Option</option>
                                                                    <option>Flat Fee</option>
                                                                    <option>Sliding Scale</option>
                                                                    <option>Close Transaction</option>
                                                                    <option>Cap</option>
                                                                    <option>Sale Price</option>
                                                                    <option>Gross Commission Per Deal</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="inside-box Eap hide">
                                                            <div class="form-group form-group-row">
                                                                <label class="w-25 mt-2">Enter Agent's Percentage:</label>
                                                                <div class="input_container select_container w-75">
                                                                    <input type="text" name="agent_percentage" class="form-control input-group">
                                                                </div>
                                                            </div>
                                                            <h4>Default Additional Closing Fee (Optional)</h4>
                                                            <div class="form-group">
                                                                <i class="fa fa-exclamation-circle pull-right" style="color: red"></i>
                                                                <a href="" data-bs-toggle="modal" data-bs-target="#viewEditClosingFee1" style="margin-bottom: 15px; padding-right: 7px;" class="pull-right">
                                                                    <i class="far fa-edit"></i> View and Edit Additional Closing Fee
                                                                </a>
                                                            </div>
                                                            <div class="clearfix"></div>
                                                        </div>

                                                        <!-- Additional Closing Fee Modal -->
                                                        <div class="modal fade additionalClosingFee" id="viewEditClosingFee1" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                                                            <div class="modal-dialog modal-1-dialog">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <h3 class="modal-title" id="staticBackdropLabel">Additional Closing Fees</h3>
                                                                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                                    </div>
                                                                    <div class="modal-body">
                                                                        <table class="table" id="additionalClosingFeeTable">
                                                                            <thead>
                                                                            <tr>
                                                                                <th scope="col">Closing Fee Item</th>
                                                                                <th scope="col">Closing Fee($)</th>
                                                                                <th scope="col">Closing Fee(%)</th>
                                                                                <th scope="col">Based On</th>
                                                                                <th scope="col">Included Towards Cap on Fees</th>
                                                                                <th scope="col">Action</th>
                                                                            </tr>
                                                                            </thead>
                                                                            <tbody id="additional_closing_fee">
                                                                            <tr>
                                                                                <td></td>
                                                                                <td></td>
                                                                                <td></td>
                                                                                <td></td>
                                                                                <td></td>
                                                                                <td></td>
                                                                            </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </div>
                                                                    <div class="modal-footer">
                                                                        <button type="button" data-bs-dismiss="modal" data-bs-toggle="modal" data-bs-target="#viewEditClosingFee2" class="btn btn-primary addClosingFee">Add Closing Fee</button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <!-- Add Additional Closing Fees Modal -->
                                                        <div class="modal fade addAdditionalClosingFee" id="viewEditClosingFee2" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                                                            <div class="modal-dialog">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <h5 class="modal-title" id="staticBackdropLabel">Add Additional Closing Fees</h5>
                                                                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                                    </div>
                                                                    <form>
                                                                        <div class="modal-body adjusted-gross-level">
                                                                            <div class="col-md-12 row">
                                                                                <div class="col-lg-6 col-md-6">
                                                                                    <label>Item Name:</label>
                                                                                    <input type="text" placeholder="Enter Item name" class="form-control closingFeeItem">
                                                                                </div>
                                                                                <div class="col-lg-6 col-md-6">
                                                                                    <label>Or Flat Fees $:</label>
                                                                                    <input type="number" placeholder="Enter Fee" class="form-control closing-flat-fee closingFlatFee">
                                                                                </div>
                                                                            </div>
                                                                            <br>
                                                                            <div class="col-md-12 row">
                                                                                <div class="col-lg-6 col-md-6 mt-3">
                                                                                    <label>Percentage %:</label>
                                                                                    <input type="number" placeholder="Percentage" class="form-control closing-fee-percentage closingFeePercentage">
                                                                                </div>
                                                                                <div class="col-lg-6 col-md-6 mt-3 closingFeeBasedOn" style="display: none;">
                                                                                    <label>Based On:</label>
                                                                                    <select class="form-select basedOn">
                                                                                        <option>Agent Commission</option>
                                                                                        <option>Brokerage Gross Commission</option>
                                                                                        <option>Sales Price</option>
                                                                                        <option>Gross Commission After Credit/Debit</option>
                                                                                    </select>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-lg-12 col-md-12" style="margin-top: 25px !important; margin-bottom: -25px; !important;">
                                                                                <div class="form-group form-group-row">
                                                                                    <label>Included Towards Cap on Fees?</label>
                                                                                    <div class="input_container" style="margin-left: 10px">
                                                                                        <label class="switch">
                                                                                            <input type="checkbox" class="includeCapFee include-cap-fee">
                                                                                            <span class="slider round"></span>
                                                                                        </label>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="modal-footer">
                                                                            <button type="button" class="btn btn-secondary" id="cancelAddClosingFee" data-bs-dismiss="modal" data-bs-toggle="modal" data-bs-target="#viewEditClosingFee1">Close</button>
                                                                            <button type="button" class="btn btn-primary saveClosingFee">Save</button>
                                                                        </div>
                                                                    </form>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <!-- Edit Additional Closing Fees Modal -->
                                                        <div class="modal fade" id="editClosingFeeModal" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                                                            <div class="modal-dialog">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <h5 class="modal-title" id="staticBackdropLabel">Edit Additional Closing Fees</h5>
                                                                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                                    </div>
                                                                    <form>
                                                                        <div class="modal-body adjusted-gross-level">
                                                                            <input type="text" class="editClosingFeeId" style="display: none;">
                                                                            <div class="col-md-12 row">
                                                                                <div class="col-lg-6 col-md-6">
                                                                                    <label>Item Name:</label>
                                                                                    <input type="text" placeholder="Enter Item name" class="form-control editfeeitem">
                                                                                </div>
                                                                                <div class="col-lg-6 col-md-6">
                                                                                    <label>Or Flat Fees $:</label>
                                                                                    <input type="number" placeholder="Enter Fee" class="form-control editflatfee">
                                                                                </div>
                                                                            </div>
                                                                            <br>
                                                                            <div class="col-md-12 row">
                                                                                <div class="col-lg-6 col-md-6 mt-3">
                                                                                    <label>Percentage %:</label>
                                                                                    <input type="number" placeholder="Percentage" class="form-control editfeepercentage">
                                                                                </div>
                                                                                <div class="col-lg-6 col-md-6 mt-3 editFeesBasedOn" style="display: none;">
                                                                                    <label>Based On:</label>
                                                                                    <select class="form-select editfeebasedon">
                                                                                        <option>Agent Commission</option>
                                                                                        <option>Brokerage Gross Commission</option>
                                                                                        <option>Sales Price</option>
                                                                                        <option>Gross Commission After Credit/Debit</option>
                                                                                    </select>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-lg-12 col-md-12" style="margin-top: 25px !important; margin-bottom: -25px; !important;">
                                                                                <div class="form-group form-group-row">
                                                                                    <label>Included Towards Cap on Fees?</label>
                                                                                    <div class="input_container" style="margin-left: 10px">
                                                                                        <label class="switch">
                                                                                            <input type="checkbox" class="editincludefee">
                                                                                            <span class="slider round"></span>
                                                                                        </label>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="modal-footer">
                                                                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal" data-bs-toggle="modal" data-bs-target="#viewEditClosingFee1">Close</button>
                                                                            <button type="button" class="btn btn-primary closingFeeUpdate">Update</button>
                                                                        </div>
                                                                    </form>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="form-group form-group-row Cof" style="display: none">
                                                            <label class="w-25 mt-2">Cap On Fees:</label>
                                                            <div class="input_container select_container w-75">
                                                                <select class="form-select cap-on-fees" name="cap_on_fees" data-choices>
                                                                    <option>No</option>
                                                                    <option>Yes</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="form-group form-group-row Ca" style="display:none;">
                                                            <label class="w-25 mt-2">Cap Amount:</label>
                                                            <div class="input_container w-75">
                                                                <input type="text" name="cap_amount" class="form-control input-group">
                                                            </div>
                                                        </div>
                                                        <div class="form-group form-group-row Rod" style="display: none">
                                                            <label class="w-25 mt-2">Roll Over Date:</label>
                                                            <div class="input_container select_container w-75">
                                                                <select class="form-select roll-over-date" name="roll_over_data" data-choices>
                                                                    <option>Select an Option</option>
                                                                    <option>Start Date</option>
                                                                    <option>Custom Date</option>
                                                                    <option>Calendar year</option>
                                                                    <option>No Role Over Date</option>
                                                                    <option>Monthly</option>
                                                                    <option>Rolling Rollover</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="form-group form-group-row Scp" style="display:none;">
                                                            <label class="w-25">Shared Commission Plan:</label>
                                                            <div class="input_container select_container w-75">
                                                                <label class="switch">
                                                                    <input type="checkbox">
                                                                    <span class="slider round"></span>
                                                                </label>
                                                            </div>
                                                        </div>
                                                        <div class="form-group form-group-row Cd" style="display: none">
                                                            <label class="w-25 mt-2">Custom Date:</label>
                                                            <div class="input_container w-75">
                                                                <input type="date" class="form-control input-group">
                                                            </div>
                                                        </div>
                                                        <div class="form-group form-group-row Clbo" style="display: none">
                                                            <label class="w-25 mt-2">Commission Levels based on:</label>
                                                            <div class="input_container select_container w-75">
                                                                <select class="form-select" name="commission_level_based_on" data-choices>
                                                                    <option>Adjusted Gross Commission</option>
                                                                    <option>Sales Price</option>
                                                                    <option>Agent Gross Commission</option>
                                                                    <option>Gross Commission</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="form-group form-group-row Prl" style="display: none">
                                                            <label class="w-25">Pro-rate Levels:</label>
                                                            <div class="input_container w-75">
                                                                <input type="checkbox" class="form-check-input">
                                                            </div>
                                                        </div>
                                                        <div class="inside-box hide Pccd">
                                                            <h4>Pre-Commission Credit/Debit (Optional)</h4>
                                                            <div class="form-group form-group-row mt-3">
                                                                <label class="w-25">Add Pre-Commission Credit/Debit?</label>
                                                                <div class="input_container select_container w-75 mt-3">
                                                                    <select class="form-select add-pre-commission-credit" name="add_pre_commission_credit" data-choices>
                                                                        <option>No</option>
                                                                        <option>Yes</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="form-group form-group-row mt-3 Copccdf" style="display: none">
                                                                <label class="w-25">Cap On Pre-Commission Credit/Debit Fees?</label>
                                                                <div class="input_container select_container w-75 mt-3">
                                                                    <select class="form-select cap-on-pre-cred-fees" name="cap_on_pre_commission" data-choices>
                                                                        <option>No</option>
                                                                        <option>Yes</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="form-group form-group-row mt-3 Pccdca" style="display: none">
                                                                <label class="w-25">Pre-Commission Credit/Debit Cap Amount:</label>
                                                                <div class="input_container w-75 mt-3">
                                                                    <input type="number" class="form-control input-group">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="card">
                                                    <div class="card-header">
                                                        <h4>Tips for Setting Up Commission Plans</h4>
                                                    </div>
                                                    <div class="card-body">
                                                        <div class="accordion" id="accordionExample">
                                                            <div class="accordion-item">
                                                                <h2 class="accordion-header" id="flat-fee-plan">
                                                                    <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                                                        Flat Fee Plans
                                                                    </button>
                                                                </h2>
                                                                <div id="collapseOne" class="accordion-collapse collapse show" aria-labelledby="flat-fee-plan" data-bs-parent="#accordionExample">
                                                                    <div class="accordion-body">
                                                                        Select the Flat Fee option from the question 'Type of Plan'. Enter the percentage owed to the agent. For example, if the agent receives 80% and the brokerage 20%, enter 80 for the agent's percentage. If you offer a 100% plan and deduct a transaction fee from each sale, enter 100 for the agent's percentage, then once you've assigned the plan to an agent, go into the agents record, select the Commission Plan option, and add in the fees for the brokerage in the Additional Closing Fees section.
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="accordion-item">
                                                                <h2 class="accordion-header" id="sliding-scale-plan">
                                                                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                                                        Sliding Scale Plans
                                                                    </button>
                                                                </h2>
                                                                <div id="collapseTwo" class="accordion-collapse collapse" aria-labelledby="sliding-scale-plan" data-bs-parent="#accordionExample">
                                                                    <div class="accordion-body">
                                                                        Select the Sliding Scale option from the question 'Type of Plan'. Choose a rollover date for this plan. If you choose Start Date, the system will use the agent's start data from their record. If you choose calendar year, we will use January 1 and if you choose custom date, you'll need to enter a specific date. Next choose whether to prorate levels. Here is an example of how pro-rate levels worked:
                                                                        <br><br>
                                                                        Agent closes a deal and the sale price is $400,000 with a 3% commission. The gross commission is $12,000.
                                                                        <br><br>
                                                                        Level 1 $0-$250,000 - 50% agent's percentage Level 2 $250,000-$500,000 - 60% agent's percentage
                                                                        <br><br>
                                                                        We will calculate the commission using the entire first level of $250,000 at 50%. $250,000 * 3% = $7,500 - we then apply the split for this level of 50% which gives the agent $3,750.
                                                                        <br><br>
                                                                        We then take the remainder of the sales price, $150,000 * 3% = $4,500 and apply the split of 60% which gives the agent $2,700 for a net total to the agent of $6,450.
                                                                        <br><br>
                                                                        If you don't choose pro-rate levels, in this same scenario the entire commission will be calculated based on 60% because the $400,000 sales price falls in level 2 giving the agent a net commission of $7,200.
                                                                        <br><br>
                                                                        The same logic is applied if you choose to base your levels on Gross Commission.
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="accordion-item">
                                                                <h2 class="accordion-header" id="closed-transaction">
                                                                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                                                        Closed Transactions
                                                                    </button>
                                                                </h2>
                                                                <div id="collapseThree" class="accordion-collapse collapse" aria-labelledby="closed-transaction" data-bs-parent="#accordionExample">
                                                                    <div class="accordion-body">
                                                                        Select the Closed Transaction option from the question 'Type of Plan'. In the Level section Add the Start and End values. For example, if the first 3 transaction are at a 50/50 split, you'll enter 1 in the Start and 3 in the End fields and enter 50 in the split % field. Then save the level and move onto the next level. This will split the first 3 closed transactions at 50% and then they will move the level 2 of your plan.
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="accordion-item">
                                                                <h2 class="accordion-header" id="cap-plans">
                                                                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseFour" aria-expanded="false" aria-controls="collapseThree">
                                                                        Cap Plans
                                                                    </button>
                                                                </h2>
                                                                <div id="collapseFour" class="accordion-collapse collapse" aria-labelledby="cap-plans" data-bs-parent="#accordionExample">
                                                                    <div class="accordion-body">
                                                                        A commission plan with a cap is a plan that requires the agent to pay the brokerage a set amount of their commission dollars until they reach that cap limit. Once they've 'capped' they will move to a higher split with the brokerage on their future deals.
                                                                        <br><br>
                                                                        To set up a commission plan with a cap start by going to Settings > Commission Plans on the side menu. Click the Add Plan button.
                                                                        <br><br>
                                                                        1. Give your plan a name.
                                                                        <br><br>
                                                                        2. Select whether this plan is active or not. You can turn old plans off but leave them in the system by selecting Inactive.
                                                                        <br><br>
                                                                        3. Select Cap from the Type of Plan drop down.
                                                                        <br><br>
                                                                        4. Select a rollover date for this plan. The rollover date is when this plan will reset and the agent will start over from the beginning.
                                                                        If you choose Start date, the plan will reset on the anniversary of the agent's start date (the start date for each agent can be entered into their Agent Profile page).
                                                                        If you choose Custom Date, you can select a date of your choosing and the plan will rollover one year from that date.
                                                                        If you choose Calendar Year, the plan will always rollover on January 1st.
                                                                        And if you choose No Roll Over Date, the plan will never reset.
                                                                        <br><br>
                                                                        5. Next choose whether to apply a fee towards the commission plan cap?
                                                                        This allows you to add a fee in the levels section (further down the page), that will count towards the agent's commission plan cap. So if you have a transaction fee of $500 on each deal, that fee will be deducted from the agent's commission after their split with the broker, and also counted towards their cap.
                                                                        <br><br>
                                                                        6. Next, do you have a cap on transaction fees? This is a different cap then the commission plan cap. This is a fee that is also deducted after the agent's split, but has it's own cap, separate from the commission plan cap. For example, if you charge 1% of the sales price but only until the agent has paid $1,000, you can set that up by adding the 1% fee based on the sales price, and select Yes to Cap on Fees, and then enter $1,000 in the Cap Amount field. The fees will be added later when we enter the levels for the commission plan. This is not where you'll factor in the cap from the commission split with the broker, this is only to track a cap on additional fees you deduct from an agent's commission.
                                                                        <br><br>
                                                                        7. You can add a pre-commission credit/debit to the plan. If you charge a franchise fee off the top of each deal you can enter that fee:
                                                                        Learn more about pre-commission credit/debits by clicking here.
                                                                        <br><br>
                                                                        8. Next we'll set up each level of our commission plan. If your cap is $40,000, then you'll enter $0-$40,000 for level 1. Using the example below, that will split the agent's commission at 70% to the agent, 30% to the broker, and count that 30% towards the $40,000 cap. Once the agent has paid $40,000 to the broker they'll move to a 100% split.
                                                                        If you want to charge a fee for each level of your plan you can enter that info into the Closing Fee Item and Amount fields. This feature allows you to adjust your fee based on the level of the commission plan. You can choose a dollar amount or a % of the gross commission, agent's commission, or sales price.
                                                                        The cap is now automatically set so once the agent has paid $40,000 in company dollar at a 70%/30% split, the agent would have capped and moved to a 100% split.
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="accordion-item">
                                                                <h2 class="accordion-header" id="sale-price">
                                                                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseFive" aria-expanded="false" aria-controls="collapseThree">
                                                                        Sale Price
                                                                    </button>
                                                                </h2>
                                                                <div id="collapseFive" class="accordion-collapse collapse" aria-labelledby="sale-price" data-bs-parent="#accordionExample">
                                                                    <div class="accordion-body">
                                                                        Select the Sale Price option from the question 'Type of Plan'. In the Level section Add the Sales Price range. For example, if a sales price of $0-$150,000 is a 75% split, enter 0 in the Start field and 150,000 in the End fields and enter 75 in the split % field. Then save the level and move onto the next level. This will split the commission at 75% when the sales price is between $0 and $150,000. This plan is not a cumulative plan, it will look at the level the sales price falls in for each transaction and apply that split only for that specific transaction.
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="Pre-Commission-Credit-debit hide">
                                                <div class="col-md-12 col-lg-12 col-xl">
                                                    <div class="card">
                                                        <div class="card-header">
                                                            <h4>Pre-commission credit/debit</h4>
                                                        </div>
                                                        <div class="card-body">
                                                            <div>
                                                                <div class="pull-right">
                                                                    <button type="button" class="btn btn-secondary" onclick="showTableData()">+ Add Credit/Debit</button>
                                                                </div>
                                                            </div>
                                                            <div class="clearfix mb-5"></div>
                                                            <table class="table table-bordered table-responsive" id="pre-commission-table">
                                                                <thead>
                                                                <tr>
                                                                    <th>Items</th>
                                                                    <th>Fee</th>
                                                                    <th>Commission</th>
                                                                    <th>Cap</th>
                                                                    <th>Action</th>
                                                                </tr>
                                                                </thead>
                                                                <tbody id="tableData" class="pre_commission_table_body" style="display:none;">
                                                                <tr class="first-row">
                                                                    <td>
                                                                        <div class="form-group-row">
                                                                            <input type="text" name="item_name" placeholder="Enter Item Name" class="form-control input-group">
                                                                            <select class="form-select item-commission" name="debit_credit">
                                                                                <option>Credit</option>
                                                                                <option>Debit</option>
                                                                            </select>
                                                                        </div>
                                                                    </td>
                                                                    <td>
                                                                        <div class="form-group-row pre-commission-fee">
                                                                            <div class="form-group-row">
                                                                                <label>%</label>
                                                                                <input type="number" name="fee_percentage" placeholder="Percentage" style="margin-top: 4px;" class="form-control input-group Percentage">
                                                                            </div>
                                                                            <div class="form-group-row">
                                                                                <label>$</label>
                                                                                <input type="number" name="flat_fee" placeholder="Flat Fee" style="margin-top: 4px;" class="form-control input-group flat-fee">
                                                                            </div>
                                                                        </div>
                                                                    </td>
                                                                    <td>
                                                                        <select class="form-select" name="commission_name">
                                                                            <option>Brokerage Gross Commission</option>
                                                                            <option>Agent Commission</option>
                                                                            <option>Sales Price</option>
                                                                            <option>Agent Commission After Closing Fees</option>
                                                                            <option>Brokerage Commission After Agent Split</option>
                                                                        </select>
                                                                    </td>
                                                                    <td>
                                                                        <div class="input_container select_container">
                                                                            <select class="form-select cap-commission-select" name="cap">
                                                                                <option>Yes</option>
                                                                                <option>No</option>
                                                                            </select>
                                                                        </div>
                                                                    </td>
                                                                    <td>
                                                                        <div class="form-group-row">
                                                                            <a onclick="hideTableData()" style="color: #3c8dbc; text-decoration: none; margin-left: 10px; cursor: pointer; font-size: 16px;"><i class="fas fa-times"></i></a>
                                                                            <a style="color: #7a9d45; text-decoration: none; margin-left: 10px; cursor: pointer; font-size: 16px;"><i class="fas fa-check"></i></a>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                                <tr class="second-row">
                                                                    <td colspan="5" >
                                                                        <div class="row">
                                                                            <div class="col-lg-3 col-md-3">
                                                                                <select class="form-select vendor-agent-select" name="vendor_agent">
                                                                                    <option>Vendor</option>
                                                                                    <option>Agent</option>
                                                                                </select>
                                                                            </div>
                                                                            <div class="col-lg-3 col-md-3">
                                                                                <input type="text" name="vendor_name" placeholder="Enter Vendor Name" class="form-control input-group enter-vendor-name">
                                                                            </div>
                                                                            <div class="col-lg-3 col-md-3 added-agent-name" style="display: none;">
                                                                                <select class="form-select" name="agent_name">
                                                                                    @forelse($agent as $agents)
                                                                                        <option>{{ $agents->first_name }} {{ $agents->last_name }}</option>
                                                                                    @empty
                                                                                        <option selected disabled>No result match</option>
                                                                                    @endforelse
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                                <tr class="third-row">
                                                                    <td colspan="5">
                                                                        <div class="row">
                                                                            <div class="col-lg-6 col-md-6 form-group-row">
                                                                                <label>Calculate agent's commission before or after this item?</label>
                                                                                <select class="form-select" name="calculate_commission">
                                                                                    <option>Before</option>
                                                                                    <option>After</option>
                                                                                </select>
                                                                            </div>
                                                                            <div class="col-lg-6 col-md-6 form-group-row include-this-credit">
                                                                                <label>Include this credit/debit in the Total Due to Brokerage?</label>
                                                                                <select class="form-select" name="include_total">
                                                                                    <option>No</option>
                                                                                    <option>Yes</option>
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="container-fluid hide Levels">
                                                <div class="row">
                                                    <div class="col-md-12 col-lg-12 col-xl">
                                                        <div class="card">
                                                            <div class="card-header">
                                                                <h4 class="hide Cls">Cap Levels</h4>
                                                                <h4 class="hide Spls">Sales Price Levels</h4>
                                                                <h4 class="hide Agcl">Adjusted Gross Commission Levels</h4>
                                                                <h4 class="hide Gcpdl">Gross Commission Per Deal Levels</h4>
                                                            </div>
                                                            <div class="card-body">
                                                                <div>
                                                                    <div class="pull-right">
                                                                        <button type="button" class="btn btn-secondary" data-bs-toggle="modal" data-bs-target="#staticBackdrop">+ Add Level</button>
                                                                    </div>
                                                                </div>
                                                                <div class="clearfix mb-5"></div>
                                                                <div class="table-responsive">
                                                                    <table class="table table-sm" id="myTable">
                                                                        <thead>
                                                                        <tr>
                                                                            <th scope="col">
                                                                                <a href="#" class="text-muted list-sort">From</a>
                                                                            </th>
                                                                            <th scope="col">
                                                                                <a href="#" class="text-muted list-sort">To</a>
                                                                            </th>
                                                                            <th scope="col">
                                                                                <a href="#" class="text-muted list-sort">Agent Percentage %</a>
                                                                            </th>
                                                                            <th scope="col">
                                                                                <a href="#" class="text-muted list-sort">Auto Switch Plans</a>
                                                                            </th>
                                                                            <th scope="col">
                                                                                <a href="#" class="text-muted list-sort">Additional Closing Fee</a>
                                                                            </th>
                                                                            <th scope="col">
                                                                                <a href="#" class="text-muted list-sort">Action</a>
                                                                            </th>
                                                                        </tr>
                                                                        </thead>
                                                                        <tbody id="gross_level" class="list">
                                                                        <tr>
                                                                            <td></td>
                                                                            <td></td>
                                                                            <td></td>
                                                                            <td></td>
                                                                            <td></td>
                                                                            <td></td>
                                                                        </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- Adjusted Gross Commission Level Modal -->
                                            <div class="modal fade" id="staticBackdrop" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title" id="staticBackdropLabel">Add Level</h5>
                                                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                        </div>
                                                        <form>
                                                            <div class="modal-body adjusted-gross-level">
                                                                <div class="col-md-12 row">
                                                                    <div class="col-lg-6 col-md-6">
                                                                        <label>From:</label>
                                                                        <input type="number" name="adjusted_gross_from" class="form-control from">
                                                                    </div>
                                                                    <div class="col-lg-6 col-md-6">
                                                                        <label>To:</label>
                                                                        <input type="number" name="adjusted_gross_to" class="form-control to">
                                                                    </div>
                                                                </div>
                                                                <br>
                                                                <div class="col-md-12 row">
                                                                    <div class="col-lg-6 col-md-6 mt-3">
                                                                        <label>Agent Percentage %:</label>
                                                                        <input type="number" name="adjusted_gross_percentage" step="any" class="form-control adjusted-gross-percentage">
                                                                    </div>
                                                                    <div class="col-lg-6 col-md-6 mt-3">
                                                                        <label>Auto Switch Plans:</label>
                                                                        <select class="form-control form-select auto-switch-plan" name="adjusted_gross_plans">
                                                                            @forelse($data as $datas)
                                                                                <option>{{ $datas->plan_name }}</option>
                                                                            @empty
                                                                                <option selected disabled value="N/A">No Result Match</option>
                                                                            @endforelse
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                                                                <button type="button" class="btn btn-primary save-level-data">Save Level</button>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>

                                            <!-- Adjusted Gross Commission Edit Level Modal -->
                                            <div class="modal fade" id="editLevel" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title">Edit Level</h5>
                                                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                        </div>
                                                        <form>
                                                            <div class="modal-body adjusted-gross-level">
                                                                <div class="col-md-12 row">
                                                                    <input type="text" class="editId" style="display: none;">
                                                                    <div class="col-lg-6 col-md-6">
                                                                        <label>From:</label>
                                                                        <input type="number" class="form-control editFrom">
                                                                    </div>
                                                                    <div class="col-lg-6 col-md-6">
                                                                        <label>To:</label>
                                                                        <input type="number" class="form-control editTo">
                                                                    </div>
                                                                </div>
                                                                <br>
                                                                <div class="col-md-12 row">
                                                                    <div class="col-lg-6 col-md-6 mt-3">
                                                                        <label>Agent Percentage %:</label>
                                                                        <input type="number" step="any" class="form-control editPercentage">
                                                                    </div>
                                                                    <div class="col-lg-6 col-md-6 mt-3">
                                                                        <label>Auto Switch Plans:</label>
                                                                        <select class="form-control form-select editPlan" name="adjusted_gross_plans">
                                                                            @forelse($data as $datas)
                                                                                <option>{{ $datas->plan_name }}</option>
                                                                            @empty
                                                                                <option selected disabled value="N/A">No Result Match</option>
                                                                            @endforelse
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                                                                <button type="button" class="btn btn-primary updateDataBtn">Save Level</button>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>

                                            <!-- Closed Transaction Add Level Modal -->
                                            <div class="modal fade addClosedTransaction" id="closedTransactionAddLevel" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title" id="staticBackdropLabel">Add Level</h5>
                                                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                        </div>
                                                        <form>
                                                            <div class="modal-body adjusted-gross-level">
                                                                <div class="col-md-12 row">
                                                                    <div class="col-lg-6 col-md-6">
                                                                        <label>Start:</label>
                                                                        <input type="number" name="" class="form-control closedTransactionStart">
                                                                    </div>
                                                                    <div class="col-lg-6 col-md-6">
                                                                        <label>End:</label>
                                                                        <input type="number" name="" class="form-control closedTransactionEnd">
                                                                    </div>
                                                                </div>
                                                                <br>
                                                                <div class="col-md-12 row">
                                                                    <div class="col-lg-6 col-md-6 mt-3">
                                                                        <label>Agent Percentage %:</label>
                                                                        <input type="number" name="" step="any" class="form-control closedTransactionPercentage">
                                                                    </div>
                                                                    <div class="col-lg-6 col-md-6 mt-3">
                                                                        <label>Auto Switch Plans:</label>
                                                                        <select class="form-select closedTransactionPlans">
                                                                            @foreach($data as $datas)
                                                                                <option>{{ $datas->plan_name }}</option>
                                                                            @endforeach
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                                                                <button type="button" class="btn btn-primary saveTransactionLevel">Save Level</button>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>

                                            <!-- Closed Transaction Edit Level Modal -->
                                            <div class="modal fade editClosedTransaction" id="editTransactionLevel" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title" id="staticBackdropLabel">Edit Level</h5>
                                                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                        </div>
                                                        <form>
                                                            <div class="modal-body adjusted-gross-level">
                                                                <div class="col-md-12 row">
                                                                    <input type="text" class="transactionId" style="display: none;">
                                                                    <div class="col-lg-6 col-md-6">
                                                                        <label>Start:</label>
                                                                        <input type="number" name="" class="form-control editTransactionStart">
                                                                    </div>
                                                                    <div class="col-lg-6 col-md-6">
                                                                        <label>End:</label>
                                                                        <input type="number" name="" class="form-control editTransactionEnd">
                                                                    </div>
                                                                </div>
                                                                <br>
                                                                <div class="col-md-12 row">
                                                                    <div class="col-lg-6 col-md-6 mt-3">
                                                                        <label>Agent Percentage %:</label>
                                                                        <input type="number" name="" step="any" class="form-control editTransactionPercentage">
                                                                    </div>
                                                                    <div class="col-lg-6 col-md-6 mt-3">
                                                                        <label>Auto Switch Plans:</label>
                                                                        <select class="form-select editTransactionPlans">
                                                                            @foreach($data as $datas)
                                                                                <option>{{ $datas->plan_name }}</option>
                                                                            @endforeach
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                                                                <button type="button" class="btn btn-primary updateTransactionLevel">Update Level</button>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="container-fluid hide Clnoct">
                                                <div class="row">
                                                    <div class="col-md-12 col-lg-12 col-xl">
                                                        <div class="card">
                                                            <div class="card-header">
                                                                <h4>Commission Levels Number of Closed Transactions</h4>
                                                            </div>
                                                            <div class="card-body">
                                                                <div>
                                                                    <div class="pull-right">
                                                                        <button type="button" class="btn btn-secondary" data-bs-toggle="modal" data-bs-target="#closedTransactionAddLevel">+ Add Level</button>
                                                                    </div>
                                                                </div>
                                                                <div class="clearfix mb-5"></div>
                                                                <div class="table-responsive">
                                                                    <table class="table table-sm closedTranasctionTable">
                                                                        <thead>
                                                                        <tr>
                                                                            <th scope="col">
                                                                                <a href="#" class="text-muted list-sort">Start</a>
                                                                            </th>
                                                                            <th scope="col">
                                                                                <a href="#" class="text-muted list-sort">End</a>
                                                                            </th>
                                                                            <th scope="col">
                                                                                <a href="#" class="text-muted list-sort">Split %</a>
                                                                            </th>
                                                                            <th scope="col">
                                                                                <a href="#" class="text-muted list-sort">Auto Switch Plans</a>
                                                                            </th>
                                                                            <th scope="col">
                                                                                <a href="#" class="text-muted list-sort">Additional Closing Fee</a>
                                                                            </th>
                                                                            <th scope="col">
                                                                                <a href="#" class="text-muted list-sort">Action</a>
                                                                            </th>
                                                                        </tr>
                                                                        </thead>
                                                                        <tbody class="list transactionTableBody">
                                                                        <tr>
                                                                            <td></td>
                                                                            <td></td>
                                                                            <td></td>
                                                                            <td></td>
                                                                            <td></td>
                                                                            <td></td>
                                                                        </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="container-fluid hide Levels-Fees">
                                                <div class="row">
                                                    <div class="col-md-12 col-lg-12 col-xl">
                                                        <div class="card">
                                                            <div class="card-header">
                                                                <h4>Commission Levels Fees</h4>
                                                            </div>
                                                            <div class="card-body">
                                                                <div class="col-md-12 row">
                                                                    <div class="col-md-8">
                                                                        <div class="form-group form-group-row Afincp" style="display: none">
                                                                            <label class="w-25">Apply fees in commission plan levels towards the commission plan cap?</label>
                                                                            <div class="input_container select_container mt-3 w-75">
                                                                                <select class="form-select" data-choices>
                                                                                    <option>No</option>
                                                                                    <option>Yes</option>
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group form-group-row">
                                                                            <label class="mt-2 w-25">Cap on Fees?</label>
                                                                            <div class="input_container w-75 select_container">
                                                                                <select class="form-select mb-3 cap-on-fees-2 cap-select" data-choices>
                                                                                    <option>No</option>
                                                                                    <option>Yes</option>
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group form-group-row CA2" style="display:none;">
                                                                            <label class="w-25 mt-2">Cap Amount:</label>
                                                                            <div class="input_container w-75">
                                                                                <input type="text" class="form-control input-group">
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group form-group-row ROD2" style="display: none">
                                                                            <label class="w-25 mt-2">Roll Over Date:</label>
                                                                            <div class="input_container select_container w-75">
                                                                                <select class="form-select roll-over-date2" data-choices>
                                                                                    <option>Select an Option</option>
                                                                                    <option>Start Date</option>
                                                                                    <option>Custom Date</option>
                                                                                    <option>Calendar year</option>
                                                                                    <option>No Role Over Date</option>
                                                                                    <option>Monthly</option>
                                                                                    <option>Rolling Rollover</option>
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="card-footer text-right">
                                                <button type="button" onclick="showCommissionPlan()" class="btn btn-default">
                                                    Cancel
                                                </button>
                                                <button type="submit" class="btn btn-primary">
                                                    Save Commission Plan
                                                </button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <div class="tab-pane" id="tab2">
                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                        <h4 style="margin-bottom: 20px; margin-top: 25px; font-size: 18px; font-weight: 500; line-height: 1.1; color: inherit;">Agent(s) Assigned to Plan</h4>
                                        <div class="table-header mt-5">
                                            <div class="col-md-6">
                                                <div class="input-group">
                                                    <div class="form-outline">
                                                        <input type="search" id="form1" placeholder="search.." class="form-control" />
                                                    </div>
                                                    <button type="button" class="btn btn-primary">
                                                        <i class="fas fa-search"></i>
                                                    </button>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="filter-btn pull-right">
                                                    <button class="btn btn-secondary">+ Add Agents to Plan</button>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="table-responsive">
                                            <table class="table table-sm" id="dtBasicExample">
                                                <thead>
                                                <tr>
                                                    <th scope="col">
                                                        <a href="#" class="text-muted list-sort">#</a>
                                                    </th>
                                                    <th scope="col">
                                                        <a href="#" class="text-muted list-sort">Agent Name</a>
                                                    </th>
                                                    <th scope="col">
                                                        <a href="#" class="text-muted list-sort">Agent Location</a>
                                                    </th>
                                                    <th scope="col">
                                                        <a href="#" class="text-muted list-sort">Agent Status</a>
                                                    </th>
                                                </tr>
                                                </thead>
                                                <tbody class="list">
                                                <tr>
                                                    <td>1</td>
                                                    <td>Test</td>
                                                    <td>Test</td>
                                                    <td>500 $</td>
                                                </tr>
                                                </tbody>
                                            </table>
                                            <ul class="pagination justify-content-end">
                                                <li class="page-item disabled"><a class="page-link" href="#">Previous</a></li>
                                                <li class="page-item"><a class="page-link" href="#">1</a></li>
                                                <li class="page-item"><a class="page-link" href="#">2</a></li>
                                                <li class="page-item"><a class="page-link" href="#">3</a></li>
                                                <li class="page-item"><a class="page-link" href="#">Next</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- / .row -->
        </div>
    </div>

    <script>
        function editCommissionPlan(id){
            var plan_id = id;
            var request = $.ajax({
                url: "/user/edit-commission-plan/" +id,
                method: "GET",
                data: {
                    plan_id: plan_id,
                },
                dataType: "html",
                success: function(res){
                    var res = JSON.parse(res);
                    var status = res.status;
                    var data = res.data;
                    console.log(data);
                    console.log(data.plan_name);
                    console.log(data.plan_status);
                    var Span_Plan_Name = $('.spanPlanName').append(data.plan_name);
                    var Plan_Name = $('.planName').val(data.plan_name);
                    if (status == true){
                        $('#commission-plan').hide();
                        $('#add-plan').show();
                    }
                }
            });
        }

        $('body').on('change', '.type-of-plan', function ()
        {
            let a = $(this).val();
            if (a == "Flat Fee"){
                $('.Eap').show();
                $('.Cof').show();
                $('.Pccd').show();
                $('.Levels').hide();
                $('.Gcpdl').hide();
                $('.Levels-Fees').hide();
                $('.Spls').hide();
                $('.Rod').hide();
                $('.Scp').hide();
                $('.Cls').hide();
                $('.Afincp').hide();
                $('.Clnoct').hide();
                $('.Clbo').hide();
                $('.Prl').hide();
                $('.Agcl').hide();

            } else if(a == "Sliding Scale"){
                $('.Rod').show();
                $('.Clbo').show();
                $('.Prl').show();
                $('.Pccd').show();
                $('.Levels').show();
                $('.Agcl').show();
                $('.Levels-Fees').show();
                $('.Eap').hide();
                $('.Cof').hide();
                $('.Ca').hide();

            } else if (a == "Close Transaction"){
                $('.Rod').show();
                $('.Pccd').show();
                $('.Clnoct').show();
                $('.Levels-Fees').show();
                $('#closedTransactionAddLevel').modal('show');
                $('.Clbo').hide();
                $('.Prl').hide();
                $('.Levels').hide();
                $('.Agcl').hide();
                $('.Eap').hide();
                $('.Pre-Commission-Credit-debit').hide();
                $('.Cof').hide();
            } else if(a == "Cap"){
                $('.Rod').show();
                $('.Scp').show();
                $('.Pccd').show();
                $('.Levels').show();
                $('.Cls').show();
                $('.Levels-Fees').show();
                $('.Afincp').show();
                $('.Clnoct').hide();
                $('.Clbo').hide();
                $('.Prl').hide();
                $('.Agcl').hide();
                $('.Eap').hide();
                $('.Cof').hide();

            } else if (a == "Sale Price"){
                $('.Pccd').show();
                $('.Levels').show();
                $('.Spls').show();
                $('.Levels-Fees').show();
                $('.Rod').hide();
                $('.Scp').hide();
                $('.Cls').hide();
                $('.Afincp').hide();
                $('.Clnoct').hide();
                $('.Clbo').hide();
                $('.Prl').hide();
                $('.Agcl').hide();
                $('.Eap').hide();
                $('.Cof').hide();

            } else if (a == "Gross Commission Per Deal"){
                $('.Pccd').show();
                $('.Levels').show();
                $('.Gcpdl').show();
                $('.Levels-Fees').show();
                $('.Spls').hide();
                $('.Rod').hide();
                $('.Scp').hide();
                $('.Cls').hide();
                $('.Afincp').hide();
                $('.Clnoct').hide();
                $('.Clbo').hide();
                $('.Prl').hide();
                $('.Agcl').hide();
                $('.Eap').hide();
                $('.Cof').hide();
            }
        });

        $('body').on('change', '.cap-on-fees', function () {
            let b = $(this).val();
            if (b == "Yes"){
                $('.Ca').show();
                $('.Rod').show();
            } else if (b == "No"){
                $('.Ca').hide();
                $('.Rod').hide();
            }
        });

        $('body').on('change', '.roll-over-date', function () {
            let c = $(this).val();
            if (c == "Custom Date"){
                $('.Cd').show();
            }
        });

        $('body').on('change', '.add-pre-commission-credit', function () {
            let d = $(this).val();
            if (d == "Yes"){
                $('.Copccdf').show();
                $('.Pre-Commission-Credit-debit').show();
            } else if (d == "No"){
                $('.Copccdf').hide();
                $('.Pre-Commission-Credit-debit').hide();
                $('.Pccdca').hide();
            }
        });

        $('body').on('change', '.cap-on-fees-2', function () {
            let e = $(this).val();
            if (e == "Yes"){
                $('.CA2').show();
                $('.ROD2').show();
            } else if (e == "No"){
                $('.CA2').hide();
                $('.ROD2').hide();
            }
        });

        $('body').on('change', '.cap-on-pre-cred-fees', function (){
            let f = $(this).val();
            if (f == "Yes"){
                $('.Pccdca').show();
            } else  if (f == "No"){
                $('.Pccdca').hide();
            }
        });

        $('body').on('change', '.item-commission', function (){
            var row1 = $('.first-row');
            var row2 = $('.second-row');
            var row3 = $('.third-row');
            var pre_commission_fee = row1.find('.pre-commission-fee')
            var percentage = row1.find('.Percentage');
            var flat_fee = row1.find('flat-fee');
            var item_commission = row1.find('.item-commission');
            var cap_select  = row1.find('.cap-commission-select');
            var include_this_credit = row3.find('.include-this-credit');
            var enter_vendor_name = row2.find('.enter-vendor-name');
            var added_agent_name = row2.find('.added-agent-name');
            if ($(this).val() == "Credit"){
                cap_select.val('No');
                cap_select.attr('disabled', 'disabled');
                include_this_credit.hide();
            } else if ($(this).val() == "Debit"){
                cap_select.val('Yes');
                cap_select.attr('disabled', false);
                include_this_credit.show();
            }

            $('body').on('change', '.vendor-agent-select', function (){
                if ($(this).val() == "Agent"){
                    item_commission.val('Debit');
                    item_commission.attr('disabled', 'disabled');
                    include_this_credit.hide();
                    enter_vendor_name.css("display", "none");
                    added_agent_name.show();
                } else if ($(this).val() == "Vendor"){
                    item_commission.val('Credit');
                    item_commission.attr('disabled', false);
                    include_this_credit.show();
                    enter_vendor_name.show();
                    added_agent_name.hide();
                }
            });

            $('body').on('change', '.Percentage', function (){
                if ($(this).val().length > 0){
                    $('.flat-fee').val('0').attr('disabled', 'disabled').css('background-color', '#EDF2F9');
                } else if ($(this).val().length <= 0){
                    $('.flat-fee').attr('disabled', false).css('background-color', '#fff');
                }
            })

            $('body').on('change', '.flat-fee', function (){
                if ($(this).val().length > 0){
                    $('.Percentage').val('0').attr('disabled', 'disabled').css('background-color', '#EDF2F9');
                } else if ($(this).val().length <= 0){
                    $('.Percentage').attr('disabled', false).css('background-color', '#fff');
                }
            })

        });

        $('body').on('click', '.save-level-data', function (){

            var from = parseInt($('.from').val());
            var from2 = parseFloat(from).toFixed(2);
            var to = parseInt($('.to').val());
            var to2 = parseFloat(to).toFixed(2);
            var adjusted_gross_percentage = $('.adjusted-gross-percentage').val();
            var auto_switch_plan = $('.auto-switch-plan').val();

            if (to < from){
                alert('To amount should be greater than from amount');
                return;
            }
            var rowId = Math.floor(Math.random() * 10);
            var html = `<tr class="tblRow" data-rowId="${rowId}">
                        <td class="gross_from"><input class="tdInput" name="adjusted_gross_from" readonly value="${from2}"></td>
                        <td class="gross_to"><input class="tdInput" name="adjusted_gross_to" readonly value="${to2}"></td>
                        <td class="gross_percentage"><input class="tdInput" name="adjusted_gross_percentage" readonly value="${adjusted_gross_percentage}"></td>
                        <td class="gross_plan"><input class="tdInput" name="adjusted_gross_plans" readonly value="${auto_switch_plan}"></td>
                        <td> <a href="" data-bs-toggle="modal" data-bs-target="#viewEditClosingFee1"><i class="far fa-edit"></i> View and Edit </a> </td>
                        <td>
                            <a href="" id="deleteBtn"><i class="far fa-trash-alt"></i></a>
                            <a href="" class="editBtn" data-bs-toggle="modal" data-bs-target="#editLevel" style="margin-left: 5px"><i class="far fa-edit"></i></a>
                        </td>
                    </tr>`;
            $('#gross_level').append(html);

            $('#myTable').on('click', '#deleteBtn', function (e){
                e.preventDefault();
                $(this).closest("tr").remove();
            });

            $('.from').val("");
            $('.to').val("");
            $('.adjusted-gross-percentage').val("");
            $('.auto-switch-plan').val("");

            $('.from').val(to2).css("background-color", "#EEEEEE").attr('disabled', 'disabled');

        });

        $('#myTable').on('click', '.editBtn', function (){
            var row = $(this).parents('.tblRow');
            var rowid = row.attr('data-rowId');

            var from_td = row.find('.gross_from').text();
            var to_td = row.find('.gross_to').text();
            var percentage_td = row.find('.gross_percentage').text();
            var plan_td = row.find('.gross_plan').text();

            var modal = $('#editLevel');
            modal.find('.editId').val(rowid);
            modal.find('.editFrom').val(parseInt(from_td));
            modal.find('.editTo').val(parseInt(to_td));
            modal.find('.editPercentage').val(parseInt(percentage_td));
            modal.find('.editPlan').val(parseInt(plan_td));

        });

        $('#editLevel').on('click', '.updateDataBtn', function (){
            var modal = $('#editLevel');

            var id_input = modal.find('.editId').val();
            var from_input = modal.find('.editFrom').val();
            var to_input = modal.find('.editTo').val();
            var percentage_input = modal.find('.editPercentage').val();
            var plan_input = modal.find('.editPlan').val();

            if (to_input < from_input){
                alert('To amount should be greater than from amount');
                return;
            }

            var row = $(`.tblRow[data-rowId=${id_input}]`);
            var update_td_from = row.find('.gross_from').text(from_input);
            var update_td_to = row.find('.gross_to').text(to_input);
            var update_td_percentage = row.find('.gross_percentage').text(percentage_input);
            var update_td_plan = row.find('.gross_plan').text(plan_input);

            $('#editLevel').modal('hide');

        });

        $('body').on('change', '.closing-flat-fee', function (){
            if ($(this).val().length>0){
                $('.closing-fee-percentage').val('0').attr('disabled', 'disabled').css('background-color', '#EDF2F9');
            } else if($(this).val().length<=0){
                $('.closing-fee-percentage').attr('disabled', false).css('background-color', '#fff');
            }
        });

        $('body').on('change', '.closing-fee-percentage', function (){
            if ($(this).val().length>0){
                $('.closing-flat-fee').val('0').attr('disabled', 'disabled').css('background-color', '#EDF2F9');
                $('.closingFeeBasedOn').show();
            } else if($(this).val().length<=0){
                $('.closing-flat-fee').attr('disabled', false).css('background-color', '#fff');
                $('.closingFeeBasedOn').hide();
            }
        });

        $('body').on('click', '.include-cap-fee', function (){
            if ($(this).is(':checked')){
                $(this).val('Yes');
            } else {
                $(this).val('No');
            }
        });

        $('body').on('click', '#cancelAddClosingFee', function (){
            $('.closingFeeItem').val("").css('border-color','#D2DDEC');
            $('.closingFlatFee').val("");
            $('.closingFeePercentage').val("");
            $('.basedOn').val("");
            $('.includeCapFee').val("");

            $('.closing-fee-percentage').attr('disabled', false).css('background-color', '#fff');
            $('.closing-flat-fee').attr('disabled', false).css('background-color', '#fff');
            $('.closingFeeBasedOn').hide();
        });

        $('body').on('click', '.saveClosingFee', function (){
            var modal = $('.addAdditionalClosingFee');
            var closing_fee_item = modal.find('.closingFeeItem').val();
            var closing_flat_fee = parseInt(modal.find('.closingFlatFee').val());
            var closing_fee_percentage = parseInt(modal.find('.closingFeePercentage').val());
            var closing_fee_based_on = modal.find('.basedOn').val();
            var include_cap_fee = modal.find('.includeCapFee').val();

            if(closing_fee_item == "" || closing_fee_item == null){
                $('.closingFeeItem').css('border-color','red');
                return;
            }

            var row_id = Math.floor(Math.random() * 10);

            console.log(row_id)

            var tbody = `<tr class="closingFeeRowId" row-id="${row_id}">
                            <td class="fee_Item"><input class="tdInput" name="closing_fee_item" readonly value="${closing_fee_item}"></td>
                            <td class="flat_Fee"><input class="tdInput" name="closing_flat_fee" readonly value="${closing_flat_fee}"></td>
                            <td class="fee_Percentage"><input class="tdInput" name="closing_fee_percentage" readonly value="${closing_fee_percentage}"></td>
                            <td class="fee_based_on"><input class="tdInput" name="fee_based_on" readonly value="${closing_fee_based_on}"></td>
                            <td class="include_cap_fee"><input class="tdInput" name="include_fees" readonly value="${include_cap_fee}"></td>
                            <td>
                                <a href="" id="closingFeeDelBtn"><i class="far fa-trash-alt"></i></a>
                                <a href="" class="closingFeeEdit" data-bs-toggle="modal" data-bs-target="#editClosingFeeModal" style="margin-left: 5px"><i class="far fa-edit"></i></a>
                            </td>
                        </tr>`;
            $('#additional_closing_fee').append(tbody);

            $('.closingFeeItem').val("").css('border-color','#D2DDEC');
            $('.closingFlatFee').val("");
            $('.closingFeePercentage').val("");
            $('.basedOn').val("");
            $('.includeCapFee').val("");

            $('.closing-fee-percentage').attr('disabled', false).css('background-color', '#fff');
            $('.closing-flat-fee').attr('disabled', false).css('background-color', '#fff');
            $('.closingFeeBasedOn').hide();

            $('.addAdditionalClosingFee').modal('hide');
            $('.additionalClosingFee').modal('show');

            $('#additionalClosingFeeTable').on('click', '#closingFeeDelBtn', function (e){
                e.preventDefault();
                $(this).closest("tr").remove();
            });
        });
        $('#additional_closing_fee').on('click', '.closingFeeEdit', function (){
            $('#viewEditClosingFee1').modal('hide');

            var row = $(this).parents('.closingFeeRowId');

            var row_id = row.attr('row-id');
            var edit_fee_item = row.find('.fee_Item').text();
            var edit_flat_fee = row.find('.flat_Fee').text();
            var edit_fee_percentage = row.find('.fee_Percentage').text();
            var edit_fee_based_on = row.find('.fee_based_on').text();
            var edit_include_fee = row.find('.include_cap_fee').text();

            console.log(row_id);

            var editModal = $('#editClosingFeeModal')
            editModal.find('.editClosingFeeId').val(row_id);
            editModal.find('.editfeeitem').val(edit_fee_item);
            editModal.find('.editflatfee').val(parseInt(edit_flat_fee));
            editModal.find('.editfeepercentage').val(parseInt(edit_fee_percentage));
            editModal.find('.editfeebasedon').val(edit_fee_based_on);
            editModal.find('.editincludefee').val(edit_include_fee);

        });
        $('#editClosingFeeModal').on('click', '.closingFeeUpdate', function (){
            var updateModal = $('#editClosingFeeModal');

            var update_row_id = updateModal.find('.editClosingFeeId').val();
            var update_fee_item = updateModal.find('.editfeeitem').val();
            var update_flat_fee = updateModal.find('.editflatfee').val();
            var update_fee_percentage = updateModal.find('.editfeepercentage').val();
            var update_fee_based_on = updateModal.find('.editfeebasedon').val();
            var update_include_fee = updateModal.find('.editincludefee').val();

            console.log(update_row_id);
            console.log(update_fee_item);
            console.log(update_flat_fee);
            console.log(update_fee_percentage);
            console.log(update_fee_based_on);
            console.log(update_include_fee);

            var updateRowId = $(`.closingFeeRowId[row-id=${update_row_id}]`)

            updateRowId.find('.fee_Item').text(update_fee_item);
            updateRowId.find('.flat_Fee').text(parseInt(update_flat_fee));
            updateRowId.find('.fee_Percentage').text(parseInt(update_fee_percentage));
            updateRowId.find('.fee_based_on').text(update_fee_based_on);
            updateRowId.find('.include_cap_fee').text(update_include_fee);

            $('#editClosingFeeModal').modal('hide');
            $('#viewEditClosingFee1').modal('show');
        });

        $('body').on('click', '.saveTransactionLevel', function (){
            var modal = $('.addClosedTransaction');
            var transaction_start = parseInt(modal.find('.closedTransactionStart').val());
            var transaction_end = parseInt(modal.find('.closedTransactionEnd').val());
            var transaction_percentage = parseInt(modal.find('.closedTransactionPercentage').val());
            var transaction_plans = modal.find('.closedTransactionPlans').val();

            if($('.closedTransactionStart').val() == ""){
                $('.closedTransactionStart').css('border-color','red');
                return;
            }else if ($('.closedTransactionEnd').val() == ""){
                $('.closedTransactionEnd').css('border-color','red');
                return;
            }else if (transaction_end < transaction_start){
                alert('End value should be equal or greater than start value');
                return;
            }

            var ID = Math.floor(Math.random() * 10);
            var tbody = `<tr class="table_row" row-id="${ID}">
                            <td class="trans_start"><input class="tdInput" name="close_transaction_start" readonly value="${transaction_start}"></td>
                            <td class="trans_end"><input class="tdInput" name="close_transaction_end" readonly value="${transaction_end}"></td>
                            <td class="trans_percentage"><input class="tdInput" name="close_transaction_percentage" readonly value="${transaction_percentage}"></td>
                            <td class="trans_plans"><input class="tdInput" name="close_transaction_plan" readonly value="${transaction_plans}">ss</td>
                            <td> <a href="" data-bs-toggle="modal" data-bs-target="#viewEditClosingFee1"><i class="far fa-edit"></i> View and Edit</a> </td>
                            <td>
                                <a href="" data-bs-toggle="modal" data-bs-target="#editTransactionLevel" ><i class="far fa-edit transactionEdit"></i></a>
                                <a href=""><i class="fas fa-trash-alt trnsactionDelBtn"></i></a>
                            </td>
                        </tr>`
            $('.transactionTableBody').append(tbody);

            $('.closedTransactionStart').val("").css('border-color','#D2DDEC');
            $('.closedTransactionEnd').val("").css('border-color','#D2DDEC');
            $('.closedTransactionPercentage').val("");
            $('.closedTransctionPlans').val("");

            $('.closedTransactionStart').val(transaction_end).css("background-color", "#EEEEEE").attr('disabled', 'disabled');

            $('.closedTranasctionTable').on('click', '.trnsactionDelBtn', function (e){
                e.preventDefault();
                $(this).closest("tr").remove();
            });
        });
        $('.closedTranasctionTable').on('click', '.transactionEdit', function (){
            var row = $(this).parents('.table_row');

            var row_id = row.attr('row-id');
            var start_trans = row.find('.trans_start').text();
            var end_trans = row.find('.trans_end').text();
            var percentage_trans = row.find('.trans_percentage').text();
            var plans_trans = row.find('.trans_plans').text();

            var modal = $('.editClosedTransaction');
            modal.find('.transactionId').val(row_id);
            modal.find('.editTransactionStart').val(parseInt(start_trans));
            modal.find('.editTransactionEnd').val(parseInt(end_trans));
            modal.find('.editTransactionPercentage').val(parseInt(percentage_trans));
            modal.find('.EditTransactionPlans').val(parseInt(plans_trans));
        })
        $('.editClosedTransaction').on('click', '.updateTransactionLevel', function (){
            var updateModal = $('.editClosedTransaction');

            var update_trans_id = updateModal.find('.transactionId').val();
            var update_trans_start = updateModal.find('.editTransactionStart').val();
            var update_trans_end = updateModal.find('.editTransactionEnd').val();
            var update_trans_percentage = updateModal.find('.editTransactionPercentage').val();
            var update_trans_plans = updateModal.find('.editTransactionPlans').val();

            console.log(update_trans_id);
            console.log(update_trans_start);
            console.log(update_trans_end);
            console.log(update_trans_percentage);
            console.log(update_trans_plans);

            console.log('Condition se Pehlay');
            if (update_trans_end < update_trans_start){
                console.log('Condition k andar');
                alert(" End value should be greatar than Start value");
                return;
            } else {
                var row = $(`.table_row[row-id=${update_trans_id}]`);
                row.find('.trans_start').text(parseInt(update_trans_start));
                row.find('.trans_end').text(parseInt(update_trans_end));
                row.find('.trans_percentage').text(parseInt(update_trans_percentage));
                row.find('.trans_plans').text(update_trans_plans);
            }

            $('.editClosedTransaction').modal('hide');

        });

        $(document).ready(function(){
            trrigered_selecBox();
        });
        function trrigered_selecBox()
        {
            $('.item-commission').trigger("change");
        }
    </script>
@endsection

@push('js')
    <script>

        function hideCommissionPlan(){
            $('#commission-plan').hide();
            $('#add-plan').show();
        }
        function showCommissionPlan(){
            $('#commission-plan').show();
            $('#add-plan').hide();
        }

        function showTableData(){
            $('#tableData').show();
        }
        function hideTableData(){
            $('#tableData').hide();
        }

    </script>
@endpush
