@extends('user.layouts.master')

@push('css')
    <style>
        h4{
            text-decoration: underline;
            font-weight: bold;
            margin: 30px 0;
        }
        label{
            font-weight: bold;
        }

    </style>
@endpush

@section('content')
    <div class="header">
        <div class="container-fluid">
            <div class="header-body">
                <div class="row align-items-end">
                    <div class="col">
                        <h1 class="header-title">
                            Settings / Permission
                        </h1>
                    </div>
                    <div class="col-auto">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 col-lg-12 col-xl">
                <div class="card">
                    <div class="card-body">
                        <div class="col-lg-12 col-md-12 row">
                            <form>
                                <h2>Permission</h2>
                                <div class="form-group mt-5">
                                    <label for="">
                                        <input type="checkbox" class="form-check-input">
                                        Allow Agents to Add Transactions
                                    </label>
                                </div>
                                <div class="form-group">
                                    <label for="">
                                        <input type="checkbox" class="form-check-input">
                                        Allow Agents to Add Transactions
                                    </label>
                                </div>
                                <div class="form-group">
                                    <label for="">
                                        <input type="checkbox" class="form-check-input">
                                        Allow agent to edit commission or sale price when transaction is at 50%
                                    </label>
                                </div>
                                <div class="form-group">
                                    <label for="">
                                        <input type="checkbox" class="form-check-input">
                                        Allow Agents to Upload Transaction Documents
                                    </label>
                                </div>
                                <div class="form-group">
                                    <label for="">
                                        <input type="checkbox" class="form-check-input">
                                        Send email to agent(s) when disbursement is created
                                    </label>
                                </div>
                                <div class="form-group">
                                    <label for="">
                                        <input type="checkbox" class="form-check-input">
                                        Allow Agents to Edit Commission Plan
                                    </label>
                                </div>
                                <div class="form-group">
                                    <label for="">
                                        <input type="checkbox" class="form-check-input">
                                        Email Agent When Disbursement Is Approved
                                    </label>
                                </div>
                                <div class="form-group">
                                    <label for="">
                                        <input type="checkbox" class="form-check-input">
                                        Add Agent Approval to Disbursement
                                    </label>
                                </div>
                                <div class="form-group">
                                    <label for="">
                                        <input type="checkbox" class="form-check-input">
                                        Allow Agents to Delete Transactions
                                    </label>
                                </div>
                                <div class="form-group">
                                    <label for="">
                                        <input type="checkbox" class="form-check-input">
                                        Allow Agents to Delete Transaction Documents
                                    </label>
                                </div>
                                <div class="form-group">
                                    <label for="">
                                        <input type="checkbox" class="form-check-input">
                                        Allow Agents to Complete Task Lists
                                    </label>
                                </div>
                                <div class="form-group">
                                    <label for="">
                                        <input type="checkbox" class="form-check-input">
                                        Allow Agents to Add/Edit Trust Account / Deposit Section
                                    </label>
                                </div>
                                <div class="form-group">
                                    <label for="">
                                        <input type="checkbox" class="form-check-input">
                                        Allow Agents to Add Notes to Transaction
                                    </label>
                                </div>
                                <div class="form-group">
                                    <label for="">
                                        <input type="checkbox" class="form-check-input">
                                        Don't Allow Agents to Edit Their Name and Company
                                    </label>
                                </div>
                                <div class="form-group">
                                    <label for="">
                                        <input type="checkbox" class="form-check-input">
                                        Send Email to Agent and Vendor When You Pay Them Using ACH Transfer
                                    </label>
                                </div>
                                <div class="form-group">
                                    <label for="">
                                        <input type="checkbox" class="form-check-input">
                                        Don't Allow Agents to Access Agent Portal
                                    </label>
                                </div>
                                <div class="form-group">
                                    <label for="">
                                        <input type="checkbox" class="form-check-input">
                                        Turn on Agent Checklist in Agent Portal
                                    </label>
                                </div>
                                <div class="form-group">
                                    <label for="">
                                        <input type="checkbox" class="form-check-input">
                                        Email Brokerage When Agent Completes Their Onboarding Package
                                    </label>
                                </div>
                                <div class="form-group">
                                    <label for="">
                                        <input type="checkbox" class="form-check-input">
                                        Don't Allow Agents to Enter Their Own Credit Cards
                                    </label>
                                </div>
                                <div class="form-group">
                                    <label for="">
                                        <input type="checkbox" class="form-check-input">
                                        Show Office on View Transaction page
                                    </label>
                                </div>
                                <div class="form-group">
                                    <label for="">
                                        <input type="checkbox" class="form-check-input">
                                        Allow Agent to View their Commission Plan details
                                    </label>
                                </div>
                                <div class="form-group">
                                    <label for="">
                                        <input type="checkbox" class="form-check-input">
                                        Restrict forms to only accept auto-fill
                                    </label>
                                </div>
                                <div class="form-group">
                                    <label for="">
                                        <input type="checkbox" class="form-check-input">
                                        Allow Default State
                                    </label>
                                </div>
                                <div class="form-group">
                                    <label for="">
                                        <input type="checkbox" class="form-check-input">
                                        Require Title/Attorney Name field to be filled out
                                    </label>
                                </div>
                                <div class="form-group">
                                    <label for="">
                                        <input type="checkbox" class="form-check-input">
                                        Allow override of commission plan in a transaction?
                                    </label>
                                </div>
                                <div class="form-group">
                                    <label for="">
                                        <input type="checkbox" class="form-check-input">
                                        Don't allow the disbursement PDF to be generated until the disbursement is approved
                                    </label>
                                </div>
                                <div class="form-group">
                                    <label for="">
                                        <input type="checkbox" class="form-check-input">
                                        Turn on custom Transaction IDs in transactions
                                    </label>
                                </div>
                                <div class="form-group">
                                    <label for="">
                                        <input type="checkbox" class="form-check-input">
                                        Don't allow agent to generate or email PDF.
                                    </label>
                                </div>
                                <div class="form-group">
                                    <label for="">
                                        <input type="checkbox" class="form-check-input">
                                        Turn on Zoho Dashboard view in Agent Portal
                                    </label>
                                </div>
                                <div class="form-group form-group-row">
                                    <label class="mt-2" style="margin-right: 40px;">Tab to show after creating transaction:</label>
                                    <select class="form-select" data-choices>
                                        <option>Title/Attorney</option>
                                        <option>None</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="">
                                        <input type="checkbox" class="form-check-input">
                                        Turn on Skyslope sync logs in Agent Portal.
                                    </label>
                                </div>
                                <div class="form-group">
                                    <label for="">
                                        <input type="checkbox" class="form-check-input">
                                        Turn on Dotloop sync logs in Agent Portal.
                                    </label>
                                </div>
                                <div class="form-group">
                                    <label for="">
                                        <input type="checkbox" class="form-check-input">
                                        Track Title Company Income on a Transaction.
                                    </label>
                                </div>
                                <div class="form-group">
                                    <label for="">
                                        <input type="checkbox" class="form-check-input">
                                        Auto collect all agent billing log items when creating a disbursement.
                                    </label>
                                </div>
                                <div class="form-group">
                                    <label for="">
                                        <input type="checkbox" class="form-check-input">
                                        Don't allow agents to have multiple offices within the same division.
                                    </label>
                                </div>
                                <div class="form-group">
                                    <label for="">
                                        <input type="checkbox" class="form-check-input">
                                        Set a custom reply-to email.
                                    </label>
                                </div>
                                <div class="form-group">
                                    <label for="">
                                        <input type="checkbox" class="form-check-input">
                                        Save PDF disbursement in Document section when a transaction is closed out.
                                    </label>
                                </div>
                                <div class="form-group">
                                    <label for="">
                                        <input type="checkbox" class="form-check-input">
                                        Set view agents default status.
                                    </label>
                                </div>
                                <div class="form-group">
                                    <label for="">
                                        <input type="checkbox" class="form-check-input">
                                        Change Rental Transaction Type to Lease.
                                    </label>
                                </div>
                                <div class="form-group">
                                    <label for="">
                                        <input type="checkbox" class="form-check-input">
                                        Show Agent's Team on Reports.
                                    </label>
                                </div>
                                <div class="form-group">
                                    <label for="">
                                        <input type="checkbox" class="form-check-input">
                                        Send email to agent(s) when a disbursement is paid
                                    </label>
                                </div>
                                <div class="form-group">
                                    <label for="">
                                        <input type="checkbox" class="form-check-input">
                                        Enable Transaction Subtypes.
                                    </label>
                                </div>
                                <div class="form-group">
                                    <label for="">
                                        <input type="checkbox" class="form-check-input">
                                        Allow creating a transaction and disbursement with a $0 sale price.
                                    </label>
                                </div>

                                <h4>Agent Disbursement Settings:-</h4>

                                <div class="form-group">
                                    <label for="">
                                        <input type="checkbox" class="form-check-input">
                                        Allow Agents to View Disbursements
                                    </label>
                                </div>
                                <div class="form-group">
                                    <label for="">
                                        <input type="checkbox" class="form-check-input">
                                        Allow Agents to Create Disbursements
                                    </label>
                                </div>
                                <div class="form-group">
                                    <label for="">
                                        <input type="checkbox" class="form-check-input">
                                        Disbursements Must be Approved
                                    </label>
                                </div>
                                <div class="form-group">
                                    <label for="">
                                        <input type="checkbox" class="form-check-input">
                                        Lock disbursement from agent editing once disbursement approval request is sent.
                                    </label>
                                </div>
                                <div class="form-group">
                                    <label for="">
                                        <input type="checkbox" class="form-check-input">
                                        Allow Agents to Delete Disbursements
                                    </label>
                                </div>

                                <h4>Agent Reports:-</h4>

                                <div class="form-group">
                                    <label for="">
                                        <input type="checkbox" class="form-check-input">
                                        Allow Agents to View Commission Detail Report
                                    </label>
                                </div>
                                <div class="form-group">
                                    <label for="">
                                        <input type="checkbox" class="form-check-input">
                                        Allow Agent to View Cap Plan Report
                                    </label>
                                </div>
                                <div class="form-group">
                                    <label for="">
                                        <input type="checkbox" class="form-check-input">
                                        Hide Pipeline Report in Agent Portal
                                    </label>
                                </div>
                                <div class="form-group">
                                    <label for="">
                                        <input type="checkbox" class="form-check-input">
                                        Allow Agent to View Sliding Scale Plan Report
                                    </label>
                                </div>

                                <h4>Agent Billings:-</h4>

                                <div class="form-group">
                                    <label for="">
                                        <input type="checkbox" class="form-check-input">
                                        Email Billing Invoices to Agents
                                    </label>
                                </div>
                                <div class="form-group">
                                    <label for="">
                                        <input type="checkbox" class="form-check-input">
                                        Send Email to Agents When Their Credit Card is Declined
                                    </label>
                                </div>
                                <div class="form-group form-group-row">
                                    <label style="margin-right: 10px">
                                        <input type="checkbox" class="form-check-input">
                                        Charge Agents late fee on unpaid invoices
                                    </label>
                                    <a href=""><i class="fas fa-edit"></i></a>
                                </div>

                                <h4>Notification:-</h4>

                                <div class="form-group form-group-row">
                                    <label>
                                        <input type="checkbox" class="form-check-input">
                                        Email when disbursement is approved
                                    </label>
                                </div>

                                <h4>Co Broke Section:-</h4>

                                <div class="form-group form-group-row">
                                    <label>
                                        <input type="checkbox" class="form-check-input">
                                        Allow Co Broke commission in disbursement
                                    </label>
                                </div>
                                <div class="form-group form-group-row">
                                    <label>
                                        <input type="checkbox" class="form-check-input">
                                        Require the Co-Broke Fields in Transaction
                                    </label>
                                </div>

                                <h4>Agent Roster Page:-</h4>

                                <div class="form-group form-group-row">
                                    <label>
                                        <input type="checkbox" class="form-check-input">
                                        Show Agent's License Number
                                    </label>
                                </div>
                                <div class="form-group form-group-row">
                                    <label>
                                        <input type="checkbox" class="form-check-input">
                                        Show Agent's ID
                                    </label>
                                </div>
                                <div class="form-group form-group-row">
                                    <label>
                                        <input type="checkbox" class="form-check-input">
                                        Show Agent's Entity
                                    </label>
                                </div>
                                <div class="form-group form-group-row">
                                    <label>
                                        <input type="checkbox" class="form-check-input">
                                        Show Agent's Email
                                    </label>
                                </div>

                                <h4>License:-</h4>

                                <div class="form-group form-group-row">
                                    <label>
                                        <input type="checkbox" class="form-check-input">
                                        Allow Auto Inactive to Expired License
                                    </label>
                                </div>

                                <h4>SkySlope:-</h4>

                                <div class="form-group form-group-row">
                                    <label>
                                        <input type="checkbox" class="form-check-input">
                                        Email agent when a new Skyslope transaction is synced
                                    </label>
                                </div>
                                <div class="form-group form-group-row">
                                    <label>
                                        <input type="checkbox" class="form-check-input">
                                        Don't sync title company info
                                    </label>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('js')
@endpush
