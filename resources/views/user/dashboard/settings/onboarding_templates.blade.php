@extends('user.layouts.master')

@push('css')
    <style>
        .switch {
            position: relative;
            display: inline-block;
            width: 60px;
            height: 34px;
        }
        .switch input {
            opacity: 0;
            width: 0;
            height: 0;
        }
        .slider {
            position: absolute;
            cursor: pointer;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            background-color: #ccc;
            -webkit-transition: .4s;
            transition: .4s;
        }
        .slider:before {
            position: absolute;
            content: "";
            height: 26px;
            width: 26px;
            left: 4px;
            bottom: 4px;
            background-color: white;
            -webkit-transition: .4s;
            transition: .4s;
        }
        .switch input:checked + .slider {
            background-color: #2196F3;
        }

        .switch input:focus + .slider {
            box-shadow: 0 0 1px #2196F3;
        }

        .switch input:checked + .slider:before {
            -webkit-transform: translateX(26px);
            -ms-transform: translateX(26px);
            transform: translateX(26px);
        }
        /* Rounded sliders */
        .slider.round {
            border-radius: 34px;
        }

        .slider.round:before {
            border-radius: 50%;
        }
    </style>
@endpush

@section('content')
    <div class="header">
        <div class="container-fluid">
            <div class="header-body">
                <div class="row align-items-end">
                    <div class="col">
                        <h1 class="header-title">
                            Settings / Onboarding Templates
                        </h1>
                    </div>
                    <div class="col-auto">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 col-lg-12 col-xl">
                <div class="card">
                    <div class="card-body">
                        <div class="col-lg-12 col-md-12 row">
                            <div class="col-lg-3 col-md-2"></div>
                            <div class="col-lg-6 col-md-8">
                                <form>
                                    <div class="form-group mt-5 form-group-row">
                                        <label class="w-25 mt-2">Choose Onboarding Template:</label>
                                        <div class="input_container select_container w-75">
                                            <select class="form-select mb-3" data-choices>
                                                <option>Create New Template</option>
                                            </select>
                                            <input type="text" placeholder="Template Name (e.g.New Onboarding Template)" class="form-control input-group">
                                        </div>
                                    </div>
                                    <div class="form-group form-group-row">
                                        <label class="w-25 mt-2">Select an existing document, or upload new document(s) for Agents to sign during onboarding:</label>
                                        <div class="input_container select_container w-75">
                                            <textarea class="form-control" rows="6" cols="50" maxlength="200" placeholder="optional"></textarea>
                                        </div>
                                    </div>
                                    <div class="form-group form-group-row">
                                        <label class="w-25 mt-2">
                                        </label>
                                        <div class="input_container w-75">
                                            <p>Uploaded:</p>
                                            <button class="btn btn-secondary">+  Add Logo</button>
                                            <br>
                                            <p>(max file size: 1 MB, accepted files: jpg,png)</p>
                                        </div>
                                    </div>
                                    <div class="form-group mt-5 form-group-row">
                                        <label class="w-25 mt-2">Choose Agent Checklist:</label>
                                        <div class="input_container select_container w-75">
                                            <select class="form-select mb-3" disabled data-choices>
                                                <option>No result match</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group mt-5 form-group-row">
                                        <label class="w-25 mt-2">Choose Agent Checklist:</label>
                                        <div class="input_container select_container w-75">
                                            <div>
                                                <input class="form-check-input list-checkbox" id="listCheckboxOne" type="checkbox">
                                                <label style="margin-left: 3px;">Show agents who have notes </label>
                                            </div>
                                            <div class="mt-3">
                                                <input class="form-check-input list-checkbox" id="listCheckboxOne" type="checkbox">
                                                <label style="margin-left: 3px;">Enter Real Estate License Number and Expiration Date </label>
                                            </div>
                                            <div class="mt-3">
                                                <input class="form-check-input list-checkbox" id="listCheckboxOne" type="checkbox">
                                                <label style="margin-left: 3px;">Upload a Copy of Driver's License </label>
                                            </div>
                                            <div class="mt-3">
                                                <input class="form-check-input list-checkbox" id="listCheckboxOne" type="checkbox">
                                                <label style="margin-left: 3px;">Upload a Copy of Car Insurance </label>
                                            </div>
                                            <div class="mt-3">
                                                <input class="form-check-input list-checkbox" id="listCheckboxOne" type="checkbox">
                                                <label style="margin-left: 3px;">Collect Credit Card Information </label>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="col-lg-3 col-md-2"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('js')
@endpush
