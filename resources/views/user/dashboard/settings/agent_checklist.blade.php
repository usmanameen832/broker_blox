@extends('user.layouts.master')

@push('css')
@endpush

@section('content')
    <div id="Agent-Checklist">
        <div class="header">
            <div class="container-fluid">
                <div class="header-body">
                    <div class="row align-items-end">
                        <div class="col">
                            <h1 class="header-title">
                                Settings / Agent Checklist
                            </h1>
                        </div>
                        <div class="col-auto">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @if(session()->has('delete'))
            <div class="alert alert-success">
                <h4>{{ session('delete') }}</h4>
            </div>
        @endif
        @if(session()->has('status'))
            <div class="alert alert-success">
                <h4>{{ session('status') }}</h4>
            </div>
        @endif
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12 col-lg-12 col-xl">
                    <div class="card">
                        <div class="card-header">
                            <h4>Agent Checklist</h4>
                        </div>
                        <div class="card-body">
                            <div class="col-md-12">
                                <div class="form-group pull-right mt-5">
                                    <div>
                                        <button type="submit" onclick="hideAgentChecklist()" class="btn btn-primary">+ Add Checklist</button>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="col-md-12">
                                <div class="table-responsive" data-list='{"valueNames": ["tables-row", "tables-first", "tables-last", "tables-handle"]}'>
                                    <table class="table table-sm">
                                        <thead>

                                        <tr>
                                            <th scope="col">
                                                <a href="#" class="text-muted list-sort">Checklist Name</a>
                                            </th>
                                            <th scope="col">
                                                <a href="#" class="text-muted list-sort">Office Name</a>
                                            </th>
                                            <th scope="col">
                                                <a href="#" class="text-muted list-sort">Action</a>
                                            </th>
                                        </tr>
                                        </thead>
                                        <tbody class="list">
                                        @foreach($checklistData as $checklists)
                                        <tr>
                                            <td>{{ $checklists->checklist_name }}</td>
                                            <td>{{ $checklists->office_location }}</td>
                                            <td>
                                                <a><i class="far fa-edit"></i></a>
                                                <a href="{{ route('deleteItemsChecklist', $checklists->id) }}"><i class="fas fa-trash-alt"></i></a>
                                            </td>
                                        </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- / .row -->
        </div>
    </div>
    <div id="Agent-Add-Checklist" style="display: none">
        <div class="header">
            <div class="container-fluid">
                <!-- Body -->
                <div class="header-body">
                    <div class="row align-items-end">
                        <div class="col">
                            <!-- Title -->
                            <h1 class="header-title">
                                Settings / Agent Checklist / Add Checklist
                            </h1>
                        </div>
                        <div class="col-auto">
                        </div>
                    </div> <!-- / .row -->
                </div> <!-- / .header-body -->

            </div>
        </div>
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12 col-lg-12 col-xl">
                    <!-- Value  -->
                    <form action="{{ route('saveItemsChecklist') }}" method="post">
                        @csrf
                        <div class="card">
                            <div class="card-header">
                                <h4>Add Checklist</h4>
                            </div>
                            <div class="card-body">
                                <div class="col-md-12 row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="mb-2">Checklist Name:</label>
                                            <div class="input_container">
                                                <input type="text" name="checklist_name" class="form-control input-group" required placeholder="Enter Checklist Name">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="mb-2">Office Location(s):</label>
                                            <div class="input_container select_container">
                                                <select class="form-select mb-3" name="office_location" data-choices>
                                                    <option>All Offices</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="mb-2">Checklist Assigned To:</label>
                                            <div class="input_container select_container">
                                                <select class="form-select mb-3" name="checklist_assigned_to" data-choices>
                                                    @foreach($data as $datas)
                                                        <option>{{ $datas->first_name }} {{ $datas->last_name }} ({{ $datas->email }})</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group mt-5 pull-right">
                                            <button type="button" onclick="showTableRow()" class="btn btn-secondary">
                                                + Add Item
                                            </button>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="table-responsive">
                                        <table class="table table-sm" id="addChecklistTable">
                                            <thead>
                                            <tr>
                                                <th scope="col">
                                                    <a href="#" class="text-muted list-sort">#</a>
                                                </th>
                                                <th scope="col">
                                                    <a href="#" class="text-muted list-sort">Item Name</a>
                                                </th>
                                                <th scope="col">
                                                    <a href="#" class="text-muted list-sort">Item Assigned To</a>
                                                </th>
                                                <th scope="col">
                                                    <a href="#" class="text-muted list-sort">Action</a>
                                                </th>
                                            </tr>
                                            </thead>
                                            <tbody class="list addChecklistTbody">
                                            <tr>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                            <tr style="display: none" id="table-row">
                                                <td></td>
                                                <td>
                                                    <div class="form-group mb-0 w-75">
                                                        <input type="text" placeholder="Item Name" class="form-control input-group checklistItemName">
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="input_container select_container">
                                                        <select class="form-select" id="itemAssignedTo">
                                                            @foreach($data as $datas)
                                                                <option value="{{ $datas->first_name }} {{ $datas->last_name }} ({{ $datas->email }})">{{ $datas->first_name }} {{ $datas->last_name }} ({{ $datas->email }})</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </td>
                                                <td>
                                                    <a class="" style="cursor: pointer; color: blue;  border: none; background-color: transparent"><i style="text-decoration: none; color: #72AFD2; font-size: 16px; margin-right: 5px;" onclick="hideTableRow()" class="far fa-times"></i>Cancel</a>
                                                    <a class="saveChecklistItems" style="cursor: pointer; color: blue;  border: none; background-color: transparent "><i style="text-decoration: none; color: #B2C697; font-size: 16px; margin-right: 5px; margin-left: 5px" class="fas fa-check"></i>Save</a>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer">
                                <div class="form-group-row pull-right">
                                    <button class="btn btn-light" style="margin-right: 5px;" onclick="showAgentChecklist()">Cancel</button>
                                    <button type="submit" class="btn btn-primary">Save</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <!-- / .row -->
        </div>
    </div>
@endsection

@push('js')
    <script>
        $('body').on('click', '.saveChecklistItems', function (){
            var item_name = $('.checklistItemName').val();
            var getAssignedItem = document.getElementById('itemAssignedTo');
            var item_assigned_to = getAssignedItem.value;

            var checklist_item_id = Math.floor(Math.random() * 5) + 1;

            var table = document.getElementById('addChecklistTable');
            var tBodyRowCount = table.tBodies[0].rows.length - 1;

            $('body').find('#table-row').first().before(
                `
                <tr class="checklistItemsTRow" checklist-tr-td="${checklist_item_id}">
                    <td> <input class="form-control input-group RowNumber" style="border: none;" type="number" readonly value="${tBodyRowCount}"></td>
                    <td> <input class="form-control input-group ItemName" name="item_name" style="border: none;" type="text" readonly value="${item_name}"></td>
                    <td> <select class="form-select AssignedTo" name="item_assigned_to">
                            <option>${item_assigned_to}</option>
                        </select> </td>
                    <td>
                        <a class="deleteChecklistItem" style="text-decoration: none; border: none; background-color: transparent; font-size: 16px"> <i class="fas fa-trash-alt"></i> </a>
                        <a class="editChecklistItem" style="text-decoration: none; border: none; background-color: transparent; font-size: 16px"> <i class="far fa-edit"></i> </a>
                    </td>
                </tr>
            `
            );
            $('#table-row').hide();

            $('body').on('click', '.deleteChecklistItem', function (e){
                e.preventDefault();
                $(this).closest("tr").remove();
            })
        });

        $('body').on('click', '.editChecklistItem', function (){

            $('.checklistItemsTRow').hide();

            var editchecklistItems = $('body').find('.checklistItemsTRow').parents();
            var items_row_id = editchecklistItems.attr('checklist-tr-td');
            var edit_row_number = editchecklistItems.find('.RowNumber').val();
            var edit_item_name = editchecklistItems.find('.ItemName').val();
            var edit_assigned_to = editchecklistItems.find('.AssignedTo').val();

            console.log(items_row_id);
            console.log(edit_row_number);
            console.log(edit_item_name);
            console.log(edit_assigned_to);

            $('body').find('#table-row').first().before(
                `
                <tr class="EditChecklistItemsRow">
                    <input type="text" class="hiddenChecklistItemId" value="${items_row_id}" style="display: none">
                    <td> <input class="form-control input-group EditRowNumber" style="border: none;" readonly type="number" value="${edit_row_number}"></td>
                    <td> <input class="form-control input-group EditItemName" required type="text" value="${edit_item_name}"></td>
                    <td> <select class="form-select EditAssignedTo">
                            <option>${edit_assigned_to}</option>
                        </select> </td>
                    <td>
                        <button class="btn btn-light" > Cancel </button>
                        <button class="btn btn-primary updateChecklistItemsRow"> Update </button>
                    </td>
                </tr>
            `)

        });

        $('body').on('click', '.updateChecklistItemsRow', function (){
            $('.EditChecklistItemsRow').hide();

            var update_checklist_row = $('.EditChecklistItemsRow').parents();
            var updateAgainstId = update_checklist_row.find('.hiddenChecklistItemId').val();
            var updateRowNumber = update_checklist_row.find('.EditRowNumber').val();
            var updateItemName = update_checklist_row.find('.EditItemName').val();
            var updateAssignedTo = update_checklist_row.find('.EditAssignedTo').val();

            console.log(updateAgainstId);
            console.log(updateRowNumber);
            console.log(updateItemName);
            console.log(updateAssignedTo);

            var row = $(`.checklistItemsTRow[checklist-tr-td=${update_checklist_row}]`);
            row.find('.RowNumber').val(updateRowNumber);
            row.find('.ItemName').val(updateItemName);
            row.find('.AssignedTo').val(updateAssignedTo);

            $('.checklistItemsTRow').show();
        })
    </script>
    <script>
        function hideAgentChecklist(){
            $('#Agent-Checklist').hide();
            $('#Agent-Add-Checklist').show();
        }
        function showAgentChecklist(){
            $('#Agent-Checklist').show();
            $('#Agent-Add-Checklist').hide();
        }
        function showTableRow(){
            $('#table-row').show();
        }
        function hideTableRow(){
            $('#table-row').hide();
        }

    </script>
@endpush
