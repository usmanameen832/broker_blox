@extends('user.layouts.master')

@push('css')
@endpush

@section('content')
    <div class="header">
        <div class="container-fluid">
            <div class="header-body">
                <div class="row align-items-end">
                    <div class="col">
                        <h1 class="header-title">
                            Sttings / Manage Tags
                        </h1>
                    </div>
                    <div class="col-auto">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 col-lg-12 col-xl">
                <div class="card">
                    <div class="card-body">
                        <h2>Tags</h2>
                        <div class="col-md-12">
                            <div class="form-group pull-right mt-5">
                                <div>
                                    <button type="button" class="btn btn-secondary">Filter</button>
                                    <button type="submit" data-bs-toggle="modal" data-bs-target="#addTag" class="btn btn-primary">+ Add Tag</button>
                                </div>
                            </div>
                        </div>

                        <!-- Modal -->
                        <div class="modal fade" id="addTag" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                            <div class="modal-dialog modal-1-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h3 class="modal-title" id="staticBackdropLabel">Add Tag</h3>
                                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                    </div>
                                    <div class="modal-body">
                                        <label> <b>Tag Description:</b> </label>
                                        <input type="text" placeholder="Enter Description" class="form-control input-group">
                                    </div>
                                    <div class="modal-footer form-group-row">
                                        <button type="button" class="btn btn-light">Cancel</button>
                                        <button type="button" class="btn btn-primary">Save</button>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12 row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="mb-2">Tag Type:</label>
                                    <div class="input_container select_container">
                                        <select class="form-select mb-3" data-choices>
                                            <option>Lead Source</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group pull-right mt-5">
                                    <div>
                                        <button type="button" class="btn btn-secondary">Reset
                                            Filter</button>
                                        <button type="submit" class="btn btn-primary">Apply
                                            Filter(s)</button>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="table-responsive" data-list='{"valueNames": ["tables-row", "tables-first", "tables-last", "tables-handle"]}'>
                                <table class="table table-sm">
                                    <thead>
                                    <tr>
                                        <th scope="col">
                                            <a href="#" class="text-muted list-sort" data-sort="tables-first">Tag</a>
                                        </th>
                                        <th scope="col">
                                            <a href="#" class="text-muted list-sort">Lead Source</a>
                                        </th>
                                        <th scope="col">
                                            <a href="#" class="text-muted list-sort">Action</a>
                                        </th>
                                    </tr>
                                    </thead>
                                    <tbody class="list">
                                    <tr>
                                        <td class="tables-first">Test</td>
                                        <td>N/A</td>
                                        <td>
                                            <a><i class="far fa-edit"></i></a>
                                            <a><i class="fas fa-trash-alt"></i></a>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- / .row -->
    </div>
@endsection

@push('js')
@endpush
