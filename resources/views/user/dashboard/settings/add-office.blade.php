@extends('user.layouts.master')

@push('css')
@endpush

@section('content')
    <div id="add-offices">
        <div class="header">
            <div class="container-fluid">
                <div class="header-body">
                    <div class="row align-items-end">
                        <div class="col">
                            <h1 class="header-title">
                                Settings / Organization / Office
                            </h1>
                        </div>
                        <div class="col-auto">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12 col-lg-12 col-xl">
                    <div class="card">
                        <div class="card-body">
                            <ul class="nav nav-tabs">
                                <li class="nav-item">
                                    <a class="nav-link active" aria-current="page" href="#">Offcies</a>
                                </li>
                            </ul>
                            <div class="tab-content" id="title-attorneys">
                                <div class="active" id="#">
                                    <div class="card" style="margin-top: 10px;">
                                        <div class="card-header">
                                            <h3>Add Office</h3>
                                        </div>
                                        <div class="card-body">
                                            <h4>Add the office information below</h4>
                                            <div class="col-lg-12 col-md-12 row">
                                                <div class="col-lg-3 col-md-2"></div>
                                                <div class="col-lg-6 col-md-8">
                                                    <form>
                                                        <div class="form-group mt-5 form-group-row">
                                                            <label class="w-25 mt-2">Real Estate License #:</label>
                                                            <div class="input_container select_container w-75">
                                                                <input type="text" class="form-control input-group">
                                                            </div>
                                                        </div>
                                                        <div class="form-group form-group-row">
                                                            <label class="w-25 mt-2">Office Name:</label>
                                                            <div class="input_container select_container w-75">
                                                                <input type="text" class="input-group form-control">
                                                            </div>
                                                        </div>
                                                        <div class="form-group form-group-row">
                                                            <label class="w-25 mt-2">External Office Id:</label>
                                                            <div class="input_container select_container w-75">
                                                                <input type="text" class="input-group form-control">
                                                            </div>
                                                        </div>
                                                        <div class="form-group form-group-row">
                                                            <label class="w-25 mt-2">KvCore Office #:</label>
                                                            <div class="input_container select_container w-75">
                                                                <input type="text" placeholder="optional" class="input-group form-control">
                                                            </div>
                                                        </div>
                                                        <div class="form-group form-group-row">
                                                            <label class="w-25 mt-2">Office Mls Id:</label>
                                                            <div class="input_container w-75">
                                                                <input type="text" class="input-group form-control">
                                                            </div>
                                                        </div>
                                                        <div class="form-group form-group-row">
                                                            <label class="w-25 mt-2">Entity Name:</label>
                                                            <div class="input_container form-group-row w-75">
                                                                <input type="text" placeholder="optional" class="input-group form-control">
                                                            </div>
                                                        </div>
                                                        <div class="form-group mt-5 form-group-row">
                                                            <label class="w-25 mt-2">Zip/Postal Code:</label>
                                                            <div class="input_container select_container w-75">
                                                                <input type="text" class="form-control input-group">
                                                            </div>
                                                        </div>
                                                        <div class="form-group form-group-row">
                                                            <label class="w-25 mt-2">Address:</label>
                                                            <div class="input_container select_container w-75">
                                                                <input type="text" class="input-group form-control">
                                                            </div>
                                                        </div>
                                                        <div class="form-group form-group-row">
                                                            <label class="w-25 mt-2">City:</label>
                                                            <div class="input_container select_container w-75">
                                                                <input type="text" class="input-group form-control">
                                                            </div>
                                                        </div>
                                                        <div class="form-group form-group-row">
                                                            <label class="w-25 mt-2">Country:</label>
                                                            <div class="input_container select_container w-75">
                                                                <input type="text" class="input-group form-control">
                                                            </div>
                                                        </div>
                                                        <div class="form-group form-group-row">
                                                            <label class="w-25 mt-2">State/Province:</label>
                                                            <div class="input_container select_container w-75">
                                                                <input type="text" class="input-group form-control">
                                                            </div>
                                                        </div>
                                                        <div class="form-group form-group-row">
                                                            <label class="w-25 mt-2">Phone Number:</label>
                                                            <div class="input_container select_container w-75">
                                                                <input type="number" class="input-group form-control">
                                                            </div>
                                                        </div>
                                                        <div class="form-group form-group-row">
                                                            <label class="w-25 mt-2">Fax Number:</label>
                                                            <div class="input_container select_container w-75">
                                                                <input type="text" placeholder="optional" class="input-group form-control">
                                                            </div>
                                                        </div>
                                                        <div class="form-group form-group-row">
                                                            <label class="w-25 mt-2">Email:</label>
                                                            <div class="input_container select_container w-75">
                                                                <input type="email" class="input-group form-control">
                                                            </div>
                                                        </div>
                                                        <div class="form-group form-group-row">
                                                            <label class="w-25 mt-2">Don't Sync to Kvcore:</label>
                                                            <div class="input_container select_container w-75">
                                                                <input type="checkbox" class="form-check-input input-group">
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                                <div class="col-lg-3 col-md-2"></div>
                                            </div>
                                        </div>
                                        <div class="card-footer">
                                            <div class="col-lg-12 col-md-12 row">
                                                <div class="col-lg-3 col-md-2"></div>
                                                <div class="col-lg-6 col-md-8">
                                                    <button class="btn btn-primary pull-right" style="margin-left: 8px;">Save Changes</button>
                                                    <button class="btn btn-light pull-right" onclick="showOffices()">Cancel</button>
                                                </div>
                                                <div class="col-lg-3 col-md-2"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('js')
    <script>
        function hideOffices() {
            $('#offices').hide();
            $('#add-offices').show();
        }
        function showOffices() {
            $('#offices').show();
            $('#add-offices').hide();
        }
    </script>
@endpush
