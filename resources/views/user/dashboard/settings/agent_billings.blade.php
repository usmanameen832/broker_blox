@extends('user.layouts.master')

@push('css')

@endpush

@section('content')
    <div class="header">
        <div class="container-fluid">
            <div class="header-body">
                <div class="row align-items-end">
                    <div class="col">
                        <h1 class="header-title">
                            Settings / Agent Billings
                        </h1>
                    </div>
                    <div class="col-auto">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-6 col-xl">
                <div class="card">
                    <div class="card-body">
                        <div class="col-md-12 col-lg-12 agent-will">
                            <div>
                                <p>
                                    To charge your agents credit cards for their monthly bills, you must first apply for a credit card processing account. We are partnered with iTransact,
                                    for all credit card billing features. Please click the iTransact Registration button to apply for your account. Account approval usually takes less than
                                    1 hour and we will notify you via email once your account is approved.
                                </p>
                                <br class="mt-5">
                                <p style="margin-bottom: 0 !important;">Transaction Fees:</p>
                                <p style="margin-bottom: 0 !important;">2.9%% + 0.30 per transaction</p>
                                <p>*There is a $4.99 monthly fee billed by iTransact for this service unless you charge more than $300 per month, then the fee will be waived.</p>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer text-right">
                        <button type="submit" class="btn btn-primary">
                            iTransact Registration
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <!-- / .row -->
    </div>
@endsection

@push('js')
@endpush
