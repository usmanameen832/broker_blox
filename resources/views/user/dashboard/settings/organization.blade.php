@extends('user.layouts.master')

@push('css')
@endpush

@section('content')
    <div id="offices">
        <div class="header">
            <div class="container-fluid">
                <div class="header-body">
                    <div class="row align-items-end">
                        <div class="col">
                            <h1 class="header-title">
                                Settings / Organization
                            </h1>
                        </div>
                        <div class="col-auto">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12 col-lg-12 col-xl">
                    <div class="card">
                        <div class="card-body">
                            <ul class="nav nav-tabs">
                                <li class="nav-item">
                                    <a class="nav-link active" aria-current="page" href="#">Offcies</a>
                                </li>
                            </ul>
                            <div class="tab-content" id="title-attorneys">
                                <div class="active" id="#">
                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                        <p class="font-24 margin-bottom-10">Offices</p>
                                        <div class="table-header">
                                            <div class="col-md-6">
                                                <div class="input-group">
                                                    <div class="form-outline">
                                                        <input type="search" id="form1" placeholder="search.." class="form-control" />
                                                    </div>
                                                    <button type="button" class="btn btn-primary">
                                                        <i class="fas fa-search"></i>
                                                    </button>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="filter-btn pull-right">
                                                    <button class="btn btn-primary" onclick="hideOffices()">+ Add Office</button>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="table-responsive" data-list='{"valueNames": ["tables-row", "tables-first", "tables-last", "tables-handle"]}'>
                                                <table class="table table-sm">
                                                    <thead>
                                                    <tr>
                                                        <th scope="col">
                                                            <a href="#" class="text-muted list-sort" data-sort="tables-first">#</a>
                                                        </th>
                                                        <th scope="col">
                                                            <a href="#" class="text-muted list-sort" data-sort="tables-last">Location Name</a>
                                                        </th>
                                                        <th scope="col">
                                                            <a href="#" class="text-muted list-sort" data-sort="tables-handle">External Office Id</a>
                                                        </th>
                                                        <th scope="col">
                                                            <a href="#" class="text-muted list-sort" data-sort="tables-handle">Kvcore Office Id</a>
                                                        </th>
                                                        <th scope="col">
                                                            <a href="#" class="text-muted list-sort" data-sort="tables-handle">Office Mls Id</a>
                                                        </th>
                                                        <th scope="col">
                                                            <a href="#" class="text-muted list-sort" data-sort="tables-handle">Entity Name</a>
                                                        </th>
                                                        <th scope="col">
                                                            <a href="#" class="text-muted list-sort" data-sort="tables-handle">Office Address</a>
                                                        </th>
                                                        <th scope="col">
                                                            <a href="#" class="text-muted list-sort" data-sort="tables-handle">Phone Number</a>
                                                        </th>
                                                        <th scope="col">
                                                            <a href="#" class="text-muted list-sort" data-sort="tables-handle">Fax Number</a>
                                                        </th>
                                                        <th scope="col">
                                                            <a href="#" class="text-muted list-sort" data-sort="tables-handle">Action</a>
                                                        </th>
                                                    </tr>
                                                    </thead>
                                                    <tbody class="list">
                                                    <tr>
                                                        <td class="tables-first">1</td>
                                                        <td class="tables-last">N/A</td>
                                                        <td class="tables-handle">123</td>
                                                        <td class="tables-handle">345</td>
                                                        <td class="tables-handle">678</td>
                                                        <td class="tables-handle">N/A</td>
                                                        <td class="tables-handle">N/A</td>
                                                        <td class="tables-handle">N/A</td>
                                                        <td class="tables-handle">9876534</td>
                                                        <td class="tables-handle">
                                                            <a><i class="far fa-edit"></i></a>
                                                            <a><i class="fas fa-trash-alt"></i></a>
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('js')
@endpush
