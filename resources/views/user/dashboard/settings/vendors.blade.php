@extends('user.layouts.master')

@push('css')
@endpush

@section('content')
    <div id="vendors">
        <div class="header">
            <div class="container-fluid">
                <div class="header-body">
                    <div class="row align-items-end">
                        <div class="col">
                            <h1 class="header-title">
                                Settings / Vendors
                            </h1>
                        </div>
                        <div class="col-auto">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12 col-lg-12 col-xl">
                    <div class="card">
                        <div class="card-body">
                            <h2>Vendors</h2>
                            <div class="col-md-12 row">
                                <div class="col-md-6">
                                    <div class="input-group">
                                        <div class="form-outline">
                                            <input type="search" id="form1" placeholder="search.." class="form-control" />
                                        </div>
                                        <button type="button" class="btn btn-primary">
                                            <i class="fas fa-search"></i>
                                        </button>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group pull-right">
                                        <button type="button" data-bs-toggle="modal" data-bs-target="#setApprover" class="btn btn-secondary">Set Approver</button>
                                        <button type="submit" onclick="hideVendors()" class="btn btn-primary">+ Add Vendors</button>
                                    </div>
                                </div>
                            </div>

                            <!-- Modal -->
                            <div class="modal fade" id="setApprover" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                                <div class="modal-dialog modal-1-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h3 class="modal-title" id="staticBackdropLabel">Set Approver</h3>
                                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                        </div>
                                        <div class="modal-body">
                                            <div class="form-group">
                                                <label> <b>Approver Broker:</b> </label>
                                                <div class="input_container select_container mt-3">
                                                    <select class="form-select mb-3" data-choices>
                                                        <option>Select an Option</option>
                                                        <option>test@gmail.com</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer form-group-row">
                                            <button type="button" class="btn btn-light">Close</button>
                                            <button type="button" class="btn btn-primary">Save</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>

                            <div class="col-md-12">
                                <div class="table-responsive" data-list='{"valueNames": ["tables-row", "tables-first", "tables-last", "tables-handle"]}'>
                                    <table class="table table-sm">
                                        <thead>
                                        <tr>
                                            <th scope="col">
                                                <a href="#" class="text-muted list-sort" data-sort="tables-first">Checklist Name</a>
                                            </th>
                                            <th scope="col">
                                                <a href="#" class="text-muted list-sort">Office Name</a>
                                            </th>
                                            <th scope="col">
                                                <a href="#" class="text-muted list-sort">Transaction Type</a>
                                            </th>
                                            <th scope="col">
                                                <a href="#" class="text-muted list-sort">Auto Selected</a>
                                            </th>
                                            <th scope="col">
                                                <a href="#" class="text-muted list-sort">Action</a>
                                            </th>
                                        </tr>
                                        </thead>
                                        <tbody class="list">
                                        <tr>
                                            <td class="tables-first">Test</td>
                                            <td>N/A</td>
                                            <td>N/A</td>
                                            <td>N/A</td>
                                            <td>
                                                <a><i class="far fa-edit"></i></a>
                                                <a><i class="fas fa-trash-alt"></i></a>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- / .row -->
        </div>
    </div>

    <div id="addVendors" style="display: none">
        <div class="header">
            <div class="container-fluid">
                <div class="header-body">
                    <div class="row align-items-end">
                        <div class="col">
                            <h1 class="header-title">
                                Settings / Vendors / Add Vendor
                            </h1>
                        </div>
                        <div class="col-auto">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12 col-lg-12 col-xl">
                    <div class="card">
                        <div class="card-body">
                            <h2>Add Vendor</h2>
                            <div class="col-md-12 row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="mb-2">Company Name:</label>
                                        <input type="text" placeholder="Enter Company Name" class="form-control input-group">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="mb-2">Address:</label>
                                        <input type="text" placeholder="Address(Optional)" class="form-control input-group">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="mb-2">Payee Name:</label>
                                        <input type="text" placeholder="Enter Payee Name(Optional)" class="form-control input-group">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="mb-2">Zip/Postal Code:</label>
                                        <input type="text" placeholder="Zip/Postal Code(Optional)" class="form-control input-group">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="mb-2">Email Address:</label>
                                        <input type="text" placeholder="Email Address" class="form-control input-group">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="mb-2">City:</label>
                                        <input type="text" placeholder="City(Optional)" class="form-control input-group">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="mb-2">Phone No:</label>
                                        <input type="text" placeholder="(_ _ _)-_ _ _-_ _ _" class="form-control input-group">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="mb-2">State/Province:</label>
                                        <input type="text" placeholder="Enter State/Province" class="form-control input-group">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="mb-2">Tax ID:</label>
                                        <input type="text" placeholder="Enter Tax id" class="form-control input-group">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="mb-2">Country:</label>
                                        <input type="text" placeholder="Enter Country Name" class="form-control input-group">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <div class="pull-right">
                                <button class="btn btn-light" onclick="showVendors()">Cancel</button>
                                <button class="btn btn-primary">Save</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('js')
    <script>
        function hideVendors(){
            $('#vendors').hide();
            $('#addVendors').show();
        }
        function showVendors(){
            $('#vendors').show();
            $('#addVendors').hide();
        }
    </script>
@endpush
