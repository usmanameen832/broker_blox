@extends('user.layouts.master')

@push('css')
    <style>
        .box-content-display {
            display: inline-block;
            vertical-align: top;
        }
        .quick-book-width {
            width: calc(100% - 270px);
        }
        .padding-10 {
            padding: 10px !important;
        }
        .quick-book-content-width {
            width: 55%;
        }
        .api-integration-btn {
            width: 270px;
        }
        .qb-btn-width {
            width: 240px !important;
        }
        .margin-bottom-10 {
            margin-top: 10px;
        }
        .api-key-logo {
            width: 260px;
        }
        .box {
            position: relative;
            border-radius: 3px;
            background: #fff;
            margin-bottom: 20px;
            width: 100%;
            box-shadow: 0 1px 1px rgb(0 0 0 / 10%);
        }
        .box-box-primary {
            border-top-color: #3c8dbc;
        }

    </style>
@endpush

@section('content')
    <div class="header">
        <div class="container-fluid">
            <div class="header-body">
                <div class="row align-items-end">
                    <div class="col">
                        <h1 class="header-title">
                            Integrations
                        </h1>
                    </div>
                    <div class="col-auto">
                    </div>
                </div>
            </div>

        </div>
    </div>
    <div class="container-fluid">
        <div class="col-md-12 col-lg-12 col-xl">
            <div class="box box-box-primary">
                <div class="box-content-display quick-book-width">
                    <div class="box-content-display padding-10">
                        <img alt="" class="api-key-logo" src="{{asset('user/assets/images/quick_book_logo.png')}}">
                    </div>
                    <div class="padding-10 box-content-display quick-book-content-width"><p>
                            Brokerblox and QuickBooks are integrated to allow you to sync your BrokerSumo
                            data to QuickBooks for easy accounting.</p>
                        <p class="cursor text-primary">Click here to learn how to setup your QuickBooks integration.</p>
                    </div>
                </div>
                <div class="box-content-display pull-right padding-10 api-integration-btn">
                    <a type="button" class="btn btn-primary ng-scope padding-10 qb-btn-width margin-bottom-10" href="">+ Add QuickBooks Desktop</a>
                    <a type="button" class="btn btn-primary ng-scope padding-10 qb-btn-width" href="">+ Add QuickBooks Online</a>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
        <div class="col-md-12 col-lg-12 col-xl">
            <div class="box box-box-primary">
                <div class="box-content-display quick-book-width">
                    <div class="box-content-display padding-10">
                        <img alt="" class="api-key-logo" src="{{asset('user/assets/images/dropbox-small.jpg')}}">
                    </div>
                    <div class="padding-10 box-content-display quick-book-content-width">
                        <p> Brokerblox and Dropbox are integrated to allow you to sync your agent and transaction documents to Dropbox. </p>
                    </div>
                </div>
                <div class="box-content-display pull-right padding-10 api-integration-btn">
                    <a type="button" class="btn btn-primary ng-scope padding-10 qb-btn-width margin-bottom-10" href="">+ Add Dropbox Integration</a>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
        <div class="col-md-12 col-lg-12 col-xl">
            <div class="box box-box-primary">
                <div class="box-content-display quick-book-width">
                    <div class="box-content-display padding-10">
                        <img alt="" class="api-key-logo" src="{{asset('user/assets/images/google-drive.png')}}">
                    </div>
                    <div class="padding-10 box-content-display quick-book-content-width">
                        <p> Brokerblox and Google Drive are integrated to allow you to sync your agent and transaction documents to Google Drive. </p>
                    </div>
                </div>
                <div class="box-content-display pull-right padding-10 api-integration-btn">
                    <a type="button" class="btn btn-primary ng-scope padding-10 qb-btn-width margin-bottom-10" href="">+ Add Google Drive Integration</a>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
        <div class="col-md-12 col-lg-12 col-xl">
            <div class="box box-box-primary">
                <div class="box-content-display quick-book-width">
                    <div class="box-content-display padding-10">
                        <img alt="" class="api-key-logo" src="{{asset('user/assets/images/zoho-reports-small.jpg')}}">
                    </div>
                    <div class="padding-10 box-content-display quick-book-content-width"><p>
                            Brokerblox and Zoho are integrated to allow you to sync your BrokerSumo data to Zoho for easy accounting.</p>
                        <p class="cursor text-primary">Click here to see the setup instructions.</p>
                    </div>
                </div>
                <div class="box-content-display pull-right padding-10 api-integration-btn">
                    <a type="button" class="btn btn-primary ng-scope padding-10 qb-btn-width margin-bottom-10" href="">+ Add Zoho Integration</a>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
        <div class="col-md-12 col-lg-12 col-xl">
            <div class="box box-box-primary">
                <div class="box-content-display quick-book-width">
                    <div class="box-content-display padding-10">
                        <img alt="" class="api-key-logo" src="{{asset('user/assets/images/dtr.jpg')}}">
                    </div>
                    <div class="padding-10 box-content-display quick-book-content-width">
                        <p> Sync your transactions into BrokerBlox to eliminate double entry of transaction data. </p>
                        <p class="cursor text-primary">Click here to watch the setup video.</p>
                    </div>
                </div>
                <div class="box-content-display pull-right padding-10 api-integration-btn">
                    <a type="button" class="btn btn-primary ng-scope padding-10 qb-btn-width margin-bottom-10" href="">+ Add DocuSign Transaction Room</a>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
        <div class="col-md-12 col-lg-12 col-xl">
            <div class="box box-box-primary">
                <div class="box-content-display quick-book-width">
                    <div class="box-content-display padding-10">
                        <img alt="" class="api-key-logo" src="{{asset('user/assets/images/dot_loop_logo.png')}}">
                    </div>
                    <div class="padding-10 box-content-display quick-book-content-width">
                        <p> Sync your loops into BrokerBlox to eliminate double entry of loop data. </p>
                        <p class="cursor text-primary">Click here to watch the setup video.</p>
                    </div>
                </div>
                <div class="box-content-display pull-right padding-10 api-integration-btn">
                    <a type="button" class="btn btn-primary ng-scope padding-10 qb-btn-width margin-bottom-10" href="">Dotloop Integration</a>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
        <div class="col-md-12 col-lg-12 col-xl">
            <div class="box box-box-primary">
                <div class="box-content-display quick-book-width">
                    <div class="box-content-display padding-10">
                        <img alt="" class="api-key-logo" src="{{asset('user/assets/images/sky-slope.jpg')}}">
                    </div>
                    <div class="padding-10 box-content-display quick-book-content-width">
                        <p>Sync your transactions into BrokerBlox to eliminate double entry of transaction data.</p>
                    </div>
                </div>
                <div class="box-content-display pull-right padding-10 api-integration-btn">
                    <a type="button" class="btn btn-primary ng-scope padding-10 qb-btn-width margin-bottom-10" href="">Edit SkySlope Setup</a>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
        <div class="col-md-12 col-lg-12 col-xl">
            <div class="box box-box-primary">
                <div class="box-content-display quick-book-width">
                    <div class="box-content-display padding-10">
                        <img alt="" class="api-key-logo" src="{{asset('user/assets/images/ez_coordinate_logo.png')}}">
                    </div>
                    <div class="padding-10 box-content-display quick-book-content-width">
                        <p>Your API key can be used to integrate with the products listed below.</p>
                        <p>EZ Coordinator is a transaction management product.Sync your transaction data from EZ Coordinator for more efficient closings.</p>
                    </div>
                </div>
                <div class="box-content-display pull-right padding-10 api-integration-btn">
                    <a type="button" class="btn btn-primary ng-scope padding-10 qb-btn-width margin-bottom-10" href="">Generate API Key</a>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
        <div class="col-md-12 col-lg-12 col-xl">
            <div class="box box-box-primary">
                <div class="box-content-display quick-book-width">
                    <div class="box-content-display padding-10">
                        <img alt="" class="api-key-logo" src="{{asset('user/assets/images/realogy.png')}}">
                    </div>
                    <div class="padding-10 box-content-display quick-book-content-width">
                        <p>Sync your Realogy offices into BrokerSumo.</p>
                        <p>Also, sync your BrokerSumo agents and transactions into Realogy to eliminate double entry of data.</p>
                    </div>
                </div>
                <div class="box-content-display pull-right padding-10 api-integration-btn">
                    <a type="button" class="btn btn-primary ng-scope padding-10 qb-btn-width margin-bottom-10" href="">Realogy Integration</a>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
        <div class="col-md-12 col-lg-12 col-xl">
            <div class="box box-box-primary">
                <div class="box-content-display quick-book-width">
                    <div class="box-content-display padding-10">
                        <img alt="" class="api-key-logo" src="{{asset('user/assets/images/docusing_logo.png')}}">
                    </div>
                    <div class="padding-10 box-content-display quick-book-content-width">
                        <p>Docusign for commission disbursement approvals</p>
                    </div>
                </div>
                <div class="box-content-display pull-right padding-10 api-integration-btn">
                    <a type="button" class="btn btn-primary ng-scope padding-10 qb-btn-width margin-bottom-10" href="">Realogy Integration</a>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
@endsection

@push('js')
@endpush
