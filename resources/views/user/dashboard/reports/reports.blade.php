@extends('user.layouts.master')

@push('css')
    <style>
        .padding-bottom-20{
            padding-bottom: 20px;
        }
    </style>
@endpush

@section('content')
    <div class="header">
        <div class="container-fluid">
            <div class="header-body">
                <div class="row align-items-end">
                    <div class="col">
                        <h1 class="header-title">
                            Reports
                        </h1>
                    </div>
                    <div class="col-auto">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 col-lg-12 col-xl">
                <div class="card">
                    <div class="card-body">
                        <div class="row align-items-center gx-0">
                            <div class="col-lg-12 col-md-12 row">
                                <div class="col-lg-6 col-md-6 padding-bottom-20">
                                    <a href="#"> <strong>Agent
                                            Billings</strong>
                                    </a>
                                    <p>An overview of all your agent billings.</p>
                                </div>
                                <div class="col-lg-6 col-md-6 padding-bottom-20">
                                    <a href="#"> <strong>Agent
                                            Billing Overview Report</strong>
                                    </a>
                                    <p>An overview of all the billings set up for each of your
                                        agents.</p>
                                </div>
                                <div class="col-lg-6 col-md-6 padding-bottom-20">
                                    <a href="#"> <strong>Agent
                                            Commission</strong>
                                    </a>
                                    <p>An overview of your agent's closed transactions, sales
                                        volume and net commissions.</p>
                                </div>
                                <div class="col-lg-6 col-md-6 padding-bottom-20">
                                    <a href="#"> <strong>Additional
                                            Closing Fees</strong>
                                    </a>
                                    <p>An itemized breakdown of all additional closing fees from
                                        your commission disbursements.</p>
                                </div>
                                <div class="col-lg-6 col-md-6 padding-bottom-20">
                                    <a href="#"> <strong>Agent Fees
                                            Report</strong>
                                    </a>
                                    <p></p>
                                </div>
                                <div class="col-lg-6 col-md-6 padding-bottom-20">
                                    <a href="#"> <strong>Agent
                                            Lead Conversion</strong>
                                    </a>
                                    <p>Shows how much an agent spent on each lead source, how many
                                        transactions they closed, commission earned from those lead
                                        sources, and the agent's cost per acquisition.</p>
                                </div>
                                <div class="col-lg-6 col-md-6 padding-bottom-20">
                                    <a href="#"> <strong>Agent
                                            Overview Report</strong>
                                    </a>
                                    <p>A report showing your agent roster and your agent's
                                        information.</p>
                                </div>
                                <div class="col-lg-6 col-md-6 padding-bottom-20">
                                    <a href="#"> <strong>Agent
                                            Monthly Report</strong>
                                    </a>
                                    <p>A month-over-month report showing your agent's sales totals.</p>
                                </div>
                                <div class="col-lg-6 col-md-6 padding-bottom-20">
                                    <a href="#"> <strong>Agent 1099s
                                            Report</strong>
                                        <p></p>
                                    </a>
                                </div>
                                <div class="col-lg-6 col-md-6 padding-bottom-20">
                                    <a href="#"> <strong>Agent Value</strong>
                                    </a>
                                    <p>An overview of all fees and net commissions collected from
                                        each agent.</p>
                                </div>
                                <div class="col-lg-6 col-md-6 padding-bottom-20">
                                    <a href="#"> <strong>Cap</strong>
                                    </a>
                                    <p>Shows agents commission volume from their rollover date
                                        along with their cap so you can see how your agents are
                                        performing.</p>
                                </div>
                                <div class="col-lg-6 col-md-6 padding-bottom-20">
                                    <a href="#"> <strong>Cap Plan Report</strong>
                                    </a>
                                    <p>Shows agents cap amount and how much they've paid towards
                                        their cap since their rollover date.</p>
                                </div>
                                <div class="col-lg-6 col-md-6 padding-bottom-20">
                                    <a href="#"> <strong>Cap On Fees
                                            Report</strong>
                                    </a>
                                    <p>Shows how much the agent has contributed towards a cap on
                                        fees for their commission plans or individual cap on fees from
                                        their agent profile.</p>
                                </div>
                                <div class="col-lg-6 col-md-6 padding-bottom-20">
                                    <a href="#"> <strong>Commission
                                            Detail Report</strong>
                                    </a>
                                    <p class="ng-scope">Shows all closed deals,
                                        their gross and net commissions and all credits and debits. The
                                        agent version gives agent's insight into their total amount
                                        paid to the brokerage or towards their cap. To turn on this report
                                        for your agents go to Settings &gt; Agent Permissions.</p>
                                </div>
                                <div class="col-lg-6 col-md-6 padding-bottom-20">
                                    <a href="#"> <strong>Escrow
                                            Company Report</strong>
                                    </a>
                                    <p>Shows you the escrow companies you've used to close
                                        transactions.</p>
                                </div>
                                <div class="col-lg-6 col-md-6 padding-bottom-20">
                                    <a href="#"> <strong>Hotsheet Report</strong>
                                    </a>
                                    <p>Shows your new listings, accepted contracts, closed
                                        transactions, expired listings and withdrawn listings.</p>
                                </div>
                                <div class="col-lg-6 col-md-6 padding-bottom-20">
                                    <a href="#"> <strong>Lead
                                            Conversion</strong>
                                    </a>
                                    <p>Shows how much you spent on each lead source, how many
                                        transactions you closed and commission earned from those lead
                                        sources and your cost per acquisition.</p>
                                </div>
                                <div class="col-lg-6 col-md-6 padding-bottom-20">
                                    <a href="#"> <strong>Office
                                            Commission</strong>
                                    </a>
                                    <p>An overview of your office's closed sales transactions,
                                        sales volume and gross and net office commissions.</p>
                                </div>
                                <div class="col-lg-6 col-md-6 padding-bottom-20">
                                    <a href="#"> <strong>Office
                                            Commission Breakdown</strong>
                                    </a>
                                    <p>An overview of your office's closed sales transactions,
                                        sales volume and gross and net office commissions - broken out by
                                        office.</p>
                                </div>
                                <div class="col-lg-6 col-md-6 padding-bottom-20">
                                    <a href="#"> <strong>Office
                                            Commission Revisions</strong>
                                    </a>
                                    <p>An overview of your office's closed sales transactions,
                                        sales volume and gross and net office commissions.</p>
                                </div>
                                <div class="col-lg-6 col-md-6 padding-bottom-20">
                                    <a href="#"> <strong>Pipeline</strong>
                                    </a>
                                    <p>Shows all pending and closed transactions, and their current
                                        closing status giving you an overview of how many deals you've
                                        closed and what your pending commission pipeline looks like for
                                        the future.</p>
                                </div>
                                <div class="col-lg-6 col-md-6 padding-bottom-20">
                                    <a href="#"> <strong>Profit and Loss
                                            Statement</strong>
                                    </a>
                                    <p>
                                        Show money you earned (income) and money you spent (expenses) so
                                        you can see how profitable you are. Also called an income
                                        statement.<br> <br>
                                    </p>
                                </div>
                                <div class="col-lg-6 col-md-6 padding-bottom-20">
                                    <a href="#"> <strong>Post
                                            Commission Report</strong>
                                    </a>
                                </div>
                                <div class="col-lg-6 col-md-6 padding-bottom-20">
                                    <a href="#"> <strong><span class="ng-binding">Rental</span>
                                            Office Commission Breakdown</strong>
                                    </a>
                                    <p>An overview of your office's closed rental transactions and
                                        gross and net office commissions - broken out by office.</p>
                                </div>
                                <div class="col-lg-6 col-md-6 padding-bottom-20">
                                    <a href="#"> <strong><span class="ng-binding">Rental</span>
                                            Office Commission</strong>
                                    </a>
                                    <p>
                                        An overview of your office's closed rental transactions and gross
                                        and net office commissions.<br> <br>
                                    </p>
                                </div>
                                <div class="col-lg-6 col-md-6 padding-bottom-20">
                                    <a href="#"> <strong>Referral
                                            Office Commission</strong>
                                    </a>
                                    <p>An overview of your office's closed referral transactions
                                        and gross and net office commissions.</p>
                                </div>
                                <div class="col-lg-6 col-md-6 padding-bottom-20">
                                    <a href="#"> <strong>Referral
                                            Office Commission Breakdown</strong>
                                    </a>
                                    <p>An overview of your office's closed referral transactions
                                        and gross and net office commissions - broken out by office.</p>
                                </div>
                                <div class="col-lg-6 col-md-6 padding-bottom-20">
                                    <a href="#"> <strong>Sliding Scale Plan Report</strong>
                                    </a>
                                    <p>Shows agents amount and how much they've paid towards their
                                        level since their rollover date.</p>
                                </div>
                                <div class="col-lg-6 col-md-6 padding-bottom-20">
                                    <a href="#">
                                        <strong>Sales Summary Report</strong>
                                    </a>
                                    <p>Shows an average of your offices sales volume and
                                        commissions by month.</p>
                                </div>
                                <div class="col-lg-6 col-md-6 padding-bottom-20">
                                    <a href="#"> <strong>Title/Attorney
                                            Report</strong>
                                    </a>
                                    <p>Shows you the title companies/attorneys you've used to close
                                        transactions.</p>
                                </div>
                                <div class="col-lg-6 col-md-6 padding-bottom-20">
                                    <a href="#"> <strong>Transaction
                                            Reconciliation</strong>
                                    </a>
                                    <p>Shows your commission disbursement net income vs. your
                                        actual net income (from your bank accounts) so you can reconcile
                                        the data to make sure they match up.</p>
                                </div>
                                <div class="col-lg-6 col-md-6 padding-bottom-20">
                                    <a href="#"> <strong>Vendors Detail</strong>
                                    </a>
                                    <p>An itemized breakdown of all credits/debits from your
                                        commission disbursements.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('js')
@endpush
