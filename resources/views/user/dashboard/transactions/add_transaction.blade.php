@extends('user.layouts.master')

@push('css')
    <style>
        .label-margin-right{
            margin-right: 8px;
        }
        .label-margin-bottom{
            margin-bottom: 10px;
        }
        .form-group-row label{
            font-weight: bold;
        }
        .input_container label{
            font-weight: normal;
        }
        .TdInput{
            border: none;
            width: 120%;
            outline: none;
        }
    </style>
@endpush

@section('content')
    <div class="header">
        <div class="container-fluid">
            <div class="header-body">
                <div class="row align-items-end">
                    <div class="col">
                        <h1 class="header-title">
                            Transactions / Add Transaction
                        </h1>
                    </div>
                    <div class="col-auto">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid AddTransactionPage">
        <div class="row">
            <div class="col-lg-12 col-md-6 col-xl">
                @if(session()->has('status'))
                    <div class="alert alert-success">
                        <h3>{{ session('status') }}</h3>
                    </div>
                @endif
                <div class="card">
                    <div class="card-header">
                        <h4>Step 1 of 2: Add Transaction Info</h4>
                        <p>Fill out the transaction and property information below.</p>
                    </div>
                    <form action="{{ route('saveTransactionInfo') }}" method="post">
                        @csrf
                        <div class="card-body row">
                            <div class="col-md-6">
                                <div class="card">
                                    <div class="card-header">
                                        <h4>Transaction Info</h4>
                                    </div>
                                    <div class="card-body">
                                        <div class="form-group">
                                            <label for="inputName" class="label-margin-bottom">Transaction Type:</label>
                                            <div class="input_container chk-box">
                                                <div class="checkbox icheck checkBoxList">
                                                    <label class="label-margin-right">
                                                        <input class="form-check-input list-checkbox listingCheckbox TrTypeVal" value="Listing" toggle="listing-box" hide-box="hide-by-listing" id="listCheckboxOne" type="checkbox">
                                                        Listing
                                                    </label>
                                                    <label class="label-margin-right">
                                                        <input class="form-check-input list-checkbox sellingCheckbox TrTypeVal" value="Selling" toggle="selling-box" hide-box="hide-by-selling"  id="listCheckboxOne" type="checkbox">
                                                        Selling
                                                    </label>
                                                    <label class="label-margin-right">
                                                        <input class="form-check-input list-checkbox dualCheckbox TrTypeVal" value="Dual(Both)" toggle="dual-box" hide-box="hide-by-dual" id="listCheckboxOne" type="checkbox">
                                                        Dual (Both)
                                                    </label>
                                                    <label class="label-margin-right">
                                                        <input class="form-check-input list-checkbox rentalCheckbox TrTypeVal" value="Rental" toggle="rental-box" hide-box="hide-by-rental" id="listCheckboxOne" type="checkbox">
                                                        Rental
                                                    </label>
                                                    <label class="label-margin-right">
                                                        <input class="form-check-input list-checkbox referralCheckbox TrTypeVal" value="Referral" toggle="referral-box" hide-box="hide-by-referral" id="listCheckboxOne" type="checkbox">
                                                        Referral
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group mt-5 form-group-row">
                                            <label class="w-25 mt-2">Transaction Status:</label>
                                            <div class="input_container select_container w-75">
                                                <select class="form-select" name="transaction_statue" data-choices>
                                                    <option>Active</option>
                                                    <option>Pending/Under Contract</option>
                                                    <option>Closed</option>
                                                    <option>Cancelled</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group form-group-row RentalAmount Tbox rental-box" style="display: none;">
                                            <label class="w-25 mt-2">Rental Amount:</label>
                                            <div class="input_container select_container w-75">
                                                <input type="text" name="rental_amount" class="input-group form-control">
                                            </div>
                                        </div>
                                        <div class="form-group form-group-row RentalTermNo Tbox rental-box" style="display: none;">
                                            <label class="w-25 mt-2">Rental Term # of months.</label>
                                            <div class="input_container select_container w-75">
                                                <input type="text" name="rental_term_of_months" class="input-group form-control">
                                            </div>
                                        </div>
                                        <div class="form-group form-group-row TotalRentalAmount Tbox rental-box" style="display: none;">
                                            <label class="w-25 mt-2">Total Rental Amount</label>
                                            <div class="input_container select_container w-75">
                                                <p>$0.00</p>
                                            </div>
                                        </div>
                                        <div class="form-group form-group-row LeaseStartDate Tbox rental-box" style="display: none;">
                                            <label class="w-25 mt-2">Lease Start Date:</label>
                                            <div class="input_container select_container w-75">
                                                <input type="date" name="lease_start_date" class="input-group form-control">
                                            </div>
                                        </div>
                                        <div class="form-group form-group-row MoveInDate Tbox rental-box" style="display: none;">
                                            <label class="w-25 mt-2">Move in Date:</label>
                                            <div class="input_container select_container w-75">
                                                <input type="date" name="move_in_date" class="input-group form-control">
                                            </div>
                                        </div>
                                        <div class="form-group form-group-row SalePrice hide-by-rental">
                                            <label class="w-25 mt-2">Sales Price:</label>
                                            <div class="input_container select_container w-75">
                                                <input type="text" name="sales_price" class="input-group form-control">
                                            </div>
                                        </div>
                                        <div class="form-group form-group-row CloseDate hide-by-rental">
                                            <label class="w-25 mt-2">Close Date:</label>
                                            <div class="input_container select_container w-75">
                                                <input type="date" name="close_date" class="input-group form-control">
                                            </div>
                                        </div>
                                        <div class="form-group form-group-row ListingLeadSource Tbox dual-box" style="display: none;">
                                            <label class="w-25 mt-2">Listing Lead Source:</label>
                                            <div class="input_container select_container w-75">
                                                <select class="form-select" name="listing_lead_source" disabled="disabled" data-choices>
                                                    <option>No Result Match</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group form-group-row SellingLeadSource Tbox dual-box" style="display: none;">
                                            <label class="w-25 mt-2">Selling Lead Source:</label>
                                            <div class="input_container select_container w-75">
                                                <select class="form-select" name="selling_lead_source" disabled="disabled" data-choices>
                                                    <option>No Result Match</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group form-group-row LeadSource hide-by-dual rental-box">
                                            <label class="w-25 mt-2">Lead Source:</label>
                                            <div class="input_container select_container w-75">
                                                <select class="form-select" name="lead_source" disabled="disabled" data-choices>
                                                    <option>No Result Match</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group form-group-row WhoYouRepresent Tbox rental-box" style="display: none">
                                            <label class="w-25 mt-2">Who do you represent?:</label>
                                            <div class="input_container select_container w-75">
                                                <select class="form-select" name="who_do_you_represent" data-choices>
                                                    <option>None</option>
                                                    <option>Tenant</option>
                                                    <option>Landlord</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group form-group-row ListingCommission hide-by-rental hide-by-referral">
                                            <label class="w-25 mt-5">Listing Commission:</label>
                                            <div class="input_container select_container w-75">
                                                <label>%</label>
                                                <input type="text" name="listing_commission_percentage" class="input-group form-control">
                                                <label>Or Flat Fee $</label>
                                                <input type="text" name="listing_commission_flat_fee" class="input-group form-control">
                                            </div>
                                        </div>
                                        <div class="form-group form-group-row LeasingCommission Tbox rental-box" style="display: none;">
                                            <label class="w-25 mt-5">Leasing Commission:</label>
                                            <div class="input_container select_container w-75">
                                                <label>%</label>
                                                <input type="text" name="leasing_commission_percentage" class="input-group form-control">
                                                <label>Or Flat Fee $</label>
                                                <input type="text" name="leasing_commission_flat_fee" class="input-group form-control">
                                            </div>
                                        </div>
                                        <div class="form-group form-group-row SellingCommission hide-by-rental hide-by-referral">
                                            <label class="w-25 mt-5">Selling Commission:</label>
                                            <div class="input_container select_container w-75">
                                                <label>%</label>
                                                <input type="text" name="selling_commission_percentage" class="input-group form-control">
                                                <label>Or Flat Fee $</label>
                                                <input type="text" name="selling_commission_flat_fee" class="input-group form-control">
                                            </div>
                                        </div>
                                        <div class="form-group form-group-row ReferralCommission Tbox referral-box" style="display: none">
                                            <label class="w-25 mt-5">Referral Commission:</label>
                                            <div class="input_container select_container w-75">
                                                <label>%</label>
                                                <input type="text" name="referral_commission_percentage" class="input-group form-control">
                                                <label>Or Flat Fee $</label>
                                                <input type="text" name="referral_commission_flat_fee" class="input-group form-control">
                                            </div>
                                        </div>
                                        <div class="form-group form-group-row OutsideReferralCompany hide-by-dual">
                                            <label class="w-25">Outside Referral Company:</label>
                                            <div class="input_container select_container w-75">
                                                <input type="text" name="outside_referral_company" class="input-group form-control">
                                            </div>
                                        </div>
                                        <div class="form-group form-group-row ReferralCompanyListing Tbox dual-box" style="display: none">
                                            <label class="w-25">Outside Referral Company(Listing):</label>
                                            <div class="input_container select_container w-75">
                                                <input type="text" name="outside_referral_company_listing" class="input-group form-control">
                                            </div>
                                        </div>
                                        <div class="form-group form-group-row">
                                            <label class="w-25 mt-2">Outside Referral Agent :</label>
                                            <div class="input_container select_container w-75">
                                                <input type="text" name="outside_referral_agent" placeholder="(Optional)" class="input-group form-control">
                                            </div>
                                        </div>
                                        <div class="form-group form-group-row OutsideReferralCommission Tbox" style="display: none">
                                            <label class="w-25 mt-5">Outside Referral Commission:</label>
                                            <div class="input_container select_container w-75">
                                                <label>%</label>
                                                <input type="text" name="outside_referral_commission_percentage" class="input-group form-control">
                                                <label>Or Flat Fee $</label>
                                                <input type="text" name="outside_referral_commission_flat_fee" class="input-group form-control">
                                            </div>
                                        </div>
                                        <div class="form-group form-group-row calcAgentCommission Tbox" style="display: none">
                                            <label class="w-25 mt-2">Calculate agent's commission before or after this item?:</label>
                                            <div class="input_container select_container w-75 mt-5">
                                                <select class="form-select" name="calculate_agent_commission" data-choices>
                                                    <option>Before</option>
                                                    <option>After</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group form-group-row ReferralCompanySelling Tbox dual-box" style="display: none;">
                                            <label class="w-25">Outside Referral Company(Selling):</label>
                                            <div class="input_container select_container w-75">
                                                <input type="text" name="" class="input-group form-control">
                                            </div>
                                        </div>
                                        <div class="form-group form-group-row ReferralAgentSelling Tbox dual-box" style="display: none;">
                                            <label class="w-25">Outside Referral Agent(Selling):</label>
                                            <div class="input_container select_container w-75">
                                                <input type="text" name="outside_referral_company_selling" placeholder="(Optional)" class="input-group form-control">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="card">
                                    <div class="card-header">
                                        <h4>Property Info</h4>
                                    </div>
                                    <div class="card-body">
                                        <div class="form-group">
                                            <div class="input_container chk-box">
                                                <div class="checkbox checkBoxList">
                                                    <label class="label-margin-right">
                                                        <input class="form-check-input list-checkbox completeAddressCheckbox" id="listCheckboxOne" type="checkbox">
                                                        Add Complete Address
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group mt-5 form-group-row houseNumber" style="display: none;">
                                            <label class="w-25 mt-2">House Number:</label>
                                            <div class="input_container select_container w-75">
                                                <input type="text" name="house_number" class="input-group form-control">
                                            </div>
                                        </div>
                                        <div class="form-group mt-2 form-group-row streetName" style="display: none;">
                                            <label class="w-25 mt-2">Street Name:</label>
                                            <div class="input_container select_container w-75">
                                                <input type="text" name="street_name" class="input-group form-control">
                                            </div>
                                        </div>
                                        <div class="form-group mt-2 form-group-row unitNumber" style="display: none;">
                                            <label class="w-25 mt-2">Unit Number:</label>
                                            <div class="input_container select_container w-75">
                                                <input type="text" name="unit_number" class="input-group form-control">
                                            </div>
                                        </div>
                                        <div class="form-group mt-2 form-group-row">
                                            <label class="w-25 mt-2">Address:</label>
                                            <div class="input_container select_container w-75">
                                                <input type="text" name="property_address" class="input-group form-control propertyAddress">
                                            </div>
                                        </div>
                                        <div class="form-group form-group-row">
                                            <label class="w-25 mt-2">Country:</label>
                                            <div class="input_container select_container w-75">
                                                <input type="text" name="country" class="input-group form-control">
                                            </div>
                                        </div>
                                        <div class="form-group form-group-row">
                                            <label class="w-25 mt-2">Zip/Postal Code:</label>
                                            <div class="input_container select_container w-75">
                                                <input type="text" name="property_postal" class="input-group form-control">
                                            </div>
                                        </div>
                                        <div class="form-group form-group-row">
                                            <label class="w-25 mt-2">City:</label>
                                            <div class="input_container select_container w-75">
                                                <input type="text" name="city" class="input-group form-control">
                                            </div>
                                        </div>
                                        <div class="form-group form-group-row">
                                            <label class="w-25 mt-2">State/Province:</label>
                                            <div class="input_container select_container w-75">
                                                <input type="text" name="state" class="input-group form-control">
                                            </div>
                                        </div>
                                        <div class="form-group form-group-row rental-box Tbox LandloardName" style="display: none;">
                                            <label class="w-25 mt-2">Landlord Name:</label>
                                            <div class="input_container select_container w-75">
                                                <input type="text" name="landloard_name" placeholder="optional" class="input-group form-control">
                                            </div>
                                        </div>
                                        <div class="form-group form-group-row">
                                            <label class="w-25 mt-2">MLS:</label>
                                            <div class="input_container select_container w-75">
                                                <input type="text" name="mls" placeholder="optional" class="input-group form-control">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!-- Buyer Info Table -->
                            <div class="col-md-6 hide-by-rental">
                                <div class="card">
                                    <div class="card-header">
                                        <h4>Buyer's Info
                                            <a href="" data-bs-toggle="modal" data-bs-target="#addBuyerInfo" class="pull-right">+ Add Buyer Info</a>
                                        </h4>
                                    </div>
                                    <div class="card-body">
                                        <div class="table-responsive">
                                            <table class="table table-sm BuyerInfoTable" id="BuyerInfo">
                                                <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>Name</th>
                                                    <th>Email</th>
                                                    <th>Phone</th>
                                                    <th>Action</th>
                                                </tr>
                                                </thead>
                                                <tbody class="list BuyerInfo">
                                                <tr>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Add Buyer Info Modal -->
                            <div class="modal fade" id="addBuyerInfo" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h4 class="modal-title" id="staticBackdropLabel">Add Buyer Info</h4>
                                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                        </div>
                                        <div class="modal-body adjusted-gross-level">
                                            <div class="col-md-12">
                                                <div class="form-group-row">
                                                    <label class="w-25">Item Name:</label>
                                                    <input type="text" placeholder="" class="form-control w-75 buyerItemName">
                                                </div>
                                                <div class="form-group-row mt-3">
                                                    <label class="w-25">Email Address:</label>
                                                    <input type="email" placeholder="Email Address" class="form-control w-75 buyerEmail">
                                                </div>
                                                <div class="form-group-row mt-3">
                                                    <label class="w-25">Phone:</label>
                                                    <input type="number" placeholder="(___)-___-___" class="form-control w-75 buyerPhone">
                                                </div>
                                                <div class="form-group-row mt-3">
                                                    <label class="w-25">Zip/Postal Code:</label>
                                                    <input type="text" placeholder="" class="form-control w-75 buyerPostalCode">
                                                </div>
                                                <div class="form-group-row mt-3">
                                                    <label class="w-25">Address:</label>
                                                    <input type="text" placeholder="" class="form-control w-75 buyerAddress">
                                                </div>
                                                <div class="form-group-row mt-3">
                                                    <label class="w-25">City:</label>
                                                    <input type="text" placeholder="" class="form-control w-75 buyerCity">
                                                </div>
                                                <div class="form-group-row mt-3">
                                                    <label class="w-25">Country:</label>
                                                    <input type="text" placeholder="" class="form-control w-75 buyerCountry">
                                                </div>
                                                <div class="form-group-row mt-3">
                                                    <label class="w-25">State/Province:</label>
                                                    <input type="text" placeholder="" class="form-control w-75 buyerState">
                                                </div>
                                                <div class="form-group-row mt-3">
                                                    <label class="w-25">Buyer Attorney:</label>
                                                    <input type="text" placeholder="" class="form-control w-75 buyerAttorney">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancel</button>
                                            <button type="button" class="btn btn-primary saveBuyerInfo">Save</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Edit Buyer Info Modal -->
                            <div class="modal fade" id="editBuyerInfoModal" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h4 class="modal-title" id="staticBackdropLabel">Edit Buyer Info</h4>
                                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                        </div>
                                        <div class="modal-body adjusted-gross-level">
                                            <div class="col-md-12">
                                                <input type="text" class="hiddenBuyerId" style="display: none">
                                                <div class="form-group-row">
                                                    <label class="w-25">Item Name:</label>
                                                    <input type="text" placeholder="" class="form-control w-75 EditBuyerItem">
                                                </div>
                                                <div class="form-group-row mt-3">
                                                    <label class="w-25">Email Address:</label>
                                                    <input type="text" placeholder="Email Address" class="form-control w-75 EditBuyerEmail">
                                                </div>
                                                <div class="form-group-row mt-3">
                                                    <label class="w-25">Phone:</label>
                                                    <input type="number" placeholder="(___)-___-___" class="form-control w-75 EditBuyerPhone">
                                                </div>
                                                <div class="form-group-row mt-3">
                                                    <label class="w-25">Zip/Postal Code:</label>
                                                    <input type="text" placeholder="" class="form-control w-75 EditBuyerPostal">
                                                </div>
                                                <div class="form-group-row mt-3">
                                                    <label class="w-25">Address:</label>
                                                    <input type="text" placeholder="" class="form-control w-75 EditBuyerAddress">
                                                </div>
                                                <div class="form-group-row mt-3">
                                                    <label class="w-25">City:</label>
                                                    <input type="text" placeholder="" class="form-control w-75 EditBuyerCity">
                                                </div>
                                                <div class="form-group-row mt-3">
                                                    <label class="w-25">Country:</label>
                                                    <input type="text" placeholder="" class="form-control w-75 EditBuyerCountry">
                                                </div>
                                                <div class="form-group-row mt-3">
                                                    <label class="w-25">State/Province:</label>
                                                    <input type="text" placeholder="" class="form-control w-75 EditBuyerState">
                                                </div>
                                                <div class="form-group-row mt-3">
                                                    <label class="w-25">Buyer Attorney:</label>
                                                    <input type="text" placeholder="" class="form-control w-75 EditBuyerAttorney">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancel</button>
                                            <button type="button" class="btn btn-primary UpdateBuyerInfo">Update</button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!-- Seller Info Table -->
                            <div class="col-md-6 hide-by-rental">
                                <div class="card">
                                    <div class="card-header">
                                        <h4>Seller's Info
                                            <a href="" data-bs-toggle="modal" data-bs-target="#addSellerInfo" class="pull-right">+ Add Seller Info</a>
                                        </h4>
                                    </div>
                                    <div class="card-body">
                                        <div class="table-responsive">
                                            <table class="table table-sm SellerInfoTable" id="SellerInfo">
                                                <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>Name</th>
                                                    <th>Email</th>
                                                    <th>Phone</th>
                                                    <th>Action</th>
                                                </tr>
                                                </thead>
                                                <tbody class="list SellerInfo">
                                                <tr>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Add Seller Info Modal -->
                            <div class="modal fade" id="addSellerInfo" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h4 class="modal-title" id="staticBackdropLabel">Add Seller Info</h4>
                                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                        </div>
                                        <div class="modal-body adjusted-gross-level">
                                            <div class="col-md-12">
                                                <div class="form-group-row">
                                                    <label class="w-25">Name:</label>
                                                    <input type="text" placeholder="" class="form-control w-75 sellerName">
                                                </div>
                                                <div class="form-group-row mt-3">
                                                    <label class="w-25">Email Address:</label>
                                                    <input type="text" placeholder="Email Address" class="form-control w-75 sellerEmail">
                                                </div>
                                                <div class="form-group-row mt-3">
                                                    <label class="w-25">Phone:</label>
                                                    <input type="text" placeholder="(___)-___-___" class="form-control w-75 sellerPhone">
                                                </div>
                                                <div class="form-group-row mt-3">
                                                    <label class="w-25">Zip/Postal Code:</label>
                                                    <input type="text" placeholder="" class="form-control w-75 sellerPostal">
                                                </div>
                                                <div class="form-group-row mt-3">
                                                    <label class="w-25">Address:</label>
                                                    <input type="text" placeholder="" class="form-control w-75 sellerAddress">
                                                </div>
                                                <div class="form-group-row mt-3">
                                                    <label class="w-25">City:</label>
                                                    <input type="text" placeholder="" class="form-control w-75 sellerCity">
                                                </div>
                                                <div class="form-group-row mt-3">
                                                    <label class="w-25">Country:</label>
                                                    <input type="text" placeholder="" class="form-control w-75 sellerCountry">
                                                </div>
                                                <div class="form-group-row mt-3">
                                                    <label class="w-25">State/Province:</label>
                                                    <input type="text" placeholder="" class="form-control w-75 sellerState">
                                                </div>
                                                <div class="form-group-row mt-3">
                                                    <label class="w-25">Seller Attorney:</label>
                                                    <input type="text" placeholder="" class="form-control w-75 sellerAttorney">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancel</button>
                                            <button type="button" class="btn btn-primary saveSellerInfo">Save</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Edit Seller Info Modal -->
                            <div class="modal fade" id="editSellerInfo" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h4 class="modal-title" id="staticBackdropLabel">Edit Seller Info</h4>
                                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                        </div>
                                        <div class="modal-body adjusted-gross-level">
                                            <div class="col-md-12">
                                                <input type="text" class="hiddenSellerId" style="display: none">
                                                <div class="form-group-row">
                                                    <label class="w-25">Name:</label>
                                                    <input type="text" placeholder="" class="form-control w-75 editSellerName">
                                                </div>
                                                <div class="form-group-row mt-3">
                                                    <label class="w-25">Email Address:</label>
                                                    <input type="text" placeholder="Email Address" class="form-control w-75 editSellerEmail">
                                                </div>
                                                <div class="form-group-row mt-3">
                                                    <label class="w-25">Phone:</label>
                                                    <input type="text" placeholder="(___)-___-___" class="form-control w-75 editSellerPhone">
                                                </div>
                                                <div class="form-group-row mt-3">
                                                    <label class="w-25">Zip/Postal Code:</label>
                                                    <input type="text" placeholder="" class="form-control w-75 editSellerPostal">
                                                </div>
                                                <div class="form-group-row mt-3">
                                                    <label class="w-25">Address:</label>
                                                    <input type="text" placeholder="" class="form-control w-75 editSellerAddress">
                                                </div>
                                                <div class="form-group-row mt-3">
                                                    <label class="w-25">City:</label>
                                                    <input type="text" placeholder="" class="form-control w-75 editSellerCity">
                                                </div>
                                                <div class="form-group-row mt-3">
                                                    <label class="w-25">Country:</label>
                                                    <input type="text" placeholder="" class="form-control w-75 editSellerCountry">
                                                </div>
                                                <div class="form-group-row mt-3">
                                                    <label class="w-25">State/Province:</label>
                                                    <input type="text" placeholder="" class="form-control w-75 editSellerState">
                                                </div>
                                                <div class="form-group-row mt-3">
                                                    <label class="w-25">Seller Attorney:</label>
                                                    <input type="text" placeholder="" class="form-control w-75 editSellerAttorney">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancel</button>
                                            <button type="button" class="btn btn-primary updateSellerInfo">Update</button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!-- Tenant's Info Table -->
                            <div class="col-md-6 rental-box Tbox" style="display: none">
                                <div class="card">
                                    <div class="card-header">
                                        <h4>Tenant's Info
                                            <a href="" data-bs-toggle="modal" data-bs-target="#addTenantInfo" class="pull-right">+ Add Tenant Info</a>
                                        </h4>
                                    </div>
                                    <div class="card-body">
                                        <div class="table-responsive">
                                            <table class="table table-sm TenantInfoTable" id="TenantInfo">
                                                <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>Name</th>
                                                    <th>Email</th>
                                                    <th>Phone</th>
                                                    <th>Action</th>
                                                </tr>
                                                </thead>
                                                <tbody class="list TenantInfo">
                                                <tr>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Add Tenant's Info Modal -->
                            <div class="modal fade" id="addTenantInfo" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h4 class="modal-title" id="staticBackdropLabel">Add Tenant Info</h4>
                                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                        </div>
                                        <div class="modal-body adjusted-gross-level">
                                            <div class="col-md-12">
                                                <div class="form-group-row">
                                                    <label class="w-25">Name:</label>
                                                    <input type="text" placeholder="" class="form-control w-75 tenantName">
                                                </div>
                                                <div class="form-group-row mt-3">
                                                    <label class="w-25">Email Address:</label>
                                                    <input type="text" placeholder="Email Address" class="form-control w-75 tenantEmail">
                                                </div>
                                                <div class="form-group-row mt-3">
                                                    <label class="w-25">Phone:</label>
                                                    <input type="text" placeholder="(___)-___-___" class="form-control w-75 tenantPhone">
                                                </div>
                                                <div class="form-group-row mt-3">
                                                    <label class="w-25">Zip/Postal Code:</label>
                                                    <input type="text" placeholder="" class="form-control w-75 tenantPostal">
                                                </div>
                                                <div class="form-group-row mt-3">
                                                    <label class="w-25">Address:</label>
                                                    <input type="text" placeholder="" class="form-control w-75 tenantAddress">
                                                </div>
                                                <div class="form-group-row mt-3">
                                                    <label class="w-25">City:</label>
                                                    <input type="text" placeholder="" class="form-control w-75 tenantCity">
                                                </div>
                                                <div class="form-group-row mt-3">
                                                    <label class="w-25">Country:</label>
                                                    <input type="text" placeholder="" class="form-control w-75 tenantCountry">
                                                </div>
                                                <div class="form-group-row mt-3">
                                                    <label class="w-25">State/Province:</label>
                                                    <input type="text" placeholder="" class="form-control w-75 tenantState">
                                                </div>
                                                <div class="form-group-row mt-3">
                                                    <label class="w-25">Buyer Attorney:</label>
                                                    <input type="text" placeholder="" class="form-control w-75 tenantAttorney">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancel</button>
                                            <button type="button" class="btn btn-primary saveTenantInfo">Save</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Edit Tenant's Info Modal -->
                            <div class="modal fade" id="editTenantInfo" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h4 class="modal-title" id="staticBackdropLabel">Add Tenant Info</h4>
                                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                        </div>
                                        <div class="modal-body adjusted-gross-level">
                                            <div class="col-md-12">
                                                <input type="text" class="hiddenTenantId" style="display: none">
                                                <div class="form-group-row">
                                                    <label class="w-25">Name:</label>
                                                    <input type="text" placeholder="" class="form-control w-75 editTenantName">
                                                </div>
                                                <div class="form-group-row mt-3">
                                                    <label class="w-25">Email Address:</label>
                                                    <input type="text" placeholder="Email Address" class="form-control w-75 editTenantEmail">
                                                </div>
                                                <div class="form-group-row mt-3">
                                                    <label class="w-25">Phone:</label>
                                                    <input type="text" placeholder="(___)-___-___" class="form-control w-75 editTenantPhone">
                                                </div>
                                                <div class="form-group-row mt-3">
                                                    <label class="w-25">Zip/Postal Code:</label>
                                                    <input type="text" placeholder="" class="form-control w-75 editTenantPostal">
                                                </div>
                                                <div class="form-group-row mt-3">
                                                    <label class="w-25">Address:</label>
                                                    <input type="text" placeholder="" class="form-control w-75 editTenantAddress">
                                                </div>
                                                <div class="form-group-row mt-3">
                                                    <label class="w-25">City:</label>
                                                    <input type="text" placeholder="" class="form-control w-75 editTenantCity">
                                                </div>
                                                <div class="form-group-row mt-3">
                                                    <label class="w-25">Country:</label>
                                                    <input type="text" placeholder="" class="form-control w-75 editTenantCountry">
                                                </div>
                                                <div class="form-group-row mt-3">
                                                    <label class="w-25">State/Province:</label>
                                                    <input type="text" placeholder="" class="form-control w-75 editTenantState">
                                                </div>
                                                <div class="form-group-row mt-3">
                                                    <label class="w-25">Buyer Attorney:</label>
                                                    <input type="text" placeholder="" class="form-control w-75 editTenantAttorney">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancel</button>
                                            <button type="button" class="btn btn-primary updateTenantInfo">Update</button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!-- Landloard Info Table -->
                            <div class="col-md-6 rental-box Tbox" style="display: none">
                                <div class="card">
                                    <div class="card-header">
                                        <h4>Landloard's Info
                                            <a href="" data-bs-toggle="modal" data-bs-target="#addLandloardInfo" class="pull-right">+ Add Landloard Info</a>
                                        </h4>
                                    </div>
                                    <div class="card-body">
                                        <div class="table-responsive">
                                            <table class="table table-sm" id="LandloardInfo">
                                                <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>Name</th>
                                                    <th>Email</th>
                                                    <th>Phone</th>
                                                    <th>Action</th>
                                                </tr>
                                                </thead>
                                                <tbody class="list LandloardInfo">
                                                <tr>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Add Landloard Info Modal -->
                            <div class="modal fade" id="addLandloardInfo" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h4 class="modal-title" id="staticBackdropLabel">Add Landloard Info</h4>
                                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                        </div>
                                        <div class="modal-body adjusted-gross-level">
                                            <div class="col-md-12">
                                                <div class="form-group-row">
                                                    <label class="w-25">Name:</label>
                                                    <input type="text" placeholder="" class="form-control w-75 landloardName">
                                                </div>
                                                <div class="form-group-row mt-3">
                                                    <label class="w-25">Email Address:</label>
                                                    <input type="text" placeholder="Email Address" class="form-control w-75 landloarEmail">
                                                </div>
                                                <div class="form-group-row mt-3">
                                                    <label class="w-25">Phone:</label>
                                                    <input type="text" placeholder="(___)-___-___" class="form-control w-75 landloarPhone">
                                                </div>
                                                <div class="form-group-row mt-3">
                                                    <label class="w-25">Zip/Postal Code:</label>
                                                    <input type="text" placeholder="" class="form-control w-75 landloarPostal">
                                                </div>
                                                <div class="form-group-row mt-3">
                                                    <label class="w-25">Address:</label>
                                                    <input type="text" placeholder="" class="form-control w-75 landloardAddress">
                                                </div>
                                                <div class="form-group-row mt-3">
                                                    <label class="w-25">City:</label>
                                                    <input type="text" placeholder="" class="form-control w-75 landloardCity">
                                                </div>
                                                <div class="form-group-row mt-3">
                                                    <label class="w-25">Country:</label>
                                                    <input type="text" placeholder="" class="form-control w-75 landloardCountry">
                                                </div>
                                                <div class="form-group-row mt-3">
                                                    <label class="w-25">State/Province:</label>
                                                    <input type="text" placeholder="" class="form-control w-75 landloardState">
                                                </div>
                                                <div class="form-group-row mt-3">
                                                    <label class="w-25">Seller Attorney:</label>
                                                    <input type="text" placeholder="" class="form-control w-75 landloardAttorney">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancel</button>
                                            <button type="button" class="btn btn-primary saveLandloarInfo">Save</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Edit Landloard Info Modal -->
                            <div class="modal fade" id="editLandloardInfo" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h4 class="modal-title" id="staticBackdropLabel">Edit Landloard Info</h4>
                                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                        </div>
                                        <div class="modal-body adjusted-gross-level">
                                            <div class="col-md-12">
                                                <input type="text" class="hiddenLandloardId" style="display: none">
                                                <div class="form-group-row">
                                                    <label class="w-25">Name:</label>
                                                    <input type="text" placeholder="" class="form-control w-75 editLandloardName">
                                                </div>
                                                <div class="form-group-row mt-3">
                                                    <label class="w-25">Email Address:</label>
                                                    <input type="text" placeholder="Email Address" class="form-control w-75 editLandloardEmail">
                                                </div>
                                                <div class="form-group-row mt-3">
                                                    <label class="w-25">Phone:</label>
                                                    <input type="text" placeholder="(___)-___-___" class="form-control w-75 editLandloardPhone">
                                                </div>
                                                <div class="form-group-row mt-3">
                                                    <label class="w-25">Zip/Postal Code:</label>
                                                    <input type="text" placeholder="" class="form-control w-75 editLandloardPostal">
                                                </div>
                                                <div class="form-group-row mt-3">
                                                    <label class="w-25">Address:</label>
                                                    <input type="text" placeholder="" class="form-control w-75 editLandloardAddress">
                                                </div>
                                                <div class="form-group-row mt-3">
                                                    <label class="w-25">City:</label>
                                                    <input type="text" placeholder="" class="form-control w-75 editLandloardCity">
                                                </div>
                                                <div class="form-group-row mt-3">
                                                    <label class="w-25">Country:</label>
                                                    <input type="text" placeholder="" class="form-control w-75 editLandloardCountry">
                                                </div>
                                                <div class="form-group-row mt-3">
                                                    <label class="w-25">State/Province:</label>
                                                    <input type="text" placeholder="" class="form-control w-75 editLandloardState">
                                                </div>
                                                <div class="form-group-row mt-3">
                                                    <label class="w-25">Seller Attorney:</label>
                                                    <input type="text" placeholder="" class="form-control w-75 editLandloardAttorney">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancel</button>
                                            <button type="button" class="btn btn-primary updateSellerInfo">Update</button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!-- Listing Agent Info -->
                            <div class="col-lg-12 col-md-12 ListingAgentInfo Tbox listing-box" style="display: none">
                                <div class="card">
                                    <div class="card-header">
                                        <h4>Listing Agent Info</h4>
                                    </div>
                                    <div class="card-body">
                                        <div class="col-lg-12 col-lg-12 row mt-3 ListingAgentInfoCard">
                                            <div class="col-lg-6 col-md-6">
                                                <div class="form-group form-group-row">
                                                    <label class="w-25">Select Listing Agent:</label>
                                                    <input type="text" name="select_listing_agent" placeholder="Search Agent" class="form-control input-group w-75">
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-md-6">
                                                <div class="form-group form-group-row">
                                                    <label class="w-25">Commission Plan for this agent:</label>
                                                    <select class="form-select w-75" name="commission_plan_for_listing_agent">
                                                        <option>No results match</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-md-6">
                                                <div class="form-group form-group-row">
                                                    <label class="w-25">%:</label>
                                                    <input type="text" name="listing_agent_percentage" class="form-control input-group w-75">
                                                </div>
                                                <div class="form-group form-group-row">
                                                    <label class="w-25">$:</label>
                                                    <input type="text" name="listing_agent_flat_fee" class="form-control input-group w-75">
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-md-6 form-group-row mt-3">
                                                <input type="checkbox" name="listing_exclude_transaction" class="form-check-input" style="margin-right: 10px;">
                                                <label>Exclude the transaction from agent's assigned commission plan</label>
                                            </div>
                                            <div class="col-lg-6 col-md-6">
                                                <div class="form-group form-group-row">
                                                    <label class="w-25">How do you want to split up the commission?:</label>
                                                    <select class="form-select w-75" name="split_listing_commission" style="height: 44px;">
                                                        <option>Brokerage Gross Commission</option>
                                                        <option>Agent Commission</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-md-6">
                                                <div class="form-group form-group-row">
                                                    <label class="w-25 mt-3">and by a:</label>
                                                    <select class="form-select w-75" name="listing_and_by_a">
                                                        <option>Percentage</option>
                                                        <option>Dollar Amount</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-12 col-lg-12 mt-5" style="margin-bottom: -20px">
                                            <div class="form-group form-group-row mb-0">
                                                <label></label>
                                                <div class="mb-0">
                                                    <div class="addAnotherListingAgent" style="cursor: pointer; color: blue">Add another agent</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!-- Selling Agent Info -->
                            <div class="col-lg-12 col-md-12 SellingAgentInfo Tbox selling-box" style="display: none">
                                <div class="card">
                                    <div class="card-header">
                                        <h4>Selling Agent Info</h4>
                                    </div>
                                    <div class="card-body">
                                        <div class="col-lg-12 col-lg-12 row mt-3 sellingAgentInfoCard">
                                            <div class="col-lg-6 col-md-6">
                                                <div class="form-group form-group-row">
                                                    <label class="w-25">Select Selling Agent:</label>
                                                    <input type="text" placeholder="Search Agent" name="select_selling_agent" class="form-control input-group w-75">
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-md-6">
                                                <div class="form-group form-group-row">
                                                    <label class="w-25">Commission Plan for this agent:</label>
                                                    <select class="form-select w-75" name="commission_plan_for_selling_agent">
                                                        <option>No results match</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-md-6">
                                                <div class="form-group form-group-row">
                                                    <label class="w-25">%:</label>
                                                    <input type="text" class="form-control input-group w-75" name="selling_agent_percentage">
                                                </div>
                                                <div class="form-group form-group-row">
                                                    <label class="w-25">$:</label>
                                                    <input type="text" class="form-control input-group w-75" name="selling_agent_flat_fee">
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-md-6 form-group-row mt-3">
                                                <input type="checkbox" class="form-check-input" name="selling_exclude_transaction" style="margin-right: 10px;">
                                                <label>Exclude the transaction from agent's assigned commission plan</label>
                                            </div>
                                            <div class="col-lg-6 col-md-6">
                                                <div class="form-group form-group-row">
                                                    <label class="w-25">How do you want to split up the commission?:</label>
                                                    <select class="form-select w-75" name="split_selling_commission" style="height: 44px;">
                                                        <option>Brokerage Gross Commission</option>
                                                        <option>Agent Commission</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-md-6">
                                                <div class="form-group form-group-row">
                                                    <label class="w-25 mt-3">and by a:</label>
                                                    <select class="form-select w-75" name="selling_and_by_a">
                                                        <option>Percentage</option>
                                                        <option>Dollar Amount</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-12 col-lg-12 mt-5" style="margin-bottom: -20px">
                                            <div class="form-group form-group-row mb-0">
                                                <label></label>
                                                <div class="mb-0">
                                                    <div class="addSellingAgent" style="cursor: pointer; color: blue">Add another agent</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!-- Dual (Both) Agent Info -->
                            <div class="col-lg-12 col-md-12 DualAgentInfo Tbox dual-box" style="display: none">
                                <div class="card">
                                    <div class="card-header">
                                        <h4>Dual (Both) Agent Info</h4>
                                    </div>
                                    <div class="card-body">
                                        <div class="col-lg-12 col-lg-12 row mt-3 dualAgentInfoCard">
                                            <div class="col-lg-6 col-md-6">
                                                <div class="form-group form-group-row">
                                                    <label class="w-25">Select Dual Agent:</label>
                                                    <input type="text" name="select_dual_agent" placeholder="Search Agent" class="form-control input-group w-75">
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-md-6">
                                                <div class="form-group form-group-row">
                                                    <label class="w-25">Commission Plan for this agent:</label>
                                                    <select class="form-select w-75" name="commission_plan_for_dual_agent">
                                                        <option>No results match</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-md-6">
                                                <div class="form-group form-group-row">
                                                    <label class="w-25">%:</label>
                                                    <input type="text" name="dual_agent_percentage" class="form-control input-group w-75">
                                                </div>
                                                <div class="form-group form-group-row">
                                                    <label class="w-25">$:</label>
                                                    <input type="text" name="dual_agent_flat_fee" class="form-control input-group w-75">
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-md-6 form-group-row mt-3">
                                                <input type="checkbox" name="dual_exclude_transaction" class="form-check-input" style="margin-right: 10px;">
                                                <label>Exclude the transaction from agent's assigned commission plan</label>
                                            </div>
                                            <div class="col-lg-6 col-md-6">
                                                <div class="form-group form-group-row">
                                                    <label class="w-25">How do you want to split up the commission?:</label>
                                                    <select class="form-select w-75" name="split_dual_commission" style="height: 44px;">
                                                        <option>Brokerage Gross Commission</option>
                                                        <option>Agent Commission</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-md-6">
                                                <div class="form-group form-group-row">
                                                    <label class="w-25 mt-3">and by a:</label>
                                                    <select class="form-select w-75" name="dual_and_by_a">
                                                        <option>Percentage</option>
                                                        <option>Dollar Amount</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-12 col-lg-12 mt-5" style="margin-bottom: -20px">
                                            <div class="form-group form-group-row mb-0">
                                                <label></label>
                                                <div class="mb-0">
                                                    <div style="cursor: pointer; color: blue" class="addDualAgent">Add another agent</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!-- Leasing Agent Info -->
                            <div class="col-lg-12 col-md-12 LeasingAgentInfo Tbox rental-box" style="display: none">
                                <div class="card">
                                    <div class="card-header">
                                        <h4>Leasing Agent Info</h4>
                                    </div>
                                    <div class="card-body">
                                        <div class="col-lg-12 col-lg-12 row mt-3 leasingAgentInfoCard">
                                            <div class="col-lg-6 col-md-6">
                                                <div class="form-group form-group-row">
                                                    <label class="w-25">Select Leasing Agent:</label>
                                                    <input type="text" placeholder="Search Agent" name="select_leasing_agent" class="form-control input-group w-75">
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-md-6">
                                                <div class="form-group form-group-row">
                                                    <label class="w-25">Commission Plan for this agent:</label>
                                                    <select class="form-select w-75" name="commission_plan_for_leasing_agent">
                                                        <option>No results match</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-md-6">
                                                <div class="form-group form-group-row">
                                                    <label class="w-25">%:</label>
                                                    <input type="text" name="leasing_agent_percentage" class="form-control input-group w-75">
                                                </div>
                                                <div class="form-group form-group-row">
                                                    <label class="w-25">$:</label>
                                                    <input type="text" name="leasing_agent_flat_fee" class="form-control input-group w-75">
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-md-6 form-group-row mt-3">
                                                <input type="checkbox" name="leasing_exclude_transaction" class="form-check-input" style="margin-right: 10px;">
                                                <label>Exclude the transaction from agent's assigned commission plan</label>
                                            </div>
                                            <div class="col-lg-6 col-md-6">
                                                <div class="form-group form-group-row">
                                                    <label class="w-25">How do you want to split up the commission?:</label>
                                                    <select class="form-select w-75" name="split_leasing_commission" style="height: 44px;">
                                                        <option>Brokerage Gross Commission</option>
                                                        <option>Agent Commission</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-md-6">
                                                <div class="form-group form-group-row">
                                                    <label class="w-25 mt-3">and by a:</label>
                                                    <select class="form-select w-75" name="leasing_and_by_a">
                                                        <option>Percentage</option>
                                                        <option>Dollar Amount</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-12 col-lg-12 mt-5" style="margin-bottom: -20px">
                                            <div class="form-group form-group-row mb-0">
                                                <label></label>
                                                <div class="mb-0">
                                                    <div class="addLeasingAgent" style="color: blue; cursor: pointer">Add another agent</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!-- Referreal Agent Info -->
                            <div class="col-lg-12 col-md-12 ReferralAgentInfo Tbox referral-box" style="display: none">
                                <div class="card">
                                    <div class="card-header">
                                        <h4>Referral Agent Info</h4>
                                    </div>
                                    <div class="card-body">
                                        <div class="col-lg-12 col-lg-12 row mt-3 referralAgentInfoCard">
                                            <div class="col-lg-6 col-md-6">
                                                <div class="form-group form-group-row">
                                                    <label class="w-25">Select Referral Agent:</label>
                                                    <input type="text" name="select_referral_agent" placeholder="Search Agent" class="form-control input-group w-75">
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-md-6">
                                                <div class="form-group form-group-row">
                                                    <label class="w-25">Commission Plan for this agent:</label>
                                                    <select class="form-select w-75" name="commission_plan_for_referral_agent">
                                                        <option>No results match</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-md-6">
                                                <div class="form-group form-group-row">
                                                    <label class="w-25">%:</label>
                                                    <input type="text" name="referral_agent_percentage" class="form-control input-group w-75">
                                                </div>
                                                <div class="form-group form-group-row">
                                                    <label class="w-25">$:</label>
                                                    <input type="text" name="referral_agent_flat_fee" class="form-control input-group w-75">
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-md-6 form-group-row mt-3">
                                                <input type="checkbox" name="referral_exclude_transaction" class="form-check-input" style="margin-right: 10px;">
                                                <label>Exclude the transaction from agent's assigned commission plan</label>
                                            </div>
                                            <div class="col-lg-6 col-md-6">
                                                <div class="form-group form-group-row">
                                                    <label class="w-25">How do you want to split up the commission?:</label>
                                                    <select class="form-select w-75" name="split_referral_commission" style="height: 44px;">
                                                        <option>Brokerage Gross Commission</option>
                                                        <option>Agent Commission</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-md-6">
                                                <div class="form-group form-group-row">
                                                    <label class="w-25 mt-3">and by a:</label>
                                                    <select class="form-select w-75" name="referral_and_by_a">
                                                        <option>Percentage</option>
                                                        <option>Dollar Amount</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-12 col-lg-12 mt-5" style="margin-bottom: -20px">
                                            <div class="form-group form-group-row mb-0">
                                                <label></label>
                                                <div class="mb-0">
                                                    <a class="addReferralAgent" style="cursor: pointer; color: blue">Add another agent</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer text-right">
                            <button type="button" class="btn btn-default">
                                Cancel
                            </button>
                            <button type="submit" class="btn btn-primary">
                                Save Changes
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        @endsection

        @push('js')
            <script>
                $('.AddTransactionPage').on('click', '.TrTypeVal', function(){
                    var Block = $(this).attr('toggle');
                    var Hide_box = $(this).attr('hide-box');
                    $('.Tbox').hide();
                    if ($(this).is(':checked')){
                        $(`.${Block}`).show();
                        $(`.${Hide_box}`).hide();
                    } else if (!$(this).is(':checked')){
                        $(`.${Hide_box}`).show();
                    }

                    if($(this).is(':checked')){
                        var TransactionTypeValue = $(this).val();
                    }

                    $('body').find('.checkBoxList').after(
                        `
                        <input type="hidden" name="transaction_type" value="${TransactionTypeValue}" />
                        `
                    );
                });

                $('body').on('click', '.completeAddressCheckbox', function (){
                    if ($("input[type=checkbox]").is(":checked")){
                        $('.houseNumber').show();
                        $('.streetName').show();
                        $('.unitNumber').show();
                        $('.propertyAddress').prop('disabled', 'disabled').css('background-color', '#EEEEEE')
                    } else {
                        $('.houseNumber').hide();
                        $('.streetName').hide();
                        $('.unitNumber').hide();
                        $('.propertyAddress').prop('disabled', false).css('background-color', '#fff')
                    }
                });

                $('body').on('click', '.addAnotherListingAgent', function (){
                    var listingAgentLength = $('body').find('.ListingAgentInfoCard').length;
                    if (listingAgentLength<4){
                        $('body').find('.ListingAgentInfoCard').last().after(
                            `
                     <div class="col-lg-12 col-lg-12 row ListingAgentInfoCard">
                          <hr>
                          <div class="col-lg-6 col-md-6">
                               <div class="form-group form-group-row">
                                    <label class="w-25">Select Listing Agent # ${listingAgentLength + 1}:</label>
                                    <input type="text" placeholder="Search Agent" class="form-control input-group w-75">
                               </div>
                          </div>
                          <div class="col-lg-6 col-md-6">
                             <div class="form-group form-group-row">
                                <label class="w-25">Commission Plan for this agent:</label>
                                  <select class="form-select w-75">
                                     <option>No results match</option>
                                  </select>
                             </div>
                          </div>
                          <div class="col-lg-6 col-md-6">
                             <div class="form-group-row">
                                                <label class="w-25">%:</label>
                                                <div class="w-75">
                                                    <input type="text" class="form-control input-group">
                                                    <h4 class="mt-1">Of</h4>
                                                    <select class="form-select">
                                                        <option>Brokerage Gross Commission</option>
                                                        <option>Agent Commission</option>
                                                    </select>
                                                </div>
                                            </div>
                             <div class="form-group-row mt-1">
                                <label class="w-25">$:</label>
                                <input type="text" class="form-control input-group w-75">
                             </div>
                          </div>
                          <div class="col-lg-12 col-lg-12 mt-5" style="margin-bottom: 0px">
                             <div class="form-group form-group-row mb-0 pull-right">
                                <label></label>
                                <div class="mb-0">
                                  <div class="removeListingAgent" style="color: red; cursor: pointer;">Remove agent</div>
                                </div>
                             </div>
                          </div>
                    </div>
                    `
                        );
                    } else {
                        $('.addAnotherListingAgent').hide();
                    }
                });

                $('body').on('click', '.removeListingAgent', function (e){
                    var listing_agent_length = $('body').find('.ListingAgentInfoCard').length;
                    if (listing_agent_length > 1){
                        $(this).closest('.ListingAgentInfoCard').remove();
                        e.preventDefault();
                        return false;
                    } else {
                        $('.addAnotherListingAgent').show();
                    }
                })

                $('body').on('click', '.addSellingAgent', function (){
                    var sellingAgentLength = $('body').find('.sellingAgentInfoCard').length;
                    console.log(sellingAgentLength, 'checking length');
                    console.log(sellingAgentLength<4, 'checking condition')
                    console.log(sellingAgentLength + 1, 'checking div addition');
                    if (sellingAgentLength<4){
                        $('body').find('.sellingAgentInfoCard').last().after(
                            `
                     <div class="col-lg-12 col-lg-12 row sellingAgentInfoCard">
                          <hr>
                          <div class="col-lg-6 col-md-6">
                               <div class="form-group form-group-row">
                                    <label class="w-25">Select Selling Agent # ${sellingAgentLength + 1}:</label>
                                    <input type="text" placeholder="Search Agent" class="form-control input-group w-75">
                               </div>
                          </div>
                          <div class="col-lg-6 col-md-6">
                             <div class="form-group form-group-row">
                                <label class="w-25">Commission Plan for this agent:</label>
                                  <select class="form-select w-75">
                                     <option>No results match</option>
                                  </select>
                             </div>
                          </div>
                          <div class="col-lg-6 col-md-6">
                             <div class="form-group-row">
                                                <label class="w-25">%:</label>
                                                <div class="w-75">
                                                    <input type="text" class="form-control input-group">
                                                    <h4 class="mt-1">Of</h4>
                                                    <select class="form-select">
                                                        <option>Brokerage Gross Commission</option>
                                                        <option>Agent Commission</option>
                                                    </select>
                                                </div>
                                            </div>
                             <div class="form-group-row mt-1">
                                <label class="w-25">$:</label>
                                <input type="text" class="form-control input-group w-75">
                             </div>
                          </div>
                          <div class="col-lg-12 col-lg-12 mt-5" style="margin-bottom: 0px">
                             <div class="form-group form-group-row mb-0 pull-right">
                                <label></label>
                                <div class="mb-0">
                                  <div class="removeSellingAgent" style="color: red; cursor: pointer;">Remove agent</div>
                                </div>
                             </div>
                          </div>
                    </div>
                    `
                        );
                    } else {
                        $('.addSellingAgent').hide();
                    }
                });

                $('body').on('click', '.removeSellingAgent', function (e){
                    var selling_agent_length = $('body').find('.sellingAgentInfoCard').length;
                    if (selling_agent_length > 1){
                        $(this).closest('.sellingAgentInfoCard').remove();
                        e.preventDefault();
                        return false;
                    } else {
                        $('.addSellingAgent').show();
                    }
                })

                $('body').on('click', '.addDualAgent', function (){
                    var dualAgentLength = $('body').find('.dualAgentInfoCard').length;
                    if (dualAgentLength<4){
                        $('body').find('.dualAgentInfoCard').last().after(
                            `
                     <div class="col-lg-12 col-lg-12 row dualAgentInfoCard">
                          <hr>
                          <div class="col-lg-6 col-md-6">
                               <div class="form-group form-group-row">
                                    <label class="w-25">Select Dual Agent # ${dualAgentLength + 1}:</label>
                                    <input type="text" placeholder="Search Agent" class="form-control input-group w-75">
                               </div>
                          </div>
                          <div class="col-lg-6 col-md-6">
                             <div class="form-group form-group-row">
                                <label class="w-25">Commission Plan for this agent:</label>
                                  <select class="form-select w-75">
                                     <option>No results match</option>
                                  </select>
                             </div>
                          </div>
                          <div class="col-lg-6 col-md-6">
                             <div class="form-group-row">
                                                <label class="w-25">%:</label>
                                                <div class="w-75">
                                                    <input type="text" class="form-control input-group">
                                                    <h4 class="mt-1">Of</h4>
                                                    <select class="form-select">
                                                        <option>Brokerage Gross Commission</option>
                                                        <option>Agent Commission</option>
                                                    </select>
                                                </div>
                                            </div>
                             <div class="form-group-row mt-1">
                                <label class="w-25">$:</label>
                                <input type="text" class="form-control input-group w-75">
                             </div>
                          </div>
                          <div class="col-lg-12 col-lg-12 mt-5" style="margin-bottom: 0px">
                             <div class="form-group form-group-row mb-0 pull-right">
                                <label></label>
                                <div class="mb-0">
                                  <div class="removeDualAgent" style="color: red; cursor: pointer;">Remove agent</div>
                                </div>
                             </div>
                          </div>
                    </div>
                    `
                        );
                    } else {
                        $('.addDualAgent').hide();
                    }
                });

                $('body').on('click', '.removeDualAgent', function (e){
                    var selling_agent_length = $('body').find('.dualAgentInfoCard').length;
                    if (selling_agent_length > 1){
                        $(this).closest('.dualAgentInfoCard').remove();
                        e.preventDefault();
                        return false;
                    } else {
                        $('.addDualAgent').show();
                    }
                })

                $('body').on('click', '.addLeasingAgent', function (){
                    var leasingAgentLength = $('body').find('.leasingAgentInfoCard').length;
                    if (leasingAgentLength<4){
                        $('body').find('.leasingAgentInfoCard').last().after(
                            `
                     <div class="col-lg-12 col-lg-12 row leasingAgentInfoCard">
                          <hr>
                          <div class="col-lg-6 col-md-6">
                               <div class="form-group form-group-row">
                                    <label class="w-25">Select Leasing Agent # ${leasingAgentLength + 1}:</label>
                                    <input type="text" placeholder="Search Agent" class="form-control input-group w-75">
                               </div>
                          </div>
                          <div class="col-lg-6 col-md-6">
                             <div class="form-group form-group-row">
                                <label class="w-25">Commission Plan for this agent:</label>
                                  <select class="form-select w-75">
                                     <option>No results match</option>
                                  </select>
                             </div>
                          </div>
                          <div class="col-lg-6 col-md-6">
                             <div class="form-group-row">
                                                <label class="w-25">%:</label>
                                                <div class="w-75">
                                                    <input type="text" class="form-control input-group">
                                                    <h4 class="mt-1">Of</h4>
                                                    <select class="form-select">
                                                        <option>Brokerage Gross Commission</option>
                                                        <option>Agent Commission</option>
                                                    </select>
                                                </div>
                                            </div>
                             <div class="form-group-row mt-1">
                                <label class="w-25">$:</label>
                                <input type="text" class="form-control input-group w-75">
                             </div>
                          </div>
                          <div class="col-lg-12 col-lg-12 mt-5" style="margin-bottom: 0px">
                             <div class="form-group form-group-row mb-0 pull-right">
                                <label></label>
                                <div class="mb-0">
                                  <div class="removeLeasingAgent" style="color: red; cursor: pointer;">Remove agent</div>
                                </div>
                             </div>
                          </div>
                    </div>
                    `
                        );
                    } else {
                        $('.addLeasingAgent').hide();
                    }
                });

                $('body').on('click', '.removeLeasingAgent', function (e){
                    var selling_agent_length = $('body').find('.leasingAgentInfoCard').length;
                    if (selling_agent_length > 1){
                        $(this).closest('.leasingAgentInfoCard').remove();
                        e.preventDefault();
                        return false;
                    } else {
                        $('.addLeasingAgent').show();
                    }
                })

                $('body').on('click', '.addReferralAgent', function (){
                    var referralAgentLength = $('body').find('.referralAgentInfoCard').length;
                    if (referralAgentLength<4){
                        $('body').find('.referralAgentInfoCard').last().after(
                            `
                     <div class="col-lg-12 col-lg-12 row referralAgentInfoCard">
                          <hr>
                          <div class="col-lg-6 col-md-6">
                               <div class="form-group form-group-row">
                                    <label class="w-25">Select Referral Agent # ${referralAgentLength + 1}:</label>
                                    <input type="text" placeholder="Search Agent" class="form-control input-group w-75">
                               </div>
                          </div>
                          <div class="col-lg-6 col-md-6">
                             <div class="form-group form-group-row">
                                <label class="w-25">Commission Plan for this agent:</label>
                                  <select class="form-select w-75">
                                     <option>No results match</option>
                                  </select>
                             </div>
                          </div>
                          <div class="col-lg-6 col-md-6">
                             <div class="form-group-row">
                                                <label class="w-25">%:</label>
                                                <div class="w-75">
                                                    <input type="text" class="form-control input-group">
                                                    <h4 class="mt-1">Of</h4>
                                                    <select class="form-select">
                                                        <option>Brokerage Gross Commission</option>
                                                        <option>Agent Commission</option>
                                                    </select>
                                                </div>
                                            </div>
                             <div class="form-group-row mt-1">
                                <label class="w-25">$:</label>
                                <input type="text" class="form-control input-group w-75">
                             </div>
                          </div>
                          <div class="col-lg-12 col-lg-12 mt-5" style="margin-bottom: 0px">
                             <div class="form-group form-group-row mb-0 pull-right">
                                <label></label>
                                <div class="mb-0">
                                  <div class="removeReferralAgent" style="color: red; cursor: pointer;">Remove agent</div>
                                </div>
                             </div>
                          </div>
                    </div>
                    `
                        );
                    } else {
                        $('.addReferralAgent').hide();
                    }
                });

                $('body').on('click', '.removeReferralAgent', function (e){
                    var selling_agent_length = $('body').find('.referralAgentInfoCard').length;
                    if (selling_agent_length > 1){
                        $(this).closest('.referralAgentInfoCard').remove();
                        e.preventDefault();
                        return false;
                    } else {
                        $('.addReferralAgent').show();
                    }
                })

                $('body').on('click', '.saveBuyerInfo', function (){
                    var buyer_item_name = $('.buyerItemName').val();
                    var buyer_email = $('.buyerEmail').val();
                    var buyer_phone = parseInt($('.buyerPhone').val());
                    var buyer_postal_code = $('.buyerPostalCode').val();
                    var buyer_address = $('.buyerAddress').val();
                    var buyer_city = $('.buyerCity').val();
                    var buyer_country = $('.buyerCountry').val();
                    var buyer_state = $('.buyerState').val();
                    var buyer_attorney = $('.buyerAttorney').val();

                    var buyer_info_id = Math.floor(Math.random() * 5) + 1;

                    var table = document.getElementById("BuyerInfo");
                    var tbodyRowCount = table.tBodies[0].rows.length;
                    $('#addBuyerInfo').modal('hide');
                    var html = `
                        <tr class="BuyerInfoTr" tr-id=${buyer_info_id}>
                            <td> <input class="TdInput" readonly value="${tbodyRowCount}"></td>
                            <td> <input class="TdInput buyer_item" name="buyer_item_name" readonly value="${buyer_item_name}"></td>
                            <td> <input class="TdInput buyer_email" name="buyer_email" readonly value="${buyer_email}"></td>
                            <td> <input class="TdInput buyer_phone" name="buyer_phone" readonly value="${buyer_phone}"></td>
                            <input type="hidden" class="buyer_postal" name="buyer_postal" value="${buyer_postal_code}">
                            <input type="hidden" class="buyer_address" name="buyer_address" value="${buyer_address}">
                            <input type="hidden" class="buyer_city" name="buyer_city" value="${buyer_city}">
                            <input type="hidden" class="buyer_country" name="buyer_country" value="${buyer_country}">
                            <input type="hidden" class="buyer_state" name="buyer_state" value="${buyer_state}">
                            <input type="hidden" class="buyer_attorney" name="buyer_attorney" value="${buyer_attorney}">
                            <td>
                              <a href="" class="editBuyerInfo" data-bs-toggle="modal" data-bs-target="#editBuyerInfoModal"><i class="fas fa-edit"></i></a>
                              <a href="" class="buyerInfoDel"><i class="fas fa-trash-alt"></i></a>
                             </td>
                         </tr>
                        `
                    $('.BuyerInfo').append(html);

                    $('.BuyerInfoTable').on('click', '.buyerInfoDel', function (e){
                        e.preventDefault();
                        $(this).closest("tr").remove();
                    })

                    $('.buyerItemName').val("");
                    $('.buyerEmail').val("");
                    $('.buyerPhone').val("");
                    $('.buyerPostalCode').val("");
                    $('.buyerAddress').val("");
                    $('.buyerCity').val("");
                    $('.buyerCountry').val("");
                    $('.buyerState').val("");
                    $('.buyerAttorney').val("");

                });
                $('.BuyerInfoTable').on('click', '.editBuyerInfo', function (){
                    var parent_row = $(this).parents('.BuyerInfoTr');
                    var buyer_row_id = parent_row.attr('tr-id');
                    var edit_buyer_item = parent_row.find('.buyer_item').val();
                    var edit_buyer_email = parent_row.find('.buyer_email').val();
                    var edit_buyer_phone = parent_row.find('.buyer_phone').val();
                    var edit_buyer_postal = parent_row.find('.buyer_postal').val();
                    var edit_buyer_address = parent_row.find('.buyer_address').val();
                    var edit_buyer_city = parent_row.find('.buyer_city').val();
                    var edit_buyer_country = parent_row.find('.buyer_country').val();
                    var edit_buyer_state = parent_row.find('.buyer_state').val();
                    var edit_buyer_attorney = parent_row.find('.buyer_attorney').val();

                    var edit_modal = $('#editBuyerInfoModal')
                    edit_modal.find('.hiddenBuyerId').val(buyer_row_id);
                    edit_modal.find('.EditBuyerItem').val(edit_buyer_item);
                    edit_modal.find('.EditBuyerEmail').val(edit_buyer_email);
                    edit_modal.find('.EditBuyerPhone').val(parseInt(edit_buyer_phone));
                    edit_modal.find('.EditBuyerPostal').val(edit_buyer_postal);
                    edit_modal.find('.EditBuyerAddress').val(edit_buyer_address);
                    edit_modal.find('.EditBuyerCity').val(edit_buyer_city);
                    edit_modal.find('.EditBuyerCountry').val(edit_buyer_country);
                    edit_modal.find('.EditBuyerState').val(edit_buyer_state);
                    edit_modal.find('.EditBuyerAttorney').val(edit_buyer_attorney);
                })
                $('#editBuyerInfoModal').on('click', '.UpdateBuyerInfo', function (){
                    var update_modal = $('#editBuyerInfoModal');
                    var update_id = update_modal.find('.hiddenBuyerId').val();
                    var update_buyer_item = update_modal.find('.EditBuyerItem').val();
                    var update_buyer_email = update_modal.find('.EditBuyerEmail').val();
                    var update_buyer_phone = update_modal.find('.EditBuyerPhone').val();
                    var update_buyer_postal = update_modal.find('.EditBuyerPostal').val();
                    var update_buyer_address = update_modal.find('.EditBuyerAddress').val();
                    var update_buyer_city = update_modal.find('.EditBuyerCity').val();
                    var update_buyer_country = update_modal.find('.EditBuyerCountry').val();
                    var update_buyer_state = update_modal.find('.EditBuyerState').val();
                    var update_buyer_attorney = update_modal.find('.EditBuyerAttorney').val();


                    var row = $(`.BuyerInfoTr[tr-id=${update_id}]`);
                    row.find('.buyer_item').val(update_buyer_item);
                    row.find('.buyer_email').val(update_buyer_email);
                    row.find('.buyer_phone').val(update_buyer_phone);
                    row.find('.buyer_postal').val(update_buyer_postal);
                    row.find('.buyer_address').val(update_buyer_address);
                    row.find('.buyer_city').val(update_buyer_city);
                    row.find('.buyer_country').val(update_buyer_country);
                    row.find('.buyer_state').val(update_buyer_state);
                    row.find('.buyer_attorney').val(update_buyer_attorney);

                    $('#editBuyerInfoModal').modal('hide');
                })


                $('body').on('click', '.saveSellerInfo', function (){
                    var seller_name = $('.sellerName').val();
                    var seller_email = $('.sellerEmail').val();
                    var seller_phone = parseInt($('.sellerPhone').val());
                    var seller_postal_code = $('.sellerPostal').val();
                    var seller_address = $('.sellerAddress').val();
                    var seller_city = $('.sellerCity').val();
                    var seller_country = $('.sellerCountry').val();
                    var seller_state = $('.sellerState').val();
                    var seller_attorney = $('.sellerAttorney').val();

                    var seller_info_id = Math.floor(Math.random() * 5) + 1;

                    var table = document.getElementById("SellerInfo");
                    var tbodyRowCount = table.tBodies[0].rows.length;
                    $('#addSellerInfo').modal('hide');
                    var html = `
                        <tr class="SellerInfoTr" Seller-tr-id=${seller_info_id}>
                            <td> <input class="TdInput" readonly value="${tbodyRowCount}"></td>
                            <td> <input class="TdInput seller_name" name="seller_name" readonly value="${seller_name}"></td>
                            <td> <input class="TdInput seller_email" name="seller_email" readonly value="${seller_email}"></td>
                            <td> <input class="TdInput seller_phone" name="seller_phone" readonly value="${seller_phone}"></td>
                            <input type="hidden" class="seller_postal" name="seller_postal" value="${seller_postal_code}">
                            <input type="hidden" class="seller_address" name="seller_address" value="${seller_address}">
                            <input type="hidden" class="seller_city" name="seller_city" value="${seller_city}">
                            <input type="hidden" class="seller_country" name="seller_country" value="${seller_country}">
                            <input type="hidden" class="seller_state" name="seller_state" value="${seller_state}">
                            <input type="hidden" class="seller_attorney" name="seller_attorney" value="${seller_attorney}">
                            <td>
                              <a href="" class="editSellerInfo" data-bs-toggle="modal" data-bs-target="#editSellerInfo"><i class="fas fa-edit"></i></a>
                              <a href="" class="sellerInfoDel"><i class="fas fa-trash-alt"></i></a>
                             </td>
                         </tr>
                        `
                    $('.SellerInfo').append(html);

                    $('.SellerInfoTable').on('click', '.sellerInfoDel', function (e){
                        e.preventDefault();
                        $(this).closest("tr").remove();
                    })

                    $('.sellerName').val("");
                    $('.sellerEmail').val("");
                    $('.sellerPhone').val("");
                    $('.sellerPostal').val("");
                    $('.sellerAddress').val("");
                    $('.sellerCity').val("");
                    $('.sellerCountry').val("");
                    $('.sellerState').val("");
                    $('.sellerAttorney').val("");

                });
                $('.SellerInfoTable').on('click', '.editSellerInfo', function (){
                    var parent_row = $(this).parents('.SellerInfoTr');
                    var seller_row_id = parent_row.attr('Seller-tr-id');
                    var edit_seller_name = parent_row.find('.seller_name').val();
                    var edit_seller_email = parent_row.find('.seller_email').val();
                    var edit_seller_phone = parent_row.find('.seller_phone').val();
                    var edit_seller_postal = parent_row.find('.seller_postal').val();
                    var edit_seller_address = parent_row.find('.seller_address').val();
                    var edit_seller_city = parent_row.find('.seller_city').val();
                    var edit_seller_country = parent_row.find('.seller_country').val();
                    var edit_seller_state = parent_row.find('.seller_state').val();
                    var edit_seller_attorney = parent_row.find('.seller_attorney').val();

                    console.log(seller_row_id);

                    var edit_modal = $('#editSellerInfo')
                    console.log(edit_modal.html())
                    edit_modal.find('.hiddenSellerId').val(seller_row_id);
                    edit_modal.find('.editSellerName').val(edit_seller_name);
                    edit_modal.find('.editSellerEmail').val(edit_seller_email);
                    edit_modal.find('.editSellerPhone').val(parseInt(edit_seller_phone));
                    edit_modal.find('.editSellerPostal').val(edit_seller_postal);
                    edit_modal.find('.editSellerAddress').val(edit_seller_address);
                    edit_modal.find('.editSellerCity').val(edit_seller_city);
                    edit_modal.find('.editSellerCountry').val(edit_seller_country);
                    edit_modal.find('.editSellerState').val(edit_seller_state);
                    edit_modal.find('.editSellerAttorney').val(edit_seller_attorney);
                })
                $('#editSellerInfo').on('click', '.updateSellerInfo', function (){
                    var update_modal = $('#editSellerInfo');
                    var update_id = update_modal.find('.hiddenSellerId').val();
                    var update_seller_name = update_modal.find('.editSellerName').val();
                    var update_seller_email = update_modal.find('.editSellerEmail').val();
                    var update_seller_phone = update_modal.find('.editSellerPhone').val();
                    var update_seller_postal = update_modal.find('.editSellerPostal').val();
                    var update_seller_address = update_modal.find('.editSellerAddress').val();
                    var update_seller_city = update_modal.find('.editSellerCity').val();
                    var update_seller_country = update_modal.find('.editSellerCountry').val();
                    var update_seller_state = update_modal.find('.editSellerState').val();
                    var update_seller_attorney = update_modal.find('.editSellerAttorney').val();

                    console.log(update_modal.html());
                    console.log(update_id);
                    console.log(update_seller_name);
                    console.log(update_seller_email);
                    console.log(update_seller_phone);
                    console.log(update_seller_address);
                    console.log(update_seller_city);
                    console.log(update_seller_country);
                    console.log(update_seller_state);
                    console.log(update_seller_attorney);

                    var row = $(`.SellerInfoTr[Seller-tr-id=${update_id}]`);
                    row.find('.seller_name').val(update_seller_name);
                    row.find('.seller_email').val(update_seller_email);
                    row.find('.seller_phone').val(update_seller_phone);
                    row.find('.seller_postal').val(update_seller_postal);
                    row.find('.seller_address').val(update_seller_address);
                    row.find('.seller_city').val(update_seller_city);
                    row.find('.seller_country').val(update_seller_country);
                    row.find('.seller_state').val(update_seller_state);
                    row.find('.seller_attorney').val(update_seller_attorney);

                    $('#editSellerInfo').modal('hide');
                })


                $('body').on('click', '.saveTenantInfo', function (){
                    var tenant_name = $('.tenantName').val();
                    var tenant_email = $('.tenantEmail').val();
                    var tenant_phone = parseInt($('.tenantPhone').val());
                    var tenant_postal_code = $('.tenantPostal').val();
                    var tenant_address = $('.tenantAddress').val();
                    var tenant_city = $('.tenantCity').val();
                    var tenant_country = $('.tenantCountry').val();
                    var tenant_state = $('.tenantState').val();
                    var tenant_attorney = $('.tenantAttorney').val();

                    var tenant_info_id = Math.floor(Math.random() * 5) + 1;

                    var table = document.getElementById("TenantInfo");
                    var tbodyRowCount = table.tBodies[0].rows.length;
                    $('#addTenantInfo').modal('hide');
                    var html = `
                        <tr class="TenantInfoTr" tenant-tr-id=${tenant_info_id}>
                            <td> <input class="TdInput" readonly value="${tbodyRowCount}"></td>
                            <td> <input class="TdInput tenant_name" name="tenant_name" readonly value="${tenant_name}"></td>
                            <td> <input class="TdInput tenant_email" name="tenant_email" readonly value="${tenant_email}"></td>
                            <td> <input class="TdInput tenant_phone" name="tenant_phone" readonly value="${tenant_phone}"></td>
                            <input type="hidden" class="tenant_postal" name="tenant_postal" value="${tenant_postal_code}">
                            <input type="hidden" class="tenant_address" name="tenant_address" value="${tenant_address}">
                            <input type="hidden" class="tenant_city" name="tenant_city" value="${tenant_city}">
                            <input type="hidden" class="tenant_country" name="tenant_country" value="${tenant_country}">
                            <input type="hidden" class="tenant_state" name="tenant_state" value="${tenant_state}">
                            <input type="hidden" class="tenant_attorney" name="tenant_attorney" value="${tenant_attorney}">
                            <td>
                              <a href="" class="editTenantInfo" data-bs-toggle="modal" data-bs-target="#editTenantInfo"><i class="fas fa-edit"></i></a>
                              <a href="" class="tenantInfoDel"><i class="fas fa-trash-alt"></i></a>
                             </td>
                         </tr>
                        `
                    $('.TenantInfo').append(html);

                    $('.TenantInfoTable').on('click', '.tenantInfoDel', function (e){
                        e.preventDefault();
                        $(this).closest("tr").remove();
                    })

                    $('.tenantName').val("");
                    $('.tenantEmail').val("");
                    $('.tenantPhone').val("");
                    $('.tenantPostal').val("");
                    $('.tenantAddress').val("");
                    $('.tenantCity').val("");
                    $('.tenantCountry').val("");
                    $('.tenantState').val("");
                    $('.tenantAttorney').val("");

                });
                $('.TenantInfoTable').on('click', '.editTenantInfo', function (){
                    var parent_row = $(this).parents('.TenantInfoTr');
                    var tenant_row_id = parent_row.attr('tenant-tr-id');
                    var edit_tenant_name = parent_row.find('.tenant_name').val();
                    var edit_tenant_email = parent_row.find('.tenant_email').val();
                    var edit_tenant_phone = parent_row.find('.tenant_phone').val();
                    var edit_tenant_postal = parent_row.find('.tenant_postal').val();
                    var edit_tenant_address = parent_row.find('.tenant_address').val();
                    var edit_tenant_city = parent_row.find('.tenant_city').val();
                    var edit_tenant_country = parent_row.find('.tenant_country').val();
                    var edit_tenant_state = parent_row.find('.tenant_state').val();
                    var edit_tenant_attorney = parent_row.find('.tenant_attorney').val();

                    var edit_modal = $('#editTenantInfo')
                    edit_modal.find('.hiddenTenantId').val(tenant_row_id);
                    edit_modal.find('.editTenantName').val(edit_tenant_name);
                    edit_modal.find('.editTenantEmail').val(edit_tenant_email);
                    edit_modal.find('.editTenantPhone').val(parseInt(edit_tenant_phone));
                    edit_modal.find('.editTenantPostal').val(edit_tenant_postal);
                    edit_modal.find('.editTenantAddress').val(edit_tenant_address);
                    edit_modal.find('.editTenantCity').val(edit_tenant_city);
                    edit_modal.find('.editTenantCountry').val(edit_tenant_country);
                    edit_modal.find('.editTenantState').val(edit_tenant_state);
                    edit_modal.find('.editTenantAttorney').val(edit_tenant_attorney);
                })
                $('#editTenantInfo').on('click', '.updateTenantInfo', function (){
                    var update_modal = $('#editTenantInfo');
                    var update_tenant_id = update_modal.find('.hiddenTenantId').val();
                    var update_tenant_name = update_modal.find('.editTenantName').val();
                    var update_tenant_email = update_modal.find('.editTenantEmail').val();
                    var update_tenant_phone = update_modal.find('.editTenantPhone').val();
                    var update_tenant_postal = update_modal.find('.editTenantPostal').val();
                    var update_tenant_address = update_modal.find('.editTenantAddress').val();
                    var update_tenant_city = update_modal.find('.editTenantCity').val();
                    var update_tenant_country = update_modal.find('.editTenantCountry').val();
                    var update_tenant_state = update_modal.find('.editTenantState').val();
                    var update_tenant_attorney = update_modal.find('.editTenantAttorney').val();

                    var update_tenant_row = $(`.TenantInfoTr[tenant-tr-id=${update_tenant_id}]`);
                    update_tenant_row.find('.tenant_name').val(update_tenant_name);
                    update_tenant_row.find('.tenant_email').val(update_tenant_email);
                    update_tenant_row.find('.tenant_phone').val(update_tenant_phone);
                    update_tenant_row.find('.tenant_postal').val(update_tenant_postal);
                    update_tenant_row.find('.tenant_address').val(update_tenant_address);
                    update_tenant_row.find('.tenant_city').val(update_tenant_city);
                    update_tenant_row.find('.tenant_country').val(update_tenant_country);
                    update_tenant_row.find('.tenant_state').val(update_tenant_state);
                    update_tenant_row.find('.tenant_attorney').val(update_tenant_attorney);

                    $('#editTenantInfo').modal('hide');
                })


                $('body').on('click', '.saveLandloarInfo', function (){
                    var landloard_name = $('.landloardName').val();
                    var landloard_email = $('.landloardEmail').val();
                    var landloard_phone = parseInt($('.landloardPhone').val());
                    var landloard_postal_code = $('.landloardPostal').val();
                    var landloard_address = $('.landloardAddress').val();
                    var landloard_city = $('.landloardCity').val();
                    var landloard_country = $('.landloardCountry').val();
                    var landloard_state = $('.landloardState').val();
                    var landloard_attorney = $('.landloardAttorney').val();

                    var landloard_info_id = Math.floor(Math.random() * 5) + 1;

                    var table = document.getElementById("LandloardInfo");
                    var tbodyRowCount = table.tBodies[0].rows.length;
                    $('#addLandloardInfo').modal('hide');
                    var html = `
                        <tr class="LandloardInfoTr" landlaord-tr-id=${landloard_info_id}>
                            <td> <input class="TdInput" readonly value="${tbodyRowCount}"></td>
                            <td> <input class="TdInput landloard_name" name="landloard_name" readonly value="${landloard_name}"></td>
                            <td> <input class="TdInput landloard_email" name="landloard_email" readonly value="${landloard_email}"></td>
                            <td> <input class="TdInput landloard_phone" name="landloard_phone" readonly value="${landloard_phone}"></td>
                            <input type="hidden" class="landloard_postal" name="landloard_postal" value="${landloard_postal_code}">
                            <input type="hidden" class="landloard_address" name="landloard_address" value="${landloard_address}">
                            <input type="hidden" class="landloard_city" name="landloard_city" value="${landloard_city}">
                            <input type="hidden" class="landloard_country" name="landloard_country" value="${landloard_country}">
                            <input type="hidden" class="landloard_state" name="landloard_state" value="${landloard_state}">
                            <input type="hidden" class="landloard_attorney" name="landloard_attorney" value="${landloard_attorney}">
                            <td>
                              <a href="" class="editLandloardInfo" data-bs-toggle="modal" data-bs-target="#editLandloardInfo"><i class="fas fa-edit"></i></a>
                              <a href="" class="landloardInfoDel"><i class="fas fa-trash-alt"></i></a>
                             </td>
                         </tr>
                        `
                    $('.LandloardInfo').append(html);

                    $('.LandloardInfoTable').on('click', '.landloardInfoDel', function (e){
                        e.preventDefault();
                        $(this).closest("tr").remove();
                    })

                    $('.landloardName').val("");
                    $('.landloardEmail').val("");
                    $('.landloardPhone').val("");
                    $('.landloardPostal').val("");
                    $('.landloardAddress').val("");
                    $('.landloardCity').val("");
                    $('.landloardCountry').val("");
                    $('.landloardState').val("");
                    $('.landloardAttorney').val("");

                });
                $('.landloardInfoTable').on('click', '.editLandloardInfo', function (){
                    var parent_row = $(this).parents('.LandloardInfoTr');
                    var landloard_row_id = parent_row.attr('landloard-tr-id');
                    var edit_landloard_name = parent_row.find('.landloard_name').val();
                    var edit_landloard_email = parent_row.find('.landloard_email').val();
                    var edit_landloard_phone = parent_row.find('.landloard_phone').val();
                    var edit_landloard_postal = parent_row.find('.landloard_postal').val();
                    var edit_landloard_address = parent_row.find('.landloard_address').val();
                    var edit_landloard_city = parent_row.find('.landloard_city').val();
                    var edit_landloard_country = parent_row.find('.landloard_country').val();
                    var edit_landloard_state = parent_row.find('.landloard_state').val();
                    var edit_landloard_attorney = parent_row.find('.landloard_attorney').val();

                    var edit_modal = $('#editLandloardInfo')
                    edit_modal.find('.hiddenLandloardId').val(landloard_row_id);
                    edit_modal.find('.editLandloardName').val(edit_landloard_name);
                    edit_modal.find('.editLandloardEmail').val(edit_landloard_email);
                    edit_modal.find('.editLandloardPhone').val(parseInt(edit_landloard_phone));
                    edit_modal.find('.editLandloardPostal').val(edit_landloard_postal);
                    edit_modal.find('.editLandloardAddress').val(edit_landloard_address);
                    edit_modal.find('.editLandloardCity').val(edit_landloard_city);
                    edit_modal.find('.editLandloardCountry').val(edit_landloard_country);
                    edit_modal.find('.editLandloardState').val(edit_landloard_state);
                    edit_modal.find('.editLandloardAttorney').val(edit_landloard_attorney);

                })
                $('#editLandloardInfo').on('click', '.updateLandloardInfo', function (){
                    var update_modal = $('#editLandloardInfo');
                    var update_landloard_id = update_modal.find('.hiddenLandloardId').val();
                    var update_landloard_name = update_modal.find('.editLandloardName').val();
                    var update_landloard_email = update_modal.find('.editLandloardEmail').val();
                    var update_landloard_phone = update_modal.find('.editLandloardPhone').val();
                    var update_landloard_postal = update_modal.find('.editLandloardPostal').val();
                    var update_landloard_address = update_modal.find('.editLandloardAddress').val();
                    var update_landloard_city = update_modal.find('.editLandloardCity').val();
                    var update_landloard_country = update_modal.find('.editLandloardCountry').val();
                    var update_landloard_state = update_modal.find('.editLandloardState').val();
                    var update_landloard_attorney = update_modal.find('.editLandloardAttorney').val();

                    var update_landloard_row = $(`.LandloardInfoTr[landloard-tr-id=${update_landloard_id}]`);
                    update_landloard_row.find('.landloard_name').val(update_landloard_name);
                    update_landloard_row.find('.landloard_email').val(update_landloard_email);
                    update_landloard_row.find('.landloard_phone').val(update_landloard_phone);
                    update_landloard_row.find('.landloard_postal').val(update_landloard_postal);
                    update_landloard_row.find('.landloard_address').val(update_landloard_address);
                    update_landloard_row.find('.landloard_city').val(update_landloard_city);
                    update_landloard_row.find('.landloard_country').val(update_landloard_country);
                    update_landloard_row.find('.landloard_state').val(update_landloard_state);
                    update_landloard_row.find('.landloard_attorney').val(update_landloard_attorney);

                    $('#editLandloardInfo').modal('hide');
                })

            </script>
    @endpush
