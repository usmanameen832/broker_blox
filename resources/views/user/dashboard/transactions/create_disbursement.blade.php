@extends('user.layouts.master')

@push('css')
    <style>
        .box{
            border: 1px solid #c6c6c2;
            border-radius: 3px;
        }
        .create_demand{
            margin: 0;
            padding: 10px;
        }
        .box-group{
            margin-bottom: 20px;
            position: relative;
            font-size: 16px;
        }
        .box-group ul{
            font-style: italic;
            list-style: none;
            padding: 0;
        }
        .col-md-4 .padding{
            padding: 0 15px;
        }
    </style>
@endpush

@section('content')
    <div class="header">
        <div class="container-fluid">
            <div class="header-body">
                <div class="row align-items-end">
                    <div class="col">
                        <h1 class="header-title">
                            Transactions / Create Disbursement
                        </h1>
                    </div>
                    <div class="col-auto">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-6 col-xl">
                <div class="card">
                    <div class="card-header">
                        <h4>Create Disbursement</h4>
                        <p>Select a transaction and enter the deduction items below.</p>
                    </div>
                    <div class="card-body">
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <div class="col-lg-6 col-md-6">
                                <div class="form-group form-group-row">
                                    <label class="w-25 mt-2">Select Transaction:</label>
                                    <div class="input_container select_container w-75">
                                        <select class="form-select mb-3" disabled="disabled" data-choices>
                                            <option>No Result Matched</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6">
                            </div>
                            <div class="col-md-12 row box create_demand">
                                <div class="col-md-4 padding">
                                    <h4>Title/Attorney:</h4>
                                    <div class="box-group">
                                        <ul>
                                            <li>
                                                <label>Agent/Attorney:</label>
                                                <span></span>
                                            </li>
                                            <li>
                                                <label>Company:</label>
                                                <span></span>
                                            </li>
                                            <li>
                                                <label>Address:</label>
                                                <span></span>
                                            </li>
                                            <li>
                                                <label>Phone:</label>
                                                <span></span>
                                            </li>
                                            <li>
                                                <label>Fax:</label>
                                                <span></span>
                                            </li>
                                            <li>
                                                <label>Email:</label>
                                                <span></span>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-md-4 padding">
                                    <h4>Transaction Info:</h4>
                                    <div class="box-group">
                                        <ul>
                                            <li>
                                                <label>Escrow No.:</label>
                                                <span></span>
                                            </li>
                                            <li>
                                                <label>Gross Commission: $0.00</label>
                                                <span></span>
                                            </li>
                                            <li>
                                                <label>MLS:</label>
                                                <span></span>
                                            </li>
                                            <li>
                                                <label>Sale Price: $0.00</label>
                                                <span></span>
                                            </li>
                                            <li>
                                                <label>Close Date:</label>
                                                <span></span>
                                            </li>
                                            <li>
                                                <label>Property:</label>
                                                <span></span>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-footer text-right">
                    <button type="button" class="btn btn-default">
                        Cancel
                    </button>
                    <button type="submit" class="btn btn-primary">
                        Create Disbursement
                    </button>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('js')
@endpush
