@extends('user.layouts.master')

@push('css')
@endpush

@section('content')
    <div class="header">
        <div class="container-fluid">
            <div class="header-body">
                <div class="row align-items-end">
                    <div class="col">
                        <h1 class="header-title">
                            Transactions / Trust Account / Deposits
                        </h1>
                    </div>
                    <div class="col-auto">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 col-lg-12 col-xl">
                <div class="card">
                    <div class="card-header">
                        <h4>Trust Account / Deposits</h4>
                    </div>
                    <div class="card-body">
                            <a href="#">
                                <i class="fas fa-download"></i>
                            </a>
                        <div class="row mt-3">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="mb-2">Date Received:</label>
                                    <div class="input_container select_container">
                                        <input type="date" class="form-control input-group">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group mt-5 pull-right">
                                    <div>
                                        <button type="button" class="btn btn-secondary">Reset
                                            Filter</button>
                                        <button type="submit" class="btn btn-primary">Apply
                                            Filter(s)</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="table-responsive" data-list='{"valueNames": ["tables-row", "tables-first", "tables-last", "tables-handle"]}'>
                            <table class="table table-sm">
                                <thead>
                                <tr>
                                    <th scope="col">
                                        <a href="#" class="text-muted list-sort" data-sort="tables-first">Transaction</a>
                                    </th>
                                    <th scope="col">
                                        <a href="#" class="text-muted list-sort" data-sort="tables-last">Date Received</a>
                                    </th>
                                    <th scope="col">
                                        <a href="#" class="text-muted list-sort" data-sort="tables-handle">From Whom Received or To Whom Paid</a>
                                    </th>
                                    <th scope="col">
                                        <a href="#" class="text-muted list-sort" data-sort="tables-handle">Description</a>
                                    </th>
                                    <th scope="col">
                                        <a href="#" class="text-muted list-sort" data-sort="tables-handle">Amount Received</a>
                                    </th>
                                    <th scope="col">
                                        <a href="#" class="text-muted list-sort" data-sort="tables-handle">Reference</a>
                                    </th>
                                    <th scope="col">
                                        <a href="#" class="text-muted list-sort" data-sort="tables-handle">Date of Deposit</a>
                                    </th>
                                    <th scope="col">
                                        <a href="#" class="text-muted list-sort" data-sort="tables-handle">Amount Paid Out</a>
                                    </th>
                                    <th scope="col">
                                        <a href="#" class="text-muted list-sort" data-sort="tables-handle">Check Number</a>
                                    </th>
                                    <th scope="col">
                                        <a href="#" class="text-muted list-sort" data-sort="tables-handle">Date of Check</a>
                                    </th>
                                    <th scope="col">
                                        <a href="#" class="text-muted list-sort" data-sort="tables-handle">Action</a>
                                    </th>
                                </tr>
                                </thead>
                                <tbody class="list">
                                <tr>
                                    <td class="tables-first">Test</td>
                                    <td class="tables-last">01-01-2021</td>
                                    <td class="tables-handle">User</td>
                                    <td class="tables-handle">Dummy Description</td>
                                    <td class="tables-handle">200$</td>
                                    <td class="tables-handle">N/A</td>
                                    <td class="tables-handle">02-02-2021</td>
                                    <td class="tables-handle">300$</td>
                                    <td class="tables-handle">N/A</td>
                                    <td class="tables-handle">N/A</td>
                                    <td class="tables-handle">
                                        <a><i class="far fa-edit"></i></a>
                                        <a><i class="fas fa-trash-alt"></i></a>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- / .row -->
    </div>
@endsection

@push('js')

@endpush
