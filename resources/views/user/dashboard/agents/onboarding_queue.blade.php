@extends('user.layouts.master')

@push('css')
@endpush

@section('content')
    <div class="header">
        <div class="container-fluid">
            <div class="header-body">
                <div class="row align-items-end">
                    <div class="col">
                        <h1 class="header-title">
                            Agents / Onboarding Queue
                        </h1>
                    </div>
                    <div class="col-auto">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 col-lg-12 col-xl">
                <div class="card">
                    <div class="card-header">
                        <h4>Agent Billing</h4>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="mb-2">Checklist Types:</label>
                                    <div class="input_container select_container">
                                        <select class="form-select mb-3" data-choices>
                                            <option>All</option>
                                            <option>Test Lists</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group pull-right" style="margin-top: 1.6rem">
                                    <div>
                                        <button type="button" class="btn btn-secondary">Reset
                                            Filter</button>
                                        <button type="submit" class="btn btn-primary">Apply
                                            Filter(s)</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="table-responsive" data-list='{"valueNames": ["tables-row", "tables-first", "tables-last", "tables-handle"]}'>
                                <table class="table table-sm">
                                    <thead>
                                    <tr>
                                        <th scope="col">
                                            <a href="#" class="text-muted list-sort" data-sort="tables-first">Agent Name</a>
                                        </th>
                                        <th scope="col">
                                            <a href="#" class="text-muted list-sort" data-sort="tables-last">Agent Office</a>
                                        </th>
                                        <th scope="col">
                                            <a href="#" class="text-muted list-sort" data-sort="tables-handle">Agent Region</a>
                                        </th>
                                        <th scope="col">
                                            <a href="#" class="text-muted list-sort" data-sort="tables-handle">Agent Onboarding Status</a>
                                        </th>
                                        <th scope="col">
                                            <a href="#" class="text-muted list-sort" data-sort="tables-handle">Onboarding Completion Date</a>
                                        </th>
                                        <th scope="col">
                                            <a href="#" class="text-muted list-sort" data-sort="tables-handle">Task List Assigned</a>
                                        </th>
                                        <th scope="col">
                                            <a href="#" class="text-muted list-sort" data-sort="tables-handle">Task List Assigned To</a>
                                        </th>
                                    </tr>
                                    </thead>
                                    <tbody class="list">
                                    <tr>
                                        <td class="tables-first">Test</td>
                                        <td class="tables-last">Test Office</td>
                                        <td class="tables-handle">USA</td>
                                        <td class="tables-handle">Pending</td>
                                        <td class="tables-handle">01-01-2021</td>
                                        <td class="tables-handle">Test List</td>
                                        <td class="tables-handle">Assigned to user</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- / .row -->
    </div>
@endsection

@push('js')
@endpush
