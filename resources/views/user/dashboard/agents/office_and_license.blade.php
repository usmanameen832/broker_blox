@extends('user.layouts.master')

@push('css')
@endpush

@section('content')
    <div class="header">
        <div class="container-fluid">
            <div class="header-body">
                <div class="row align-items-end">
                    <div class="col">
                        <h1 class="header-title">
                            Agents / Offices & Licenses
                        </h1>
                    </div>
                    <div class="col-auto">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 col-lg-12 col-xl">
                <div class="card">
                    <div class="card-body">
                        <ul class="nav nav-tabs">
                            <li class="nav-item">
                                <a class="nav-link active" aria-current="page" href="#">Offices & Licenses</a>
                            </li>
                        </ul>
                        <div class="tab-content">
                            <div class="active" id="#">
                                <div class="col-lg-12 col-md-12 col-sm-12">
                                    <p class="font-24 margin-bottom-10">Offices &amp; Licenses</p>
                                    <div class="col-lg-12 col-md-12 office-location">
                                        <div class="col-lg-6 col-md-6">
                                            <div class="form-group">
                                                <label class="mb-2">Office Location(s):</label>
                                                <div class="input_container select_container">
                                                    <select class="form-select mb-3" disabled="disabled" data-choices>
                                                        <option>No Result Matched</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 col-md-6">
                                            <div class="pull-right" style="margin-top: 1.6rem;">
                                                <button type="button" class="btn btn-md btn-primary btn-padding" disabled="disabled"> Go To Agent Profile</button>
                                                <button type="button" class="btn btn-default btn-padding" disabled="disabled">Filter</button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-12 col-md-12 col-sm-12 margin-bottom-10 no-padding">
                                        <div class="collapse margin-bottom-10" id="collapseExample">
                                            <div class="well">
                                                <h5>
                                                    Specify the filters below and select ‘Apply
                                                    Filter(s)’. Filters will also apply to search items. <a data-toggle="collapse" data-target="#collapseExample"><i class="fa fa-minus pull-right"></i></a>
                                                </h5>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label>Agent Status:</label>
                                                            <div class="input_container select_container">
                                                                <select class="form-control chosen-select ng-pristine ng-untouched ng-valid localytics-chosen" data-chosen="" data-ng-model="filterDetail.status" data-ng-options="agentStatus.id as agentStatus.name for agentStatus in filterAgentStatusList" style="display: none;">
                                                                    <option value="" class="" selected="selected"></option>
                                                                    <option value="number:-2" label="All" selected="selected">All</option>
                                                                    <option value="number:-1" label="Pre-license and Active">Pre-license and Active</option>
                                                                    <option value="number:0" label="Pre-license">Pre-license</option>
                                                                    <option value="number:2" label="Active">Active</option>
                                                                    <option value="number:3" label="Inactive">Inactive</option>
                                                                </select>
                                                                <div class="chosen-container chosen-container-single" style="width: 0px;" title="">
                                                                    <a class="chosen-single" tabindex="-1"><span>All</span><div><b></b></div></a>
                                                                    <div class="chosen-drop"><div class="chosen-search"><input type="text" autocomplete="off">
                                                                        </div><ul class="chosen-results"></ul></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group filter_action">
                                                            <div class="pull-right">
                                                                <button type="button" class="btn btn-default">Reset Filter
                                                                </button>
                                                                <button type="submit" class="btn btn-primary btn_apply_filter">Apply Filter(s)
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="col-lg-12 col-md-12 col-sm-12 margin-bottom-10 inside-box header-green" >
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- / .row -->
    </div>
@endsection

@push('js')
@endpush
