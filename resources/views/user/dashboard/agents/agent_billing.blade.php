@extends('user.layouts.master')

@push('css')
@endpush

@section('content')
    <div class="header">
        <div class="container-fluid">
            <div class="header-body">
                <div class="row align-items-end">
                    <div class="col">
                        <h1 class="header-title">
                            Agents / Agent Billing
                        </h1>
                    </div>
                    <div class="col-auto">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 col-lg-12 col-xl">
                <div class="card">
                    <div class="card-body">
                        <ul class="nav nav-tabs" id="nav-tabs" role="tablist">
                            <li class="nav-item" role="presentation">
                                <a class="nav-link active" data-bs-toggle="tab" data-bs-target="#tab-billing" id="billing-tab" type="button" role="tab">Agent Billing</a>
                            </li>
                            <li class="nav-item" role="presentation">
                                <a class="nav-link" data-bs-toggle="tab" data-bs-target="#tab-log" id="log-tab" type="button" role="tab">Billing Log</a>
                            </li>
                            <li class="nav-item" role="presentation">
                                <a class="nav-link" data-bs-toggle="tab" data-bs-target="#tab-entries" id="entries-tab" type="button" role="tab">Card Declined Entries</a>
                            </li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane show active" id="tab-billing">
                                <div class="col-lg-12 col-md-12 col-sm-12">
                                    <h4 style="margin-bottom: 30px; margin-top: 20px; font-size: 18px; font-weight: 500; line-height: 1.1; color: inherit;">Agent Billing</h4>
                                    <div class="table-header">
                                        <div class="input-group">
                                            <div class="form-outline">
                                                <input type="search" id="form1" placeholder="search.." class="form-control" />
                                            </div>
                                            <button type="button" class="btn btn-primary">
                                                <i class="fas fa-search"></i>
                                            </button>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="table-responsive" data-list='{"valueNames": ["tables-row", "tables-first", "tables-last", "tables-handle"]}'>
                                            <table class="table table-sm">
                                                <thead>
                                                <tr>
                                                    <th scope="col">
                                                        <a href="#" class="text-muted list-sort" data-sort="tables-first">#</a>
                                                    </th>
                                                    <th scope="col">
                                                        <a href="#" class="text-muted list-sort" data-sort="tables-last">Agent Name</a>
                                                    </th>
                                                    <th scope="col">
                                                        <a href="#" class="text-muted list-sort" data-sort="tables-handle">Balance</a>
                                                    </th>
                                                    <th scope="col">
                                                        <a href="#" class="text-muted list-sort" data-sort="tables-handle">Credit</a>
                                                    </th>
                                                    <th scope="col">
                                                        <a href="#" class="text-muted list-sort" data-sort="tables-handle">Next Monthly Billing Date</a>
                                                    </th>
                                                    <th scope="col">
                                                        <a href="#" class="text-muted list-sort" data-sort="tables-handle">Next Quarterly Billing Date</a>
                                                    </th>
                                                    <th scope="col">
                                                        <a href="#" class="text-muted list-sort" data-sort="tables-handle">Next Bi-Annual Billing Date</a>
                                                    </th>
                                                    <th scope="col">
                                                        <a href="#" class="text-muted list-sort" data-sort="tables-handle">Next Annual Billing Date</a>
                                                    </th>
                                                    <th scope="col">
                                                        <a href="#" class="text-muted list-sort" data-sort="tables-handle">Amount</a>
                                                    </th>
                                                    <th scope="col">
                                                        <a href="#" class="text-muted list-sort" data-sort="tables-handle">Recurring</a>
                                                    </th>
                                                    <th scope="col">
                                                        <a href="#" class="text-muted list-sort" data-sort="tables-handle">Last Billing Date</a>
                                                    </th>
                                                    <th scope="col">
                                                        <a href="#" class="text-muted list-sort" data-sort="tables-handle">Action</a>
                                                    </th>
                                                </tr>
                                                </thead>
                                                <tbody class="list">
                                                <tr>
                                                    <td class="tables-first">1</td>
                                                    <td class="tables-first">Test</td>
                                                    <td class="tables-first">500 $</td>
                                                    <td class="tables-first">250 $</td>
                                                    <td class="tables-first">01-01-2021</td>
                                                    <td class="tables-first">02-02-2021</td>
                                                    <td class="tables-last">03-03-2021</td>
                                                    <td class="tables-handle">04-04-2021</td>
                                                    <td class="tables-handle">650 $</td>
                                                    <td class="tables-handle">Pending</td>
                                                    <td class="tables-handle">02-02-2021</td>
                                                    <td class="tables-handle">
                                                        <a><i class="far fa-edit"></i></a>
                                                        <a><i class="fas fa-trash-alt"></i></a>
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="tab-log">
                                <div class="col-lg-12 col-md-12 col-sm-12">
                                    <div class="table-header">
                                        <h4 style="margin-bottom: 30px; margin-top: 3px; font-size: 18px; font-weight: 500; line-height: 1.1; color: inherit;">Agent Billing Log</h4>
                                        <div class="filter-btn">
                                            <button class="btn btn-primary" disabled="disabled" style="margin-top: 2rem">Email All Unpaid Invoices</button>
                                        </div>
                                    </div>
                                    <div class="col-lg-12 col-md-12 row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="mb-2">Select Status:</label>
                                                <div class="input_container select_container">
                                                    <select class="form-select mb-3" data-choices>
                                                        <option>All</option>
                                                        <option>Paid</option>
                                                        <option>Unpaid</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="mb-2">Agent:</label>
                                                <div class="input_container select_container">
                                                    <select class="form-select mb-3" data-choices>
                                                        <option>All Agents</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="mb-2">Start Date:</label>
                                                <input type="date" class="form-control input-group">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="mb-2">Date Option:</label>
                                                <div class="input_container select_container">
                                                    <select class="form-select mb-3" data-choices>
                                                        <option>This Month</option>
                                                        <option>This Year</option>
                                                        <option>Last Month</option>
                                                        <option>Last Year</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group form-group-row pull-right">
                                                <button class="btn btn-secondary" style="margin-right: 5px">Reset Filter</button>
                                                <button class="btn btn-primary">Apply Filter</button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="table-responsive" data-list='{"valueNames": ["tables-row", "tables-first", "tables-last", "tables-handle"]}'>
                                            <table class="table table-sm">
                                                <thead>
                                                <tr>
                                                    <th scope="col">
                                                        <a href="#" class="text-muted list-sort" data-sort="tables-first">#</a>
                                                    </th>
                                                    <th scope="col">
                                                        <a href="#" class="text-muted list-sort" data-sort="tables-last">Billing Date</a>
                                                    </th>
                                                    <th scope="col">
                                                        <a href="#" class="text-muted list-sort" data-sort="tables-handle">Agent Name</a>
                                                    </th>
                                                    <th scope="col">
                                                        <a href="#" class="text-muted list-sort" data-sort="tables-handle">Description</a>
                                                    </th>
                                                    <th scope="col">
                                                        <a href="#" class="text-muted list-sort" data-sort="tables-handle">Amount</a>
                                                    </th>
                                                    <th scope="col">
                                                        <a href="#" class="text-muted list-sort" data-sort="tables-handle">Status</a>
                                                    </th>
                                                    <th scope="col">
                                                        <a href="#" class="text-muted list-sort">Memo</a>
                                                    </th>
                                                    <th scope="col">
                                                        <a href="#" class="text-muted list-sort" data-sort="tables-handle">Pay Date</a>
                                                    </th>
                                                    <th scope="col">
                                                        <a href="#" class="text-muted list-sort">Action</a>
                                                    </th>
                                                </tr>
                                                </thead>
                                                <tbody class="list">
                                                <tr>
                                                    <td class="tables-first">1</td>
                                                    <td class="tables-last">01-01-2021</td>
                                                    <td class="tables-handle">Test</td>
                                                    <td class="tables-handle">Dummy Description</td>
                                                    <td class="tables-handle">500 $</td>
                                                    <td class="tables-handle">Pending</td>
                                                    <td>Dummy Memo</td>
                                                    <td class="tables-handle">02-02-2021</td>
                                                    <td>
                                                        <a><i class="far fa-edit"></i></a>
                                                        <a><i class="fas fa-trash-alt"></i></a>
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="tab-entries">
                                <div class="col-lg-12 col-md-12 col-sm-12">
                                    <h4 style="margin-bottom: 30px; margin-top: 20px; font-size: 18px; font-weight: 500; line-height: 1.1; color: inherit;">Card Declined Entries</h4>
                                    <div class="table-header">
                                        <div class="input-group">
                                            <div class="form-outline">
                                                <input type="search" id="form1" placeholder="search.." class="form-control" />
                                            </div>
                                            <button type="button" class="btn btn-primary">
                                                <i class="fas fa-search"></i>
                                            </button>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="table-responsive" data-list='{"valueNames": ["tables-row", "tables-first", "tables-last", "tables-handle"]}'>
                                            <table class="table table-sm">
                                                <thead>
                                                <tr>
                                                    <th scope="col">
                                                        <a href="#" class="text-muted list-sort" data-sort="tables-first">#</a>
                                                    </th>
                                                    <th scope="col">
                                                        <a href="#" class="text-muted list-sort" data-sort="tables-last">Billing Date</a>
                                                    </th>
                                                    <th scope="col">
                                                        <a href="#" class="text-muted list-sort" data-sort="tables-handle">Agent Name</a>
                                                    </th>
                                                    <th scope="col">
                                                        <a href="#" class="text-muted list-sort" data-sort="tables-handle">Description</a>
                                                    </th>
                                                    <th scope="col">
                                                        <a href="#" class="text-muted list-sort" data-sort="tables-handle">Amount</a>
                                                    </th>
                                                    <th scope="col">
                                                        <a href="#" class="text-muted list-sort" data-sort="tables-handle">Status</a>
                                                    </th>
                                                    <th scope="col">
                                                        <a href="#" class="text-muted list-sort">Memo</a>
                                                    </th>
                                                    <th scope="col">
                                                        <a href="#" class="text-muted list-sort">Action</a>
                                                    </th>
                                                </tr>
                                                </thead>
                                                <tbody class="list">
                                                <tr>
                                                    <td class="tables-first">1</td>
                                                    <td class="tables-first">01-01-2021</td>
                                                    <td class="tables-first">Test</td>
                                                    <td class="tables-first">Test Description</td>
                                                    <td class="tables-first">500 $</td>
                                                    <td class="tables-first">Pending</td>
                                                    <td class="tables-last">Dummy Memo</td>
                                                    <td class="tables-handle">
                                                        <a><i class="far fa-edit"></i></a>
                                                        <a><i class="fas fa-trash-alt"></i></a>
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('js')
@endpush
