@extends('user.layouts.master')

@push('css')

@endpush

@section('content')
    <div class="header">
        <div class="container-fluid">
            <div class="header-body">
                <div class="row align-items-end">
                    <div class="col">
                        <h1 class="header-title">
                            Agents / Add Agents
                        </h1>
                    </div>
                    <div class="col-auto">
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="addAgents">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 col-md-6 col-xl">
                    <div class="card">
                        <div class="card-body">
                            <div class="header">
                                <h4 style="margin-bottom: 30px; margin-top: 0; font-size: 18px;">Add Agent: Who Will Fill Out Agent Information?</h4>
                                <p style="margin: 0 0 10px; ">Select who will fill out the Agent information:</p>
                            </div>
                            <div class="col-md-12 col-lg-12 agent-will">
                                <div class="left-cont">
                                    <input type="radio" name="addAgent" value="1" id="agent_will" class="">
                                </div>
                                <div class="right-cont">
                                    <label for="addAgent" class="vertical_top">
                                        Agent Will
                                        <span class="tag_extra">(send onboarding package)</span>
                                    </label>
                                    <p>
                                        Agent(s) will be emailed the link to a webpage containing their onboarding package, where they will be asked for their
                                        information. You will be able to customize this boarding package on the next page, before sending to the Agent(s).
                                    </p>
                                </div>
                            </div>
                            <div class="col-md-12 col-lg-12 agent-will">
                                <div class="left-cont">
                                    <input type="radio" name="addAgent" value="1" class="" id="i_will">
                                </div>
                                <div class="right-cont">
                                    <label for="addAgent" class="vertical_top">
                                        I Will
                                        <span class="tag_extra">(add single agent)</span>
                                    </label>
                                    <p>
                                        You will be manually entering information for a single agent in the following steps. Please have the agent information ready.
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer text-right">
                            <button type="button" class="btn btn-secondary">
                                Cancel
                            </button>
                            <button type="button" onclick="checkCommissionPlan()" class="btn btn-primary hideAddAgents">
                                Continue
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="sendOnboardingPackage" style="display: none">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 col-md-6 col-xl">
                    <div class="card">
                        <div class="card-body">
                            <div class="header">
                                <h4 style="margin-bottom: 30px; margin-top: 0; font-size: 18px;">Send Onboarding Package - Step 1</h4>
                                <p style="margin: 0 0 10px; ">Customize the Onboarding process for the agent(s) specified:</p>
                            </div>
                            <div class="col-md-12 col-lg-12">
                                <div class="row">
                                    <div class="col-md-1 col-lg-2"></div>
                                    <div class="col-md-10 col-lg-8">
                                        <form role="form">
                                            <div class="form-group">
                                                <label> <b>Choose Onboarding Template:</b> </label>
                                                <div class="input_container select_container w-75 mt-2">
                                                    <select class="form-select mb-3" data-choices>
                                                        <option>No result match</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="card-footer text-right">
                                                <button type="button" class="btn btn-secondary showAddAgents">
                                                    Back
                                                </button>
                                                <button type="button" class="btn btn-primary">
                                                    Next
                                                </button>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="col-md-1 col-lg-2"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="addSingleAgent" style="display: none">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 col-md-6 col-xl">
                    <div class="card">
                        <div class="card-body">
                            <div class="header">
                                <button class="btn btn-primary pull-right" data-bs-toggle="modal" data-bs-target="#addBrokerAsAgent">Add Broker As Agent</button>
                                <h4 style="margin-bottom: 30px; margin-top: 0; font-size: 18px;">Add New Agent</h4>
                                <p style="margin: 0 0 10px; ">Fill out the Agent information below.</p>
                            </div>

                            <!-- Modal -->
                            <div class="modal fade" id="addBrokerAsAgent" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                                <div class="modal-dialog modal-1-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h3 class="modal-title" id="staticBackdropLabel">Add Broker as Agent</h3>
                                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                        </div>
                                        <div class="modal-body">
                                            <div class="form-group">
                                                <label> <b>Commission Plan:</b> </label>
                                                <div class="input_container select_container mt-3">
                                                    <select class="form-select mb-3" data-choices>
                                                        <option>Test</option>
                                                        <option>test2</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer form-group-row">
                                            <button type="button" class="btn btn-light">Cancel</button>
                                            <button type="button" class="btn btn-primary">Save</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 col-lg-12">
                                <div class="row">
                                    <div class="col-md-1 col-lg-2"></div>
                                    <div class="col-md-10 col-lg-8">
                                        <form role="form">
                                            <div class="form-group">
                                                <label> <b>First Name:</b> </label>
                                                <input type="text" id="first_name" name="first_name" required placeholder="Enter First Name" class="form-control input-group w-75">
                                            </div>
                                            <div class="form-group">
                                                <label> <b>Last Name:</b> </label>
                                                <input type="text" id="last_name" name="last_name" required placeholder="Enter Last Name" class="form-control input-group w-75">
                                            </div>
                                            <div class="form-group">
                                                <label> <b>Email</b> </label>
                                                <input type="text" id="email" name="email" required placeholder="Enter(Will Be Your Username)" class="form-control input-group w-75">
                                            </div>
                                            <div class="form-group">
                                                <label> <b>Assign Commission Plan:</b> </label>
                                                <div class="input_container select_container w-75 mt-2">
                                                    <select class="form-select mb-3" required id="assign_commission_plan" name="assign_commission_plan" data-choices>
                                                        @foreach($planName as $planNames)
                                                            <option>{{ $planNames->plan_name }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label> <b>Assign office location:</b> </label>
                                                <div class="input_container select_container w-75 mt-2">
                                                    <select class="form-select mb-3" id="assign_office_location" name="assign_office_location" disabled data-choices>
                                                        <option>No Result Match</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label> <b>Start Date:</b> </label>
                                                <input type="date" onfocus="(this.type='date')" required id="start_date" name="start_date" class="form-control input-group w-75">
                                            </div>
                                            <div class="card-footer text-right">
                                                <button type="button" class="btn btn-secondary showAddAgents2">
                                                    Cancel
                                                </button>
                                                <button type="button" onclick="checkAgentSave()" class="btn btn-primary">
                                                    Save
                                                </button>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="col-md-1 col-lg-2"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Send Invitation Modal -->
    <div class="modal fade" id="sendInvitationModal" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <div class="modal-dialog modal-1-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="modal-title" id="staticBackdropLabel">Send Invitation</h3>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <p>
                        Your agent will be emailed an invitation to set up their BrokerBlox agent account which will allow them to update their
                        personal information, view their monthly agent billings and their transactions.
                    </p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-light">Cancel</button>
                    <button type="button" class="btn btn-primary addAgentBilling">Confirm</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Add Agent Billing Modal -->
    <div class="modal fade" id="addAgentBillingModal" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <div class="modal-dialog modal-1-dialog">
            <form action="{{route('viewAgents')}}" method="GET">
                <input type="hidden" name="agent_id" id="agent_id" />
                <div class="modal-content">
                    <div class="modal-header">
                        <h3 class="modal-title" id="staticBackdropLabel">Add Agent Billing</h3>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <p>
                            Agent added successfully! Would you like to add agent billings for this agent right now (you can always add them later)?
                        </p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" data-bs-dismiss="modal" class="btn btn-light">No</button>
                        <button type="submit" class="btn btn-primary"><a style="text-decoration: none">Yes</a></button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection

@push('js')
    <script>
        function checkCommissionPlan(){
            if (!$('#agent_will').is(':checked') && !$('#i_will').is(':checked')){
                swal("Please select the right option");

                return false;
            } else {
                var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
                var request = $.ajax({
                    url: "{{route('checkCommissionPlanData')}}",
                    method: "get",
                    data: {_token: CSRF_TOKEN,},
                    dataType: "html",
                    success: function(res){
                        var res = JSON.parse(res);
                        var status = res.status ;
                        var redirect = res.redirect;
                        console.log(status);
                        console.log("status mai kiaaa araha hai bataooo");
                        if(status == false){
                            swal("Please add a commission plan first.")
                                .then((value) => {
                                    window.location.href = redirect;
                                });
                        }
                        else if($('#agent_will').is(':checked')){
                            $('#addAgents').hide();
                            $('#sendOnboardingPackage').show();
                        } else if ($('#i_will').is(':checked')){
                            $('#addAgents').hide();
                            $('#addSingleAgent').show();
                        }
                    }
                });
                console.log('Route hit hogaya hai Data get hogaya hai');
            }
        }

        function checkAgentSave(){
            let first_name = $('#first_name').val();
            let last_name = $('#last_name').val();
            let email   = $('#email').val();
            let assign_commission_plan = $('#assign_commission_plan').val();
            let assign_office_location = $('#assign_office_location').val();
            let start_date = $('#start_date').val();
            if (first_name=="" || first_name==null){
                $('#first_name').css('border-color','red');
            } else if (last_name=="" || last_name==null){
                $('#last_name').css('border-color','red');
            } else if (email=="" || email==null){
                $('#email').css('border-color','red');
            } else if (start_date=="" || start_date==null){
                $('#start_date').css('border-color','red');
            }
            else {
                var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
                var request = $.ajax({
                    url: "{{route('saveAgent')}}",
                    method: "post",
                    data: {
                        _token: CSRF_TOKEN,
                        first_name: first_name,
                        last_name: last_name,
                        email: email,
                        assign_commission_plan: assign_commission_plan,
                        assign_office_location: assign_office_location,
                        start_date: start_date,
                    },
                    dataType: "html",
                    success: function (res) {
                        var res = JSON.parse(res);
                        var status = res.status;
                        var id = res.id;
                        var test = $('#agent_id').val(id.id);
                        if (status == true) {
                            $('#sendInvitationModal').modal('show');
                            $("body").on('click', '.addAgentBilling', function (){
                                $('#sendInvitationModal').modal('hide');
                                $('#addAgentBillingModal').modal('show');
                            })
                        }
                    }
                });
            }
        }

        $("body").on('click','.showAddAgents',function (){
            $('#addAgents').show();
            $('#sendOnboardingPackage').hide();
        });

        $("body").on('click','.showAddAgents2',function (){
            $('#addAgents').show();
            $('#addSingleAgent').hide();
        });

    </script>
@endpush
