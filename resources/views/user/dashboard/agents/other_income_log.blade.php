@extends('user.layouts.master')

@push('css')
@endpush

@section('content')
    <div class="header">
        <div class="container-fluid">
            <div class="header-body">
                <div class="row align-items-end">
                    <div class="col">
                        <h1 class="header-title">
                            Agents / Other Income Log
                        </h1>
                    </div>
                    <div class="col-auto">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 col-lg-12 col-xl">
                <div class="card">
                    <div class="card-header">
                        <h4>Other Income Log</h4>
                    </div>
                    <div class="card-body">
                        <div class="table-header">
                            <div class="input-group">
                                <button type="button" class="btn btn-primary">
                                    Download Excel <i class="fas fa-download"></i>
                                </button>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="mb-2">Close Date:</label>
                                    <div class="input_container select_container">
                                        <input type="date" class="form-control input-group">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="mb-2">Agents:</label>
                                    <div class="input_container select_container">
                                        <select class="form-select mb-3" data-choices>
                                            <option>All Agents</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group filter_action pull-right">
                                    <div>
                                        <button type="button" class="btn btn-secondary">Reset
                                            Filter</button>
                                        <button type="submit" class="btn btn-primary">Apply
                                            Filter(s)</button>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="table-responsive">
                                <table class="table table-sm" id="dtBasicExample">
                                    <thead>
                                    <tr>
                                        <th scope="col">
                                            <a href="#" class="text-muted list-sort">#</a>
                                        </th>
                                        <th scope="col">
                                            <a href="#" class="text-muted list-sort">Agent Name</a>
                                        </th>
                                        <th scope="col">
                                            <a href="#" class="text-muted list-sort">Item Name</a>
                                        </th>
                                        <th scope="col">
                                            <a href="#" class="text-muted list-sort">Income Amount</a>
                                        </th>
                                        <th scope="col">
                                            <a href="#" class="text-muted list-sort">Date</a>
                                        </th>
                                        <th scope="col">
                                            <a href="#" class="text-muted list-sort">Details</a>
                                        </th>
                                        <th scope="col">
                                            <a href="#" class="text-muted list-sort">Status</a>
                                        </th>
                                        <th scope="col">
                                            <a href="#" class="text-muted list-sort">Pay Date</a>
                                        </th>
                                        <th scope="col">
                                            <a href="#" class="text-muted list-sort">Action</a>
                                        </th>
                                    </tr>
                                    </thead>
                                    <tbody class="list">
                                    <tr>
                                        <td>1</td>
                                        <td>Test</td>
                                        <td>Test</td>
                                        <td>500 $</td>
                                        <td>02-02-2021</td>
                                        <td>Dummy Details</td>
                                        <td>Pending</td>
                                        <td>02-05-2021</td>
                                        <td>
                                            <a><i class="far fa-edit"></i></a>
                                            <a><i class="fas fa-trash-alt"></i></a>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                                <ul class="pagination justify-content-end">
                                    <li class="page-item disabled"><a class="page-link" href="#">Previous</a></li>
                                    <li class="page-item"><a class="page-link" href="#">1</a></li>
                                    <li class="page-item"><a class="page-link" href="#">2</a></li>
                                    <li class="page-item"><a class="page-link" href="#">3</a></li>
                                    <li class="page-item"><a class="page-link" href="#">Next</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('js')
@endpush
