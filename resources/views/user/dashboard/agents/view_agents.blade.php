@extends('user.layouts.master')

@push('css')
    <style>
        label{
            font-weight: bold;
        }
        .panel {
            margin-bottom: 20px;
            background-color: #fff;
            border: 1px solid transparent;
            border-radius: 4px;
        }
        .panel-skyblue .panel-heading {
            background-color: #3FCDF5;
            border-color: #3FCDF5;
            border-radius: 3px;
            color: #fff;
        }
        .panel-heading {
            padding: 10px 15px;
            border-bottom: 1px solid transparent;
        }
        .huge1 {
            font-size: 22px;
            font-weight: bold;
        }
        .icon1 {
            color: #39bae7;
            font-size: 54px;
            float: right;
        }
        .agent-permissions h4{
            text-decoration: underline;
            font-weight: bold;
            margin: 30px 0;
        }
    </style>
@endpush

@section('content')
    <div id="view-agent">
        <div class="header">
            <div class="container-fluid">
                <div class="header-body">
                    <div class="row align-items-end">
                        <div class="col">
                            <h1 class="header-title">
                                Agents / View Agents
                            </h1>
                        </div>
                        <div class="col-auto">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12 col-lg-12 col-xl">
                    <div class="card">
                        <div class="card-header">
                            <h4>View Agents</h4>
                        </div>
                        <div class="card-body">
                            <div class="col-md-12">
                                <div class="input-group">
                                    <div class="form-outline">
                                        <input type="search" id="form1" placeholder="search.." class="form-control" />
                                    </div>
                                    <button type="button" class="btn btn-primary">
                                        <i class="fas fa-search"></i>
                                    </button>
                                </div>
                                <label style="padding: 10px">
                                    <b>0 Records</b>
                                </label>
                            </div>
                            <div class="col-md-12 mt-3 row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="mb-2">Commission Plan(s):</label>
                                        <div class="input_container select_container">
                                            <select class="form-select mb-3" data-choices>
                                                <option selected disabled value="">Select an Option</option>
                                                <option>Test(20%)</option>
                                                <option>User(40%)</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="mb-2">Office Location(s):</label>
                                        <div class="input_container select_container">
                                            <select class="form-select mb-3" disabled="disabled" data-choices>
                                                <option>No Result Matched</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="mb-2">Agent Status:</label>
                                        <div class="input_container select_container">
                                            <select class="form-select mb-3" data-choices>
                                                <option>Active, Pending, Pre-License</option>
                                                <option>Pending</option>
                                                <option>Pre-License</option>
                                                <option>Active</option>
                                                <option>Inactive</option>
                                                <option>Terminated</option>
                                                <option>Deseased</option>
                                                <option>All</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group mt-5">
                                        <label class="mincheckbox mincheckbox-success">
                                            Show agents who have notes
                                            <input class="form-check-input list-checkbox" id="listCheckboxOne" type="checkbox">
                                        </label>
                                        <div class="form-group pull-right mt-5">
                                            <div>
                                                <button type="button" class="btn btn-secondary">Reset
                                                    Filter</button>
                                                <button type="submit" class="btn btn-primary">Apply
                                                    Filter(s)</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="table-responsive" data-list='{"valueNames": ["tables-row", "tables-first", "tables-last", "tables-handle"]}'>
                                    <table class="table table-sm">
                                        <thead>
                                        <tr>
                                            <th scope="col">
                                                <a href="#" class="text-muted list-sort" data-sort="tables-first">Agent Name</a>
                                            </th>
                                            <th scope="col">
                                                <a href="#" class="text-muted list-sort" data-sort="tables-last">Commission Plan</a>
                                            </th>
                                            <th scope="col">
                                                <a href="#" class="text-muted list-sort" data-sort="tables-handle">Start Date</a>
                                            </th>
                                            <th scope="col">
                                                <a href="#" class="text-muted list-sort" data-sort="tables-handle">Location</a>
                                            </th>
                                            <th scope="col">
                                                <a href="#" class="text-muted list-sort" data-sort="tables-handle">Status</a>
                                            </th>
                                            <th scope="col">
                                                <a href="#" class="text-muted list-sort" data-sort="tables-handle">Status Change Date</a>
                                            </th>
                                            <th scope="col">
                                                <a href="#" class="text-muted list-sort">Action</a>
                                            </th>
                                        </tr>
                                        </thead>
                                        <tbody class="list">
                                        @foreach($viewAgent as $viewAgents)
                                            <tr>
                                                <td class="tables-first"><a href="javascript:void(0)" onclick="viewAgentProfile('{{$viewAgents->id}}')">{{ $viewAgents->first_name }} {{ $viewAgents->last_name }}</a> </td>
                                                <td class="tables-last">{{ $viewAgents->assign_commission_plan }}</td>
                                                <td class="tables-handle">{{ $viewAgents->start_date }}</td>
                                                <td class="tables-handle"></td>
                                                <td class="tables-handle">{{ $viewAgents->view_agent->plan_status }}</td>
                                                <td class="tables-handle">N/A</td>
                                                <td>
                                                    <a href="#"><i class="far fa-edit"></i></a>
                                                    <a href="{{ route('deleteAgent', $viewAgents->id) }}"><i class="fas fa-trash-alt"></i></a>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="agent-profile" style="display: none">
        <div class="header">
            <div class="container-fluid">
                <div class="header-body">
                    <div class="row align-items-end">
                        <div class="col">
                            <h1 class="header-title">
                                Agents / View Agents / <span class="span_first_name"></span> <span class="span_last_name"></span> / Agent Profile
                            </h1>
                        </div>
                        <div class="col-auto">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12 col-lg-12 col-xl">
                    <div class="card">
                        <div class="card-body">
                            <ul class="nav nav-tabs" id="nav-tabs" role="tablist">
                                <li class="nav-item" role="presentation">
                                    <a class="nav-link active" data-bs-toggle="tab" data-bs-target="#agent_profile" id="agent-profile-tab" type="button" role="tab">Agent Profile</a>
                                </li>
                                <li class="nav-item" role="presentation">
                                    <a class="nav-link" data-bs-toggle="tab" data-bs-target="#documents" id="documents-tab" type="button" role="tab">Documents</a>
                                </li>
                                <li class="nav-item" role="presentation">
                                    <a class="nav-link" data-bs-toggle="tab" data-bs-target="#commission-plan-fee" id="commission-plan-tab" type="button" role="tab">Commission Plan & Fees</a>
                                </li>
                                <li class="nav-item" role="presentation">
                                    <a class="nav-link" data-bs-toggle="tab" data-bs-target="#commission-log" id="commission-log-tab" type="button" role="tab">Commission Log</a>
                                </li>
                                <li class="nav-item" role="presentation">
                                    <a class="nav-link" data-bs-toggle="tab" data-bs-target="#agent-billing" id="agent-billing-tab" type="button" role="tab">Agent Billing</a>
                                </li>
                                <li class="nav-item" role="presentation">
                                    <a class="nav-link" data-bs-toggle="tab" data-bs-target="#billing-log" id="billing-log-tab" type="button" role="tab">Billing Log</a>
                                </li>
                                <li class="nav-item" role="presentation">
                                    <a class="nav-link" data-bs-toggle="tab" data-bs-target="#credit-card-info" id="credit-card-info-tab" type="button" role="tab">Credit Card Info</a>
                                </li>
                                <li class="nav-item" role="presentation">
                                    <a class="nav-link" data-bs-toggle="tab" data-bs-target="#agent-permissions" id="agent-permissions-tab" type="button" role="tab">Agent Permissions</a>
                                </li>
                                <li class="nav-item" role="presentation">
                                    <a class="nav-link" data-bs-toggle="tab" data-bs-target="#notes" id="notes-tab" type="button" role="tab">Notes</a>
                                </li>
                                <li class="nav-item" role="presentation">
                                    <a class="nav-link" data-bs-toggle="tab" data-bs-target="#payment-log" id="payment-log-tab" type="button" role="tab">Payment Log</a>
                                </li>
                                <li class="nav-item" role="presentation">
                                    <a class="nav-link" data-bs-toggle="tab" data-bs-target="#task-checklist" id="task-checklist-tab" type="button" role="tab">Task Checklist</a>
                                </li>
                                <li class="nav-item" role="presentation">
                                    <a class="nav-link" data-bs-toggle="tab" data-bs-target="#other-income" id="other-income-tab" type="button" role="tab">Other Income</a>
                                </li>
                                <li class="nav-item" role="presentation">
                                    <a class="nav-link" data-bs-toggle="tab" data-bs-target="#history" id="history-tab" type="button" role="tab">History</a>
                                </li>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane show active" id="agent_profile">
                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                        <div class="form-group pull-right mt-3">
                                            <button class="btn btn-light showViewAgent">Cancel</button>
                                            <button class="btn btn-primary">Save Changes</button>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="card">
                                                    <div class="card-header">
                                                        <h4>Contact Info</h4>
                                                    </div>
                                                    <div class="card-body">
                                                        <form>
                                                            <div class="form-group mt-2 form-group-row">
                                                                <label class="w-25 mt-2">First Name:</label>
                                                                <input type="text" class="form-control input-group w-75 input_first_name">
                                                            </div>
                                                            <div class="form-group form-group-row">
                                                                <label class="w-25 mt-2">Last Name:</label>
                                                                <input type="text" class="input-group form-control w-75 input_last_name">
                                                            </div>
                                                            <div class="form-group form-group-row">
                                                                <label class="w-25 mt-2">Nick Name:</label>
                                                                <input type="text" class="input-group form-control w-75">
                                                            </div>
                                                            <div class="form-group form-group-row">
                                                                <label class="w-25 mt-2">Lead Source:</label>
                                                                <div class="input_container w-75">
                                                                    <label>
                                                                        <input type="radio">
                                                                        Male
                                                                    </label>
                                                                    <label>
                                                                        <input type="radio">
                                                                        Female
                                                                    </label>
                                                                </div>
                                                            </div>
                                                            <div class="form-group form-group-row">
                                                                <label class="w-25 mt-2">DOB:</label>
                                                                <input type="date" class="input-group form-control w-75">
                                                            </div>
                                                            <div class="form-group form-group-row">
                                                                <label class="w-25 mt-2">Phone:</label>
                                                                <input type="text" placeholder="(_ _ _)-_ _ _-_ _ _" class="input-group form-control w-75">
                                                            </div>
                                                            <div class="form-group form-group-row">
                                                                <label class="w-25 mt-2">Contact Email:</label>
                                                                <div class="form-group-row w-75">
                                                                    <p>test@gmail.com</p>
                                                                    <div>
                                                                        <a href="#" style="margin-left: 100px"><i class="fas fa-edit"></i></a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group form-group-row">
                                                                <label class="w-25 mt-2">Personal Email:</label>
                                                                <input type="email" placeholder="Optional" class="input-group form-control w-75">
                                                            </div>
                                                            <div class="form-group form-group-row">
                                                                <label class="w-25 mt-2">Zip/Postal Code:</label>
                                                                <input type="number" class="input-group form-control w-75">
                                                            </div>
                                                            <div class="form-group form-group-row">
                                                                <label class="w-25 mt-2">Address:</label>
                                                                <input type="text" class="input-group form-control w-75">
                                                            </div>
                                                            <div class="form-group form-group-row">
                                                                <label class="w-25 mt-2">Address2:</label>
                                                                <input type="text" class="input-group form-control w-75">
                                                            </div>
                                                            <div class="form-group form-group-row">
                                                                <label class="w-25 mt-2">City:</label>
                                                                <input type="text" class="input-group form-control w-75">
                                                            </div>
                                                            <div class="form-group form-group-row">
                                                                <label class="w-25 mt-2">Country:</label>
                                                                <input type="text" class="input-group form-control w-75">
                                                            </div>
                                                            <div class="form-group form-group-row">
                                                                <label class="w-25 mt-2">State/Province:</label>
                                                                <input type="text" class="input-group form-control w-75">
                                                            </div>
                                                            <div class="form-group form-group-row">
                                                                <label class="w-25 mt-2">External Agent Id:</label>
                                                                <input type="text" class="input-group form-control w-75">
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="card">
                                                    <div class="card-header">
                                                        <h4>Onboarding</h4>
                                                    </div>
                                                    <div class="card-body">
                                                        <form action="">
                                                            <div class="col-md-12 form-group form-group-row">
                                                                <label for="" class="w-25">Onboarding Status:</label>
                                                                <p>Onboarding Complete</p>
                                                            </div>
                                                            <div class="col-md-12 form-group">
                                                                <button class="btn btn-primary pull-right">Re-send Onboarding</button>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                                <div class="card">
                                                    <div class="card-header">
                                                        <h4>Agent Info</h4>
                                                    </div>
                                                    <div class="card-body">
                                                        <form action="">
                                                            <div class="form-group form-group-row mt-2">
                                                                <label for="" class="w-25 mt-2">Agent Status:</label>
                                                                <div class="input_container select_container w-75">
                                                                    <select class="form-select" data-choices>
                                                                        <option>Pending</option>
                                                                        <option>Pre-license</option>
                                                                        <option>Active</option>
                                                                        <option>Inactive</option>
                                                                        <option>Terminated</option>
                                                                        <option>Deceased</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="form-group form-group-row">
                                                                <label class="w-25 mt-2">DBA/Entity Name:</label>
                                                                <div class="input_container select_container w-75">
                                                                    <input type="text" placeholder="Optional" class="input-group form-control">
                                                                </div>
                                                            </div>
                                                            <div class="form-group form-group-row">
                                                                <label class="w-25 mt-2">DBA/Entity Start Date:</label>
                                                                <div class="input_container select_container w-75">
                                                                    <input type="date" class="input-group form-control">
                                                                </div>
                                                            </div>
                                                            <div class="form-group form-group-row">
                                                                <label class="w-25 mt-2">DBA/Entity End Date:</label>
                                                                <div class="input_container select_container w-75">
                                                                    <input type="date" class="input-group form-control">
                                                                </div>
                                                            </div>
                                                            <div class="form-group form-group-row">
                                                                <label class="w-25 mt-2">Driver's License #:</label>
                                                                <div class="input_container select_container w-75">
                                                                    <input type="text" placeholder="Optional" class="input-group form-control">
                                                                </div>
                                                            </div>
                                                            <div class="form-group form-group-row">
                                                                <label class="w-25 mt-2">Website:</label>
                                                                <div class="input_container select_container w-75">
                                                                    <input type="text" placeholder="Optional" class="input-group form-control">
                                                                </div>
                                                            </div>
                                                            <div class="form-group form-group-row">
                                                                <label class="w-25">Profile Photo Upload: <br>
                                                                    <span>(Optional)</span>
                                                                </label>
                                                                <div class="input_container w-75">
                                                                    <button class="btn btn-secondary">+ Add Photo</button>
                                                                </div>
                                                            </div>

                                                            <div class="form-group form-group-row">
                                                                <label class="w-25 mt-2">Disbursement Instructions:</label>
                                                                <div class="input_container w-75">
                                                                    <textarea name="" placeholder="Optional" class="form-control" cols="43" rows="4"></textarea>
                                                                </div>
                                                            </div>
                                                            <div class="form-group form-group-row">
                                                                <label class="w-25 mt-2">Agent Bio:</label>
                                                                <div class="input_container w-75">
                                                                    <textarea name="" class="form-control" cols="43" rows="4"></textarea>
                                                                </div>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="card">
                                                    <div class="card-header">
                                                        <div class="col-md-12 form-group form-group-row mb-0">
                                                            <h4 class="col-md-6">Agent Work Info</h4>
                                                            <div class="col-md-6">
                                                                <a href="#" class="pull-right mt-3">Manage Offices/Licenses</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="card-body text-center">
                                                        <h4>No Office Assigned</h4>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="card">
                                                    <div class="card-header">
                                                        <h4>Account Info</h4>
                                                    </div>
                                                    <div class="card-body">
                                                        <div class="form-group form-group-row">
                                                            <label class="w-25">Username:</label>
                                                            <div class="w-75 form-group-row">
                                                                <a href="#" style="margin-right: 10px;">test@gmail.com</a>
                                                                <a href="#"> <i class="fas fa-edit"></i> </a>
                                                            </div>
                                                        </div>
                                                        <div class="form-group form-group-row">
                                                            <label class="w-25">Password:</label>
                                                            <div class="w-75 form-group-row">
                                                                <p style="margin-right: 50px;">********</p>
                                                                <a href="#"> Change password </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card-footer text-right">
                                            <button type="button" class="btn btn-default showViewAgent">
                                                Cancel
                                            </button>
                                            <button type="submit" class="btn btn-primary">
                                                Save Changes
                                            </button>
                                        </div>
                                    </div>
                                </div>

                                <div class="tab-pane" id="documents">
                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                        <h4 style="margin-bottom: 30px; margin-top: 20px; font-size: 18px; font-weight: 500; line-height: 1.1; color: inherit;">Document(s)</h4>
                                        <div class="col-md-12 clearfix">
                                            <button class="btn btn-primary pull-right">+ Add Document(s)</button>
                                        </div>
                                        <div class="col-md-12 mt-3">
                                            <div class="table-responsive">
                                                <table class="table table-sm">
                                                    <thead>
                                                    <tr>
                                                        <th scope="col">
                                                            <a href="#" class="text-muted list-sort">Document Name</a>
                                                        </th>
                                                        <th scope="col">
                                                            <a href="#" class="text-muted list-sort">Date Upload</a>
                                                        </th>
                                                        <th scope="col">
                                                            <a href="#" class="text-muted list-sort">Status</a>
                                                        </th>
                                                        <th scope="col">
                                                            <a href="#" class="text-muted list-sort">Action</a>
                                                        </th>
                                                    </tr>
                                                    </thead>
                                                    <tbody class="list">
                                                    <tr>
                                                        <td class="tables-first">Test</td>
                                                        <td class="tables-last">01-01-2021</td>
                                                        <td class="tables-handle">Test</td>
                                                        <td>
                                                            <a href="#"><i class="far fa-edit"></i></a>
                                                            <a href="#"><i class="fas fa-trash-alt"></i></a>
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="tab-pane" id="commission-plan-fee">
                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                        <div class="col-md-6 mt-3">
                                            <div class="card">
                                                <div class="card-header">
                                                    <h4>Commission Plan Datails</h4>
                                                </div>
                                                <div class="card-body">
                                                    <form>
                                                        <div class="form-group mt-2 form-group-row">
                                                            <label class="w-25 mt-2">Plan Assigned:</label>
                                                            <div class="input_container select_container w-75">
                                                                <select class="form-select" id="assignCommissionPlan">
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="form-group form-group-row">
                                                            <label class="w-25">Flat Fee:</label>
                                                            <p class="w-75"> Yes </p>
                                                        </div>
                                                        <div class="form-group form-group-row">
                                                            <label class="w-25">Agent Percentage:</label>
                                                            <p class="w-75"> 25 </p>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="card">
                                                <div class="card-header">
                                                    <h4>Transaction Type Commission Plan</h4>
                                                </div>
                                                <div class="card-body">
                                                    <form action="">
                                                        <div class="form-group form-group-row">
                                                            <label class="w-25">Transaction Type:</label>
                                                            <p class="w-75"> <b>Commission Plan</b> </p>
                                                        </div>
                                                        <div class="form-group form-group-row mt-2">
                                                            <label for="" class="w-25 mt-2">Listing:</label>
                                                            <div class="input_container select_container w-75">
                                                                <select class="form-select" data-choices>
                                                                    <option>Default Plan</option>
                                                                    <option>Test</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="form-group form-group-row mt-2">
                                                            <label for="" class="w-25 mt-2">Selling:</label>
                                                            <div class="input_container select_container w-75">
                                                                <select class="form-select" data-choices>
                                                                    <option>Default Plan</option>
                                                                    <option>Test</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="form-group form-group-row mt-2">
                                                            <label for="" class="w-25 mt-2">Dual:</label>
                                                            <div class="input_container select_container w-75">
                                                                <select class="form-select" data-choices>
                                                                    <option>Default Plan</option>
                                                                    <option>Test</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="form-group form-group-row mt-2">
                                                            <label for="" class="w-25 mt-2">Rental:</label>
                                                            <div class="input_container select_container w-75">
                                                                <select class="form-select" data-choices>
                                                                    <option>Default Plan</option>
                                                                    <option>Test</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="form-group form-group-row mt-2">
                                                            <label for="" class="w-25 mt-2">Referral:</label>
                                                            <div class="input_container select_container w-75">
                                                                <select class="form-select" data-choices>
                                                                    <option>Default Plan</option>
                                                                    <option>Test</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="card">
                                                <div class="card-header">
                                                    <h4>Additional Closing Fees</h4>
                                                </div>
                                                <div class="card-body">
                                                    <div class="col-md-12">
                                                        <button class="btn btn-primary pull-right">+ Add Closing Fees</button>
                                                    </div>

                                                    <div class="clearfix"></div>

                                                    <div class="table-responsive mt-3">
                                                        <table class="table table-sm" id="dtBasicExample">
                                                            <thead>
                                                            <tr>
                                                                <th scope="col">
                                                                    <a href="#" class="text-muted list-sort">Fee Item</a>
                                                                </th>
                                                                <th scope="col">
                                                                    <a href="#" class="text-muted list-sort">Type</a>
                                                                </th>
                                                                <th scope="col">
                                                                    <a href="#" class="text-muted list-sort">Fee</a>
                                                                </th>
                                                                <th scope="col">
                                                                    <a href="#" class="text-muted list-sort">Commission</a>
                                                                </th>
                                                                <th scope="col">
                                                                    <a href="#" class="text-muted list-sort">QB Sync?</a>
                                                                </th>
                                                                <th scope="col">
                                                                    <a href="#" class="text-muted list-sort">End Date</a>
                                                                </th>
                                                                <th scope="col">
                                                                    <a href="#" class="text-muted list-sort">Cap Amount</a>
                                                                </th>
                                                                <th scope="col">
                                                                    <a href="#" class="text-muted list-sort">Roll Over Type</a>
                                                                </th>
                                                                <th scope="col">
                                                                    <a href="#" class="text-muted list-sort">Action</a>
                                                                </th>
                                                            </tr>
                                                            </thead>
                                                            <tbody class="list">
                                                            <tr>
                                                                <td></td>
                                                                <td></td>
                                                                <td></td>
                                                                <td></td>
                                                                <td></td>
                                                                <td></td>
                                                                <td></td>
                                                                <td></td>
                                                                <td>
                                                                    <a href="#"><i class="far fa-edit"></i></a>
                                                                    <a href="#"><i class="far fa-copy"></i></a>
                                                                    <a href="#"> <i class="far fa-trash-alt"></i> </a>
                                                                </td>
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="card">
                                                <div class="card-header">
                                                    <h4>Pre-Commission Credits/Debits</h4>
                                                </div>
                                                <div class="card-body">
                                                    <div class="col-md-12">
                                                        <button class="btn btn-primary pull-right">+ Add Credit/Debit</button>
                                                    </div>
                                                    <div class="clearfix"></div>

                                                    <div class="table-responsive mt-3">
                                                        <table class="table table-sm" id="dtBasicExample">
                                                            <thead>
                                                            <tr>
                                                                <th scope="col">
                                                                    <a href="#" class="text-muted list-sort">Items</a>
                                                                </th>
                                                                <th scope="col">
                                                                    <a href="#" class="text-muted list-sort">Fee</a>
                                                                </th>
                                                                <th scope="col">
                                                                    <a href="#" class="text-muted list-sort">Commission</a>
                                                                </th>
                                                                <th scope="col">
                                                                    <a href="#" class="text-muted list-sort">End Date</a>
                                                                </th>
                                                                <th scope="col">
                                                                    <a href="#" class="text-muted list-sort">Action</a>
                                                                </th>
                                                            </tr>
                                                            </thead>
                                                            <tbody class="list">
                                                            <tr>
                                                                <td></td>
                                                                <td></td>
                                                                <td></td>
                                                                <td></td>
                                                                <td>
                                                                    <a href="#"><i class="far fa-edit"></i></a>
                                                                    <a href="#"><i class="far fa-copy"></i></a>
                                                                    <a href="#"> <i class="far fa-trash-alt"></i> </a>
                                                                </td>
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="card">
                                                <div class="card-header">
                                                    <h4>Post-Commission Credits/Debits</h4>
                                                </div>
                                                <div class="card-body">
                                                    <div class="col-md-12">
                                                        <button class="btn btn-primary pull-right">+ Add Credit/Debit</button>
                                                    </div>
                                                    <div class="clearfix"></div>

                                                    <div class="table-responsive mt-3">
                                                        <table class="table table-sm" id="dtBasicExample">
                                                            <thead>
                                                            <tr>
                                                                <th scope="col">
                                                                    <a href="#" class="text-muted list-sort">Items</a>
                                                                </th>
                                                                <th scope="col">
                                                                    <a href="#" class="text-muted list-sort">Fee</a>
                                                                </th>
                                                                <th scope="col">
                                                                    <a href="#" class="text-muted list-sort">Vendor/Agent Name</a>
                                                                </th>
                                                                <th scope="col">
                                                                    <a href="#" class="text-muted list-sort">End Date</a>
                                                                </th>
                                                                <th scope="col">
                                                                    <a href="#" class="text-muted list-sort">Action</a>
                                                                </th>
                                                            </tr>
                                                            </thead>
                                                            <tbody class="list">
                                                            <tr>
                                                                <td></td>
                                                                <td></td>
                                                                <td></td>
                                                                <td></td>
                                                                <td>
                                                                    <a href="#"><i class="far fa-edit"></i></a>
                                                                    <a href="#"><i class="far fa-copy"></i></a>
                                                                    <a href="#"> <i class="far fa-trash-alt"></i> </a>
                                                                </td>
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="card">
                                                <div class="card-header">
                                                    <h4>Garnishments</h4>
                                                </div>
                                                <div class="card-body">
                                                    <div class="col-md-12">
                                                        <button class="btn btn-primary pull-right">+ Add Garnishments</button>
                                                    </div>
                                                    <div class="clearfix"></div>

                                                    <div class="table-responsive mt-3">
                                                        <table class="table table-sm" id="dtBasicExample">
                                                            <thead>
                                                            <tr>
                                                                <th scope="col">
                                                                    <a href="#" class="text-muted list-sort">Fee Items</a>
                                                                </th>
                                                                <th scope="col">
                                                                    <a href="#" class="text-muted list-sort">Type</a>
                                                                </th>
                                                                <th scope="col">
                                                                    <a href="#" class="text-muted list-sort">Fee</a>
                                                                </th>
                                                                <th scope="col">
                                                                    <a href="#" class="text-muted list-sort">Commission</a>
                                                                </th>
                                                                <th scope="col">
                                                                    <a href="#" class="text-muted list-sort">QB Sync?</a>
                                                                </th>
                                                                <th scope="col">
                                                                    <a href="#" class="text-muted list-sort">Start Date</a>
                                                                </th>
                                                                <th scope="col">
                                                                    <a href="#" class="text-muted list-sort">End Date</a>
                                                                </th>
                                                                <th scope="col">
                                                                    <a href="#" class="text-muted list-sort">Total Amount</a>
                                                                </th>
                                                                <th scope="col">
                                                                    <a href="#" class="text-muted list-sort">Action</a>
                                                                </th>
                                                            </tr>
                                                            </thead>
                                                            <tbody class="list">
                                                            <tr>
                                                                <td></td>
                                                                <td></td>
                                                                <td></td>
                                                                <td></td>
                                                                <td></td>
                                                                <td></td>
                                                                <td></td>
                                                                <td></td>
                                                                <td>
                                                                    <a href="#"><i class="far fa-edit"></i></a>
                                                                    <a href="#"><i class="far fa-copy"></i></a>
                                                                    <a href="#"> <i class="far fa-trash-alt"></i> </a>
                                                                </td>
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card-footer text-right">
                                            <button type="button" class="btn btn-default showViewAgent">
                                                Cancel
                                            </button>
                                            <button type="submit" class="btn btn-primary">
                                                Save Changes
                                            </button>
                                        </div>
                                    </div>
                                </div>

                                <div class="tab-pane" id="commission-log">
                                    <div class="col-md-12 col-lg-12 col-xl">
                                        <div class="card">
                                            <div class="card-header">
                                                <h4>Commission Log</h4>
                                            </div>
                                            <div class="card-body">
                                                <form>
                                                    <div class="col-md-12 row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="mb-2">Close Date:</label>
                                                                <div class="input_container select_container">
                                                                    <input type="date" class="form-control input-group">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="mb-2">Pay Status:</label>
                                                                <div class="input_container select_container">
                                                                    <select class="form-select mb-3" data-choices>
                                                                        <option>All</option>
                                                                        <option>Paid</option>
                                                                        <option>Unpaid</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="form-group filter_action pull-right">
                                                                <div>
                                                                    <button type="button" class="btn btn-secondary">Reset
                                                                        Filter</button>
                                                                    <button type="submit" class="btn btn-primary">Apply
                                                                        Filter(s)</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>

                                                <div class="col-md-12 row">
                                                    <div class="col-lg-4 col-md-4">
                                                        <div class="panel panel-skyblue">
                                                            <div class="panel-heading">
                                                                <div class="form-group-row">
                                                                    <div class="col-md-6 text-left">
                                                                        <div class="huge1">0</div>
                                                                        <div>Deals in Total</div>
                                                                    </div>
                                                                    <div class="col-md-6">
                                                                        <i class="fas fa-home icon1"></i>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4 col-md-4">
                                                        <div class="panel panel-skyblue">
                                                            <div class="panel-heading">
                                                                <div class="form-group-row">
                                                                    <div class="col-md-6 text-left">
                                                                        <div class="huge1">$&nbsp;0</div>
                                                                        <div>Total Agent Commission</div>
                                                                    </div>
                                                                    <div class="col-md-6">
                                                                        <i class="fas fa-tag icon1"></i>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4 col-md-4">
                                                        <div class="panel panel-skyblue">
                                                            <div class="panel-heading">
                                                                <div class="form-group-row">
                                                                    <div class="col-md-6 text-left">
                                                                        <div class="huge1">$&nbsp;0</div>
                                                                        <div>Total Sales Volume</div>
                                                                    </div>
                                                                    <div class="col-md-6">
                                                                        <i class="fas fa-dollar-sign icon1 "></i>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="table-responsive">
                                                    <table class="table table-sm" id="dtBasicExample" data-list='{"valueNames": ["tables-row", "tables-first", "tables-last", "tables-handle"]}'>
                                                        <thead>
                                                        <tr>
                                                            <th scope="col">
                                                                <a href="#" class="text-muted list-sort" data-sort="tables-first">Close Date</a>
                                                            </th>
                                                            <th scope="col">
                                                                <a href="#" class="text-muted list-sort" data-sort="tables-last">My Commission</a>
                                                            </th>
                                                            <th scope="col">
                                                                <a href="#" class="text-muted list-sort" data-sort="tables-handle">Address</a>
                                                            </th>
                                                            <th scope="col">
                                                                <a href="#" class="text-muted list-sort" data-sort="tables-handle">Sale Price</a>
                                                            </th>
                                                            <th scope="col">
                                                                <a href="#" class="text-muted list-sort" data-sort="tables-handle">Status</a>
                                                            </th>
                                                        </tr>
                                                        </thead>
                                                        <tbody class="list">
                                                        <tr>
                                                            <td class="tables-first"></td>
                                                            <td class="tables-last"></td>
                                                            <td class="tables-handle"></td>
                                                            <td class="tables-handle"></td>
                                                            <td class="tables-handle"></td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="tab-pane" id="agent-billing">
                                    <div class="col-lg-12 col-md-12 col-sm-12 mt-3">
                                        <div class="card">
                                            <div class="card-header">
                                                <h4>Monthly Billing Items</h4>
                                            </div>
                                            <div class="card-body">
                                                <div class="col-md-12">
                                                    <div class="form-group form-group-row">
                                                        <label class="mt-2" style="margin-right: 12px;">Monthly Billing On:</label>
                                                        <select class="form-select" style="width: 5rem; margin-right: .3rem">
                                                            <option selected>1</option>
                                                            <option>2</option>
                                                            <option>3</option>
                                                            <option>4</option>
                                                            <option>5</option>
                                                            <option>6</option>
                                                            <option>7</option>
                                                            <option>8</option>
                                                            <option>9</option>
                                                            <option>10</option>
                                                            <option>11</option>
                                                            <option>12</option>
                                                            <option>13</option>
                                                            <option>14</option>
                                                            <option>15</option>
                                                            <option>16</option>
                                                            <option>17</option>
                                                            <option>18</option>
                                                            <option>19</option>
                                                            <option>20</option>
                                                            <option>21</option>
                                                            <option>22</option>
                                                            <option>23</option>
                                                            <option>24</option>
                                                            <option>25</option>
                                                            <option>26</option>
                                                            <option>27</option>
                                                            <option>28</option>
                                                            <option>29</option>
                                                            <option>30</option>
                                                        </select>
                                                        <b class="mt-2">of every month</b>
                                                    </div>
                                                </div>
                                                <div class="col-md-12 clearfix">
                                                    <button class="btn btn-secondary pull-right">+ Add New Item</button>
                                                </div>
                                                <div class="col-md-12 mt-3">
                                                    <div class="table-responsive">
                                                        <table class="table table-sm">
                                                            <thead>
                                                            <tr>
                                                                <th scope="col">
                                                                    <a href="#" class="text-muted list-sort">Description</a>
                                                                </th>
                                                                <th scope="col">
                                                                    <a href="#" class="text-muted list-sort">Quantity</a>
                                                                </th>
                                                                <th scope="col">
                                                                    <a href="#" class="text-muted list-sort">Price</a>
                                                                </th>
                                                                <th scope="col">
                                                                    <a href="#" class="text-muted list-sort">Total Amount</a>
                                                                </th>
                                                                <th scope="col">
                                                                    <a href="#" class="text-muted list-sort">Status</a>
                                                                </th>
                                                                <th scope="col">
                                                                    <a href="#" class="text-muted list-sort">Don't Charge CC</a>
                                                                </th>
                                                                <th scope="col">
                                                                    <a href="#" class="text-muted list-sort">Start Date</a>
                                                                </th>
                                                                <th scope="col">
                                                                    <a href="#" class="text-muted list-sort">End Date</a>
                                                                </th>
                                                                <th scope="col">
                                                                    <a href="#" class="text-muted list-sort">Memo</a>
                                                                </th>
                                                                <th scope="col">
                                                                    <a href="#" class="text-muted list-sort">Action</a>
                                                                </th>
                                                            </tr>
                                                            </thead>
                                                            <tbody class="list">
                                                            <tr>
                                                                <td></td>
                                                                <td></td>
                                                                <td></td>
                                                                <td></td>
                                                                <td></td>
                                                                <td></td>
                                                                <td></td>
                                                                <td></td>
                                                                <td></td>
                                                                <td>
                                                                    <a href="#"><i class="far fa-edit"></i></a>
                                                                    <a href="#"><i class="fas fa-trash-alt"></i></a>
                                                                </td>
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <button class="btn btn-primary pull-right">+ Add New Section</button>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="col-lg-12 col-md-12 col-sm-12 mt-3">
                                        <div class="card">
                                            <div class="card-header">
                                                <h4>One Time Invoice</h4>
                                            </div>
                                            <div class="card-body">
                                                <div class="col-md-12 clearfix">
                                                    <button class="btn btn-secondary pull-right">+ Add New Item</button>
                                                </div>
                                                <div class="col-md-12 mt-3">
                                                    <div class="table-responsive">
                                                        <table class="table table-sm">
                                                            <thead>
                                                            <tr>
                                                                <th scope="col">
                                                                    <a href="#" class="text-muted list-sort">Description</a>
                                                                </th>
                                                                <th scope="col">
                                                                    <a href="#" class="text-muted list-sort">Quantity</a>
                                                                </th>
                                                                <th scope="col">
                                                                    <a href="#" class="text-muted list-sort">Price</a>
                                                                </th>
                                                                <th scope="col">
                                                                    <a href="#" class="text-muted list-sort">Total Amount</a>
                                                                </th>
                                                                <th scope="col">
                                                                    <a href="#" class="text-muted list-sort">Memo</a>
                                                                </th>
                                                                <th scope="col">
                                                                    <a href="#" class="text-muted list-sort">Action</a>
                                                                </th>
                                                            </tr>
                                                            </thead>
                                                            <tbody class="list">
                                                            <tr>
                                                                <td class="tables-first"></td>
                                                                <td class="tables-last"></td>
                                                                <td class="tables-handle"></td>
                                                                <td class="tables-handle"></td>
                                                                <td class="tables-handle"></td>
                                                                <td>
                                                                    <a href="#"><i class="far fa-edit"></i></a>
                                                                    <a href="#"><i class="fas fa-trash-alt"></i></a>
                                                                </td>
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="pull-right form-group-row">
                                                <button class="btn btn-primary" style="margin-right: 5px;">Email One Time Invoice</button>
                                                <button class="btn btn-primary">Log One Time Invoice</button>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="col-lg-12 col-md-12 col-sm-12 mt-3">
                                        <div class="card">
                                            <div class="card-header">
                                                <h4>Quarterly Billing Items</h4>
                                            </div>
                                            <div class="card-body">
                                                <div class="col-md-12">
                                                    <div class="form-group form-group-row">
                                                        <label class="mt-2" style="margin-right: 12px;">Quaterly Billing On:</label>
                                                        <select class="form-select" style="width: 5rem; margin-right: .3rem">
                                                            <option selected>JAN</option>
                                                            <option>FEB</option>
                                                            <option>MAR</option>
                                                            <option>APR</option>
                                                            <option>MAY</option>
                                                            <option>JUN</option>
                                                            <option>JUL</option>
                                                            <option>AUG</option>
                                                            <option>SEP</option>
                                                            <option>OCT</option>
                                                            <option>NOV</option>
                                                            <option>DEC</option>
                                                        </select>
                                                        <select class="form-select" style="width: 5rem; margin-right: .3rem">
                                                            <option selected>1</option>
                                                            <option>2</option>
                                                            <option>3</option>
                                                            <option>4</option>
                                                            <option>5</option>
                                                            <option>6</option>
                                                            <option>7</option>
                                                            <option>8</option>
                                                            <option>9</option>
                                                            <option>10</option>
                                                            <option>11</option>
                                                            <option>12</option>
                                                            <option>13</option>
                                                            <option>14</option>
                                                            <option>15</option>
                                                            <option>16</option>
                                                            <option>17</option>
                                                            <option>18</option>
                                                            <option>19</option>
                                                            <option>20</option>
                                                            <option>21</option>
                                                            <option>22</option>
                                                            <option>23</option>
                                                            <option>24</option>
                                                            <option>25</option>
                                                            <option>26</option>
                                                            <option>27</option>
                                                            <option>28</option>
                                                            <option>29</option>
                                                            <option>30</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-12 clearfix">
                                                    <button class="btn btn-secondary pull-right">+ Add New Item</button>
                                                </div>
                                                <div class="col-md-12 mt-3">
                                                    <div class="table-responsive">
                                                        <table class="table table-sm">
                                                            <thead>
                                                            <tr>
                                                                <th scope="col">
                                                                    <a href="#" class="text-muted list-sort">Description</a>
                                                                </th>
                                                                <th scope="col">
                                                                    <a href="#" class="text-muted list-sort">Quantity</a>
                                                                </th>
                                                                <th scope="col">
                                                                    <a href="#" class="text-muted list-sort">Price</a>
                                                                </th>
                                                                <th scope="col">
                                                                    <a href="#" class="text-muted list-sort">Total Amount</a>
                                                                </th>
                                                                <th scope="col">
                                                                    <a href="#" class="text-muted list-sort">Status</a>
                                                                </th>
                                                                <th scope="col">
                                                                    <a href="#" class="text-muted list-sort">Don't Charge CC</a>
                                                                </th>
                                                                <th scope="col">
                                                                    <a href="#" class="text-muted list-sort">Start Date</a>
                                                                </th>
                                                                <th scope="col">
                                                                    <a href="#" class="text-muted list-sort">End Date</a>
                                                                </th>
                                                                <th scope="col">
                                                                    <a href="#" class="text-muted list-sort">Memo</a>
                                                                </th>
                                                                <th scope="col">
                                                                    <a href="#" class="text-muted list-sort">Action</a>
                                                                </th>
                                                            </tr>
                                                            </thead>
                                                            <tbody class="list">
                                                            <tr>
                                                                <td></td>
                                                                <td></td>
                                                                <td></td>
                                                                <td></td>
                                                                <td></td>
                                                                <td></td>
                                                                <td></td>
                                                                <td></td>
                                                                <td></td>
                                                                <td>
                                                                    <a href="#"><i class="far fa-edit"></i></a>
                                                                    <a href="#"><i class="fas fa-trash-alt"></i></a>
                                                                </td>
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-12 col-md-12 col-sm-12 mt-3">
                                        <div class="card">
                                            <div class="card-header">
                                                <h4>Bi-Annual Billing Items</h4>
                                            </div>
                                            <div class="card-body">
                                                <div class="col-md-12">
                                                    <div class="form-group form-group-row">
                                                        <label class="mt-2" style="margin-right: 12px;">Bi-Annual Billing On:</label>
                                                        <select class="form-select" style="width: 5rem; margin-right: .3rem">
                                                            <option selected>JAN</option>
                                                            <option>FEB</option>
                                                            <option>MAR</option>
                                                            <option>APR</option>
                                                            <option>MAY</option>
                                                            <option>JUN</option>
                                                            <option>JUL</option>
                                                            <option>AUG</option>
                                                            <option>SEP</option>
                                                            <option>OCT</option>
                                                            <option>NOV</option>
                                                            <option>DEC</option>
                                                        </select>
                                                        <select class="form-select" style="width: 5rem; margin-right: .3rem">
                                                            <option selected>1</option>
                                                            <option>2</option>
                                                            <option>3</option>
                                                            <option>4</option>
                                                            <option>5</option>
                                                            <option>6</option>
                                                            <option>7</option>
                                                            <option>8</option>
                                                            <option>9</option>
                                                            <option>10</option>
                                                            <option>11</option>
                                                            <option>12</option>
                                                            <option>13</option>
                                                            <option>14</option>
                                                            <option>15</option>
                                                            <option>16</option>
                                                            <option>17</option>
                                                            <option>18</option>
                                                            <option>19</option>
                                                            <option>20</option>
                                                            <option>21</option>
                                                            <option>22</option>
                                                            <option>23</option>
                                                            <option>24</option>
                                                            <option>25</option>
                                                            <option>26</option>
                                                            <option>27</option>
                                                            <option>28</option>
                                                            <option>29</option>
                                                            <option>30</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-12 clearfix">
                                                    <button class="btn btn-secondary pull-right">+ Add New Item</button>
                                                </div>
                                                <div class="col-md-12 mt-3">
                                                    <div class="table-responsive">
                                                        <table class="table table-sm">
                                                            <thead>
                                                            <tr>
                                                                <th scope="col">
                                                                    <a href="#" class="text-muted list-sort">Description</a>
                                                                </th>
                                                                <th scope="col">
                                                                    <a href="#" class="text-muted list-sort">Quantity</a>
                                                                </th>
                                                                <th scope="col">
                                                                    <a href="#" class="text-muted list-sort">Price</a>
                                                                </th>
                                                                <th scope="col">
                                                                    <a href="#" class="text-muted list-sort">Total Amount</a>
                                                                </th>
                                                                <th scope="col">
                                                                    <a href="#" class="text-muted list-sort">Status</a>
                                                                </th>
                                                                <th scope="col">
                                                                    <a href="#" class="text-muted list-sort">Don't Charge CC</a>
                                                                </th>
                                                                <th scope="col">
                                                                    <a href="#" class="text-muted list-sort">Start Date</a>
                                                                </th>
                                                                <th scope="col">
                                                                    <a href="#" class="text-muted list-sort">End Date</a>
                                                                </th>
                                                                <th scope="col">
                                                                    <a href="#" class="text-muted list-sort">Memo</a>
                                                                </th>
                                                                <th scope="col">
                                                                    <a href="#" class="text-muted list-sort">Action</a>
                                                                </th>
                                                            </tr>
                                                            </thead>
                                                            <tbody class="list">
                                                            <tr>
                                                                <td></td>
                                                                <td></td>
                                                                <td></td>
                                                                <td></td>
                                                                <td></td>
                                                                <td></td>
                                                                <td></td>
                                                                <td></td>
                                                                <td></td>
                                                                <td>
                                                                    <a href="#"><i class="far fa-edit"></i></a>
                                                                    <a href="#"><i class="fas fa-trash-alt"></i></a>
                                                                </td>
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-12 col-md-12 col-sm-12 mt-3">
                                        <div class="card">
                                            <div class="card-header">
                                                <h4>Annual Billing Items</h4>
                                            </div>
                                            <div class="card-body">
                                                <div class="col-md-12">
                                                    <div class="form-group form-group-row">
                                                        <label class="mt-2" style="margin-right: 12px;">Annual Billing On:</label>
                                                        <select class="form-select" style="width: 5rem; margin-right: .3rem">
                                                            <option selected>JAN</option>
                                                            <option>FEB</option>
                                                            <option>MAR</option>
                                                            <option>APR</option>
                                                            <option>MAY</option>
                                                            <option>JUN</option>
                                                            <option>JUL</option>
                                                            <option>AUG</option>
                                                            <option>SEP</option>
                                                            <option>OCT</option>
                                                            <option>NOV</option>
                                                            <option>DEC</option>
                                                        </select>
                                                        <select class="form-select" style="width: 5rem; margin-right: .3rem">
                                                            <option selected>1</option>
                                                            <option>2</option>
                                                            <option>3</option>
                                                            <option>4</option>
                                                            <option>5</option>
                                                            <option>6</option>
                                                            <option>7</option>
                                                            <option>8</option>
                                                            <option>9</option>
                                                            <option>10</option>
                                                            <option>11</option>
                                                            <option>12</option>
                                                            <option>13</option>
                                                            <option>14</option>
                                                            <option>15</option>
                                                            <option>16</option>
                                                            <option>17</option>
                                                            <option>18</option>
                                                            <option>19</option>
                                                            <option>20</option>
                                                            <option>21</option>
                                                            <option>22</option>
                                                            <option>23</option>
                                                            <option>24</option>
                                                            <option>25</option>
                                                            <option>26</option>
                                                            <option>27</option>
                                                            <option>28</option>
                                                            <option>29</option>
                                                            <option>30</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-12 clearfix">
                                                    <button class="btn btn-secondary pull-right">+ Add New Item</button>
                                                </div>
                                                <div class="col-md-12 mt-3">
                                                    <div class="table-responsive">
                                                        <table class="table table-sm">
                                                            <thead>
                                                            <tr>
                                                                <th scope="col">
                                                                    <a href="#" class="text-muted list-sort">Description</a>
                                                                </th>
                                                                <th scope="col">
                                                                    <a href="#" class="text-muted list-sort">Quantity</a>
                                                                </th>
                                                                <th scope="col">
                                                                    <a href="#" class="text-muted list-sort">Price</a>
                                                                </th>
                                                                <th scope="col">
                                                                    <a href="#" class="text-muted list-sort">Total Amount</a>
                                                                </th>
                                                                <th scope="col">
                                                                    <a href="#" class="text-muted list-sort">Status</a>
                                                                </th>
                                                                <th scope="col">
                                                                    <a href="#" class="text-muted list-sort">Don't Charge CC</a>
                                                                </th>
                                                                <th scope="col">
                                                                    <a href="#" class="text-muted list-sort">Start Date</a>
                                                                </th>
                                                                <th scope="col">
                                                                    <a href="#" class="text-muted list-sort">End Date</a>
                                                                </th>
                                                                <th scope="col">
                                                                    <a href="#" class="text-muted list-sort">Memo</a>
                                                                </th>
                                                                <th scope="col">
                                                                    <a href="#" class="text-muted list-sort">Action</a>
                                                                </th>
                                                            </tr>
                                                            </thead>
                                                            <tbody class="list">
                                                            <tr>
                                                                <td></td>
                                                                <td></td>
                                                                <td></td>
                                                                <td></td>
                                                                <td></td>
                                                                <td></td>
                                                                <td></td>
                                                                <td></td>
                                                                <td></td>
                                                                <td>
                                                                    <a href="#"><i class="far fa-edit"></i></a>
                                                                    <a href="#"><i class="fas fa-trash-alt"></i></a>
                                                                </td>
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <button class="btn btn-primary pull-right">+ Add New Annual Section</button>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="card-header">
                                        <button class="btn btn-light pull-right mt-5 mb-0 showViewAgent">Cancel</button>
                                    </div>
                                </div>

                                <div class="tab-pane" id="billing-log">
                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                        <h4 style="margin-bottom: 10px; margin-top: 20px; font-size: 18px; font-weight: 500; line-height: 1.1; color: inherit;">Billing Log</h4>
                                        <p>Agent Credit: $0.00 <a href="#"><i class="fas fa-plus-circle"></i></a> </p>
                                        <div class="col-md-12 clearfix row">
                                            <div class="col-md-10"> <button class="btn btn-primary pull-right">Email all Outstanding Invoices</button> </div>
                                            <div class="col-md-2"> <button class="btn btn-secondary pull-right">Refresh</button> </div>
                                        </div>
                                        <div class="col-md-12 mt-3">
                                            <div class="table-responsive">
                                                <table class="table table-sm" data-list='{"valueNames": ["tables-row", "tables-first", "tables-last", "tables-handle"]}'>
                                                    <thead>
                                                    <tr>
                                                        <th scope="col">
                                                            <a href="#" class="text-muted list-sort">#</a>
                                                        </th>
                                                        <th scope="col">
                                                            <a href="#" class="text-muted list-sort" data-sort="tables-first">Description</a>
                                                        </th>
                                                        <th scope="col">
                                                            <a href="#" class="text-muted list-sort" data-sort="tables-last">Billing Date</a>
                                                        </th>
                                                        <th scope="col">
                                                            <a href="#" class="text-muted list-sort" data-sort="tables-handle">Amount</a>
                                                        </th>
                                                        <th scope="col">
                                                            <a href="#" class="text-muted list-sort" data-sort="tables-handle">Status</a>
                                                        </th>
                                                        <th scope="col">
                                                            <a href="#" class="text-muted list-sort">Memo</a>
                                                        </th>
                                                        <th scope="col">
                                                            <a href="#" class="text-muted list-sort" data-sort="tables-handle">Pay Date</a>
                                                        </th>
                                                        <th scope="col">
                                                            <a href="#" class="text-muted list-sort" data-sort="tables-handle">Action</a>
                                                        </th>
                                                    </tr>
                                                    </thead>
                                                    <tbody class="list">
                                                    <tr>
                                                        <td></td>
                                                        <td class="tables-first"></td>
                                                        <td class="tables-last"></td>
                                                        <td class="tables-handle"></td>
                                                        <td class="tables-handle"></td>
                                                        <td></td>
                                                        <td class="tables-handle"></td>
                                                        <td>
                                                            <a href="#"><i class="far fa-edit"></i></a>
                                                            <a href="#"><i class="fas fa-trash-alt"></i></a>
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="tab-pane" id="credit-card-info">
                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                        <div class="col-md-6 mt-3">
                                            <div class="card">
                                                <div class="card-header">
                                                    <h4>Credit Card Info</h4>
                                                </div>
                                                <div class="card-body">
                                                    <p>Looks like you are not signed up for this feature!
                                                        Learn more about how to enable this option by visiting the <a href="#">features page</a> .</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card-footer text-right">
                                            <button type="button" class="btn btn-light showViewAgent">
                                                Cancel
                                            </button>
                                        </div>
                                    </div>
                                </div>

                                <div class="tab-pane agent-permissions" id="agent-permissions">
                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                        <form>
                                            <div class="form-group mt-5">
                                                <label for="">
                                                    <input type="checkbox" class="form-check-input">
                                                    Allow Agents to Add Transactions
                                                </label>
                                            </div>
                                            <div class="form-group">
                                                <label for="">
                                                    <input type="checkbox" class="form-check-input">
                                                    Allow Agents to Edit Transactions
                                                </label>
                                            </div>
                                            <div class="form-group">
                                                <label for="">
                                                    <input type="checkbox" class="form-check-input">
                                                    Allow agent to edit commission or sale price when transaction is at 50%
                                                </label>
                                            </div>
                                            <div class="form-group">
                                                <label for="">
                                                    <input type="checkbox" class="form-check-input">
                                                    Allow Agents to Upload Transaction Documents
                                                </label>
                                            </div>
                                            <div class="form-group">
                                                <label for="">
                                                    <input type="checkbox" class="form-check-input">
                                                    Send email to agent(s) when disbursement is created
                                                </label>
                                            </div>
                                            <div class="form-group">
                                                <label for="">
                                                    <input type="checkbox" class="form-check-input">
                                                    Allow Agents to Edit Commission Plan
                                                </label>
                                            </div>
                                            <div class="form-group">
                                                <label for="">
                                                    <input type="checkbox" class="form-check-input">
                                                    Email Agent When Disbursement Is Approved
                                                </label>
                                            </div>
                                            <div class="form-group">
                                                <label for="">
                                                    <input type="checkbox" class="form-check-input">
                                                    Allow Agents to Delete Transactions
                                                </label>
                                            </div>
                                            <div class="form-group">
                                                <label for="">
                                                    <input type="checkbox" class="form-check-input">
                                                    Allow Agents to Delete Transaction Documents
                                                </label>
                                            </div>
                                            <div class="form-group">
                                                <label for="">
                                                    <input type="checkbox" class="form-check-input">
                                                    Allow Agents to Complete Task Lists
                                                </label>
                                            </div>
                                            <div class="form-group">
                                                <label for="">
                                                    <input type="checkbox" class="form-check-input">
                                                    Allow Agents to Add / Edit Trust Account / Deposit Section
                                                </label>
                                            </div>
                                            <div class="form-group">
                                                <label for="">
                                                    <input type="checkbox" class="form-check-input">
                                                    Allow Agents to Add Notes to Transaction
                                                </label>
                                            </div>
                                            <div class="form-group">
                                                <label for="">
                                                    <input type="checkbox" class="form-check-input">
                                                    Don't Allow Agents to Edit Their Name and Company
                                                </label>
                                            </div>
                                            <div class="form-group">
                                                <label for="">
                                                    <input type="checkbox" class="form-check-input">
                                                    Don't Allow Agents to Enter Their Own Credit Cards
                                                </label>
                                            </div>
                                            <div class="form-group">
                                                <label for="">
                                                    <input type="checkbox" class="form-check-input">
                                                    Allow Agent to View their Commission Plan details
                                                </label>
                                            </div>

                                            <h4>Agent Disbursement Settings:-</h4>

                                            <div class="form-group">
                                                <label for="">
                                                    <input type="checkbox" class="form-check-input">
                                                    Allow Agents to View Disbursements
                                                </label>
                                            </div>
                                            <div class="form-group">
                                                <label for="">
                                                    <input type="checkbox" class="form-check-input">
                                                    Allow Agents to Create Disbursements
                                                </label>
                                            </div>
                                            <div class="form-group">
                                                <label for="">
                                                    <input type="checkbox" class="form-check-input">
                                                    Disbursements Must be Approved
                                                </label>
                                            </div>
                                            <div class="form-group">
                                                <label for="">
                                                    <input type="checkbox" class="form-check-input">
                                                    Lock disbursement from agent editing once disbursement approval request is sent.
                                                </label>
                                            </div>
                                            <div class="form-group">
                                                <label for="">
                                                    <input type="checkbox" class="form-check-input">
                                                    Allow Agents to Delete Disbursements
                                                </label>
                                            </div>

                                            <h4>Agent Reports:-</h4>

                                            <div class="form-group">
                                                <label for="">
                                                    <input type="checkbox" class="form-check-input">
                                                    Turn on Commission Detail Report
                                                </label>
                                            </div>
                                            <div class="form-group">
                                                <label for="">
                                                    <input type="checkbox" class="form-check-input">
                                                    Allow Agent to View Cap Plan Report
                                                </label>
                                            </div>
                                            <div class="form-group">
                                                <label for="">
                                                    <input type="checkbox" class="form-check-input">
                                                    Allow Agent to View Sliding Scale Plan Report
                                                </label>
                                            </div>

                                        </form>
                                    </div>
                                </div>

                                <div class="tab-pane" id="notes">
                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                        <h4 style="margin-bottom: 10px; margin-top: 20px; font-size: 18px; font-weight: 500; line-height: 1.1; color: inherit;">Notes</h4>
                                        <div class="col-md-12 clearfix">
                                            <button class="btn btn-primary pull-right">+ Add Notes</button>
                                        </div>
                                        <div class="col-md-12 mt-3">
                                            <div class="table-responsive">
                                                <table class="table table-sm">
                                                    <thead>
                                                    <tr>
                                                        <th scope="col">
                                                            <a href="#" class="text-muted list-sort">#</a>
                                                        </th>
                                                        <th scope="col">
                                                            <a href="#" class="text-muted list-sort">Date</a>
                                                        </th>
                                                        <th scope="col">
                                                            <a href="#" class="text-muted list-sort">User</a>
                                                        </th>
                                                        <th scope="col">
                                                            <a href="#" class="text-muted list-sort">Notes</a>
                                                        </th>
                                                        <th scope="col">
                                                            <a href="#" class="text-muted list-sort">Action</a>
                                                        </th>
                                                    </tr>
                                                    </thead>
                                                    <tbody class="list">
                                                    <tr>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td>
                                                            <a href="#"><i class="far fa-edit"></i></a>
                                                            <a href="#"><i class="fas fa-trash-alt"></i></a>
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="card-footer text-right">
                                            <button class="btn btn-light showViewAgent">Cancel</button>
                                        </div>
                                    </div>
                                </div>

                                <div class="tab-pane" id="payment-log">
                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                        <h4 style="margin-bottom: 20px; margin-top: 20px; font-size: 18px; font-weight: 500; line-height: 1.1; color: inherit;">ACH Transfer Payment Log</h4>
                                        <div class="col-md-12 mt-3">
                                            <div class="table-responsive">
                                                <table class="table table-sm">
                                                    <thead>
                                                    <tr>
                                                        <th scope="col">
                                                            <a href="#" class="text-muted list-sort">Date</a>
                                                        </th>
                                                        <th scope="col">
                                                            <a href="#" class="text-muted list-sort">Amount</a>
                                                        </th>
                                                        <th scope="col">
                                                            <a href="#" class="text-muted list-sort">Status</a>
                                                        </th>
                                                        <th scope="col">
                                                            <a href="#" class="text-muted list-sort">Type</a>
                                                        </th>
                                                        <th scope="col">
                                                            <a href="#" class="text-muted list-sort">Transaction</a>
                                                        </th>
                                                        <th scope="col">
                                                            <a href="#" class="text-muted list-sort">Notes</a>
                                                        </th>
                                                    </tr>
                                                    </thead>
                                                    <tbody class="list">
                                                    <tr>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="card-footer text-right">
                                            <button class="btn btn-light showViewAgent">Cancel</button>
                                        </div>
                                    </div>
                                </div>

                                <div class="tab-pane" id="task-checklist">
                                    <div class="col-lg-12 col-md-12 col-sm-12 mt-3">
                                        <div class="card">
                                            <div class="card-header">
                                                <h4>Task Checklist</h4>
                                            </div>
                                            <div class="card-body">
                                                <p>Go to <a href="#">Settings > Agent Checklist</a> to create your first checklist.</p>
                                            </div>
                                            <div class="card-footer text-right">
                                                <button class="btn btn-light showViewAgent">Cancel</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="tab-pane" id="other-income">
                                    <div class="col-md-12 col-lg-12 col-xl">
                                        <div class="card mt-3">
                                            <div class="card-body">
                                                <h4 style="margin-bottom: 10px; margin-top: 10px; font-size: 18px; font-weight: 500; line-height: 1.1; color: inherit;">Document(s)</h4>
                                                <div class="col-md-12 clearfix">
                                                    <button class="btn btn-primary pull-right">+ Add Other Income</button>
                                                </div>
                                                <form>
                                                    <div class="col-md-12 row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="mb-2">Close Date:</label>
                                                                <div class="input_container select_container">
                                                                    <input type="date" class="form-control input-group">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group pull-right">
                                                                <div class="mt-5">
                                                                    <button type="button" class="btn btn-secondary">Reset
                                                                        Filter</button>
                                                                    <button type="submit" class="btn btn-primary">Apply
                                                                        Filter(s)</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>

                                                <div class="table-responsive">
                                                    <table class="table table-sm" id="dtBasicExample">
                                                        <thead>
                                                        <tr>
                                                            <th scope="col">
                                                                <a href="#" class="text-muted list-sort">Item Name</a>
                                                            </th>
                                                            <th scope="col">
                                                                <a href="#" class="text-muted list-sort">Income Amount</a>
                                                            </th>
                                                            <th scope="col">
                                                                <a href="#" class="text-muted list-sort">Date</a>
                                                            </th>
                                                            <th scope="col">
                                                                <a href="#" class="text-muted list-sort">Details</a>
                                                            </th>
                                                            <th scope="col">
                                                                <a href="#" class="text-muted list-sort">Status</a>
                                                            </th>
                                                            <th scope="col">
                                                                <a href="#" class="text-muted list-sort">Pay Date</a>
                                                            </th>
                                                            <th scope="col">
                                                                <a href="#" class="text-muted list-sort">Action</a>
                                                            </th>
                                                        </tr>
                                                        </thead>
                                                        <tbody class="list">
                                                        <tr>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            <td>
                                                                <a href=""> <i class="fas fa-edit"></i> </a>
                                                                <a href=""> <i class="fas fa-trash-alt"></i> </a>
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="tab-pane" id="history">
                                    <div class="col-md-12 col-lg-12 col-xl">
                                        <div class="card mt-3">
                                            <div class="card-body">
                                                <h4 style="margin-bottom: 30px; margin-top: 10px; font-size: 18px; font-weight: 500; line-height: 1.1; color: inherit;">Agent History</h4>
                                                <form>
                                                    <div class="col-md-12 row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="mb-2">Close Date:</label>
                                                                <div class="input_container select_container">
                                                                    <input type="date" class="form-control input-group">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="mb-2">Search By Column Name:</label>
                                                                <div class="input_container select_container">
                                                                    <input type="text" class="form-control input-group">
                                                                </div>
                                                            </div>
                                                            <div class="form-group pull-right">
                                                                <div>
                                                                    <button type="button" class="btn btn-secondary">Reset
                                                                        Filter</button>
                                                                    <button type="submit" class="btn btn-primary">Apply
                                                                        Filter(s)</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>

                                                <div class="table-responsive">
                                                    <table class="table table-sm" id="dtBasicExample">
                                                        <thead>
                                                        <tr>
                                                            <th scope="col">
                                                                <a href="#" class="text-muted list-sort">Date</a>
                                                            </th>
                                                            <th scope="col">
                                                                <a href="#" class="text-muted list-sort">Office</a>
                                                            </th>
                                                            <th scope="col">
                                                                <a href="#" class="text-muted list-sort">Column Name</a>
                                                            </th>
                                                            <th scope="col">
                                                                <a href="#" class="text-muted list-sort">Previous Value</a>
                                                            </th>
                                                            <th scope="col">
                                                                <a href="#" class="text-muted list-sort">New Value</a>
                                                            </th>
                                                            <th scope="col">
                                                                <a href="#" class="text-muted list-sort">User</a>
                                                            </th>
                                                        </tr>
                                                        </thead>
                                                        <tbody class="list">
                                                        <tr>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('js')
    <script>
        $(function () {
            viewAgentProfile();
        })
        function viewAgentProfile(id=null){
            let agent_id = '{{$agent_id}}';
            if(agent_id == '') {
                agent_id = id;
            }
            console.log(agent_id);
            var request = $.ajax({
                url: "/user/agent-profile",
                method: "GET",
                data: {
                    agent_id: agent_id,
                },
                dataType: "html",
                success: function(res){
                    var res = JSON.parse(res);
                    var status = res.status ;
                    var data = res.data;
                    var span_first_name = $('.span_first_name').append(data.first_name);
                    var span_last_name = $('.span_last_name').append(data.last_name);
                    var input_first_name = $('.input_first_name').val(data.first_name);
                    var input_last_name = $('.input_last_name').val(data.last_name);
                    $('#assignCommissionPlan').append('');
                    res.commissionPlans.map((plans) => {
                        console.log(data.view_agent.plan_name);
                        console.log(plans.plan_name);
                        let plan = plans.plan_name +' ('+plans.agent_percentage+'%)' ;
                        let html;
                        if(data.view_agent.plan_name == plans.plan_name) {
                            let d_plan = data.view_agent.plan_name+' ('+data.view_agent.agent_percentage+'%)';
                            html = `<option value="`+plans.id+`" selected>`+ d_plan +`</option>`;
                        } else {
                            html = `<option value="`+plans.id+`">`+ plan +`</option>`;
                        }
                        $('#assignCommissionPlan').append(html);
                    });
                    if(status == true){
                        $('#view-agent').hide();
                        $('#agent-profile').show();
                    }
                }
            });
        }

        $('body').on('click', '.showViewAgent', function (){
            $('#view-agent').show();
            $('#agent-profile').hide();
        });
    </script>
@endpush
