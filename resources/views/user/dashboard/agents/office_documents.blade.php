@extends('user.layouts.master')

@push('css')
@endpush

@section('content')
    <div class="header">
        <div class="container-fluid">
            <div class="header-body">
                <div class="row align-items-end">
                    <div class="col">
                        <h1 class="header-title">
                            Agents / Office Document(s)
                        </h1>
                    </div>
                    <div class="col-auto">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-6 col-xl">
                <div class="card">
                    <div class="card-header">
                        <h4>Office Document(s)</h4>
                        <p>These folder(s) will be accessible to each agent to view and download in their agent portal.</p>
                    </div>
                    <div class="card-body">
                        <div class="table_top_content clearfix">
                            <div class="pull-right">
                                <button class="btn btn-primary" type="button" data-bs-toggle="modal" data-bs-target="#addFolders">+ Add Folder(s)</button>
                            </div>
                        </div>
                        <div class="table-responsive">
                            <table class="table table-sm">
                                <thead>
                                <tr>
                                    <th scope="col">
                                        <a href="#" class="text-muted list-sort">#</a>
                                    </th>
                                    <th scope="col">
                                        <a href="#" class="text-muted list-sort">Folder Name</a>
                                    </th>
                                    <th scope="col">
                                        <a href="#" class="text-muted list-sort">Action</a>
                                    </th>
                                </tr>
                                </thead>
                                <tbody class="list">
                                <tr>
                                    <td class="tables-first">1</td>
                                    <td class="tables-handle">Test Folder</td>
                                    <td class="tables-handle">
                                        <a><i class="far fa-edit"></i></a>
                                        <a><i class="fas fa-trash-alt"></i></a>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <!-- Modal -->
                    <div class="modal fade" id="addFolders" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                        <div class="modal-dialog modal-1-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h3 class="modal-title" id="staticBackdropLabel">Add New Folder</h3>
                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                </div>
                                <div class="modal-body">
                                    <div class="form-group">
                                        <label> <b>Folder Name:</b> </label>
                                        <input type="text" placeholder="Enter Folder Name" class="form-control input-group mt-2">
                                    </div>
                                </div>
                                <div class="modal-footer form-group-row">
                                    <button type="button" class="btn btn-light">Cancel</button>
                                    <button type="button" class="btn btn-primary">Save Folder</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('js')
@endpush
