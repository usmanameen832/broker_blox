@extends('user.layouts.master')

@push('css')
@endpush

@section('content')
    <div class="header">
        <div class="container-fluid">
            <div class="header-body">
                <div class="row align-items-end">
                    <div class="col">
                        <h1 class="header-title">
                            Setup Checklist
                        </h1>
                    </div>
                    <div class="col-auto">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 col-lg-12 col-xl">
                <div class="card">
                    <div class="card-body">
                        <div class="row align-items-center gx-0">
                            <div class="table-responsive">
                                <table class="table table-sm">
                                    <tbody class="list">
                                    <tr>
                                        <td>Complete Your Account Settings (required)</td>
                                        <td>Incomplete</td>
                                        <td> <a href="{{ route('accountSettings') }}"> Setup Now </a> </td>
                                    </tr>
                                    <tr>
                                        <td>Add a Commission Plan (required)</td>
                                        <td>Complete</td>
                                        <td><a href="{{ route('commissionPlan') }}"> Setup Now </a> </td>
                                    </tr>
                                    <tr>
                                        <td>Create an Onboarding Template</td>
                                        <td>Incomplete</td>
                                        <td><a href="{{ route('docusignTemplate') }}"> Setup Now </a> </td>
                                    </tr>
                                    <tr>
                                        <td>Get Approved For Agent Billings</td>
                                        <td>Incomplete</td>
                                        <td><a href="{{ route('creditCardSetting') }}"> Setup Now </a> </td>
                                    </tr>
                                    <tr>
                                        <td>Add Staff Users</td>
                                        <td>Incomplete</td>
                                        <td><a href="{{ route('staffUsers') }}"> Setup Now </a> </td>
                                    </tr>
                                    <tr>
                                        <td>Add Your Offices</td>
                                        <td>Incomplete</td>
                                        <td><a href="{{ route('addOffice') }}"> Setup Now </a> </td>
                                    </tr>
                                    <tr>
                                        <td>Add Agents</td>
                                        <td>Incomplete</td>
                                        <td><a href="{{ route('addAgents') }}"> Setup Now </a> </td>
                                    </tr>
                                    <tr>
                                        <td>Add Dotloop Integrations</td>
                                        <td>Incomplete</td>
                                        <td><a href="{{ route('integrations') }}"> Setup Now </a> </td>
                                    </tr>
                                    <tr>
                                        <td>Add Skyslope Integrations</td>
                                        <td>Incomplete</td>
                                        <td><a href="{{ route('integrations') }}"> Setup Now </a> </td>
                                    </tr>
                                    <tr>
                                        <td>Have Questions? Contact our Team at <a
                                                href="#">support@brokerblox.com</a>.
                                        </td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td>Contact Sales to Setup a 1-on-1 Go Live Call - email
                                            <a href="#">support@brokerblox.com</a>.</td>
                                        <td>Incomplete</td>
                                        <td></td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('js')
@endpush
