@extends('user.layouts.master')

@push('css')
@endpush

@section('content')
    <div id="adminUsers">
        <div class="header">
            <div class="container-fluid">
                <div class="header-body">
                    <div class="row align-items-end">
                        <div class="col">
                            <h1 class="header-title">
                                My Account / Admin Users
                            </h1>
                        </div>
                        <div class="col-auto">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12 col-lg-12 col-xl">
                    <div class="card">
                        <div class="card-body">
                            <div class="table-header">
                                <div class="col-md-6">
                                    <div class="input-group">
                                        <div class="form-outline">
                                            <input type="search" placeholder="search.." class="form-control" />
                                        </div>
                                        <button type="button" class="btn btn-primary">
                                            <i class="fas fa-search"></i>
                                        </button>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div style="float: right">
                                        <button class="btn btn-primary" onclick="hideAdminUsers()">+ Add User(s)</button>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="table-responsive" data-list='{"valueNames": ["tables-row", "tables-first", "tables-last", "tables-handle"]}'>
                                    <table class="table table-sm">
                                        <thead>
                                        <tr>
                                            <th scope="col">
                                                <a href="#" class="text-muted list-sort" data-sort="tables-first">Name</a>
                                            </th>
                                            <th scope="col">
                                                <a href="#" class="text-muted list-sort" data-sort="tables-last">Email(Username)</a>
                                            </th>
                                            <th scope="col">
                                                <a href="#" class="text-muted list-sort" data-sort="tables-handle">Account Status</a>
                                            </th>
                                            <th scope="col">
                                                <a href="#" class="text-muted list-sort" data-sort="tables-handle">Super Admin</a>
                                            </th>
                                            <th scope="col">
                                                <a href="#" class="text-muted list-sort">Action</a>
                                            </th>
                                        </tr>
                                        </thead>
                                        <tbody class="list">
                                        <tr>
                                            <td class="tables-first">Test</td>
                                            <td class="tables-last">test@gmail.com</td>
                                            <td class="tables-handle">Pending</td>
                                            <td class="tables-handle">Test Superadmin</td>
                                            <td>
                                                <a><i class="far fa-edit"></i></a>
                                                <a><i class="fas fa-trash-alt"></i></a>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>`
    </div>

    <div id="addAdminUsers" style="display: none">
        <div class="header">
            <div class="container-fluid">
                <div class="header-body">
                    <div class="row align-items-end">
                        <div class="col">
                            <h1 class="header-title">
                                My Account / Admin Users / Add User(s)
                            </h1>
                        </div>
                        <div class="col-auto">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12 col-lg-12 col-xl">
                    <div class="card">
                        <div class="card-body">
                            <h2>Add New Admin User</h2>
                            <p>Account invitation(s) will be sent to the emails below, which will ask these users to create a password for their account.</p>
                            <div class="col-lg-12 col-md-12 row">
                                <div class="col-lg-2 col-md-1"></div>
                                <div class="col-lg-8 col-md-10">
                                    <form>
                                        <div class="form-group form-group-row">
                                            <label class="w-25 mt-2">Add the Email(s) of the User(s) you want to invite:</label>
                                            <div class="input_container select_container w-75">
                                                <textarea class="form-control" rows="4" cols="50" maxlength="200" placeholder="e.g test@gmail.com"></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group form-group-row">
                                            <label class="w-25">Never sync these users to kvcore:</label>
                                            <div class="input_container select_container w-75">
                                                <label class="switch">
                                                    <input type="checkbox" class="form-check-input">
                                                </label>
                                            </div>
                                        </div>
                                        <div class="form-group mt-3 form-group-row">
                                            <label class="w-25 mt-2">Assign Office Location:</label>
                                            <div class="input_container select_container w-75">
                                                <select class="form-select" disabled name="" id="" data-choices>
                                                    <option>No Result Match</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="card-footer pull-right">
                                            <button class="btn btn-light" onclick="hideAdminUsers()">Cancel</button>
                                            <button class="btn btn-primary">Save</button>
                                        </div>
                                    </form>
                                </div>
                                <div class="col-lg-2 col-md-1"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('js')
    <script>
        function hideAdminUsers(){
            $('#adminUsers').hide();
            $('#addAdminUsers').show();
        }
        function showAdminUsers(){
            $('#adminUsers').show();
            $('#addAdminUsers').hide();
        }
    </script>
@endpush
