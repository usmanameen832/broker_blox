@extends('user.layouts.master')

@push('css')
    <style>
        .switch {
            position: relative;
            display: inline-block;
            width: 60px;
            height: 34px;
        }
        .switch input {
            opacity: 0;
            width: 0;
            height: 0;
        }
        .slider {
            position: absolute;
            cursor: pointer;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            background-color: #ccc;
            -webkit-transition: .4s;
            transition: .4s;
        }
        .slider:before {
            position: absolute;
            content: "";
            height: 26px;
            width: 26px;
            left: 4px;
            bottom: 4px;
            background-color: white;
            -webkit-transition: .4s;
            transition: .4s;
        }
        .switch input:checked + .slider {
            background-color: #2196F3;
        }

        .switch input:focus + .slider {
            box-shadow: 0 0 1px #2196F3;
        }

        .switch input:checked + .slider:before {
            -webkit-transform: translateX(26px);
            -ms-transform: translateX(26px);
            transform: translateX(26px);
        }
        /* Rounded sliders */
        .slider.round {
            border-radius: 34px;
        }

        .slider.round:before {
            border-radius: 50%;
        }
    </style>
@endpush

@section('content')
    <div class="header">
        <div class="container-fluid">
            <div class="header-body">
                <div class="row align-items-end">
                    <div class="col">
                        <h1 class="header-title">
                            Complete Your Account Info
                        </h1>
                    </div>
                    <div class="col-auto">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 col-lg-12 col-xl">
                <div class="card">
                    <div class="card-header">
                        <h4>Broker Info</h4>
                    </div>
                    <div class="card-body">
                        <div class="col-lg-12 col-md-12 row">
                            <div class="col-lg-1 col-md-1"></div>
                            <div class="col-lg-10 col-md-10">
                                <form>
                                    <div class="form-group mt-3 form-group-row">
                                        <label class="w-25 mt-2">Real Estate License #:</label>
                                        <div class="input_container select_container w-75">
                                            <input type="text" class="form-control input-group">
                                        </div>
                                    </div>
                                    <div class="form-group form-group-row">
                                        <label class="w-25 mt-2">Broker First Name:</label>
                                        <div class="input_container select_container w-75">
                                            <input type="text" class="input-group form-control">
                                        </div>
                                    </div>
                                    <div class="form-group form-group-row">
                                        <label class="w-25 mt-2">Broker Last Name:</label>
                                        <div class="input_container select_container w-75">
                                            <input type="text" class="input-group form-control">
                                        </div>
                                    </div>
                                    <div class="form-group form-group-row">
                                        <label class="w-25 mt-2">Signature:</label>
                                        <div class="input_container select_container w-75">
                                            <input type="text" placeholder="optional" class="input-group form-control">
                                        </div>
                                    </div>
                                    <div class="form-group form-group-row">
                                        <label class="w-25 mt-2">
                                            Upload Signature:
                                            <span>(Optional)</span>
                                        </label>
                                        <div class="input_container w-75">
                                            <button class="btn btn-secondary">+  Add Signature</button>
                                            <br>
                                            <a href="#">Customize by State/Office</a>
                                        </div>
                                    </div>
                                    <div class="form-group form-group-row">
                                        <label class="w-25">Username:</label>
                                        <div class="input_container w-75">
                                            <a href="#">test@gmail.com </a> <i class="far fa-edit"></i>
                                        </div>
                                    </div>
                                    <div class="form-group form-group-row">
                                        <label class="w-25">Password:</label>
                                        <div class="input_container w-75 form-group-row">
                                            <p>******</p> <a href="#" style="margin-left: 10px">Change Password</a>
                                        </div>
                                    </div>
                                    <div class="form-group form-group-row">
                                        <label class="w-25">Hide Setup Checklist:</label>
                                        <div class="input_container select_container w-75">
                                            <label class="switch">
                                                <input type="checkbox">
                                                <span class="slider round"></span>
                                            </label>
                                        </div>
                                    </div>

                                    <div class="card">
                                        <div class="card-header">
                                            <p style="font-size: 18px;">Two-Factor Authentication</p>
                                        </div>
                                        <div class="card-body">
                                            <label>
                                                <input type="checkbox" class="form-check-input">
                                                Enable Two-Factor Verification
                                            </label>
                                        </div>
                                    </div>

                                    <br>
                                    <h2>Company Info</h2>
                                    <div class="form-group mt-5 form-group-row">
                                        <label class="w-25 mt-2">Company Name:</label>
                                        <div class="input_container select_container w-75">
                                            <input type="text" class="form-control input-group">
                                        </div>
                                    </div>
                                    <div class="form-group form-group-row">
                                        <label class="w-25 mt-2">Company Address:</label>
                                        <div class="input_container select_container w-75">
                                            <input type="text" class="input-group form-control">
                                        </div>
                                    </div>
                                    <div class="form-group form-group-row">
                                        <label class="w-25 mt-2">City:</label>
                                        <div class="input_container select_container w-75">
                                            <input type="text" class="input-group form-control">
                                        </div>
                                    </div>
                                    <div class="form-group form-group-row">
                                        <label class="w-25 mt-2">Zip/Postal Code:</label>
                                        <div class="input_container select_container w-75">
                                            <input type="text" class="input-group form-control">
                                        </div>
                                    </div>
                                    <div class="form-group form-group-row">
                                        <label class="w-25 mt-2">Country:</label>
                                        <div class="input_container select_container w-75">
                                            <input type="text" class="input-group form-control">
                                        </div>
                                    </div>
                                    <div class="form-group form-group-row">
                                        <label class="w-25 mt-2">State/Province:</label>
                                        <div class="input_container select_container w-75">
                                            <input type="text" class="input-group form-control">
                                        </div>
                                    </div>
                                    <div class="form-group form-group-row">
                                        <label class="w-25 mt-2">Phone Number:</label>
                                        <div class="input_container select_container w-75">
                                            <input type="number" class="input-group form-control">
                                        </div>
                                    </div>
                                    <div class="form-group form-group-row">
                                        <label class="w-25 mt-2">Fax:
                                            <span>(Optional)</span>
                                        </label>
                                        <div class="input_container select_container w-75">
                                            <input type="text" placeholder="(_ _ _) -_ _ _-_ _ _" class="input-group form-control">
                                        </div>
                                    </div>
                                    <div class="form-group form-group-row">
                                        <label class="w-25 mt-2">Website:</label>
                                        <div class="input_container select_container w-75">
                                            <input type="text" placeholder="optional" class="input-group form-control">
                                        </div>
                                    </div>
                                    <div class="form-group form-group-row">
                                        <label class="w-25 mt-2">Email:</label>
                                        <div class="input_container select_container w-75">
                                            <input type="email" class="input-group form-control">
                                        </div>
                                    </div>
                                    <div class="form-group form-group-row">
                                        <label class="w-25 mt-2">Tax ID:</label>
                                        <div class="input_container select_container w-75">
                                            <input type="text" placeholder="optional" class="input-group form-control">
                                        </div>
                                    </div>
                                    <div class="form-group form-group-row">
                                        <label class="w-25 mt-2">‘Previously with’ label:</label>
                                        <div class="input_container select_container w-75">
                                            <input type="text" placeholder="Previous Company" class="input-group form-control">
                                        </div>
                                    </div>
                                    <div class="form-group form-group-row">
                                        <label class="w-25 mt-2">Disbursement Instructions:</label>
                                        <div class="input_container select_container w-75">
                                            <textarea class="form-control" rows="4" cols="50" maxlength="200" placeholder="optional"></textarea>
                                        </div>
                                    </div>
                                    <div class="form-group form-group-row">
                                        <label class="w-25 mt-2">Disbursement Notes:</label>
                                        <div class="input_container select_container w-75">
                                            <textarea class="form-control" rows="4" cols="50" maxlength="200" placeholder="optional"></textarea>
                                        </div>
                                    </div>
                                    <div class="form-group form-group-row">
                                        <label class="w-25 mt-2">
                                            Logo:
                                            <span>(Optional)</span>
                                        </label>
                                        <div class="input_container w-75">
                                            <button class="btn btn-secondary">+  Add Logo</button>
                                            <br>
                                            <p>(max file size: 1 MB, accepted files: jpg,png)</p>
                                        </div>
                                    </div>
                                    <div class="form-group form-group-row">
                                        <label class="w-25">Don't allow broker to get approved disbursement email:</label>
                                        <div class="input_container select_container w-75">
                                            <label class="switch">
                                                <input type="checkbox">
                                                <span class="slider round"></span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="card-footer pull-right">
                                        <button class="btn btn-light">Cancel</button>
                                        <button class="btn btn-primary">Submit</button>
                                    </div>
                                </form>
                            </div>
                            <div class="col-lg-1 col-md-1"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('js')
@endpush
