@extends('user.layouts.master')

@push('css')
@endpush

@section('content')
    <div class="header">
        <div class="container-fluid">
            <div class="header-body">
                <div class="row align-items-end">
                    <div class="col">
                        <h1 class="header-title">
                            Money / 1099s Exports
                        </h1>
                    </div>
                    <div class="col-auto">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-6 col-xl">
                <div class="card">
                    <div class="card-header">
                        <h4>Agent 1099s</h4>
                        <p style="padding: 0 10px">We've partnered with Tax1099, powered by Zenwork, to provide a simple way to generate your 1099s. Your data can be imported directly into their 1099 portal. Tax1099.com will create your forms, which you'll be able to view and edit before submitting to the IRS and state, if applicable. You'll also be able to select from USPS and email vendor form delivery options. Tax1099 Essential accounts are free to create, and pay-as-you-go to use. For more information or assistance with Tax1099.com, please contact Tax1099 representative Tracey Herrig by email: support@brokerblox.com.</p>
                        <p style="padding: 0 10px">Reports are based on the Pay Date of the transaction, not the close date.</p>
                    </div>
                    <div class="card-body">
                        <div class="col-md-12 col-lg-12 agent-will">
                            <div class="left-cont">
                                <input type="radio" name="addAgent" value="1" class="">
                            </div>
                            <div class="right-cont">
                                <label for="addAgent" class="vertical_top">
                                    Agent's Net Commission
                                </label>
                            </div>
                        </div>
                        <div class="col-md-12 col-lg-12 agent-will">
                            <div class="left-cont">
                                <input type="radio" name="addAgent" value="1" class="">
                            </div>
                            <div class="right-cont">
                                <label for="addAgent" class="vertical_top">
                                    Agent's Gross Commission
                                </label>
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <div class="col-lg-4 col-md-4">
                                <div class="form-group form-group-row">
                                    <label class="w-25 mt-2 fw-bold">Year:</label>
                                    <div class="input_container w-75">
                                        <input type="text" class="input-group form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4">
                                <div class="form-group-row">
                                    <button class="btn btn-primary" style="margin-right: 5px;">Generate Report</button>
                                    <button class="btn btn-primary">Check Status</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-6 col-xl">
                <div class="card">
                    <div class="card-header">
                        <h4>Vendor 1099s</h4>
                    </div>
                    <div class="card-body">
                        <div class="col-md-12 col-lg-12 agent-will">
                            <div class="left-cont">
                                <input type="radio" name="addAgent" value="1" class="">
                            </div>
                            <div class="right-cont">
                                <label for="addAgent" class="vertical_top">
                                    Agent's Net Commission
                                </label>
                            </div>
                        </div>
                        <div class="col-md-12 col-lg-12 agent-will">
                            <div class="left-cont">
                                <input type="radio" name="addAgent" value="1" class="">
                            </div>
                            <div class="right-cont">
                                <label for="addAgent" class="vertical_top">
                                    Agent's Gross Commission
                                </label>
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <div class="col-lg-4 col-md-4">
                                <div class="form-group form-group-row">
                                    <label class="w-25 mt-2 fw-bold">Year:</label>
                                    <div class="input_container w-75">
                                        <input type="text" class="input-group form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4">
                                <div class="form-group-row">
                                    <button class="btn btn-primary" style="margin-right: 5px;">Generate Report</button>
                                    <button class="btn btn-primary">Check Status</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('js')
@endpush
