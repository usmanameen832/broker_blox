
<!-- Favicon -->
<link rel="shortcut icon" href="{{asset('user/assets/images/favicon.png')}}" type="image/x-icon"/>

{{--Font-awesome Icons CDN--}}
<link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>

{{--Jquery Cdn--}}
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
{{--Sweet Alert--}}
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

<!-- Map CSS -->
<link rel="stylesheet" href="{{asset('https://api.mapbox.com/mapbox-gl-js/v0.53.0/mapbox-gl.css')}}" />

<!-- Libs CSS -->
<link rel="stylesheet" href="{{asset('user/assets/css/libs.bundle.css')}}" />

<!-- Theme CSS -->
<link rel="stylesheet" href="{{asset('user/assets/css/theme.bundle.css')}}" id="stylesheetLight" />
<link rel="stylesheet" href="{{asset('user/assets/css/theme-dark.bundle.css')}}" id="stylesheetDark" />

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-156446909-1"></script><script>window.dataLayer = window.dataLayer || [];function gtag(){dataLayer.push(arguments);}gtag("js", new Date());gtag("config", "UA-156446909-1");</script>
