<!-- JAVASCRIPT -->
<!-- Map JS -->
<script src='https://api.mapbox.com/mapbox-gl-js/v0.53.0/mapbox-gl.js'></script>

<!-- Vendor JS -->
<script src="{{asset('/user/js/vendor.bundle.js')}}"></script>

<!-- Theme JS -->
<script src="{{asset('/user/js/theme.bundle.js')}}"></script>
