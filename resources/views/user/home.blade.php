@extends('user.layouts.master')

@push('css')
@endpush

@section('content')
    <div class="header">
        <div class="container-fluid">
            <div class="header-body">
                <div class="row align-items-end">
                    <div class="col">
                        <h1 class="header-title">
                            Dashboard
                        </h1>
                    </div>
{{--                    <div class="col-auto">--}}
{{--                        <a href="#" class="btn btn-primary lift">--}}
{{--                            Create Report--}}
{{--                        </a>--}}
{{--                    </div>--}}
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 col-lg-12 col-xl">
                <div class="card">
                    <div class="card-body">
                        <div class="row align-items-center gx-0">
                            <div class="col">
                                <h6 class="text-uppercase text-muted mb-2">
                                    Transaction Closed This Month
                                </h6>
                                <span class="h2 mb-0">
                                    $ 00
                                </span>
                                <span class="badge bg-success-soft mt-n1">
                                    +3.5%
                                </span>
                            </div>
                            <div class="col-auto">
                                <span class="h2 fas fa-chart-bar text-muted mb-0"></span>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer" style="padding: 10px 1.5rem 3px 1.5rem;">
                        <a href="{{ route('viewTransactions') }}">
                            <p class="pull-left">View Details</p>
                            <i class="fas fa-arrow-circle-right pull-right"></i>
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-12 col-lg-6 col-xl">
                <div class="card">
                    <div class="card-body">
                        <div class="row align-items-center gx-0">
                            <div class="col">
                                <h6 class="text-uppercase text-muted mb-2">
                                    Transaction Closing This Month
                                </h6>
                                <span class="h2 mb-0">
                                   $ 00
                                </span>
                            </div>
                            <div class="col-auto">
                                <span class="h2 fas fa-chart-bar text-muted mb-0"></span>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer" style="padding: 10px 1.5rem 3px 1.5rem;">
                        <a href="{{ route('viewTransactions') }}">
                            <p class="pull-left">View Details</p>
                            <i class="fas fa-arrow-circle-right pull-right"></i>
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-12 col-lg-6 col-xl">
                <div class="card">
                    <div class="card-body" style="padding: 1.3rem 1.5rem 1rem 1.5rem !important;">
                        <div class="row align-items-center gx-0">
                            <div class="col">
                                <h6 class="text-uppercase text-muted mb-2">
                                    Outstanding Onboarding Packages
                                </h6>
                                <span class="h2 mb-0">
                                  $ 00
                                </span>
                            </div>
                            <div class="col-auto">
                                <span class="h2 fas fa-user-plus text-muted mb-0"></span>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer" style="padding: 10px 1.5rem 3px 1.5rem;">
                        <a href="{{ route('viewAgents') }}">
                            <p class="pull-left">View Details</p>
                            <i class="fas fa-arrow-circle-right pull-right"></i>
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-12 col-lg-6 col-xl">
                <div class="card">
                    <div class="card-body">
                        <div class="row align-items-center gx-0">
                            <div class="col">
                                <h6 class="text-uppercase text-muted mb-2">
                                   Sales Volume This Month
                                </h6>
                                <span class="h2 mb-0">
                                  $ 00
                                </span>
                            </div>
                            <div class="col-auto">
                                <span class="h2 far fa-dollar-sign text-muted mb-0"></span>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer" style="padding: 10px 1.5rem 3px 1.5rem;">
                        <a href="{{ route('viewTransactions') }}">
                            <p class="pull-left">View Details</p>
                            <i class="fas fa-arrow-circle-right pull-right"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12 col-xl-8">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-header-title">
                            Conversions
                        </h4>
                        <span class="text-muted me-3">
                  Last year comparision:
                </span>
                        <div class="form-check form-switch">
                            <input class="form-check-input" type="checkbox" id="cardToggle" data-toggle="chart" data-target="#conversionsChart" data-trigger="change" data-action="add" data-dataset="1" />
                            <label class="form-check-label" for="cardToggle"></label>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="chart">
                            <canvas id="conversionsChart" class="chart-canvas"></canvas>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-xl-4">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-header-title">
                            Traffic Channels
                        </h4>
                        <ul class="nav nav-tabs nav-tabs-sm card-header-tabs">
                            <li class="nav-item" data-toggle="chart" data-target="#trafficChart" data-trigger="click" data-action="toggle" data-dataset="0">
                                <a href="#" class="nav-link active" data-bs-toggle="tab">
                                    All
                                </a>
                            </li>
                            <li class="nav-item" data-toggle="chart" data-target="#trafficChart" data-trigger="click" data-action="toggle" data-dataset="1">
                                <a href="#" class="nav-link" data-bs-toggle="tab">
                                    Direct
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="card-body">
                        <div class="chart chart-appended">
                            <canvas id="trafficChart" class="chart-canvas" data-toggle="legend" data-target="#trafficChartLegend"></canvas>
                        </div>
                        <div id="trafficChartLegend" class="chart-legend"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12 col-xl-4">
                <div class="card card-fill">
                    <div class="card-header">
                        <h4 class="card-header-title">
                            Projects
                        </h4>
                        <a href="#" class="small">View all</a>
                    </div>
                    <div class="card-body">
                        <div class="list-group list-group-flush my-n3">
                            <div class="list-group-item">
                                <div class="row align-items-center">
                                    <div class="col-auto">
                                        <a href="#" class="avatar avatar-4by3">
                                            <img src="{{asset('user/assets/images/project-1.jpg')}}" alt="..." class="avatar-img rounded">
                                        </a>
                                    </div>
                                    <div class="col ms-n2">
                                        <h4 class="mb-1">
                                            <a href="#">Homepage Redesign</a>
                                        </h4>
                                        <p class="card-text small text-muted">
                                            <time datetime="2018-05-24">Updated 4hr ago</time>
                                        </p>
                                    </div>
                                    <div class="col-auto">
                                        <div class="dropdown">
                                            <a href="#" class="dropdown-ellipses dropdown-toggle" role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="far fa-ellipsis-v"></i>
                                            </a>
                                            <div class="dropdown-menu dropdown-menu-end">
                                                <a href="#" class="dropdown-item">
                                                    Action
                                                </a>
                                                <a href="#" class="dropdown-item">
                                                    Another action
                                                </a>
                                                <a href="#" class="dropdown-item">
                                                    Something else here
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="list-group-item">
                                <div class="row align-items-center">
                                    <div class="col-auto">
                                        <a href="#" class="avatar avatar-4by3">
                                            <img src="{{asset('user/assets/images/project-2.jpg')}}" alt="..." class="avatar-img rounded">
                                        </a>
                                    </div>
                                    <div class="col ms-n2">
                                        <h4 class="mb-1">
                                            <a href="#">Travels & Time</a>
                                        </h4>
                                        <p class="card-text small text-muted">
                                            <time datetime="2018-05-24">Updated 4hr ago</time>
                                        </p>
                                    </div>
                                    <div class="col-auto">
                                        <div class="dropdown">
                                            <a href="#" class="dropdown-ellipses dropdown-toggle" role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="far fa-ellipsis-v"></i>
                                            </a>
                                            <div class="dropdown-menu dropdown-menu-end">
                                                <a href="#" class="dropdown-item">
                                                    Action
                                                </a>
                                                <a href="#" class="dropdown-item">
                                                    Another action
                                                </a>
                                                <a href="#" class="dropdown-item">
                                                    Something else here
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="list-group-item">
                                <div class="row align-items-center">
                                    <div class="col-auto">
                                        <a href="#" class="avatar avatar-4by3">
                                            <img src="{{asset('user/assets/images/project-3.jpg')}}" alt="..." class="avatar-img rounded">
                                        </a>
                                    </div>
                                    <div class="col ms-n2">
                                        <h4 class="mb-1">
                                            <a href="#">Safari Exploration</a>
                                        </h4>
                                        <p class="card-text small text-muted">
                                            <time datetime="2018-05-24">Updated 4hr ago</time>
                                        </p>
                                    </div>
                                    <div class="col-auto">
                                        <div class="dropdown">
                                            <a href="#" class="dropdown-ellipses dropdown-toggle" role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="far fa-ellipsis-v"></i>
                                            </a>
                                            <div class="dropdown-menu dropdown-menu-end">
                                                <a href="#" class="dropdown-item">
                                                    Action
                                                </a>
                                                <a href="#" class="dropdown-item">
                                                    Another action
                                                </a>
                                                <a href="#" class="dropdown-item">
                                                    Something else here
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="list-group-item">
                                <div class="row align-items-center">
                                    <div class="col-auto">
                                        <a href="#" class="avatar avatar-4by3">
                                            <img src="{{asset('user/assets/images/project-5.jpg')}}" alt="..." class="avatar-img rounded">
                                        </a>
                                    </div>
                                    <div class="col ms-n2">
                                        <h4 class="mb-1">
                                            <a href="#">Personal Site</a>
                                        </h4>
                                        <p class="card-text small text-muted">
                                            <time datetime="2018-05-24">Updated 4hr ago</time>
                                        </p>
                                    </div>
                                    <div class="col-auto">
                                        <div class="dropdown">
                                            <a href="#" class="dropdown-ellipses dropdown-toggle" role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="far fa-ellipsis-v"></i>
                                            </a>
                                            <div class="dropdown-menu dropdown-menu-end">
                                                <a href="#" class="dropdown-item">
                                                    Action
                                                </a>
                                                <a href="#" class="dropdown-item">
                                                    Another action
                                                </a>
                                                <a href="#" class="dropdown-item">
                                                    Something else here
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-xl-8">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-header-title">
                            Sales
                        </h4>
                        <ul class="nav nav-tabs nav-tabs-sm card-header-tabs">
                            <li class="nav-item" data-toggle="chart" data-target="#salesChart" data-trigger="click" data-action="toggle" data-dataset="0">
                                <a class="nav-link active" href="#" data-bs-toggle="tab">
                                    All
                                </a>
                            </li>
                            <li class="nav-item" data-toggle="chart" data-target="#salesChart" data-trigger="click" data-action="toggle" data-dataset="1">
                                <a class="nav-link" href="#" data-bs-toggle="tab">
                                    Direct
                                </a>
                            </li>
                            <li class="nav-item" data-toggle="chart" data-target="#salesChart" data-trigger="click" data-action="toggle" data-dataset="2">
                                <a class="nav-link" href="#" data-bs-toggle="tab">
                                    Organic
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="card-body">
                        <div class="chart">
                            <canvas id="salesChart" class="chart-canvas"></canvas>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection

@push('js')
@endpush
