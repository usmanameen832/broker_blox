<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class CommissionPlans extends Model
{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'plan_name',
        'plan_status',
        'type_of_plan',
        'agent_percentage',
        'cap_amount',
        'roll_over_data',
        'cap_on_fees',
        'commission_level_based_on',
        'add_pre_commission_credit',
        'cap_on_pre_commission',
        'day',
//        Additional Closing Fee
        'closing_fee_item',
        'closing_flat_fee',
        'closing_fee_percentage',
        'fee_based_on',
        'include_fees',
//        Pre-Commission Debit/Credit
        'item_name',
        'debit_credit',
        'fee_percentage',
        'flat_fee',
        'commission_name',
        'cap',
        'vendor_agent',
        'vendor_name',
        'agent_name',
        'calculate_commission',
        'include_total',
//        Adjusted Gross Commission Level
        'adjusted_gross_from',
        'adjusted_gross_to',
        'adjusted_gross_percentage',
        'adjusted_gross_plans',
//        Commission Levels Number of Closed Transactions
        'close_transaction_start',
        'close_transaction_end',
        'close_transaction_percentage',
        'close_transaction_plan',
//        Commission Levels Fees
        'cap_on_fees2',
        'cap_amount2',
        'roll_over_date2',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
}
