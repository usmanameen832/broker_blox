<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class TransactionInfo extends Model
{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'agent_id',
        'transaction_type',
        'transaction_statue',
        'sales_price',
        'rental_amount',
        'rental_term_of_months',
        'total_rental_amount',
        'close_date',
        'lease_start_date',
        'move_in_date',
        'lead_source',
        'who_do_you_represent',
        'listing_lead_source',
        'selling_lead_source',
        'listing_commission_percentage',
        'listing_commission_flat_fee',
        'selling_commission_percentage',
        'selling_commission_flat_fee',
        'leasing_commission_percentage',
        'leasing_commission_flat_fee',
        'referral_commission_percentage',
        'referral_commission_flat_fee',
        'outside_referral_commission_percentage',
        'outside_referral_commission_flat_fee',
        'calculate_agent_commission',
        'outside_referral_company',
        'outside_referral_company_listing',
        'outside_referral_agent',
        'outside_referral_company_selling',
        'outside_referral_agent_selling',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function propertyInfoRelation(){
        return $this->hasOne(PropertyInfo::class,'transaction_id', 'id');
    }

    public function buyerInfoRelation(){
        return $this->hasOne(BuyerInfo::class, 'transaction_id', 'id');
    }

    public function sellerInfoRelation(){
        return $this->hasOne(SellerInfo::class, 'transaction_id', 'id');
    }

    public function tenantInfoRelation(){
        return $this->hasOne(TenantInfo::class, 'transaction_id', 'id');
    }

    public function landloardInfoRelation(){
        return $this->hasOne(LandloardInfo::class, 'transaction_id', 'id');
    }

    public function listingAgentRelation(){
        return $this->hasOne(ListingAgentInfo::class, 'transaction_id', 'id');
    }

    public function sellingAgentRelation(){
        return $this->hasOne(SellingAgentInfo::class, 'transaction_id', 'id');
    }

    public function dualAgentRelation(){
        return $this->hasOne(DualAgentInfo::class, 'transaction_id', 'id');
    }

    public function leasingAgentRelation(){
        return $this->hasOne(LeasingAgentInfo::class, 'transaction_id', 'id');
    }

    public function referralAgentRelation(){
        return $this->hasOne(ReferralAgentInfo::class, 'transaction_id', 'id');
    }
}
