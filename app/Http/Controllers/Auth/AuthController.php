<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use function GuzzleHttp\Promise\all;

class AuthController extends Controller
{
    //
    public function showLoginForm(){
        return view('auth.login');
    }

    public function showSignupForm(){
        return view('auth.signup');
    }

    public function saveUser(Request $request){
        if (User::where('email', $request->email)->exists()){
            return back()->with('error', 'Email Already Exists');
        } else {
            $saveUser = User::create([
                'email' => $request->email,
                'company_name' => $request->company_name,
                'number_of_agents' => $request->number_of_agents,
                'first_name' => $request->first_name,
                'last_name' => $request->last_name,
                'phone_number' => $request->phone_number,
                'password' => bcrypt($request->password),
            ]);
            return back()->with('status', 'Successfully Signed Up');
        }

    }

    public function postLogin(Request $request){
        $request->validate([
            'email' => 'required',
            'password' => 'required'
        ]);
        if (Auth::attempt(['email' => $request->email, 'password' => $request->password])){
            return redirect()->route('home');
        } else {
            return back()->with('error', 'Credentials does not match');
        }
    }

    public function logoutAdmin(){
//        Auth::guard('user')->logout();
        \Session::flush();
        return redirect('/');
    }
}
