<?php

namespace App\Http\Controllers\User\Dashboard\Transactions;

use App\Http\Controllers\Controller;
use App\Models\TransactionInfo;
use Illuminate\Http\Request;

class ViewTransactionsController extends Controller
{
    //
    public function viewTransactions(){
        $Transaction = TransactionInfo::with('propertyInfoRelation', 'buyerInfoRelation', 'sellerInfoRelation', 'tenantInfoRelation', 'landloardInfoRelation', 'listingAgentRelation', 'sellingAgentRelation', 'dualAgentRelation', 'leasingAgentRelation', 'referralAgentRelation')->get();
        return view('user.dashboard.transactions.view_transactions', [
            'transaction' => $Transaction,
        ]);
    }

    public function viewTransactionRecord($id){
        $transactionInfo = TransactionInfo::with('propertyInfoRelation', 'buyerInfoRelation', 'sellerInfoRelation', 'tenantInfoRelation', 'landloardInfoRelation', 'listingAgentRelation', 'sellingAgentRelation', 'dualAgentRelation', 'leasingAgentRelation', 'referralAgentRelation')->where('id', '=', $id)->first();
        return response()->json([
            'status' => true,
            'transactionData' => $transactionInfo,
        ]);
    }
}
