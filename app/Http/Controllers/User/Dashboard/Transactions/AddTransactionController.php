<?php

namespace App\Http\Controllers\User\Dashboard\Transactions;

use App\Http\Controllers\Controller;
use App\Models\AddTransaction;
use App\Models\BuyerInfo;
use App\Models\DualAgentInfo;
use App\Models\LandloardInfo;
use App\Models\LeasingAgentInfo;
use App\Models\ListingAgentInfo;
use App\Models\PropertyInfo;
use App\Models\ReferralAgentInfo;
use App\Models\SellerInfo;
use App\Models\SellingAgentInfo;
use App\Models\TenantInfo;
use App\Models\TransactionInfo;
use Illuminate\Http\Request;

class AddTransactionController extends Controller
{
    //
    public function addTransaction(){
        return view('user.dashboard.transactions.add_transaction');
    }

    public function saveTransactionInfo(Request $request){
        $transactionInfo = TransactionInfo::create([
            'transaction_type' => $request->transaction_type,
            'transaction_statue' => $request->transaction_statue,
            'sales_price' => $request->sales_price,
            'rental_amount' => $request->rental_amount,
            'rental_term_of_months' => $request->rental_term_of_months,
            'total_rental_amount' => $request->total_rental_amount,
            'close_date' => $request->close_date,
            'lease_start_date' => $request->lease_start_date,
            'move_in_date' => $request->move_in_date,
            'lead_source' => $request->lead_source,
            'who_do_you_represent' => $request->who_do_you_represent,
            'listing_lead_source' => $request->listing_lead_source,
            'selling_lead_source' => $request->selling_lead_source,
            'listing_commission_percentage' => $request->listing_commission_percentage,
            'listing_commission_flat_fee' => $request->listing_commission_flat_fee,
            'selling_commission_percentage' => $request->selling_commission_percentage,
            'selling_commission_flat_fee' => $request->selling_commission_flat_fee,
            'leasing_commission_percentage' => $request->leasing_commission_percentage,
            'leasing_commission_flat_fee' => $request->leasing_commission_flat_fee,
            'referral_commission_percentage' => $request->referral_commission_percentage,
            'referral_commission_flat_fee' => $request->referral_commission_flat_fee,
            'outside_referral_company' => $request->outside_referral_company,
            'outside_referral_company_listing' => $request->outside_referral_company_listing,
            'outside_referral_agent' => $request->outside_referral_agent,
            'outside_referral_company_selling' => $request->outside_referral_company_selling,
            'outside_referral_agent_selling' => $request->outside_referral_agent_selling,
        ]);
        $propertyInfo = PropertyInfo::create([
            'transaction_id' => $transactionInfo->id,
            'house_number' => $request->house_number,
            'street_name' => $request->street_name,
            'unit_number' => $request->unit_number,
            'property_address' => $request->property_address,
            'country' => $request->country,
            'property_postal' => $request->property_postal,
            'city' => $request->city,
            'state' => $request->state,
            'landloard_name' => $request->landloard_name,
            'mls' => $request->mls,
        ]);
        $buyerInfo = BuyerInfo::create([
            'transaction_id' => $transactionInfo->id,
            'buyer_item_name' => $request->buyer_item_name,
            'buyer_email' => $request->buyer_email,
            'buyer_phone' => $request->buyer_phone,
            'buyer_postal' => $request->buyer_postal,
            'buyer_address' => $request->buyer_address,
            'buyer_city' => $request->buyer_city,
            'buyer_country' => $request->buyer_country,
            'buyer_state' => $request->buyer_state,
            'buyer_attorney' => $request->buyer_attorney,
        ]);
        $sellerInfo = SellerInfo::create([
            'transaction_id' => $transactionInfo->id,
            'seller_name' => $request->seller_name,
            'seller_email' => $request->seller_email,
            'seller_phone' => $request->seller_phone,
            'seller_postal' => $request->seller_postal,
            'seller_address' => $request->seller_address,
            'seller_city' => $request->seller_city,
            'seller_country' => $request->seller_country,
            'seller_state' => $request->seller_state,
            'seller_attorney' => $request->seller_attorney,
        ]);
        $tenantInfo = TenantInfo::create([
            'transaction_id' => $transactionInfo->id,
            'tenant_name' => $request->tenant_name,
            'tenant_email' => $request->tenant_email,
            'tenant_phone' => $request->tenant_phone,
            'tenant_postal' => $request->tenant_postal,
            'tenant_address' => $request->tenant_address,
            'tenant_city' => $request->tenant_city,
            'tenant_country' => $request->tenant_country,
            'tenant_state' => $request->tenant_state,
            'tenant_attorney' => $request->tenant_attorney,
        ]);
        $landloardInfo = LandloardInfo::create([
            'transaction_id' => $transactionInfo->id,
            'landloard_name' => $request->landloard_name,
            'landloard_email' => $request->landloard_email,
            'landloard_phone' => $request->landloard_phone,
            'landloard_postal' => $request->landloard_postal,
            'landloard_address' => $request->landloard_address,
            'landloard_city' => $request->landloard_city,
            'landloard_country' => $request->landloard_country,
            'landloard_state' => $request->landloard_state,
            'landloard_attorney' => $request->landloard_attorney,
        ]);
        $listingAgentInfo = ListingAgentInfo::create([
            'transaction_id' => $transactionInfo->id,
            'select_listing_agent' => $request->select_listing_agent,
            'commission_plan_for_listing_agent' => $request->commission_plan_for_listing_agent,
            'listing_agent_percentage' => $request->listing_agent_percentage,
            'listing_agent_flat_fee' => $request->listing_agent_flat_fee,
            'listing_exclude_transaction' => $request->listing_exclude_transaction,
            'split_listing_commission' => $request->split_listing_commission,
            'listing_and_by_a' => $request->listing_and_by_a,
        ]);
        $sellingAgentInfo = SellingAgentInfo::create([
            'transaction_id' => $transactionInfo->id,
            'select_selling_agent' => $request->select_selling_agent,
            'commission_plan_for_selling_agent' => $request->commission_plan_for_selling_agent,
            'selling_agent_percentage' => $request->selling_agent_percentage,
            'selling_agent_flat_fee' => $request->selling_agent_flat_fee,
            'selling_exclude_transaction' => $request->selling_exclude_transaction,
            'split_selling_commission' => $request->split_selling_commission,
            'selling_and_by_a' => $request->selling_and_by_a,
        ]);
        $dualAgentInfo = DualAgentInfo::create([
            'transaction_id' => $transactionInfo->id,
            'select_dual_agent' => $request->select_dual_agent,
            'commission_plan_for_dual_agent' => $request->commission_plan_for_dual_agent,
            'dual_agent_percentage' => $request->dual_agent_percentage,
            'dual_agent_flat_fee' => $request->dual_agent_flat_fee,
            'dual_exclude_transaction' => $request->dual_exclude_transaction,
            'split_dual_commission' => $request->split_dual_commission,
            'dual_and_by_a' => $request->dual_and_by_a,
        ]);
        $leasingAgentInfo = LeasingAgentInfo::create([
            'transaction_id' => $transactionInfo->id,
            'select_leasing_agent' => $request->select_leasing_agent,
            'commission_plan_for_leasing_agent' => $request->commission_plan_for_leasing_agent,
            'leasing_agent_percentage' => $request->leasing_agent_percentage,
            'leasing_agent_flat_fee' => $request->leasing_agent_flat_fee,
            'leasing_exclude_transaction' => $request->leasing_exclude_transaction,
            'split_leasing_commission' => $request->split_leasing_commission,
            'leasing_and_by_a' => $request->leasing_and_by_a,
        ]);
        $referralAgentInfo = ReferralAgentInfo::create([
            'transaction_id' => $transactionInfo->id,
            'select_referral_agent' => $request->select_referral_agent,
            'commission_plan_for_referral_agent' => $request->commission_plan_for_referral_agent,
            'referral_agent_percentage' => $request->referral_agent_percentage,
            'referral_agent_flat_fee' => $request->referral_agent_flat_fee,
            'referral_exclude_transaction' => $request->referral_exclude_transaction,
            'split_referral_commission' => $request->split_referral_commission,
            'referral_and_by_a' => $request->referral_and_by_a,
        ]);
        return back()->with('status', 'Transaction Added Successfully');
    }
}
