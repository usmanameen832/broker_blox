<?php

namespace App\Http\Controllers\User\Dashboard\Transactions;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class TrustAccountDepositsController extends Controller
{
    //
    public function trustAccountDeposits(){
        return view('user.dashboard.transactions.trust_account');
    }
}
