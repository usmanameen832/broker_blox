<?php

namespace App\Http\Controllers\User\Dashboard\Transactions;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CreateDisbursementController extends Controller
{
    //
    public function createDisbursement(){
        return view('user.dashboard.transactions.create_disbursement');
    }
}
