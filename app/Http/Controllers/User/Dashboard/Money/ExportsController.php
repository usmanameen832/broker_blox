<?php

namespace App\Http\Controllers\User\Dashboard\Money;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ExportsController extends Controller
{
    //
    public function exports(){
        return view('user.dashboard.money.exports');
    }
}
