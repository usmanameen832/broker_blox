<?php

namespace App\Http\Controllers\User\Dashboard\MyAccount;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AccountSettingsController extends Controller
{
    //
    public function accountSettings(){
        return view('user.dashboard.my_account.account_settings');
    }
}
