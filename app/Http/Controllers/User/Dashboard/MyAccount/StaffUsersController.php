<?php

namespace App\Http\Controllers\User\Dashboard\MyAccount;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class StaffUsersController extends Controller
{
    //
    public function staffUsers(){
        return view('user.dashboard.my_account.staff_users');
    }
}
