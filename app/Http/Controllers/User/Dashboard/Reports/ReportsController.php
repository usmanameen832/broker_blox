<?php

namespace App\Http\Controllers\User\Dashboard\Reports;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use phpDocumentor\Reflection\Utils;

class ReportsController extends Controller
{
    //
    public function reports(){
        return view('user.dashboard.reports.reports');
    }
}
