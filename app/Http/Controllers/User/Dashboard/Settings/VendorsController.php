<?php

namespace App\Http\Controllers\User\Dashboard\Settings;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class VendorsController extends Controller
{
    //
    public function vendors(){
        return view('user.dashboard.settings.vendors');
    }
}
