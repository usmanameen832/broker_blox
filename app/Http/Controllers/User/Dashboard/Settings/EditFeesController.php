<?php

namespace App\Http\Controllers\User\Dashboard\Settings;

use App\Http\Controllers\Controller;
use App\Models\AgentBilling;
use App\Models\ClosingFee;
use App\Models\PostCreditDebit;
use App\Models\PreCreditDebit;
use Illuminate\Http\Request;

class EditFeesController extends Controller
{
    //
    public function autoFillItems(){
        $closingFeeData = ClosingFee::all();
        $preCreditDebitData = PreCreditDebit::all();
        $postCreditDebitData = PostCreditDebit::all();
        $agentBillingData = AgentBilling::all();
        return view('user.dashboard.settings.edit_fees', [
            'closingFeeData' => $closingFeeData,
            'preCreditDebitData' => $preCreditDebitData,
            'postCreditDebitData' => $postCreditDebitData,
            'agentBillingData' => $agentBillingData,
        ]);
    }

    public function saveClosingFee(Request $request){
        $savefee = ClosingFee::create([
           'item_name' =>  $request->item_name,
           'item_type' =>  $request->item_type,
           'based_on' =>  $request->based_on,
           'closing_fee_percentage' =>  $request->closing_fee_percentage,
           'closing_flat_fee' =>  $request->closing_flat_fee,
        ]);
        return back()->with('closingFeeStatus', 'New closing fee add successfully');
    }

    public function deleteClosingFee($id){
        $deleteFee = ClosingFee::where('id', '=', $id)->delete();
        return back()->with('feeDeleteStatus', 'Closing Fee Deleted Successfully');
    }

    public function editClosingFee($id){
        $editFee = ClosingFee::where('id', '=', $id)->first();
    }

    public function updateClosingFee(Request $request, $id){
        $update = ClosingFee::where('id', '=', $id)->update([
            'item_name' => $request->item_name,
            'item_type' => $request->item_type,
            'based_on' => $request->based_on,
            'closing_fee_percentage' => $request->closing_fee_percentage,
            'closing_flat_fee' => $request->closing_flat_fee,
        ]);
        return back()->with('updateStatus', 'Closing Fee Updated Successfully');
    }

    public function savePreCreditDebit(Request $request){
        $savePreCreditDebit = PreCreditDebit::create([
            'pre_credit_item' => $request->pre_credit_item,
            'pre_credit_item_type' => $request->pre_credit_item_type,
            'pre_credit_based_on' => $request->pre_credit_based_on,
            'pre_credit_percentage' => $request->pre_credit_percentage,
            'pre_credit_fee' => $request->pre_credit_fee,
            'calculate_agent_commission' => $request->calculate_agent_commission,
            'total_due_brokerage' => $request->total_due_brokerage,
        ]);
        return back()->with('preCreditDebit', 'New item added successfully');
    }

    public function deletePreCredit($id){
        $deletePreCredit = PreCreditDebit::where('id', '=', $id)->delete();
        return back()->with('preCreditStatus', 'Item Deleted Successfully');
    }

    public function editPreCredit($id){
        $editPreCredit = PreCreditDebit::where('id', '=', $id)->first();
    }

    public function updatePreCredit(Request $request, $id){
        $updatePreCredit = PreCreditDebit::where('id', '=', $id)->update([
            'pre_credit_item' => $request->pre_credit_item,
            'pre_credit_item_type' => $request->pre_credit_item_type,
            'pre_credit_based_on' => $request->pre_credit_based_on,
            'pre_credit_percentage' => $request->pre_credit_percentage,
            'pre_credit_fee' => $request->pre_credit_fee,
            'calculate_agent_commission' => $request->calculate_agent_commission,
            'total_due_brokerage' => $request->total_due_brokerage,
        ]);
        return back()->with('updateStatus', 'Record Updated Successfully');
    }

    public function savePostCreditDebit(Request $request){
        $savePostCreditDebit = PostCreditDebit::create([
            'post_credit_item' => $request->post_credit_item,
            'post_item_type' => $request->post_item_type,
            'post_credit_based_on' => $request->post_credit_based_on,
            'post_credit_percentage' => $request->post_credit_percentage,
            'post_credit_fee' => $request->post_credit_fee,
        ]);
        return back()->with('postCreditDebit', 'New item added successfully');
    }

    public function deletePostCredit($id){
        $deletePostCredit = PostCreditDebit::where('id', '=', $id)->delete();
        return back()->with('postCreditStatus', 'Item Deleted Successfully');
    }

    public function editPostCredit($id){
        $editPostCredit = PostCreditDebit::where('id', '=', $id)->first();
    }

    public function updatePostCredit(Request $request, $id){
        $updatePostCredit = PostCreditDebit::where('id', '=', $id)->update([
            'post_credit_item' => $request->post_credit_item,
            'post_item_type' => $request->post_item_type,
            'post_credit_based_on' => $request->post_credit_based_on,
            'post_credit_percentage' => $request->post_credit_percentage,
            'post_credit_fee' => $request->post_credit_fee,
        ]);
        return back()->with('updateStatus', 'Record Updated Successfully');
    }

    public function saveAgentBilling(Request $request){
        $saveAgentBilling = AgentBilling::create([
            'billing_description' => $request->billing_description,
            'billing_quantity' => $request->billing_quantity,
            'billing_price' => $request->billing_price,
            'total_amount' => $request->total_amount,
        ]);
        return back()->with('agentBillingStatus', 'New item added successfully');
    }

    public function deleteAgentBilling($id){
        $deleteAgentBilling = AgentBilling::where('id', '=', $id)->delete();
        return back()->with('agentBillingStatus', 'Item Deleted Successfully');
    }

    public function editAgentBilling($id){
        $editAgentBilling = AgentBilling::where('id', '=', $id)->first();
    }

    public function updateAgentBilling(Request $request, $id){
        $updateAgentBilling = AgentBilling::where('id', '=', $id)->update([
            'billing_description' => $request->billing_description,
            'billing_quantity' => $request->billing_quantity,
            'billing_price' => $request->billing_price,
            'total_amount' => $request->total_amount,
        ]);
        return back()->with('updateStatus', 'Billing Item Updated Successfully');
    }
}
