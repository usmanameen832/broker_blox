<?php

namespace App\Http\Controllers\User\Dashboard\Settings;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ImportAgentDataController extends Controller
{
    //
    public function importAgentBillingData(){
        return view('user.dashboard.settings.import_agent_data');
    }
}
