<?php

namespace App\Http\Controllers\User\Dashboard\Settings;

use App\Http\Controllers\Controller;
use App\Models\ClosingVendor;
use Illuminate\Http\Request;

class ClosingVendorsController extends Controller
{
    //
    public function titleCompany(){
        $company = ClosingVendor::all();
        return view('user.dashboard.settings.closing_vendors', [
            'company' => $company,
        ]);
    }

    public function saveTitleCompany(Request $request){
        $saveAttorney = ClosingVendor::create([
            'company' => $request->company,
            'agent_attorney' => $request->agent_attorney,
            'email' => $request->email,
            'phone' => $request->phone,
            'fax' => $request->fax,
            'internal_company' => $request->internal_company,
            'address_1' => $request->address_1,
            'address_2' => $request->address_2,
            'zip_code' => $request->zip_code,
            'city' => $request->city,
            'state' => $request->state,
            'country' => $request->country,
        ]);
        return back()->with('status', 'Company Saved Successfully');
    }

    public function deleteTitleCompany($id){
        $deteleAttorney = ClosingVendor::where('id', '=', $id)->delete();
        return back()->with('delete', 'Company Deleted Successfully');
    }

    public function editTitleCompany($id){
        $editCompany = ClosingVendor::where('id', '=', $id)->first();
        return response()->json([
            'status' => true,
            'companyData' => $editCompany,
        ]);
    }

    public function updateTitleCompany($id, Request $request){
        dd($request->all());
        $updateCompany = ClosingVendor::where('id', '=', $id)->update([

        ]);
    }
}
