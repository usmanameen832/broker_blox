<?php

namespace App\Http\Controllers\User\Dashboard\Settings;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class OrganizationController extends Controller
{
    //
    public function organization(){
        return view('user.dashboard.settings.organization');
    }
}
