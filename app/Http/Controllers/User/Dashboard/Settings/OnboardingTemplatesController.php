<?php

namespace App\Http\Controllers\User\Dashboard\Settings;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class OnboardingTemplatesController extends Controller
{
    //
    public function docusignTemplate(){
        return view('user.dashboard.settings.onboarding_templates');
    }
}
