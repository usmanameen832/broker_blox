<?php

namespace App\Http\Controllers\User\Dashboard\Settings;

use App\Http\Controllers\Controller;
use App\Models\AddAgent;
use App\Models\CommissionPlans;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class CommissionPlansController extends Controller
{
    //
    public function commissionPlan(){
        $data = CommissionPlans::all();
        $agent = AddAgent::all();
        return view('user.dashboard.settings.commission_plans', [
            'data' => $data,
            'agent' => $agent,
        ]);
    }

    public function postCommissionPlan(Request $request){
        if (CommissionPlans::where('plan_name', '=' , $request->plan_name)->first()){
            return back()->with('error', 'Plan name already exist');
        } else {
            $commissionPlan = CommissionPlans::create([
                'id' => $request->id,
                'plan_name' => $request->plan_name,
                'plan_status' => $request->plan_status,
                'type_of_plan' => $request->type_of_plan,
                'agent_percentage' => $request->agent_percentage,
                'cap_amount' => $request->cap_amount,
                'roll_over_data' => $request->roll_over_data,
                'cap_on_fees' => $request->cap_on_fees,
                'commission_level_based_on' => $request->commission_level_based_on,
                'add_pre_commission_credit' => $request->add_pre_commission_credit,
                'cap_on_pre_commission' => $request->cap_on_pre_commission,
                'day' => date('d/m/y'),
//                Additional Closing Fees
                'closing_fee_item' => $request->closing_fee_item,
                'closing_flat_fee' => $request->closing_flat_fee,
                'closing_fee_percentage' => $request->closing_fee_percentage,
                'fee_based_on' => $request->fee_based_on,
                'include_fees' => $request->include_fees,
//                Pre-Commission Debit/Credit
                'item_name' => $request->item_name,
                'debit_credit' => $request->debit_credit,
                'fee_percentage' => $request->fee_percentage,
                'flat_fee' => $request->flat_fee,
                'commission_name' => $request->commission_name,
                'cap' => $request->cap,
                'vendor_agent' => $request->vendor_agent,
                'vendor_name' => $request->vendor_name,
                'agent_name' => $request->agent_name,
                'calculate_commission' => $request->calculate_commission,
                'include_total' => $request->include_total,
//                Adjusted Gross Commission Level
                'adjusted_gross_from' => $request->adjusted_gross_from,
                'adjusted_gross_to' => $request->adjusted_gross_to,
                'adjusted_gross_percentage' => $request->adjusted_gross_percentage,
                'adjusted_gross_plans' => $request->adjusted_gross_plans,
//                Commission Levels Number of Closed Transactions
                'close_transaction_start' => $request->close_transaction_start,
                'close_transaction_end' => $request->close_transaction_end,
                'close_transaction_percentage' => $request->close_transaction_percentage,
                'close_transaction_plan' => $request->close_transaction_plan,
//                Commission Levels Fees
                'cap_on_fees2' => $request->cap_on_fees2,
                'cap_amount2' => $request->cap_amount2,
                'roll_over_date2' => $request->roll_over_date2,
            ]);
        }
        return redirect()->back()->with('status', 'Commission Plan Created');
    }

    public function deleteCommissionPlan($id){
        $delete = CommissionPlans::where('id', $id)->delete();
        return redirect()->back()->with('delete_Message', 'Commission Plan is Successfully Deleted');
    }

    public function editCommissionPlan($id){
        $editPlan = CommissionPlans::where('id', '=', $id)->first();

        if(isset($editPlan)){
          return response()->json([
              'status' => true,
              'data' => $editPlan,
          ]);
        }
    }
}
