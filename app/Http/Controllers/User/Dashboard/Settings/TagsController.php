<?php

namespace App\Http\Controllers\User\Dashboard\Settings;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class TagsController extends Controller
{
    //
    public function manageTags(){
        return view('user.dashboard.settings.tags');
    }
}
