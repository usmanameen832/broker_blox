<?php

namespace App\Http\Controllers\User\Dashboard\Settings;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AddOfficeController extends Controller
{
    //
    public function addOffice(){
        return view('user.dashboard.settings.add-office');
    }
}
