<?php

namespace App\Http\Controllers\User\Dashboard\Settings;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class TransactionChecklistController extends Controller
{
    //
    public function taskChecklist(){
        return view('user.dashboard.settings.transaction_checklist');
    }
}
