<?php

namespace App\Http\Controllers\User\Dashboard\Settings;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class DefaultStateController extends Controller
{
    //
    public function defaultState(){
        return view('user.dashboard.settings.default_state');
    }
}
