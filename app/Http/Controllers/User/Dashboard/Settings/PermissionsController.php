<?php

namespace App\Http\Controllers\User\Dashboard\Settings;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class PermissionsController extends Controller
{
    //
    public function agentPermission(){
        return view('user.dashboard.settings.permissions');
    }
}
