<?php

namespace App\Http\Controllers\User\Dashboard\Settings;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class IntegrationsController extends Controller
{
    //
    public function integrations(){
        return view('user.dashboard.settings.integrations');
    }
}
