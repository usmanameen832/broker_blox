<?php

namespace App\Http\Controllers\User\Dashboard\Settings;

use App\Http\Controllers\Controller;
use App\Models\FieldValidation;
use Illuminate\Http\Request;

class FieldValidationController extends Controller
{
    //
    public function fieldValidation(){
        $fieldValidationData = FieldValidation::all();
        return view('user.dashboard.settings.field_validation', [
            'fieldValidationData' => $fieldValidationData,
        ]);
    }

    public function saveFieldValidation(Request $request){
        FieldValidation::create([
            'page_name' => $request->page_name,
            'view_agent_field' => $request->view_agent_field,
            'add_agent_field' => $request->add_agent_field,
            'office_licenses_field' => $request->office_licenses_field,
            'regular_expression_name' => $request->regular_expression_name,
            'regular_expression_type' => $request->regular_expression_type,
            'error_message' => $request->error_message,
            'required' => $request->required,
        ]);
        return back()->with('status', 'validation added successfully');
    }
}
