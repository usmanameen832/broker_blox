<?php

namespace App\Http\Controllers\User\Dashboard\Settings;

use App\Http\Controllers\Controller;
use App\Models\AgentChecklist;
use App\Models\User;
use Illuminate\Http\Request;

class AgentChecklistController extends Controller
{
    //
    public function agentChecklist(){
        $userData = User::all();
        $checklistData = AgentChecklist::all();
        return view('user.dashboard.settings.agent_checklist', [
            'data' => $userData,
            'checklistData' => $checklistData,
        ]);
    }

    public function saveItemsChecklist(Request $request){
        $itemsChecklist = AgentChecklist::create([
            'checklist_name' => $request->checklist_name,
            'office_location' => $request->office_location,
            'checklist_assigned_to' => $request->checklist_assigned_to,
            'item_name' => $request->item_name,
            'item_assigned_to' => $request->item_assigned_to,
        ]);
        return back()->with('status', 'Checklist Successfully Saved');
    }

    public function deleteItemsChecklist($id){
        $deleteChecklist = AgentChecklist::where('id', $id)->delete();
        return back()->with('delete', 'Checklist Deleted Successfully');
    }
}
