<?php

namespace App\Http\Controllers\User\Dashboard\Agents;

use App\Http\Controllers\Controller;
use App\Models\AddAgent;
use App\Models\CommissionPlans;
use Illuminate\Http\Request;

class ViewAgentsController extends Controller
{
    //
    public function viewAgents(Request $request){
        $viewAgent = AddAgent::with('view_agent')->get();
        return view('user.dashboard.agents.view_agents', [
            'viewAgent' => $viewAgent,
            'agent_id' => $request->agent_id,
        ]);
    }

    public function agentProfile(Request $request){
        $agentProfile = AddAgent::with('view_agent')->where('id', '=', $request->agent_id)->first();
        $commissionPlans = CommissionPlans::all();
        if (isset($agentProfile)){
            return response()->json([
                "status" => true,
                'data' => $agentProfile,
                'commissionPlans' => $commissionPlans
            ]);
        }
    }

    public function deleteAgent($id){
        $delete = AddAgent::where('id', $id)->delete();
        return back()->with('delete', 'Agent Deleted Successfuly');

    }
}
