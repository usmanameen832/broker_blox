<?php

namespace App\Http\Controllers\User\Dashboard\Agents;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class OnboardingQueueController extends Controller
{
    //
    public function onboardingQueue(){
        return view('user.dashboard.agents.onboarding_queue');
    }
}
