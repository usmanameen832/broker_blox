<?php

namespace App\Http\Controllers\User\Dashboard\Agents;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AgentBillingController extends Controller
{
    //
    public function agentBilling(){
        return view('user.dashboard.agents.agent_billing');
    }
}
