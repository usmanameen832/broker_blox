<?php

namespace App\Http\Controllers\User\Dashboard\Agents;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class OfficeAndLicenseController extends Controller
{
    //
    public function officeAndLicense(){
        return view('user.dashboard.agents.office_and_license');
    }
}
