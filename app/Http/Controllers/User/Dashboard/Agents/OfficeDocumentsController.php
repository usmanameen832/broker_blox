<?php

namespace App\Http\Controllers\User\Dashboard\Agents;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class OfficeDocumentsController extends Controller
{
    //
    public function officeDocuments(){
        return view('user.dashboard.agents.office_documents');
    }
}
