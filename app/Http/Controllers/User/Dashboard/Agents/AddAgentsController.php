<?php

namespace App\Http\Controllers\User\Dashboard\Agents;

use App\Http\Controllers\Controller;
use App\Models\AddAgent;
use App\Models\CommissionPlans;
use http\Env\Response;
use Illuminate\Http\Request;

class AddAgentsController extends Controller
{
    //
    public function addAgents(){
        $planName = CommissionPlans::all();
        return view('user.dashboard.agents.add_agents', [
            'planName' => $planName,
        ]);
    }

    public function saveAgent(Request $request){
        $saveAgent = AddAgent::create([
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'email' => $request->email,
            'assign_commission_plan' => $request->assign_commission_plan,
            'assign_office_location' => $request->assign_office_location,
            'start_date' => $request->start_date,
        ]);
        return response()->json([
            "status" => true,
            'id' => $saveAgent,
        ]);
    }

    public function checkCommissionPlanData(){
        $commissionPlanData = CommissionPlans::get();
        if (count($commissionPlanData) > 0){
            return response()->json([
                "status" => true,
                "redirect" => url('/')."/user/add-agents"
            ]);
        } else {
            return response()->json([
                "status" => false,
                "redirect" => url('/')."/user/commission-plan"
            ]);
        }
    }
}
