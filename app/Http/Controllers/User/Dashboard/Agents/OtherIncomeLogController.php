<?php

namespace App\Http\Controllers\User\Dashboard\Agents;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class OtherIncomeLogController extends Controller
{
    //
    public function otherIncomeLog(){
        return view('user.dashboard.agents.other_income_log');
    }
}
