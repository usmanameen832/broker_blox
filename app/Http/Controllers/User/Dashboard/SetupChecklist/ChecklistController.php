<?php

namespace App\Http\Controllers\User\Dashboard\SetupChecklist;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ChecklistController extends Controller
{
    //
    public function setupChecklist(){
        return view('user.dashboard.setupchecklist.checklist');
    }
}
