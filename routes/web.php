<?php

if (App::environment('production')) {
    URL::forceScheme('https');
}
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\Auth\AuthController;

//Superadmin Controllers
//use App\Http\Controllers\Superadmin\Auth\AuthController;
use App\Http\Controllers\User\HomeControler;
use App\Http\Controllers\User\Dashboard\SetupChecklist\ChecklistController;

//Agent Controllers
use App\Http\Controllers\User\Dashboard\Agents\ViewAgentsController;
use App\Http\Controllers\User\Dashboard\Agents\AddAgentsController;
use App\Http\Controllers\User\Dashboard\Agents\OfficeAndLicenseController;
use App\Http\Controllers\User\Dashboard\Agents\AgentBillingController;
use App\Http\Controllers\User\Dashboard\Agents\OtherIncomeLogController;
use App\Http\Controllers\User\Dashboard\Agents\OfficeDocumentsController;
use App\Http\Controllers\User\Dashboard\Agents\OnboardingQueueController;

//Transaction Controllers
use App\Http\Controllers\User\Dashboard\Transactions\ViewTransactionsController;
use App\Http\Controllers\User\Dashboard\Transactions\AddTransactionController;
use App\Http\Controllers\User\Dashboard\Transactions\CreateDisbursementController;
use App\Http\Controllers\User\Dashboard\Transactions\TrustAccountDepositsController;

//Money Controller
use App\Http\Controllers\User\Dashboard\Money\ExportsController;

//Reports Controller
use App\Http\Controllers\User\Dashboard\Reports\ReportsController;

//My Account Controllers
use App\Http\Controllers\User\Dashboard\MyAccount\AccountSettingsController;
use App\Http\Controllers\User\Dashboard\MyAccount\StaffUsersController;

//Settings Controllers
use App\Http\Controllers\User\Dashboard\Settings\AgentBillingsController;
use App\Http\Controllers\User\Dashboard\Settings\AgentChecklistController;
use App\Http\Controllers\User\Dashboard\Settings\ClosingVendorsController;
use App\Http\Controllers\User\Dashboard\Settings\CommissionPlansController;
use App\Http\Controllers\User\Dashboard\Settings\DefaultStateController;
use App\Http\Controllers\User\Dashboard\Settings\EditFeesController;
use App\Http\Controllers\User\Dashboard\Settings\FieldValidationController;
use App\Http\Controllers\User\Dashboard\Settings\ImportAgentDataController;
use App\Http\Controllers\User\Dashboard\Settings\IntegrationsController;
use App\Http\Controllers\User\Dashboard\Settings\OnboardingTemplatesController;
use App\Http\Controllers\User\Dashboard\Settings\OrganizationController;
use App\Http\Controllers\User\Dashboard\Settings\AddOfficeController;
use App\Http\Controllers\User\Dashboard\Settings\PermissionsController;
use App\Http\Controllers\User\Dashboard\Settings\TagsController;
use App\Http\Controllers\User\Dashboard\Settings\TransactionChecklistController;
use App\Http\Controllers\User\Dashboard\Settings\VendorsController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });


Route::get('/', [AuthController::class, 'showLoginForm'])->name('login');
Route::get('/sign-up', [AuthController::class, 'showSignupForm'])->name('signUp');
Route::post('/save-user', [AuthController::class, 'saveUser'])->name('saveUser');
Route::post('/post-login', [AuthController::class, 'postLogin'])->name('postLogin');


//  User Routes
Route::prefix('user')->group(function (){

    Route::group(['middleware', 'user'], function (){

        // Home Routes
        Route::get('/home', [HomeControler::class, 'home'])->name('home');
        // End Home Routes

        // Auth Routes
        Route::post('/logout', [AuthController::class, 'logoutAdmin'])->name('logout');
        // End Auth Routes

        // Setup Checklist Routes
        Route::get('/setup-checklist', [ChecklistController::class, 'setupChecklist'])->name('setupChecklist');
        // End Setup Checklist Routes

        // Agents Routes
        // View Agents Routes //
        Route::get('/view-agents', [ViewAgentsController::class, 'viewAgents'])->name('viewAgents');
        Route::get('/agent-profile', [ViewAgentsController::class, 'agentProfile'])->name('agentProfiles');
        Route::get('/deleteAgent/{id}', [ViewAgentsController::class, 'deleteAgent'])->name('deleteAgent');
        // Add Agents Routes //
        Route::get('/add-agents', [AddAgentsController::class, 'addAgents'])->name('addAgents');
        Route::get('/check-commission-plan-data', [AddAgentsController::class, 'checkCommissionPlanData'])->name('checkCommissionPlanData');
        Route::post('/save-agent', [AddAgentsController::class, 'saveAgent'])->name('saveAgent');

        Route::get('/manage-office-license', [OfficeAndLicenseController::class, 'officeAndLicense'])->name('officeAndLicense');
        Route::get('/agent-billing', [AgentBillingController::class, 'agentBilling'])->name('agentBilling');
        Route::get('/other-income-log', [OtherIncomeLogController::class, 'otherIncomeLog'])->name('otherIncomeLog');
        Route::get('/office-documents', [OfficeDocumentsController::class, 'officeDocuments'])->name('officeDocuments');
        Route::get('/onboarding-queue', [OnboardingQueueController::class, 'onboardingQueue'])->name('onboardingQueue');
        // End Agents Routes

        // Transaction Routes
        Route::get('/view-transaction', [ViewTransactionsController::class, 'viewTransactions'])->name('viewTransactions');
        Route::get('/transaction-record/{id}', [ViewTransactionsController::class, 'viewTransactionRecord'])->name('viewTransactionRecord');
        // Add Transaction Routes
        Route::get('/add-transaction', [AddTransactionController::class, 'addTransaction'])->name('addTransaction');
        Route::post('/save-transaction', [AddTransactionController::class, 'saveTransactionInfo'])->name('saveTransactionInfo');

        Route::get('/create-disbursement', [CreateDisbursementController::class, 'createDisbursement'])->name('createDisbursement');
        Route::get('/trust-account-deposits', [TrustAccountDepositsController::class, 'trustAccountDeposits'])->name('trustAccountDeposits');
        // End Transaction Routes

        // Money Routes
        Route::get('/1099s-exports', [ExportsController::class, 'exports'])->name('exports');
        // End Money Routes

        // Reports Routes
        Route::get('/reports', [ReportsController::class, 'reports'])->name('reports');
        // End Reports Routes

        // My Account Routes
        Route::get('/account-info', [AccountSettingsController::class, 'accountSettings'])->name('accountSettings');
        Route::get('/staff-users', [StaffUsersController::class, 'staffUsers'])->name('staffUsers');
        // End My Account Routes

        // Settings Routes
        Route::get('/credit-card-setting', [AgentBillingsController::class, 'creditCardSetting'])->name('creditCardSetting');
        // Agent Checklist Routes //
        Route::get('/agent-checklist', [AgentChecklistController::class, 'agentChecklist'])->name('agentChecklist');
        Route::post('/save-items-checklist', [AgentChecklistController::class, 'saveItemsChecklist'])->name('saveItemsChecklist');
        Route::get('/delete-items-checklist/{id}', [AgentChecklistController::class, 'deleteItemsChecklist'])->name('deleteItemsChecklist');

        Route::get('/title-company', [ClosingVendorsController::class, 'titleCompany'])->name('titleCompany');
        Route::post('save-title-attorney', [ClosingVendorsController::class, 'saveTitleCompany'])->name('saveTitleCompany');
        Route::get('/delete-title-attorney/{id}', [ClosingVendorsController::class, 'deleteTitleCompany'])->name('deleteTitleCompany');
        Route::get('/edit-title-company/{id}', [ClosingVendorsController::class, 'editTitleCompany'])->name('editTitleCompany');
        Route::post('/update-title-company/{id}', [ClosingVendorsController::class, 'updateTitleCompany'])->name('updateTitleCompany');

        // Commission Plan Routes //
        Route::get('/commission-plan', [CommissionPlansController::class, 'commissionPlan'])->name('commissionPlan');
        Route::post('/post-commission-plan', [CommissionPlansController::class, 'postCommissionPlan'])->name('postCommissionPlan');
        Route::get('/delete-commission-plan/{id}', [CommissionPlansController::class, 'deleteCommissionPlan'])->name('deleteCommissionPlan');
        Route::post('/post-adjusted-gross-commission-level', [CommissionPlansController::class, 'postAdjustedGrossLevel'])->name('postAdjustedGrossLevel');
        Route::get('/edit-commission-plan/{id}', [CommissionPlansController::class, 'editCommissionPlan'])->name('editCommissionPlan');

        Route::get('/default-state', [DefaultStateController::class, 'defaultState'])->name('defaultState');
        // Edit Fees, Auto Fill Items Routes //
        Route::get('/auto-fill-items', [EditFeesController::class, 'autoFillItems'])->name('autoFillItems');
        Route::post('/save-closing-fee', [EditFeesController::class, 'saveClosingFee'])->name('saveClosingFee');
        Route::get('/delete-closing-fee/{id}', [EditFeesController::class, 'deleteClosingFee'])->name('deleteClosingFee');
        Route::get('/edit-closing-fee/{id}', [EditFeesController::class, 'editClosingFee'])->name('editClosingFee');
        Route::post('/update-closing-fee/{id}', [EditFeesController::class, 'updateClosingFee'])->name('updateClosingFee');
        Route::post('/save-pre-credit-debit', [EditFeesController::class, 'savePreCreditDebit'])->name('savePreCreditDebit');
        Route::get('/delete-pre-credit/{id}', [EditFeesController::class, 'deletePreCredit'])->name('deletePreCredit');
        Route::get('/edit-pre-credit/{id}', [EditFeesController::class, 'editPreCredit'])->name('editPreCredit');
        Route::post('/update-pre-credit/{id}', [EditFeesController::class, 'updatePreCredit'])->name('updatePreCredit');
        Route::post('/save-post-credit-debit', [EditFeesController::class, 'savePostCreditDebit'])->name('savePostCreditDebit');
        Route::get('/delete-post-credit/{id}', [EditFeesController::class, 'deletePostCredit'])->name('deletePostCredit');
        Route::get('/edit-post-credit/{id}', [EditFeesController::class, 'editPostCredit'])->name('editPostCredit');
        Route::post('/update-post-credit/{id}', [EditFeesController::class, 'updatePostCredit'])->name('updatePostCredit');
        Route::post('/save-agent-billing', [EditFeesController::class, 'saveAgentBilling'])->name('saveAgentBilling');
        Route::get('/delete-agent-billing/{id}', [EditFeesController::class, 'deleteAgentBilling'])->name('deleteAgentBilling');
        Route::get('/edit-agent-billing/{id}', [EditFeesController::class, 'editAgentBilling'])->name('editAgentBilling');
        Route::post('/update-agent-billing/{id}', [EditFeesController::class, 'updateAgentBilling'])->name('updateAgentBilling');


        Route::get('/field-validation', [FieldValidationController::class, 'fieldValidation'])->name('fieldValidation');
        Route::post('/save-field-validation', [FieldValidationController::class, 'saveFieldValidation'])->name('saveFieldValidation');

        Route::get('/import-agent-billing-data', [ImportAgentDataController::class, 'importAgentBillingData'])->name('importAgentBillingData');
        Route::get('/integration', [IntegrationsController::class, 'integrations'])->name('integrations');
        Route::get('/docusign-template', [OnboardingTemplatesController::class, 'docusignTemplate'])->name('docusignTemplate');
        Route::get('/organization', [OrganizationController::class, 'organization'])->name('organization');
        Route::get('/add-office', [AddOfficeController::class, 'addOffice'])->name('addOffice');
        Route::get('/agent-permission', [PermissionsController::class, 'agentPermission'])->name('agentPermission');
        Route::get('/manage-tags', [TagsController::class, 'manageTags'])->name('manageTags');
        Route::get('/task-checklist', [TransactionChecklistController::class, 'taskChecklist'])->name('taskChecklist');
        Route::get('/vendors', [VendorsController::class, 'vendors'])->name('vendors');
        // End Settings Routes
    });
});
