<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLeasingAgentInfosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('leasing_agent_infos', function (Blueprint $table) {
            $table->id();
            $table->string('transaction_id');
            $table->string('select_leasing_agent')->nullable();
            $table->string('commission_plan_for_leasing_agent')->nullable();
            $table->integer('leasing_agent_percentage')->nullable();
            $table->integer('leasing_agent_flat_fee')->nullable();
            $table->string('leasing_exclude_transaction')->nullable();
            $table->string('split_leasing_commission')->nullable();
            $table->string('leasing_and_by_a')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('leasing_agent_infos');
    }
}
