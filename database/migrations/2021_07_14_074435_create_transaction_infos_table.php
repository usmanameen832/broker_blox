<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransactionInfosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transaction_infos', function (Blueprint $table) {
            $table->id();
            $table->integer('agent_id')->nullable();
            $table->string('transaction_type')->nullable();
            $table->string('transaction_statue')->nullable();
            $table->integer('sales_price')->nullable();
            $table->integer('rental_amount')->nullable();
            $table->string('rental_term_of_months')->nullable();
            $table->integer('total_rental_amount')->nullable();
            $table->date('close_date')->nullable();
            $table->date('lease_start_date')->nullable();
            $table->date('move_in_date')->nullable();
            $table->string('lead_source')->nullable();
            $table->string('who_do_you_represent')->nullable();
            $table->string('listing_lead_source')->nullable();
            $table->string('selling_lead_source')->nullable();
            $table->integer('listing_commission_percentage')->nullable();
            $table->integer('listing_commission_flat_fee')->nullable();
            $table->integer('selling_commission_percentage')->nullable();
            $table->integer('selling_commission_flat_fee')->nullable();
            $table->integer('leasing_commission_percentage')->nullable();
            $table->integer('leasing_commission_flat_fee')->nullable();
            $table->integer('referral_commission_percentage')->nullable();
            $table->integer('referral_commission_flat_fee')->nullable();
            $table->integer('outside_referral_commission_percentage')->nullable();
            $table->integer('outside_referral_commission_flat_fee')->nullable();
            $table->string('calculate_agent_commission')->nullable();
            $table->string('outside_referral_company')->nullable();
            $table->string('outside_referral_company_listing')->nullable();
            $table->string('outside_referral_agent')->nullable();
            $table->string('outside_referral_company_selling')->nullable();
            $table->string('outside_referral_agent_selling')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transaction_infos');
    }
}
