<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCommissionPlansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('commission_plans', function (Blueprint $table) {
            $table->id();
            $table->string('plan_name');
            $table->string('plan_status');
            $table->string('type_of_plan');
            $table->integer('agent_percentage')->nullable();
            $table->string('cap_amount')->nullable();
            $table->string('roll_over_data')->nullable();
            $table->string('cap_on_fees')->nullable();
            $table->string('commission_level_based_on')->nullable();
            $table->string('add_pre_commission_credit')->nullable();
            $table->string('cap_on_pre_commission')->nullable();
            $table->date('day');
//            Additional Closing Fee
            $table->string('closing_fee_item')->nullable();
            $table->integer('closing_flat_fee')->nullable();
            $table->integer('closing_fee_percentage')->nullable();
            $table->string('fee_based_on')->nullable();
            $table->string('include_fees')->nullable();
//            Pre-Commission Debit/Credit
            $table->string('item_name')->nullable();
            $table->string('debit_credit')->nullable();
            $table->integer('fee_percentage')->nullable();
            $table->integer('flat_fee')->nullable();
            $table->string('commission_name')->nullable();
            $table->string('cap')->nullable();
            $table->string('vendor_agent')->nullable();
            $table->string('vendor_name')->nullable();
            $table->string('agent_name')->nullable();
            $table->string('calculate_commission')->nullable();
            $table->string('include_total')->nullable();
//            Adjusted Gross Commission Level
            $table->double('adjusted_gross_from',15,2)->nullable();
            $table->double('adjusted_gross_to',15,2)->nullable();
            $table->integer('adjusted_gross_percentage')->nullable();
            $table->string('adjusted_gross_plans')->nullable();
//            Commission Levels Number of Closed Transactions
            $table->integer('close_transaction_start')->nullable();
            $table->integer('close_transaction_end')->nullable();
            $table->integer('close_transaction_percentage')->nullable();
            $table->string('close_transaction_plan')->nullable();
//            Commission Levels Fees
            $table->string('cap_on_fees2')->nullable();
            $table->integer('cap_amount2')->nullable();
            $table->string('roll_over_date2')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('commission_plans');
    }
}
