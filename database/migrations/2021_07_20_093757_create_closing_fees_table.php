<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClosingFeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('closing_fees', function (Blueprint $table) {
            $table->id();
            $table->string('item_name')->nullable();
            $table->string('item_type')->nullable();
            $table->string('based_on')->nullable();
            $table->integer('closing_fee_percentage')->nullable();
            $table->integer('closing_flat_fee')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('closing_fees');
    }
}
