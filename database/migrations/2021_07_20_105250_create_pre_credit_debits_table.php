<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePreCreditDebitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pre_credit_debits', function (Blueprint $table) {
            $table->id();
            $table->string('pre_credit_item')->nullable();
            $table->string('pre_credit_item_type')->nullable();
            $table->string('pre_credit_based_on')->nullable();
            $table->string('pre_credit_percentage')->nullable();
            $table->string('pre_credit_fee')->nullable();
            $table->string('calculate_agent_commission')->nullable();
            $table->string('total_due_brokerage')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pre_credit_debits');
    }
}
