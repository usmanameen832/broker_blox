<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSellingAgentInfosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('selling_agent_infos', function (Blueprint $table) {
            $table->id();
            $table->integer('transaction_id');
            $table->string('select_selling_agent')->nullable();
            $table->string('commission_plan_for_selling_agent')->nullable();
            $table->integer('selling_agent_percentage')->nullable();
            $table->integer('selling_agent_flat_fee')->nullable();
            $table->string('selling_exclude_transaction')->nullable();
            $table->string('split_selling_commission')->nullable();
            $table->string('selling_and_by_a')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('selling_agent_infos');
    }
}
