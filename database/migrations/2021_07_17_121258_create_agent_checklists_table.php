<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAgentChecklistsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('agent_checklists', function (Blueprint $table) {
            $table->id();
            $table->string('checklist_name');
            $table->string('office_location')->nullable();
            $table->string('checklist_assigned_to')->nullable();
            $table->string('item_name');
            $table->string('item_assigned_to')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('agent_checklists');
    }
}
