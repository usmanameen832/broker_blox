<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDualAgentInfosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dual_agent_infos', function (Blueprint $table) {
            $table->id();
            $table->integer('transaction_id');
            $table->string('select_dual_agent')->nullable();
            $table->string('commission_plan_for_dual_agent')->nullable();
            $table->integer('dual_agent_percentage')->nullable();
            $table->integer('dual_agent_flat_fee')->nullable();
            $table->string('dual_exclude_transaction')->nullable();
            $table->string('split_dual_commission')->nullable();
            $table->string('dual_and_by_a')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dual_agent_infos');
    }
}
