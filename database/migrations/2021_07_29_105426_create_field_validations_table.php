<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFieldValidationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('field_validations', function (Blueprint $table) {
            $table->id();
            $table->string('page_name')->nullable();
            $table->string('view_agent_field')->nullable();
            $table->string('add_agent_field')->nullable();
            $table->string('office_licenses_field')->nullable();
            $table->string('regular_expression_name')->nullable();
            $table->string('regular_expression_type')->nullable();
            $table->string('error_message')->nullable();
            $table->string('required')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('field_validations');
    }
}
