<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateListingAgentInfosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('listing_agent_infos', function (Blueprint $table) {
            $table->id();
            $table->integer('transaction_id');
            $table->string('select_listing_agent')->nullable();
            $table->string('commission_plan_for_listing_agent')->nullable();
            $table->integer('listing_agent_percentage')->nullable();
            $table->integer('listing_agent_flat_fee')->nullable();
            $table->string('listing_exclude_transaction')->nullable();
            $table->string('split_listing_commission')->nullable();
            $table->string('listing_and_by_a')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('listing_agent_infos');
    }
}
