<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReferralAgentInfosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('referral_agent_infos', function (Blueprint $table) {
            $table->id();
            $table->integer('transaction_id');
            $table->string('select_referral_agent')->nullable();
            $table->string('commission_plan_for_referral_agent')->nullable();
            $table->integer('referral_agent_percentage')->nullable();
            $table->integer('referral_agent_flat_fee')->nullable();
            $table->string('referral_exclude_transaction')->nullable();
            $table->string('split_referral_commission')->nullable();
            $table->string('referral_and_by_a')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('referral_agent_infos');
    }
}
