<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTenantInfosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tenant_infos', function (Blueprint $table) {
            $table->id();
            $table->integer('transaction_id');
            $table->string('tenant_name')->nullable();
            $table->string('tenant_email')->nullable();
            $table->integer('tenant_phone')->nullable();
            $table->string('tenant_postal')->nullable();
            $table->string('tenant_address')->nullable();
            $table->string('tenant_city')->nullable();
            $table->string('tenant_country')->nullable();
            $table->string('tenant_state')->nullable();
            $table->string('tenant_attorney')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tenant_infos');
    }
}
