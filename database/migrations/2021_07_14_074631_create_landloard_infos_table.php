<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLandloardInfosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('landloard_infos', function (Blueprint $table) {
            $table->id();
            $table->integer('transaction_id');
            $table->string('landloard_name')->nullable();
            $table->string('landloard_email')->nullable();
            $table->integer('landloard_phone')->nullable();
            $table->string('landloard_postal')->nullable();
            $table->string('landloard_address')->nullable();
            $table->string('landloard_city')->nullable();
            $table->string('landloard_country')->nullable();
            $table->string('landloard_state')->nullable();
            $table->string('landloard_attorney')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('landloard_infos');
    }
}
