<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePropertyInfosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('property_infos', function (Blueprint $table) {
            $table->id();
            $table->integer('transaction_id');
            $table->string('house_number')->nullable();
            $table->string('street_name')->nullable();
            $table->string('unit_number')->nullable();
            $table->string('property_address')->nullable();
            $table->string('country')->nullable();
            $table->integer('property_postal')->nullable();
            $table->string('city')->nullable();
            $table->string('state')->nullable();
            $table->string('landloard_name')->nullable();
            $table->string('mls')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('property_infos');
    }
}
