<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePostCreditDebitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('post_credit_debits', function (Blueprint $table) {
            $table->id();
            $table->string('post_credit_item')->nullable();
            $table->string('post_item_type')->nullable();
            $table->string('post_credit_based_on')->nullable();
            $table->string('post_credit_percentage')->nullable();
            $table->string('post_credit_fee')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('post_credit_debits');
    }
}
